var thinky = require(__dirname+'/../util/thinky.js'),
    r = thinky.r,
    Preference = require(__dirname+'/../model/preference.js'),
    Org = require(__dirname+'/../model/org.js'),
    Branch = require(__dirname+'/../model/branch.js'),
    User = require(__dirname+'/../model/user.js');

// list preferences
// TODO: all filter, page size and offset, columns, sort
exports.listPreferences = function (req, res) {
    var count;
    var pno=1,offset=0,limit=10;
    if(req.query.psize != undefined && req.query.psize != null && !isNaN(req.query.psize)){
        limit = parseInt(req.query.psize);
    }

    if(req.query.pno != undefined && req.query.pno != null && !isNaN(req.query.pno)){
       pno =  parseInt(req.query.pno);
    }

    offset = (pno -1) * limit;

    var sort =req.query.sort;
    var pluck = req.query.pluck;
    var token = req.body.token || req.query.token || req.headers['x-access-token'];
    var tokenObject = JSON.parse(req.decoded);

    var address =req.query.address;
    var name = req.query.name;
    var mobile =req.query.mobile;

    var filter ={
        portfolioId: tokenObject.portfolioId,
        status: 'active'
    }

    if(address !=null && address !=undefined){
        filter.address =address;
    }
    if(name !=null && name !=undefined){
        filter.name =name;
    }

    if(mobile !=null && mobile !=undefined){
        filter.mobile =mobile;
    }

    
   Preference.orderBy({index: r.desc('createdOn')}).filter(filter).getJoin({portfolio: true}).skip(offset).limit(limit).run().then(function(preferences) {
       r.table("preference").filter(filter).count().run().then(function(total) {
        res.json({
            data: preferences[0],
            total: (count!=undefined?count:0),
            pno: pno,
            psize: limit
        });
    });    
    }).error(handleError(res)); 
    handleError(res);  
};
// get by id
exports.getPreference = function (req, res) {
    var id = req.params.id;
    Preference.get(id).getJoin({branch: true,user :true}).run().then(function(preference) {
     res.json(preference);
    }).error(handleError(res));
};


// delete by id
exports.deletePreference = function (req, res) {
    var id = req.params.id;
    Preference.get(id).delete().run().then(function(branch) {
        res.json({
            status: "success"
        });
    }).error(handleError(res));
};

exports.bulkDeletePreference = function (req, res) {
    var ids = req.query.ids;
    console.log("ids"+ids);
    if(ids && ids != null){
        var idsArray = ids.split(",");
        for(var i=0;i<idsArray.length;i++){
            var vendId =idsArray[i];
            console.log("vend"+vendId);
            var token = req.body.token || req.query.token || req.headers['x-access-token'];
            var tokenObject = JSON.parse(req.decoded);
            Preference.filter({portfolioId: tokenObject.portfolioId,id: vendId,status: 'active'}).run().then(function(vendr){
                if(vendr && vendr>0){
                    Preference.get(vendr.id).update({status:'inactive'}).run().then(function(branch) {
                        
                    }).error(handleError(res));
                }
            });
        }
        res.json({
            status: "success"
        });
    }else{
        res.send(404, {error: 'Select a preference'});
    }  
};


// Add user
exports.addPreference = function (req, res) {
    var newPreference = new Preference(req.body);
    var user = JSON.parse(req.decoded);
    newPreference.createdBy =user.userId;
    newPreference.portfolioId=user.portfolioId;
    newPreference.updatedBy =user.userId;
    newPreference.updatedOn =r.now();
    console.log(newPreference);
    newPreference.save().then(function(result) {
        res.json({
            result: result
        });
    }).error(handleError(res));
};

// update user
exports.updatePreference = function (req, res) {
    var vend = new Preference(req.body);
    var user = JSON.parse(req.decoded);
    vend.updatedBy =user.userId;
    vend.updatedOn =r.now();
    Preference.get(vend.id).update(vend).then(function(result) {
        res.json({
            result: result
        });
    }).error(handleError(res));
};

function handleError(res) {
    return function(error) {
        console.log(error.message);
        return res.send(500, {error: error.message});
    }
}
