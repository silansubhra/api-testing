var thinky = require(__dirname+'/../util/thinky.js'),
    r = thinky.r,
    jwt = require('jsonwebtoken'),
    BlogSubscription = require(__dirname+'/../model/blogSubscription.js'),
    Org = require(__dirname+'/../model/org.js'),
    Portfolio = require(__dirname+'/../model/portfolio.js'),
    User = require(__dirname+'/../model/user.js'),
    emailService = require(__dirname+'/emailservice.js'),
    env = require(__dirname+'/../../env'),
    config = require(__dirname+'/../../config/'+ env.name),
    Preference = require(__dirname+'/../model/preference.js'),
    validator = require(__dirname+'/../../node_modules/validator');

// list blogSubscriptions
// TODO: all filter, page size and offset, columns, sort
exports.listBlogSubscriptions = function (req, res) {
    var count;
    var pno=1,offset=0,limit=10;
    if(req.query.psize != undefined && req.query.psize != null && !isNaN(req.query.psize)){
        limit = parseInt(req.query.psize);
    }
    if(req.query.pno != undefined && req.query.pno != null && !isNaN(req.query.pno)){
       pno =  parseInt(req.query.pno);
    }

    offset = (pno -1) * limit;
var token = req.body.token || req.query.token || req.headers['x-access-token'];
    var tokenObject = JSON.parse(req.decoded);

        BlogSubscription.filter({portfolioId: tokenObject.portfolioId}).count().execute().then(function(total) {
            count = total;
            console.log(total);
        });



        BlogSubscription.orderBy({index: r.desc('createdOn')}).filter({portfolioId: tokenObject.portfolioId}).skip(offset).limit(limit).run().then(function(blogSubscriptions) {
            console.log(JSON.stringify(blogSubscriptions));
            res.json({
                data: blogSubscriptions,
                total: count,
                pno: pno,
                psize: limit
            });
        }).error(handleError(res));
    handleError(res);
};

// get by id
exports.getBlogSubscription = function (req, res) {
    var id = req.params.id;
    BlogSubscription.get(id).getJoin({portfolio: true,user :true}).run().then(function(blogSubscription) {
     res.json(blogSubscription);
    }).error(handleError(res));
};


// delete by id
exports.deleteBlogSubscription = function (req, res) {
    var id = req.params.id;
    BlogSubscription.get(id).delete().run().then(function(portfolio) {
        res.json({
            status: "success"
        });
    }).error(handleError(res));
};

//unsubscribe
exports.unSubscribe = function (req, res) {
    var id = req.params.id;
    BlogSubscription.get(id).update({"subscriptionStatus":"unsubscribed"}).run().then(function(result) {
        res.json({
            status: "success"
        });
    }).error(handleError(res));
};
 
exports.addBlogSubscription = function (req, res) {
    var key = req.params.key;
    if(key == undefined || key == null){
         res.send(500, {error: 'API key not set'});
    }
    console.log(req.query.emailId);
    var newBlogSubscription;

    if(validator.isEmail(req.query.emailId)){
        var verificationtoken = jwt.sign({email: req.query.emailId}, config.secret,{expiresIn:"7d"})
        newBlogSubscription =  new BlogSubscription({
            emailId :req.query.emailId,
            verificationToken :verificationtoken

        });
        Portfolio.filter({apiKey:key}).run().then(function(result){
            if(result && result.length >0){
                var portfolio = result[0];
                newBlogSubscription.portfolioId = result[0].id;
                Preference.filter({portfolioId:portfolio.id}).run().then(function(eConfig){

                    var portfolioEmailId,portfolioSenderName;

                    if(eConfig && eConfig.length>0){
                        var mailConf = eConfig[0];
                        portfolioEmailId= mailConf.emailConfig.gmail.auth.user;
                        portfolioSenderName= mailConf.emailConfig.gmail.auth.name;
                        var sender = mailConf.emailConfig.gmail;
                    }else{
                        var sender = config.smtp.gmail;
                    }
                    if(!portfolioEmailId){
                      portfolioEmailId = portfolio.email;
                    }
                    if(!portfolioSenderName){
                      portfolioSenderName = portfolio.name;
                    }
                    BlogSubscription.filter({portfolioId:newBlogSubscription.portfolioId,emailId:req.query.emailId}).run().then(function(email){
                        if(!email || email.length < 1){
                            newBlogSubscription.save().then(function(result) {
                                 emailService.sendEmail(sender, {
                                    from: '"'+portfolioSenderName +'" <'+portfolioEmailId+'>', // sender address
                                    to: newBlogSubscription.emailId, // list of receivers
                                    subject: portfolio.name+' Blog Confirmation ',   // Subject line
                                    text: 'Hello User! You have been requested for '+portfolio.name+' for news letter subscribtion.\n Please <a href="https://truneto.com/blog-confirmation/'+verificationtoken+'">click here</a> to confirm. \n\nBest,\n'+portfolio.name, // plain text
                                    html: '<b>Dear User </b>!<br><br>! You have been requested with '+portfolio.name+' '+'for news letter subscribtion.<br>Please <a href="https://truneto.com/blog-confirmation/'+verificationtoken+'">click here</a> to confirm.<br><br>Best,<br>'+portfolio.name
                                });
                                res.json({
                                    result: {'status':'success','message':'Thanks for subscribing. Just one more step to go. We have sent you an email please verify your emailId.'}
                                });
                            }).error(handleError(res)); 
                        }else{
                            res.json({
                                result: {'status':'success','message':'Already subscribed'}
                            });
                        }    
                    }).error(handleError(res));   
                }).error(handleError(res)); 
            }
        }).error(handleError(res)); 
    }else{
        res.send(500, {error: 'Enter a valid email'});
    }
}

exports.blogConfirmation = function(req, res){
    var token = req.params.id;
    BlogSubscription.filter({'verificationToken': token}).run().then(function(result){
        if(result && result.length>0){
            var id = result[0].id;
            BlogSubscription.get(id).update({'subscriptionStatus':'subscribed','verificationToken':null}).then(function(subscribed){
                res.status(200).send({'success':true});
            })
        }else{
            return res.status(403).send({
                success: false,
                message: 'Invalid url or token'
            });
        }
    }).error(handleError(res)); 
}


// update user
exports.updateBlogSubscription = function (req, res) {
    BlogSubscription.get(req.body.id).update(req.body).then(function(result) {
        var user = JSON.parse(req.decoded);
        result.updatedBy =user.userId;
        result.updatedOn =r.now();
        res.json({
            result: result
        });
    }).error(handleError(res));
};

function handleError(res) {
    return function(error) {
        console.log(error.message);
        return res.send(500, {error: error.message});
    }
}
