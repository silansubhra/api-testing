var thinky = require(__dirname+'/../util/thinky.js'),
    r = thinky.r,
    Item = require(__dirname+'/../model/item.js');

// list items
// TODO: all filter, page size and offset, columns, sort
exports.listServices = function (req, res) {
    var count;
    var pno=1,offset=0,limit=10;
    if(req.query.psize != undefined && req.query.psize != null && !isNaN(req.query.psize)){
        limit = parseInt(req.query.psize);
    }

    if(req.query.pno != undefined && req.query.pno != null && !isNaN(req.query.pno)){
       pno =  parseInt(req.query.pno);
    }

    offset = (pno -1) * limit;

    var sort =req.query.sort;
    var pluck = req.query.pluck;
    var pId= req.query.parentId;
    var filter ={
            status: 'active',
            type: 'service',
            parentId: null
        }   
    if(pId && pId !=null){
     filter.parentId= pId
    }    
    
    
    if(req.decoded !=undefined && req.decoded !=null){
        var tokenObject = JSON.parse(req.decoded);
        filter.portfolioId =tokenObject.portfolioId;
        Item.filter(filter).count().execute().then(function(total) {
            count = total;
            console.log(total);
        });    
        Item.orderBy(r.desc('createdOn')).filter(filter).getJoin({slot: true}).skip(offset).limit(limit).run().then(function(items) {
            res.json({
                data: items,
                total: count,
                pno: pno,
                psize: limit
            });
        }).error(handleError(res));
        handleError(res);
    }else{
        var apiKey = req.apikey ;
        Portfolio.filter({apiKey: apiKey}).run().then(function(portfolio){
            var portId = portfolio[0].id;
            filter.portfolioId = portId

            Item.filter(filter).count().execute().then(function(total) {
                count = total;
                console.log(total);
            });    
            Item.orderBy(r.desc('createdOn')).filter(filter).getJoin({slot: true}).skip(offset).limit(limit).run().then(function(items) {
                res.json({
                    data: items,
                    total: count,
                    pno: pno,
                    psize: limit
                });
            }).error(handleError(res));
            handleError(res);
        });
    }

};

// get by id
exports.getService = function (req, res) {
    var id = req.params.id;
    Item.get(id).getJoin({slot: true}).run().then(function(item) {
     res.json({
         item: item
     });
    }).error(handleError(res));
};


// delete by id
exports.deleteService = function (req, res) {
    var id = req.params.id;
    Item.get(id).delete().run().then(function(item) {
        res.json({
            status: "success"
        });
    }).error(handleError(res));
};

// Add user
exports.addService = function (req, res) {
    var newItem = new Item(req.body);
    var user = JSON.parse(req.decoded);
    newItem.portfolioId=user.portfolioId;
    newItem.createdBy =user.userId;
    newItem.updatedBy =user.userId;
    newItem.updatedOn =r.now();
    newItem.type = "service";
    var nm =newItem.name.toLowerCase();
    var newchar = '-'
    nm = nm.split(' ').join(newchar);
    newItem.uName = nm;
    newItem.save().then(function(result) {
        res.json({
            result: result
        });
    }).error(handleError(res));
};

// update user
exports.updateService = function (req, res) {
    Item.get(req.body.id).update(req.body).then(function(result) {
        var user = JSON.parse(req.decoded);
        result.updatedBy =user.userId;
        result.updatedOn =r.now();
        res.json({
            result: result
        });
    }).error(handleError(res));
};

exports.searchServices = function (req, res) {
    var count;
    var pno=1,offset=0,limit=10;
    if(req.query.psize != undefined && req.query.psize != null && !isNaN(req.query.psize)){
        limit = parseInt(req.query.psize);
    }

    if(req.query.pno != undefined && req.query.pno != null && !isNaN(req.query.pno)){
       pno =  parseInt(req.query.pno);
    }

    offset = (pno -1) * limit;

    var input =req.params.input;
    if(input && input!=null){
    var q ="(?i)"+input;
    }else{
        var q ="";
    }
    var pluck = req.query.pluck;
    var filter ={
        status: 'active',
        type :"service"
    }
    if(req.decoded !=undefined && req.decoded !=null){
        var tokenObject = JSON.parse(req.decoded);
        filter.portfolioId =tokenObject.portfolioId;
        Item.filter(filter).filter(function(doc){
            return doc('name').match(q)}).count().execute().then(function(total) {
                count = total;
                console.log(total);
        });    
        Item.orderBy(r.desc('name')).filter(filter).filter(function(doc){
        return doc('name').match(q)}).getJoin({slot: true}).skip(offset).limit(limit).run().then(function(items) {
            res.json({
                data: items,
                total: count,
                pno: pno,
                psize: limit
            });
        }).error(handleError(res));
        handleError(res);

    }else{
        var apiKey = req.body.apikey || req.query.apikey || req.headers['x-api-key'];
        Portfolio.filter({apiKey: apiKey}).run().then(function(portfolio){
            var portId = portfolio[0].id;
            filter.portfolioId = portId
            Item.filter(filter).filter(function(doc){
                return doc('name').match(q)}).count().execute().then(function(total) {
                    count = total;
                    console.log(total);
            });    
            Item.orderBy(r.desc('name')).filter(filter).filter(function(doc){
            return doc('name').match(q)}).getJoin({slot: true}).skip(offset).limit(limit).run().then(function(items) {
                res.json({
                    data: items,
                    total: count,
                    pno: pno,
                    psize: limit
                });
            }).error(handleError(res));
            handleError(res);
        });
    }

};

function handleError(res) {
    return function(error) {
        console.log(error.message);
        return res.send(500, {error: error.message});
    }
}