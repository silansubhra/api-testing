var thinky = require(__dirname+'/../util/thinky.js'),
    r = thinky.r,
    Department = require(__dirname+'/../model/department.js'),
    Org = require(__dirname+'/../model/org.js');

// list departments
// TODO: all filter, page size and offset, columns, sort
exports.listDepartments = function (req, res) {
    var count;
    var pno=1,offset=0,limit=10;
    if(req.query.psize != undefined && req.query.psize != null && !isNaN(req.query.psize)){
        limit = parseInt(req.query.psize);
    }

    if(req.query.pno != undefined && req.query.pno != null && !isNaN(req.query.pno)){
       pno =  parseInt(req.query.pno);
    }

    offset = (pno -1) * limit;

    var sort =req.query.sort;
    var pluck = req.query.pluck;
    var token = req.body.token || req.query.token || req.headers['x-access-token'];
    var tokenObject = JSON.parse(req.decoded);
    if(sort == undefined || sort == null ){
        Department.filter({orgId: tokenObject.orgId}).count().execute().then(function(total) {
                count = total;
                console.log(total);
            });    
        Department.orderBy(r.desc('createdOn')).filter({orgId: tokenObject.orgId}).skip(offset).limit(limit).run().then(function(departments) {
            res.json({
                data: departments,
                total: count,
                pno: pno,
                psize: limit
            });
        }).error(handleError(res));
        handleError(res);
    }else{
        var result =sort.substring(0, 1);
        sortLength =sort.length;
        if(result ==='-'){
            field=sort.substring(1,sortLength);
            console.log("field--"+field);
            console.log(typeof field);
            console.log("has field--"+Department.hasFields(field));
            if(Department.hasFields(field)){
                Department.filter({portfolioId: tokenObject.portfolioId}).count().execute().then(function(total) {
                    count = total;
                    console.log(total);
                });    
                Department.orderBy(r.desc(field)).filter({portfolioId: tokenObject.portfolioId}).skip(offset).limit(limit).run().then(function(departments) {
                res.json({
                    data: departments,
                    total: count,
                    pno: pno,
                    psize: limit
                });
                }).error(handleError(res));
                handleError(res);   
            }else{
                res.status(500).send({ message: 'No field exist.'});
            }    
        }else{
            field=sort.substring(0,sortLength);
            console.log("field--"+field);
            console.log("has field--"+Department.hasFields(field));
            if(Department.hasFields(field)){
                Department.filter({portfolioId: tokenObject.portfolioId}).count().execute().then(function(total) {
                    count = total;
                    console.log(total);
                });    
                Department.orderBy(r.asc(field)).filter({portfolioId: tokenObject.portfolioId}).skip(offset).limit(limit).run().then(function(departments) {
                res.json({
                    data: departments,
                    total: count,
                    pno: pno,
                    psize: limit
                });
                }).error(handleError(res));
                handleError(res);  
            }else{
                res.status(500).send({ message: 'No field exist.'});
            }       
        }
    }    
};
// get by id
exports.getDepartment = function (req, res) {
    var id = req.params.id;
    Department.get(id).getJoin({org: true}).run().then(function(department) {
     res.json({
         department: department
     });
    }).error(handleError(res));
};


// delete by id
exports.deleteDepartment = function (req, res) {
    var id = req.params.id;
    Department.get(id).delete().run().then(function(org) {
        res.json({
            status: "success"
        });
    }).error(handleError(res));
};

//bulk delete
exports.bulkDeleteDepartment = function (req, res) {
    console.log("bulk delete called")
    var ids = req.query.ids;
    console.log("ids"+ids);
    if(ids && ids != null){
        var idsArray = ids.split(",");
        for(var i=0;i<idsArray.length;i++){
            var deptId =idsArray[i];
            console.log("dept"+deptId);
            var token = req.body.token || req.query.token || req.headers['x-access-token'];
            var tokenObject = JSON.parse(req.decoded);
            Department.filter({portfolioId: tokenObject.portfolioId,id: deptId,status: 'active'}).run().then(function(deptt){
                if(deptt && deptt>0){
                    Department.get(deptt.id).update({status:'inactive'}).run().then(function(branch) {
                        
                    }).error(handleError(res));
                }
            });
        }
        res.json({
            status: "success"
        });
    }else{
        res.send(404, {error: 'Select a department'});
    }  
};

// Add user
exports.addDepartment = function (req, res) {
    var newDepartment = new Department(req.body);
    var user = JSON.parse(req.decoded);
    newDepartment.createdBy =user.userId;
    newDepartment.portfolioId=user.portfolioId;
    newDepartment.orgId =user.orgId;
    newDepartment.updatedBy =user.userId;
    newDepartment.updatedOn =r.now();
    console.log(newDepartment);
    newDepartment.save().then(function(result) {
        res.json({
            result: result
        });
    }).error(handleError(res));
};

// update user
exports.updateDepartment = function (req, res) {
    var dep = new Department(req.body);
    var user = JSON.parse(req.decoded);
    dep.updatedBy =user.userId;
    dep.updatedOn =r.now();
    Department.get(dep.id).update(dep).then(function(result) {
        
        res.json({
            result: result
        });
    }).error(handleError(res));
};

function handleError(res) {
    return function(error) {
        console.log(error.message);
        return res.send(500, {error: error.message});
    }
}