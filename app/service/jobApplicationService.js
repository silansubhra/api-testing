var thinky = require(__dirname+'/../util/thinky.js'),
    r = thinky.r,
    JobApplication = require(__dirname+'/../model/jobApplication.js'),
    smsService  = require(__dirname+'/smsservice.js'),
    emailService  = require(__dirname+'/emailservice.js'),
    Org = require(__dirname+'/../model/org.js'),
    Customer = require(__dirname+'/../model/customer.js'),
    Portfolio = require(__dirname+'/../model/portfolio.js'),
    env         = require(__dirname+'/../../env'),
    config      = require(__dirname+'/../../config/' + env.name);


// list jobapplication
// TODO: all filter, page size and offset, columns, sort
exports.listApplication = function (req, res) {
    var count;
    var pno=1,offset=0,limit=10;
    if(req.query.psize != undefined && req.query.psize != null && !isNaN(req.query.psize)){
        limit = parseInt(req.query.psize);
    }

    if(req.query.pno != undefined && req.query.pno != null && !isNaN(req.query.pno)){
       pno =  parseInt(req.query.pno);
    }

    offset = (pno -1) * limit;

    var sort =req.query.sort;
    var pluck = req.query.pluck;
    var token = req.body.token || req.query.token || req.headers['x-access-token'];
    var tokenObject = JSON.parse(req.decoded);
    if(sort == undefined || sort == null ){
        JobApplication.filter({portfolioId: tokenObject.portfolioId}).count().execute().then(function(total) {
                count = total;
                console.log(total);
            });    
        JobApplication.orderBy(r.desc('createdOn')).filter({portfolioId: tokenObject.portfolioId}).getJoin({customer: true}).skip(offset).limit(limit).run().then(function(jobapplication) {
            res.json({
                data: jobapplication,
                total: count,
                pno: pno,
                psize: limit
            });
        }).error(handleError(res));
        handleError(res);
    }else{
        var result =sort.substring(0, 1);
        var sortLength =sort.length;
        if(result ==='-'){
            field=sort.substring(1,sortLength);
            console.log("field--"+field);
            console.log(typeof field);
            console.log("has field--"+Contact.hasFields(field));
            if(Contact.hasFields(field)){
                Contact.filter({portfolioId: tokenObject.portfolioId}).count().execute().then(function(total) {
                    count = total;
                    console.log(total);
                });    
                Contact.orderBy(r.desc(field)).filter({portfolioId: tokenObject.portfolioId}).skip(offset).limit(limit).run().then(function(jobapplication) {
                res.json({
                    data: jobapplication,
                    total: count,
                    pno: pno,
                    psize: limit
                });
                }).error(handleError(res));
                handleError(res);   
            }else{
                res.status(500).send({ message: 'No field exist.'});
            }    
        }else{
            field=sort.substring(0,sortLength);
            console.log("field--"+field);
            console.log("has field--"+Contact.hasFields(field));
            if(Contact.hasFields(field)){
                Contact.filter({portfolioId: tokenObject.portfolioId}).count().execute().then(function(total) {
                    count = total;
                    console.log(total);
                });    
                Contact.orderBy(r.asc(field)).filter({portfolioId: tokenObject.portfolioId}).skip(offset).limit(limit).run().then(function(jobapplication) {
                res.json({
                    data: jobapplication,
                    total: count,
                    pno: pno,
                    psize: limit
                });
                }).error(handleError(res));
                handleError(res);  
            }else{
                res.status(500).send({ message: 'No field exist.'});
            }       
        }
    }    
};

// get by id
/*exports.getContact = function (req, res) {
    var id = req.params.id;
    Contact.get(id).getJoin({branch: true}).run().then(function(contact) {
     res.json({
         contact: contact
     });
    }).error(handleError(res));
};*/


// delete by id
exports.deleteApplication = function (req, res) {
    var id = req.params.id;
    JobApplication.get(id).delete().run().then(function(branch) {
        res.json({
            status: "success"
        });
    }).error(handleError(res));
};

exports.addJobApplication = function (req, res) {
    var newJobApplication = new JobApplication(req.body);
    var user = JSON.parse(req.decoded);
    newJobApplication.createdBy =user.userId;
    newJobApplication.portfolioId=user.portfolioId;
    newJobApplication.orgId = user.orgId;
    newJobApplication.updatedBy =user.userId;
    newJobApplication.updatedOn =r.now();
    console.log(newJobApplication);
    newJobApplication.save().then(function(result) {
        res.json({
            result: result
        });
    }).error(handleError(res));
};

exports.addExternalApplication = function (req, res) {
    var apiKey = req.query.apikey;

    console.log(JSON.stringify(req.body));

    
    var newApplication = new JobApplication(req.body);
    if(newApplication == undefined || apiKey == null || apiKey == undefined){
        return res.send(500, {error: 'No data passed to API'});
    }else{
        if(newApplication.mobile == undefined || newApplication.mobile == null || newApplication.mobile == ""){
            return res.send(500, {error: 'Missing required field mobile'});
        }
        if(newApplication.name == undefined || newApplication.name == null || newApplication.name == ""){
            return res.send(500, {error: 'Missing required field name'});
        }
        if(newApplication.email == undefined || newApplication.email == null || newApplication.email == ""){
            return res.send(500, {error: 'Missing required field email'});
        }
        Portfolio.filter({apiKey:apiKey}).run().then(function(result){
            if(result && result.length >0){
                var portfolio = result[0];
                newApplication.portfolioId = portfolio.id;
                var mob = parseInt(newApplication.mobile);
                JobApplication.filter({portfolioId:newApplication.portfolioId,jobId:req.body.jobId,mobile:mob}).run().then(function(app) {   
                    if(app && app.length > 0){
                        //exists
                        return res.send(500, {error: 'Already applied for the job'});
                    }
                    JobApplication.filter({portfolioId:newApplication.portfolioId,jobId:req.body.jobId,email:req.body.email}).run().then(function(app1) {
                        if(app1 && app1.length > 0){
                            return res.send(500, {error: 'Already applied for the job'});
                        }
                    });    
                        var newCustomer = new Customer({
                            name: newApplication.name,
                            //message: newContact.message,
                            status: "active",
                            mobile: newApplication.mobile,
                            email: newApplication.email,
                            portfolioId: newApplication.portfolioId,
                        });
                        newCustomer.save().then(function(result) {
                         newCustomer.customerId = result.id;
                        });
                    newApplication.save().then(function(newApp) {
                        /*var portfolioSmsId = portfolio.smsProvider.senderId;
                        if(portfolioSmsId == undefined || portfolioSmsId == null || portfolioSmsId === ""){
                            portfolioSmsId = config.sms.msg91.senderId;
                        }

                        console.log('portfolio.portfolioSmsId: '+ portfolioSmsId);

                        var authKey = portfolio.smsProvider.authKey;
                        if(authKey == undefined || authKey == null || authKey === ""){
                            authKey = config.sms.msg91.authKey;
                        }
                        console.log('portfolio.authKey: '+ authKey);

                        if(portfolio.mobile != undefined && portfolio.mobile != null){
                              smsService.sendSms(req, authKey, portfolioSmsId, portfolio.mobile,'Dear '+portfolio.name +'! You have received a new inquiry.Please find below details '+'.<br><br>'+'From:'+' '+contact.name +'.<br>'+'Mobile:'+' ' + contact.mobile +'.<br>'+'Date:'+' '+contact.email+'.<br>'+'Message:'+' '+contact.message+'. \n\nBest,\nTeam OnGround');
                        }
                        if(portfolio.email != undefined && portfolio.email != null){
                            emailService.sendEmail(config.smtp.gmail, {
                                portfolioId: portfolio.id, 
                                from: '"Zinetgo.com" <support@zinetgo.com>', // sender message
                                to: portfolio.email, // list of receivers
                                subject: 'You have received a new inquiry request', // Subject line
                                text: 'Dear '+portfolio.name +'! You have received a new inquiry request.Please find below details '+'.<br><br>'+'From:'+' '+contact.name +'.<br>'+'Mobile:'+' ' + contact.mobile +'.<br>'+'Date:'+' '+contact.email+'.<br>'+'Message:'+' '+contact.message+'. \n\nBest,\nTeam OnGround', // plain text
                                html: '<b>Dear '+portfolio.name +'</b>!<br><br>You have received a new inquiry request.Please find below details '+'.<br><br>'+'From:'+' '+contact.name +'.<br>'+'Mobile:'+' ' + contact.mobile +'.<br>'+'Date:'+' '+contact.email+'.<br>'+'Message:'+' '+contact.message+'.<br><br>Best,<br>Team OnGround' // html body
                            });
                        }

                        var portfolioEmailId,portfolioSenderName;

                        if(portfolio.emailProvider){
                          portfolioEmailId= portfolio.emailProvider.auth.user;
                          portfolioSenderName= portfolio.emailProvider.auth.name;
                        }
                        if(!portfolioEmailId){
                          portfolioEmailId = portfolio.email;
                        }
                        if(!portfolioSenderName){
                          portfolioSenderName = portfolio.name;
                        }

                        console.log('portfolio.emailProvider: '+ portfolio.emailProvider);

                        if(contact.mobile != undefined && contact.mobile != null){
                            smsService.sendSms(req, authKey, portfolioSmsId, contact.mobile,'Dear user, your request for inquiry with '+ portfolio.name + ' is confirmed. Our customer care will contact you shortly. For any queries please call us at '+ portfolio.mobile +' \n\nBest,\n'+ portfolio.name);
                        }

                        if(contact.email != undefined && contact.email != null){
                            emailService.sendEmail(portfolio.emailProvider, {
                                from: '"'+portfolioSenderName +'" <'+portfolioEmailId+'>', // sender message
                                to: contact.email, // list of receivers
                                subject: 'We have received your request for inquiry request', // Subject line
                                text: 'Dear '+contact.name +'! We have received your inquiry request '+'.\n\nBest,\n'+portfolio.name, // plain text
                                html: '<b>Dear '+contact.name +'</b>!<br><br>We have received your inquiry request '+'.<br><br>Best,<br>'+portfolio.name
                            });
                        }*/
                    res.json({
                        result: {'status':'success','message':'We have received your job application'}
                    });
                    }).error(handleError(res));
                }).error(handleError(res));        
            }
        }).error(function(error){
            
        });
    }    
};
// update user
exports.updateApplication = function (req, res) {
    JobApplication.get(req.body.id).update(req.body).then(function(result) {
        var user = JSON.parse(req.decoded);
        result.updatedBy =user.userId;
        result.updatedOn =r.now();
        res.json({
            result: result
        });
    }).error(handleError(res));
};

function handleError(res) {
    return function(error) {
        console.log(error.message);
        return res.send(500, {error: error.message});
    }
}