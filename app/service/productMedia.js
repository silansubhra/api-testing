var thinky = require(__dirname+'/../util/thinky.js'),
    r = thinky.r,
    ProductMedia = require(__dirname+'/../model/productMedia.js'),
    Product = require(__dirname+'/../model/product.js'),
    Media = require(__dirname+'/../model/media.js');

// list productMedias
// TODO: all filter, page size and offset, columns, sort
exports.listProductMedias = function (req, res) {
    var count;
    var pno=1,offset=0,limit=10;
    if(req.query.psize != undefined && req.query.psize != null && !isNaN(req.query.psize)){
        limit = parseInt(req.query.psize);
    }

    if(req.query.pno != undefined && req.query.pno != null && !isNaN(req.query.pno)){
       pno =  parseInt(req.query.pno);
    }

    offset = (pno -1) * limit;

    ProductMedia.count().execute().then(function(total) {
        count = total;
        console.log(count);
    });

    console.log(req.decoded);

    ProductMedia.orderBy({index: r.desc('createdOn')}).skip(offset).limit(limit).run().then(function(productMedias) {
        res.json({
            data: productMedias,
            total: count,
            pno: pno,
            psize: limit
        });
    }).error(handleError(res));
};

// get by id
exports.getProductMedia = function (req, res) {
    var id = req.params.id;
    ProductMedia.get(id).getJoin({product: true,media :true}).run().then(function(productMedia) {
     res.json({
         productMedia: productMedia
     });
    }).error(handleError(res));
};


// delete by id
exports.deleteProductMedia = function (req, res) {
    var id = req.params.id;
    ProductMedia.get(id).delete().run().then(function(product) {
        res.json({
            status: "success"
        });
    }).error(handleError(res));
};

// Add 
exports.addProductMedia = function (req, res) {
    var newProductMedia = new ProductMedia(req.body);
    var user = JSON.parse(req.decoded);
    var portfolio = JSON.parse(req.decoded);
    newProductMedia.portfolioId=user.portfolioId;
    newProductMedia.createdBy =user.userId;
    newProductMedia.updatedBy =user.userId;
    newProductMedia.updatedOn =r.now();
    console.log(newProductMedia);
    newProductMedia.save().then(function(result) {
        res.json({
            result: result
        });
    }).error(handleError(res));
};

// update 
exports.updateProductMedia = function (req, res) {
    ProductMedia.get(req.body.id).update(req.body).then(function(result) {
        var user = JSON.parse(req.decoded);
        result.updatedBy =user.userId;
        result.updatedOn =r.now();
        res.json({
            result: result
        });
    }).error(handleError(res));
};

function handleError(res) {
    return function(error) {
        console.log(error.message);
        return res.send(500, {error: error.message});
    }
}