var thinky = require(__dirname+'/../util/thinky.js'),
    r = thinky.r,
    QuotationLineItem = require(__dirname+'/../model/quotelineitem.js'),
    Org = require(__dirname+'/../model/org.js'),
    Branch = require(__dirname+'/../model/branch.js'),
    Lead = require(__dirname+'/../model/lead.js');

// list quotationLineItems
// TODO: all filter, page size and offset, columns, sort
exports.listQuotationLineItems = function (req, res) {
    var count;
    var pno=1,offset=0,limit=10;
    if(req.query.psize != undefined && req.query.psize != null && !isNaN(req.query.psize)){
        limit = parseInt(req.query.psize);
    }

    if(req.query.pno != undefined && req.query.pno != null && !isNaN(req.query.pno)){
       pno =  parseInt(req.query.pno);
    }

    offset = (pno -1) * limit;

    var token = req.body.token || req.query.token || req.headers['x-access-token'];
    var tokenObject = JSON.parse(req.decoded);

        QuotationLineItem.filter({portfolioId: tokenObject.portfolioId}).count().execute().then(function(total) {
            count = total;
            console.log(total);
        });

        QuotationLineItem.orderBy({index: r.desc('createdOn')}).filter({portfolioId: tokenObject.portfolioId}).getJoin({quote: true,lead :true,item: true}).skip(offset).limit(limit).run().then(function(quotationLineItems) {
            res.json({
                data: quotationLineItems,
                total: count,
                pno: pno,
                psize: limit
            });
        }).error(handleError(res));
    
    handleError(res);
};

// get by id
exports.getQuotationLineItem = function (req, res) {
    var id = req.params.id;
    QuotationLineItem.get(id).getJoin({branch: true,lead :true}).run().then(function(quotationLineItem) {
     res.json({
         quotationLineItem: quotationLineItem
     });
    }).error(handleError(res));
};


// delete by id
exports.deleteQuotationLineItem = function (req, res) {
    var id = req.params.id;
    QuotationLineItem.get(id).delete().run().then(function(branch) {
        res.json({
            status: "success"
        });
    }).error(handleError(res));
};

// Add lead
exports.addQuotationLineItem = function (req, res) {
    var newQuotationLineItem = new QuotationLineItem(req.body);
    var user = JSON.parse(req.decoded);
    var portfolio = JSON.parse(req.decoded);
    newQuotationLineItem.portfolioId=user.portfolioId;
    newQuotationLineItem.createdBy =user.userId;
    newQuotationLineItem.updatedBy =user.userId;
    newQuotationLineItem.updatedOn =r.now();
    console.log(newQuotationLineItem);
    newQuotationLineItem.save().then(function(result) {
        res.json({
            result: result
        });
    }).error(handleError(res));
};

// update lead
exports.updateQuotationLineItem = function (req, res) {
    QuotationLineItem.get(req.body.id).update(req.body).then(function(result) {
        var user = JSON.parse(req.decoded);
        result.updatedBy =user.userId;
        result.updatedOn =r.now();
        res.json({
            result: result
        });
    }).error(handleError(res));
};

function handleError(res) {
    return function(error) {
        console.log(error.message);
        return res.send(500, {error: error.message});
    }
}