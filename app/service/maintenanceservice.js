var thinky = require(__dirname+'/../util/thinky.js'),
r = thinky.r,
Maintenance = require(__dirname+'/../model/maintenance.js'),
User = require(__dirname+'/../model/user.js'),
Portfolio = require(__dirname+'/../model/portfolio.js'),
env = require(__dirname+'/../../env'),
config = require(__dirname+'/../../config/'+ env.name);


exports.listMaintenances = function (req, res) {
    var count;
    var pno=1,offset=0,limit=10;
    if(req.query.psize != undefined && req.query.psize != null && !isNaN(req.query.psize)){
        limit = parseInt(req.query.psize);
    }

    if(req.query.pno != undefined && req.query.pno != null && !isNaN(req.query.pno)){
       pno =  parseInt(req.query.pno);
    }

    offset = (pno -1) * limit;

    var sort =req.query.sort;
    var token = req.body.token || req.query.token || req.headers['x-access-token'];
    var tokenObject = JSON.parse(req.decoded);
    var filter ={
        
        portfolioId: tokenObject.portfolioId,
        status: 'active'
    }
    if(sort == undefined || sort == null ){

        Maintenance.orderBy(r.desc('createdOn')).filter(filter).getJoin({portfolio: true, employee:true, item: true, asset: true}).skip(offset).limit(limit).run().then(function(mains) {
            Maintenance.filter(filter).count().execute().then(function(count) {
                res.json({
                    data: mains,
                    total: (count!=undefined?count:0),
                    pno: pno,
                    psize: limit
                });
        });            
        }).error(handleError(res));
        handleError(res);

        }else{
        var result =sort.substring(0, 1);
        sortLength =sort.length;
        if(result ==='-'){
            field=sort.substring(1,sortLength);
            console.log("field--"+field);
            console.log(typeof field);
            console.log("has field--"+Maintenance.hasFields(field));
            if(Maintenance.hasFields(field)){
                Maintenance.filter(filter).count().execute().then(function(total) {
                    count = total;
                    console.log(total);
                });    
                Maintenance.orderBy(r.desc(field)).filter(filter).skip(offset).getJoin({portfolio: true, employee:true, item:true, asset: true}).limit(limit).run().then(function(mains) {
                res.json({
                    data: mains,
                    total: count,
                    pno: pno,
                    psize: limit
                });
                }).error(handleError(res));
                handleError(res);
            }else{
                res.status(500).send({ message: 'No field exist.'});
            }    
        }else{
            field=sort.substring(0,sortLength);
            console.log("field--"+field);
            console.log("has field--"+Maintenance.hasFields(field));
            if(Maintenance.hasFields(field)){
                Maintenance.filter(filter).count().execute().then(function(total) {
                    count = total;
                    console.log(total);
                });    
                Maintenance.orderBy(r.asc(field)).filter(filter).getJoin({portfolio: true, employee: true, item: true, asset: true}).skip(offset).limit(limit).run().then(function(mains) {
                res.json({
                    data: mains,
                    total: count,
                    pno: pno,
                    psize: limit
                });
                }).error(handleError(res));
                handleError(res);  
            }else{
                res.status(500).send({ message: 'No field exist.'});
            }       
        }
    }    
};

// get by id
exports.getMaintenance = function (req, res) {
    var id = req.params.id;
    Maintenance.get(id).getJoin({portfolio: true, employee: true, item: true, asset: true}).run().then(function(main) {
     res.json(main);
    }).error(handleError(res));
};

// delete by id
exports.deleteMaintenance = function (req, res) {
    var id = req.params.id;
    Maintenance.get(id).delete().run().then(function(branch) {
        res.json({
            status: "success"
        });
    }).error(handleError(res));
}

exports.addMaintenance = function (req, res) {
    var newMaintenance = new Maintenance(req.body);
    var user = JSON.parse(req.decoded);
    newMaintenance.createdBy =user.userId;
    newMaintenance.portfolioId=user.portfolioId;
    newMaintenance.updatedBy =user.userId;
    newMaintenance.updatedOn =r.now();
    newMaintenance.save().then(function(result) {
        res.json({
            maintenance: result
        });
    }).error(handleError(res));
};


// update Maintenance
exports.updateMaintenance = function (req, res) {
    var maint = req.body;
    var user = JSON.parse(req.decoded);
        maint.updatedBy =user.userId;
        maint.updatedOn =r.now();
    Maintenance.get(req.params.id).update(maint).then(function(result) {
        res.json({
            result: result
        });
    }).error(handleError(res));
};


function handleError(res) {
    return function(error) {
        console.log(error.message);
        return res.send(500, {error: error.message});
    }
}