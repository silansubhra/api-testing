var thinky = require(__dirname+'/../util/thinky.js'),
r = thinky.r,
Asset = require(__dirname+'/../model/asset.js'),
User = require(__dirname+'/../model/user.js'),
Portfolio = require(__dirname+'/../model/portfolio.js'),
env = require(__dirname+'/../../env'),
config = require(__dirname+'/../../config/'+ env.name);



exports.listAssets = function (req, res) { 
    var count;
    var pno=1,offset=0,limit=10;
    if(req.query.psize != undefined && req.query.psize != null && !isNaN(req.query.psize)){
        limit = parseInt(req.query.psize);
    }

    if(req.query.pno != undefined && req.query.pno != null && !isNaN(req.query.pno)){
       pno =  parseInt(req.query.pno);
    }

    offset = (pno -1) * limit;

    var sort =req.query.sort;
    var token = req.body.token || req.query.token || req.headers['x-access-token'];
    var tokenObject = JSON.parse(req.decoded);
    var filter ={
        
        portfolioId: tokenObject.portfolioId,
        status: 'active'
    }

    
    if(sort == undefined || sort == null ){

        Asset.orderBy(r.desc('createdOn')).filter(filter).getJoin({portfolio: true, customer:true}).skip(offset).limit(limit).run().then(function(assets) {
            Asset.filter(filter).count().execute().then(function(count) {
                res.json({
                    data: assets,
                    total: (count!=undefined?count:0),
                    pno: pno,
                    psize: limit
                });
        });            
        }).error(handleError(res));
        handleError(res);

    }else{
        var result =sort.substring(0, 1);
        sortLength =sort.length;
        if(result ==='-'){
            field=sort.substring(1,sortLength);
            console.log("field--"+field);
            console.log(typeof field);
            console.log("has field--"+Asset.hasFields(field));
            if(Asset.hasFields(field)){
                Asset.filter(filter).count().execute().then(function(total) {
                    count = total;
                    console.log(total);
                });    
                Asset.orderBy(r.desc(field)).filter(filter).skip(offset).getJoin({portfolio: true, customer:true}).limit(limit).run().then(function(assets) {
                res.json({
                    data: assets,
                    total: count,
                    pno: pno,
                    psize: limit
                });
                }).error(handleError(res));
                handleError(res);   
            }else{
                res.status(500).send({ message: 'No field exist.'});
            }    
        }else{
            field=sort.substring(0,sortLength);
            console.log("field--"+field);
            console.log("has field--"+Asset.hasFields(field));
            if(Asset.hasFields(field)){
                Asset.filter(filter).count().execute().then(function(total) {
                    count = total;
                    console.log(total);
                });    
                Asset.orderBy(r.asc(field)).filter(filter).getJoin({portfolio: true, customer: true}).skip(offset).limit(limit).run().then(function(assets) {
                res.json({
                    data: assets,
                    total: count,
                    pno: pno,
                    psize: limit
                });
                }).error(handleError(res));
                handleError(res);  
            }else{
                res.status(500).send({ message: 'No field exist.'});
            }       
        }
    }    
};
// get by id
exports.getAsset = function (req, res) {
    var id = req.params.id;
    Asset.get(id).getJoin({portfolio: true, customer: true}).run().then(function(asset) {
     res.json(asset);
    }).error(handleError(res));
};

// delete by id
exports.deleteAsset = function (req, res) {
    var id = req.params.id;
    Asset.get(id).delete().run().then(function(branch) {
        res.json({
            status: "success"
        });
    }).error(handleError(res));

}


exports.addAsset = function (req, res) {
    var newAsset = new Asset(req.body);
    var user = JSON.parse(req.decoded);
    newAsset.createdBy =user.userId;
    newAsset.portfolioId=user.portfolioId;
    newAsset.updatedBy =user.userId;
    newAsset.updatedOn =r.now();
    console.log(newAsset);
    Asset.orderBy({index: r.desc('rank')}).filter({portfolioId: user.portfolioId}).run().then(function(asse){
        if (asse && asse.length>0) {
            assey=asse[0];
            newAsset.rank=assey.rank+1;
        }else{
            newAsset.rank=1;
        }
        Portfolio.get(user.portfolioId).run().then(function(as){
            var nm =as.name;
            newAsset.cCode =nm.substring(0, 3).toUpperCase();
        
            newAsset.assetId=newAsset.cCode +' '+ newAsset.rank;
            console.log("assetId" +newAsset.assetId);
            newAsset.save().then(function(result) {
                res.json({
                    asset: result
                });
            }).error(handleError(res));
        }).error(handleError(res));
    }).error(handleError(res));
};


// update asset
exports.updateAsset = function (req, res) {
    var asse = req.body;
    var user = JSON.parse(req.decoded);
        asse.updatedBy =user.userId;
        asse.updatedOn =r.now();
    Asset.get(req.params.id).update(asse).then(function(result) {
        res.json({
            result: result
        });
    }).error(handleError(res));
};


function handleError(res) {
    return function(error) {
        console.log(error.message);
        return res.send(500, {error: error.message});
    }
}