var thinky = require(__dirname+'/../util/thinky.js'),
    r = thinky.r,
    ProductInventory = require(__dirname+'/../model/productInventory.js'),
    Org = require(__dirname+'/../model/org.js'),
    Product = require(__dirname+'/../model/item.js'),
    Branch = require(__dirname+'/../model/branch.js');
    Employee = require(__dirname+'/../model/employee.js'),

// list productInventories
// TODO: all filter, page size and offset, columns, sort
exports.listProductInventories = function (req, res) {
    var count;
    var pno=1,offset=0,limit=10;
    if(req.query.psize != undefined && req.query.psize != null && !isNaN(req.query.psize)){
        limit = parseInt(req.query.psize);
    }

    if(req.query.pno != undefined && req.query.pno != null && !isNaN(req.query.pno)){
       pno =  parseInt(req.query.pno);
    }

    offset = (pno -1) * limit;

    var token = req.body.token || req.query.token || req.headers['x-access-token'];
    var tokenObject = JSON.parse(req.decoded);
    var productId = req.params.id;

    ProductInventory.orderBy(r.desc('createdOn')).filter({portfolioId: tokenObject.portfolioId,productId:productId}).getJoin({product: true}).skip(offset).limit(limit).run().then(function(productInventories) {
        ProductInventory.filter({portfolioId: tokenObject.portfolioId,productId:productId}).count().execute().then(function(count) {
            res.json({
                data: productInventories,
                total: (count!=undefined?count:0),
                pno: pno,
                psize: limit
            });
        });            
    }).error(handleError(res));
    handleError(res);
};

// get by id
exports.getProductInventory = function (req, res) {
    var id = req.params.id;
    ProductInventory.get(id).getJoin({product :true}).run().then(function(productInventory) {
     res.json({
         productInventory: productInventory
     });
    }).error(handleError(res));
};

// delete by id
exports.deleteProductInventory = function (req, res) {
    var id = req.params.id;
    ProductInventory.get(id).delete().run().then(function(service) {
        res.json({
            status: "success"
        });
    }).error(handleError(res));
};

// Add user
exports.addProductInventory = function (req, res) {
    var newProductInventory = new ProductInventory(req.body);
    var user = JSON.parse(req.decoded);
    newProductInventory.createdBy =user.userId;
    newProductInventory.portfolioId=user.portfolioId;
    newProductInventory.updatedBy =user.userId;
    newProductInventory.updatedOn =r.now();
    newProductInventory.productId = req.params.id;
    console.log(newProductInventory);
    Product.get(newProductInventory.productId).run().then(function(prd){
        if(prd.inHouseStock < parseInt(newProductInventory.quantity)){
            if(prd.inHouseStock === 0){
                return res.send(500, {error: 'You do not enough stock'});
            }else{
                return res.send(500, {error: 'Can not assign more quantity than '+prd.inHouseStock });
            }
            
        }else{
            ProductInventory.filter({employeeId: newProductInventory.employeeId,portfolioId: user.portfolioId,productId: newProductInventory.productId}).run().then(function(pie){
                if(pie && pie.length >0){
                    var nqty = newProductInventory.quantity;
                    var qty = pie[0].quantity;
                    if(nqty < qty){
                        var cmt = 'de-assigned from employee '+pie[0].empObj.name;
                    }else{
                        var cmt = 'assigned to employee '+pie[0].empObj.name;
                    }
                    ProductInventory.get(pie[0].id).update({quantity: nqty}).run().then(function(result){
                        var inhStock = (prd.inHouseStock + qty) - nqty;
                        var empStock = (prd.employeeStock - qty) + nqty;
                        Product.get(result.productId).update({inHouseStock : inhStock ,employeeStock: empStock}).run().then(function(nPrd){
                            r.table("inventorylog").insert([
                            {employeeId: newProductInventory.employeeId ,productId: newProductInventory.productId,quantity: newProductInventory.quantity,comment:cmt,inhouse:nPrd.inHouseStock,emp:nPrd.employeeStock,before:qty,after:nqty,productName:prd.name,portfolioId: user.portfolioId,
                            createdBy :user.userId,updatedBy :user.userId,createdOn:r.now()}
                            ]).run()
                            res.json({
                                result: result
                            });
                        }).error(handleError(res));
                    }).error(handleError(res));    
                }else{
                    Employee.get(newProductInventory.employeeId).run().then(function(emp) {
                        newProductInventory.productObj = prd ;
                        newProductInventory.empObj = emp ;
                        newProductInventory.save().then(function(result) {
                            var lStock = prd.inHouseStock - newProductInventory.quantity;
                            var empStock = prd.employeeStock + newProductInventory.quantity;
                            Product.get(result.productId).update({inHouseStock : lStock ,employeeStock: empStock}).run().then(function(nPrd){
                                r.table("inventorylog").insert([
                                {employeeId: newProductInventory.employeeId ,productId: newProductInventory.productId,quantity: newProductInventory.quantity,comment:'assigned to employee '+emp.name,inhouse:nPrd.inHouseStock,emp:nPrd.employeeStock,before:0,after:newProductInventory.quantity,productName:prd.name,portfolioId: user.portfolioId,
                                createdBy :user.userId,updatedBy :user.userId,createdOn:r.now()}
                                ]).run()
                                res.json({
                                    result: result
                                });
                            }).error(handleError(res));
                       }).error(handleError(res));
                    }).error(handleError(res));   
                }
            }).error(handleError(res));
        }
    }).error(handleError(res));
};

// update user
exports.updateProductInventory = function (req, res) {
    var tsk = new ProductInventory(req.body);
    var user = JSON.parse(req.decoded);
    tsk.updatedBy =user.userId;
    tsk.updatedOn =r.now();
    ProductInventory.get(req.params.id).run().then(function(pi) {
        var nQty = pi.quantity - parseInt(tsk.quantity);
        Product.get(tsk.productId).run().then(function(prd){
            if(nQty < 0){
                if(prd.stock < nQty){
                    if(prd.stock === 0){
                        return  res.send(500, {error: 'You do not have enough stock'});
                    }else{
                        return  res.send(500, {error: 'Can not assign more quantity than '+prd.stock });
                    }
                  
                }else{
                    var nStock = prd.stock - Math.abs(nQty);
                    ProductInventory.get(tsk.id).update(tsk).then(function(result) {
                        Product.get(prd.id).update({inHouseStock : nStock}).then(function(nPrd){
                            res.json({
                                result: result
                            });
                        }).error(handleError(res));
                    }).error(handleError(res));
                }
            }else{
                var nStock = prd.stock + Math.abs(nQty);
                ProductInventory.get(tsk.id).update(tsk).then(function(result) {
                    Product.get(prd.id).update({inHouseStock : nStock}).then(function(nPrd){
                        res.json({
                            result: result
                        });
                    }).error(handleError(res));
                }).error(handleError(res));
            }    
        });  
    });
};

function handleError(res) {
    return function(error) {
        console.log(error.message);
        return res.send(500, {error: error.message});
    }
}
