var thinky = require(__dirname+'/../util/thinky.js'),
    r = thinky.r,
    Bank = require(__dirname+'/../model/bank.js'),
    Org = require(__dirname+'/../model/org.js'),
    Branch = require(__dirname+'/../model/branch.js'),
    Service = require(__dirname+'/../model/service.js'),
    Employee = require(__dirname+'/../model/employee.js');

// list modes
// TODO: all filter, page size and offset, columns, sort
exports.listModes = function (req, res) {
    var count;
    var pno=1,offset=0,limit=10;
    if(req.query.psize != undefined && req.query.psize != null && !isNaN(req.query.psize)){
        limit = parseInt(req.query.psize);
    }

    if(req.query.pno != undefined && req.query.pno != null && !isNaN(req.query.pno)){
       pno =  parseInt(req.query.pno);
    }

    offset = (pno -1) * limit;

    var token = req.body.token || req.query.token || req.headers['x-access-token'];
    var tokenObject = JSON.parse(req.decoded);

        Bank.filter({portfolioId: tokenObject.portfolioId}).count().execute().then(function(total) {
            count = total;
            console.log(total);
        });

        Bank.orderBy({index: r.desc('createdOn')}).filter({portfolioId: tokenObject.portfolioId}).skip(offset).limit(limit).run().then(function(modes) {
            res.json({
                data: modes,
                total: count,
                pno: pno,
                psize: limit
            });
        }).error(handleError(res));
    
    handleError(res);
};

// get by id
exports.getMode = function (req, res) {
    var id = req.params.id;
    Bank.get(id).getJoin({portfolio :true}).run().then(function(bank) {
     res.json({
         bank: bank
     });
    }).error(handleError(res));
};


// delete by id
exports.deleteMode = function (req, res) {
    var id = req.params.id;
    Bank.get(id).delete().run().then(function(portfolio) {
        res.json({
            status: "success"
        });
    }).error(handleError(res));
};

// Add user
exports.addMode = function (req, res) {
    var newMode = new Bank(req.body);
    var user = JSON.parse(req.decoded);
    newMode.portfolioId=user.portfolioId;
    newMode.createdBy =user.userId;
    newMode.updatedBy =user.userId;
    newMode.updatedOn =r.now();
    newMode.bankAccount = newMode.bankName+' '+newMode.accountNo;
    console.log(newMode);
    newMode.save().then(function(result) {
        res.json({
            result: result
        });
    }).error(handleError(res));
};

// update user
exports.updateMode = function (req, res) {
    Bank.get(req.body.id).update(req.body).then(function(result) {
        res.json({
            result: result
        });
    }).error(handleError(res));
};

function handleError(res) {
    return function(error) {
        console.log(error.message);
        return res.send(500, {error: error.message});
    }
}