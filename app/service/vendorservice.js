var thinky = require(__dirname+'/../util/thinky.js'),
    r = thinky.r,
    Vendor = require(__dirname+'/../model/vendor.js'),
    Org = require(__dirname+'/../model/org.js'),
    Branch = require(__dirname+'/../model/branch.js'),
    User = require(__dirname+'/../model/user.js');

// list vendors
// TODO: all filter, page size and offset, columns, sort
exports.listVendors = function (req, res) {
    var count;
    var pno=1,offset=0,limit=10;
    if(req.query.psize != undefined && req.query.psize != null && !isNaN(req.query.psize)){
        limit = parseInt(req.query.psize);
    }

    if(req.query.pno != undefined && req.query.pno != null && !isNaN(req.query.pno)){
       pno =  parseInt(req.query.pno);
    }

    offset = (pno -1) * limit;

    var sort =req.query.sort;
    var pluck = req.query.pluck;
    var token = req.body.token || req.query.token || req.headers['x-access-token'];
    var tokenObject = JSON.parse(req.decoded);

    var address =req.query.address;
    var name = req.query.name;
    var mobile =req.query.mobile;

    var filter ={
        portfolioId: tokenObject.portfolioId,
        status: 'active'
    }

    if(address !=null && address !=undefined){
        filter.address =address;
    }
    if(name !=null && name !=undefined){
        filter.name =name;
    }

    if(mobile !=null && mobile !=undefined){
        filter.mobile =mobile;
    }

    if(sort == undefined || sort == null ){
       Vendor.orderBy({index: r.desc('createdOn')}).filter(filter).getJoin({portfolio: true}).skip(offset).limit(limit).run().then(function(vendors) {
           r.table("vendor").filter(filter).count().run().then(function(total) {
            res.json({
                data: vendors,
                total: (count!=undefined?count:0),
                pno: pno,
                psize: limit
            });
        });    
        }).error(handleError(res)); 
        handleError(res);
    }else{
        var result =sort.substring(0, 1);
        sortLength =sort.length;
        if(result ==='-'){
            field=sort.substring(1,sortLength);
            console.log("field--"+field);
            console.log(typeof field);
            console.log("has field--"+Vendor.hasFields(field));
            if(Vendor.hasFields(field)){
                Vendor.filter(filter).count().execute().then(function(total) {
                    count = total;
                    console.log(total);
                });    
                Vendor.orderBy(r.desc(field)).filter(filter).skip(offset).limit(limit).run().then(function(vendors) {
                res.json({
                    data: vendors,
                    total: count,
                    pno: pno,
                    psize: limit
                });
                }).error(handleError(res));
                handleError(res);   
            }else{
                res.status(500).send({ message: 'No field exist.'});
            }    
        }else{
            field=sort.substring(0,sortLength);
            console.log("field--"+field);
            console.log("has field--"+Vendor.hasFields(field));
            if(Vendor.hasFields(field)){
                Vendor.filter(filter).count().execute().then(function(total) {
                    count = total;
                    console.log(total);
                });    
                Vendor.orderBy(r.asc(field)).filter(filter).skip(offset).limit(limit).run().then(function(vendors) {
                res.json({
                    data: vendors,
                    total: count,
                    pno: pno,
                    psize: limit
                });
                }).error(handleError(res));
                handleError(res);  
            }else{
                res.status(500).send({ message: 'No field exist.'});
            }       
        }
    }    
};
// get by id
exports.getVendor = function (req, res) {
    var id = req.params.id;
    Vendor.get(id).getJoin({branch: true,user :true}).run().then(function(vendor) {
     res.json(vendor);
    }).error(handleError(res));
};


// delete by id
exports.deleteVendor = function (req, res) {
    var id = req.params.id;
    Vendor.get(id).delete().run().then(function(branch) {
        res.json({
            status: "success"
        });
    }).error(handleError(res));
};

exports.bulkDeleteVendor = function (req, res) {
    var ids = req.query.ids;
    console.log("ids"+ids);
    if(ids && ids != null){
        var idsArray = ids.split(",");
        for(var i=0;i<idsArray.length;i++){
            var vendId =idsArray[i];
            console.log("vend"+vendId);
            var token = req.body.token || req.query.token || req.headers['x-access-token'];
            var tokenObject = JSON.parse(req.decoded);
            Vendor.filter({portfolioId: tokenObject.portfolioId,id: vendId,status: 'active'}).run().then(function(vendr){
                if(vendr && vendr>0){
                    Vendor.get(vendr.id).update({status:'inactive'}).run().then(function(branch) {
                        
                    }).error(handleError(res));
                }
            });
        }
        res.json({
            status: "success"
        });
    }else{
        res.send(404, {error: 'Select a vendor'});
    }  
};


// Add user
exports.addVendor = function (req, res) {
    var newVendor = new Vendor(req.body);
    var user = JSON.parse(req.decoded);
    newVendor.createdBy =user.userId;
    newVendor.portfolioId=user.portfolioId;
    newVendor.updatedBy =user.userId;
    newVendor.updatedOn =r.now();
    console.log(newVendor);
    newVendor.save().then(function(result) {
        res.json({
            result: result
        });
    }).error(handleError(res));
};

// update user
exports.updateVendor = function (req, res) {
    var vend = new Vendor(req.body);
    var user = JSON.parse(req.decoded);
    vend.updatedBy =user.userId;
    vend.updatedOn =r.now();
    Vendor.get(vend.id).update(vend).then(function(result) {
        
        res.json({
            result: result
        });
    }).error(handleError(res));
};

function handleError(res) {
    return function(error) {
        console.log(error.message);
        return res.send(500, {error: error.message});
    }
}
