var thinky = require(__dirname+'/../util/thinky.js'),
    r = thinky.r,
    Review = require(__dirname+'/../model/review.js'),
    Org = require(__dirname+'/../model/org.js'),
    MediaService = require(__dirname+'/mediaservice.js'),
    og = require(__dirname+'/../util/og.js'),
    env = require(__dirname+'/../../env'),
    config = require(__dirname+'/../../config/'+ env.name),
    Branch = require(__dirname+'/../model/branch.js');

// list reviews
// TODO: all filter, page size and offset, columns, sort
exports.listReviews = function (req, res) {
    var count;
    var pno=1,offset=0,limit=10;
    if(req.query.psize != undefined && req.query.psize != null && !isNaN(req.query.psize)){
        limit = parseInt(req.query.psize);
    }

    if(req.query.pno != undefined && req.query.pno != null && !isNaN(req.query.pno)){
       pno =  parseInt(req.query.pno);
    }

    offset = (pno -1) * limit;
    var filter ={
        status: 'active'
    }
    if(req.decoded !=undefined && req.decoded !=null){
        var tokenObject = JSON.parse(req.decoded);
        filter.mpId =tokenObject.mpId;
        filter.portfolioId =tokenObject.portfolioId;
       Review.orderBy({index: r.desc('createdOn')}).filter(filter).getJoin({portfolio: true,user: true}).skip(offset).limit(limit).run().then(function(reviews) {
            r.table("review").filter(filter).count().run().then(function(total) {
                res.json({
                    data: reviews,
                    total: (count!=undefined?count:0),
                    pno: pno,
                    psize: limit
                });
            });
        }).error(handleError(res)); 
        handleError(res);
    }else{
        var apiKey = req.apikey ;
        Portfolio.filter({apiKey: apiKey}).run().then(function(portfolio){
            var portId = portfolio[0].id;
            filter.mpId = portId
            console.log("filter="+JSON.stringify(filter));
            Review.orderBy({index: r.desc('createdOn')}).filter(filter).getJoin({portfolio: true,user: true}).skip(offset).limit(limit).run().then(function(reviews) {
                r.table("review").filter(filter).count().run().then(function(total) {
                    res.json({
                        data: reviews,
                        total: (count!=undefined?count:0),
                        pno: pno,
                        psize: limit
                    });
                });
            }).error(handleError(res));
            handleError(res);
        });    
    } 
};
// get by id
exports.getReview = function (req, res) {
    var id = req.params.id;
    Review.get(id).getJoin({branch :true}).run().then(function(review) {
     res.json({
         review: review
     });
    }).error(handleError(res));
};

// delete by id
exports.deleteReview = function (req, res) {
    var id = req.params.id;
    Review.get(id).delete().run().then(function(service) {
        res.json({
            status: "success"
        });
    }).error(handleError(res));
};

// Add user
exports.addReview = function (req, res) {
    var newReview = new Review(req.body);
    var user = JSON.parse(req.decoded);
    newReview.createdBy =user.userId;
    newReview.portfolioId=user.portfolioId;
    newReview.updatedBy =user.userId;
    newReview.updatedOn =r.now();

    newReview.save().then(function(result) {
        res.json({
            result: result
        });
    }).error(handleError(res));
};

// update user
exports.updateReview = function (req, res) {
    var cat = new Review(req.body);
    var user = JSON.parse(req.decoded);
    cat.updatedBy =user.userId;
    cat.updatedOn =r.now();
    Review.get(cat.id).update(cat).then(function(result) {
        
        res.json({
            result: result
        });
    }).error(handleError(res));
};

function handleError(res) {
    return function(error) {
        console.log(error.message);
        return res.send(500, {error: error.message});
    }
}