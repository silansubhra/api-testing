var thinky = require(__dirname+'/../util/thinky.js'),
    r = thinky.r,
    Quotation = require(__dirname+'/../model/quotation.js'),
    Org = require(__dirname+'/../model/org.js'),
    Branch = require(__dirname+'/../model/branch.js'),
    Lead = require(__dirname+'/../model/lead.js'),
    MediaService = require(__dirname+'/mediaservice.js'),
    Media = require(__dirname+'/../model/media.js'),
    og = require(__dirname+'/../util/og.js'),
    env = require(__dirname+'/../../env'),
    config = require(__dirname+'/../../config/'+ env.name);
    var moment = require('moment');
    var fs = require('fs');
    var url = require('url');
    var pdf = require('html-pdf');
    //var phantom = require('phantom'),
    var path = require('path'),
    EmailTemplate = require('email-templates').EmailTemplate,
    templatesDir = path.resolve(__dirname, '..', 'templates');
    var options = { format: 'Letter' };
    var ejs = require('ejs');  

// list quotations
// TODO: all filter, page size and offset, columns, sort
exports.listQuotations = function (req, res) {
    var count;
    var pno=1,offset=0,limit=10;
    if(req.query.psize != undefined && req.query.psize != null && !isNaN(req.query.psize)){
        limit = parseInt(req.query.psize);
    }

    if(req.query.pno != undefined && req.query.pno != null && !isNaN(req.query.pno)){
       pno =  parseInt(req.query.pno);
    }

    offset = (pno -1) * limit;

    var token = req.body.token || req.query.token || req.headers['x-access-token'];
    var tokenObject = JSON.parse(req.decoded);

        Quotation.filter({portfolioId: tokenObject.portfolioId}).count().execute().then(function(total) {
            count = total;
            console.log(total);
        });

        Quotation.orderBy({index: r.desc('createdOn')}).filter({portfolioId: tokenObject.portfolioId}).getJoin({portfolio: true,lead :true, customer: true}).skip(offset).limit(limit).run().then(function(quotations) {
            res.json({
                data: quotations,
                total: count,
                pno: pno,
                psize: limit
            });
        }).error(handleError(res));
    
    handleError(res);
};

// get by id
exports.getQuotation = function (req, res) {
    var id = req.params.id;
    Quotation.get(id).getJoin({branch: true,lead :true, customer: true}).run().then(function(quotation) {
     res.json({
         quotation: quotation
     });
    }).error(handleError(res));
};


exports.getQuotationNo = function (req, res) {

    var token = req.body.token || req.query.token || req.headers['x-access-token'];
    var tokenObject = JSON.parse(req.decoded);

    Portfolio.get(tokenObject.portfolioId).getJoin({org: true}).run().then(function(portfolio){
        Quotation.orderBy({index: r.desc('rank')}).filter({portfolioId: tokenObject.portfolioId}).run().then(function(ld){
            if(ld && ld.length>0){
                var rank = ld[0].rank || 0;
            }else{
                var rank = 0;
            }
            
            Preference.filter({portfolioId:tokenObject.portfolioId}).run().then(function(eConfig){
                if(eConfig && eConfig.length>0){
                    var mailConf = eConfig[0];
                    if(mailConf && mailConf.quotation && mailConf.quotation.prefix && mailConf.quotation.prefix !=''){
                        if(mailConf.quotation.sequence && rank < parseInt(mailConf.quotation.sequence)){
                            //newLead.leadId= mailConf.invoice.prefix +'-'+mailConf.invoice.sequence;
                            var nrank=parseInt(mailConf.quotation.sequence)+1;
                            var ino = mailConf.quotation.prefix +'-'+nrank;
                        }else{
                            ino= mailConf.quotation.prefix +'-'+(rank+1);
                        }
                    }else{
                        var ino = rank+1;
                    }
                }else{
                    var ino = rank+1;
                }
                res.json({
                    quote: ino,
                    portfolio: portfolio
                });
            });
        });
    });
};

// delete by id
exports.deleteQuotation = function (req, res) {
    var id = req.params.id;
    Quotation.get(id).delete().run().then(function(branch) {
        res.json({
            status: "success"
        });
    }).error(handleError(res));
};

exports.addQuotation = function (req, res) {
    var newQuotation = new Quotation(req.body);
    var user = JSON.parse(req.decoded);
    newQuotation.createdBy =user.userId;
    newQuotation.portfolioId=user.portfolioId;
    newQuotation.updatedBy =user.userId;
    newQuotation.updatedOn =r.now();
    Quotation.orderBy({index: r.desc('rank')}).filter({portfolioId: user.portfolioId}).run().then(function(ld){
        if (ld && ld.length>0) {
            led=ld[0];
            newQuotation.rank=led.rank+1;
        }else{
            newQuotation.rank=1;
        } 
        console.log(newQuotation);
        Org.get(user.orgId).run().then(function(org){
            var leads = {
                from:newQuotation.from.split("\n").join("<br>") || '',
                to:newQuotation.to.split("\n").join("<br>") || '',
                qno:newQuotation.quotationNo || '',
                pan:newQuotation.panNo || '',
                gst:newQuotation.gstNo || '',
                date:moment(newQuotation.date).format('DD-MMM-YYYY') || '',
                stotal:newQuotation.subTotal || 0,
                tax:newQuotation.tax || 0,
                discount:newQuotation.discount ||'',
                total:newQuotation.total || '',
                note:newQuotation.notes.split("\n").join("<br>") || '',
                terms:newQuotation.terms.split("\n").join("<br>") || '',
                tableData:newQuotation.services || ''
            }
            if(org){
                leads.isrc = org.coverImage;
            }
            newQuotation.save().then(function(quote){
                pdfile = config.media.upload.local.path + '/'+quote.id+'.pdf';
                 var html = ejs.render(fs.readFileSync(path.join(templatesDir, 'quotation-gst/html.ejs'), 'utf8'),leads);
                pdf.create(html, options).toFile(pdfile, function(err, res1) {
                  if (err) return console.log(err);
                  console.log('---'+JSON.stringify(res1));
                  var email = false;
                    upload(req,res,res1.filename,quote.id,email);
                });
            });
        });
    });
};


exports.addEmailQuotation = function (req, res) {
    var newQuotation = new Quotation(req.body);
    var user = JSON.parse(req.decoded);
    newQuotation.createdBy =user.userId;
    newQuotation.portfolioId=user.portfolioId;
    newQuotation.updatedBy =user.userId;
    newQuotation.updatedOn =r.now();
    Org.get(user.orgId).getJoin({portfolios: true}).run().then(function(org){
        var leads = {
            from:newQuotation.from.split("\n").join("<br>") || '',
            to:newQuotation.to.split("\n").join("<br>") || '',
            qno:newQuotation.quotationNo || '',
            pan:newQuotation.panNo || '',
            gst:newQuotation.gstNo || '',
            date:moment(newQuotation.date).format('DD-MMM-YYYY') || '',
            stotal:newQuotation.subTotal || 0,
            tax:newQuotation.tax || 0,
            discount:newQuotation.discount ||'',
            total:newQuotation.total || '',
            note:newQuotation.notes.split("\n").join("<br>") || '',
            terms:newQuotation.terms.split("\n").join("<br>") || '',
            tableData:newQuotation.services || ''
        }
        if(org){
            leads.isrc = org.portfolios[0].letterheadheader || org.coverImage;
        }
        console.log(leads.isrc);
        newQuotation.save().then(function(quote){
            var html = ejs.render(fs.readFileSync(path.join(templatesDir, 'quotation-gst/html.ejs'), 'utf8'),leads);
            pdfile = config.media.upload.local.path + '/'+quote.id+'.pdf';
            pdf.create(html, options).toFile(pdfile, function(err, res1) {
              if (err) return console.log(err);
              console.log('---'+JSON.stringify(res1));
              var email = true;
                upload(req,res,res1.filename,quote.id,email,html,leads.isrc);
            });

        });
    });
    
};

var upload = function(req,res,path,id,email){
   var user = JSON.parse(req.decoded);
    //var scriptName = path.basename(path);
    fs.readFile(path,  function (err,pdata){
        var file = 
            { name: id+'.pdf',
              data: pdata,
              encoding: '7bit',
              mimetype: 'application/pdf'
          };
        if (!err) {
            console.log(file);
            console.log('received data: ' + file.name);
            var obj = new Object();
            obj.name = file.name;
            obj.mime = file.mimetype;
            obj.forId = id;
            obj.userId = user.userId;
            obj.updatedBy =  user.userId;
            obj.portfolioId = user.portfolioId;
            obj.updatedOn = r.now();
            var newMedia = new Media(obj);
            var sendResponse = function(error, data){
                console.log('error==='+error);
                console.log(data);
                if(!error){
                    var coverImage = config.media.upload.s3.accessUrl + '/media/'+ file.name;
                    Quotation.get(id).update({mediaId: data.mediaId, pdf: coverImage}).then(function(r1) {
                        if(email == false){
                            res.json({
                                result: r1.pdf
                            });
                        }else{
                            Lead.filter({leadId: r1.leadId,portfolioId: user.portfolioId}).run().then(function(lead){
                                Customer.get(lead[0].customerId).run().then(function(customer){
                                    if(customer.email){
                                        console.log(customer.email);
                                        //var template1 = new EmailTemplate(path.join(templatesDir, 'invoice'));
                                        Preference.filter({portfolioId:user.portfolioId}).run().then(function(eConfig){
                                            var portfolioEmailId,portfolioSenderName;
                                            if(eConfig && eConfig.length>0 && eConfig[0].emailConfig){
                                                var mailConf = eConfig[0];
                                                 portfolioEmailId= mailConf.emailConfig.gmail.auth.user;
                                                 portfolioSenderName= mailConf.emailConfig.gmail.auth.name;
                                                 var sender = mailConf.emailConfig.gmail;
                                            }else{
                                                var sender=config.smtp.gmail;
                                                portfolioEmailId= 'support@zinetgo.com';
                                                portfolioSenderName= 'Zinetgo';
                                            }
                                            emailService.sendEmail(sender, {
                                                from: '"'+user.name +'" <'+portfolioEmailId+'>', // sender address
                                                to: customer.email, // list of receivers
                                                subject: 'Your '+user.name+' quotation for '+r1.leadId, // Subject line
                                                text: 'Invoice',
                                                html:'Dear '+customer.name+',<br> Find the below quotation attached from '+user.name+'.<br>'+'<p>Quotation Number: '+r1.quotationNo+'</p>Amount: '+r1.total+'</p>',
                                                attachments: [
                                                    {  
                                                        filename: r1.leadId+'.pdf',
                                                        path: r1.pdf, 
                                                        contentType: 'application/pdf'
                                                    }
                                                ]  
                                            }), function(err, success) {
                                                console.log(success);
                                                if (err) {
                                                    console.log(err);
                                                }
                                            }
                                            res.send(200,{message:'email send successfully'});
                                        });
                                    }else{
                                        res.send(500,{error:'please update the customer email address'})
                                    }
                                }).error(handleError(res));
                            }).error(handleError(res));
                        }
                    }).error(handleError(res));
                    
                }else{
                    return res.send(500, {error: error.message});
                }
            }
            newMedia.save().then(function(media) {  
                MediaService.uploadPdf(file, media.id, sendResponse);
            });
        } else {
            console.log(err);
        }
    });
}

// update lead
exports.updateQuotation = function (req, res) {
    var quo = new Quotation(req.body);
    var user = JSON.parse(req.decoded);
    quo.updatedBy =user.userId;
    quo.updatedOn =r.now();
    Quotation.get(quo.id).update(quo).then(function(result) {
        
        res.json({
            result: result
        });
    }).error(handleError(res));
};

function handleError(res) {
    return function(error) {
        console.log(error.message);
        return res.send(500, {error: error.message});
    }
}