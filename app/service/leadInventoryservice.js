var thinky = require(__dirname+'/../util/thinky.js'),
    r = thinky.r,
    LeadInventory = require(__dirname+'/../model/leadInventory.js'),
    Org = require(__dirname+'/../model/org.js'),
    ProductInventory = require(__dirname+'/../model/productInventory.js'),
    Lead = require(__dirname+'/../model/lead.js'),
    Product = require(__dirname+'/../model/item.js'),
    Branch = require(__dirname+'/../model/branch.js');

// list leadInventories
// TODO: all filter, page size and offset, columns, sort
exports.listLeadInventories = function (req, res) {
    var count;
    var pno=1,offset=0,limit=10;
    if(req.query.psize != undefined && req.query.psize != null && !isNaN(req.query.psize)){
        limit = parseInt(req.query.psize);
    }

    if(req.query.pno != undefined && req.query.pno != null && !isNaN(req.query.pno)){
       pno =  parseInt(req.query.pno);
    }

    offset = (pno -1) * limit;

    var token = req.body.token || req.query.token || req.headers['x-access-token'];
    var tokenObject = JSON.parse(req.decoded);
    var productId = req.params.id;

    LeadInventory.orderBy(r.desc('createdOn')).filter({portfolioId: tokenObject.portfolioId,productId:productId}).getJoin({product: true}).skip(offset).limit(limit).run().then(function(leadInventories) {
        LeadInventory.filter({portfolioId: tokenObject.portfolioId,productId:productId}).count().execute().then(function(count) {
            res.json({
                data: leadInventories,
                total: (count!=undefined?count:0),
                pno: pno,
                psize: limit
            });
        });            
    }).error(handleError(res));
    handleError(res);
};

// get by id
exports.getLeadInventory = function (req, res) {
    var id = req.params.id;
    LeadInventory.get(id).getJoin({product :true}).run().then(function(leadInventory) {
     res.json({
         leadInventory: leadInventory
     });
    }).error(handleError(res));
};

// delete by id
exports.deleteLeadInventory = function (req, res) {
    var id = req.params.id;
    LeadInventory.get(id).delete().run().then(function(service) {
        res.json({
            status: "success"
        });
    }).error(handleError(res));
};

// Add user
exports.addLeadInventory = function (req, res) {
    var newLeadInventory = new LeadInventory(req.body);
    var user = JSON.parse(req.decoded);
    newLeadInventory.createdBy =user.userId;
    newLeadInventory.portfolioId=user.portfolioId;
    newLeadInventory.updatedBy =user.userId;
    newLeadInventory.updatedOn =r.now();
    newLeadInventory.leadId = req.params.id;
    console.log(newLeadInventory);
    Product.get(newLeadInventory.productId).run().then(function(prd){
        if(prd.stock < parseInt(newLeadInventory.quantity)){
            if(prd.stock ===0){
                return  res.send(500, {error: 'The employee do not have this stock'});
            }else{
                return res.send(500, {error: 'Can not assign more quantity than '+prd.stock });
            }
            
        }else{
            Lead.get(newLeadInventory.leadId).run().then(function(lead){
                ProductInventory.filter({productId : newLeadInventory.productId, employeeId: newLeadInventory.employeeId,portfolioId: lead.portfolioId}).run().then(function(pi){
                    if(!pi || pi.length < 1){
                        return res.send(500, {error: 'This employee do not have this stock '});
                    }else{
                        if(pi[0].quantity < parseInt(newLeadInventory.quantity)){
                            return res.send(500, {error: 'This employee has only '+ pi[0].quantity +'quantity'});
                        }else{
                            newLeadInventory.productObj = prd ;
                            LeadInventory.filter({portfolioId: user.portfolioId,productId: newLeadInventory.productId, leadId: newLeadInventory.leadId}).run().then(function(li){
                                if(li && li.length >0){
                                    var nqty = newLeadInventory.quantity + li[0].quantity;
                                    LeadInventory.get(li[0].id).update({quantity: nqty}).then(function(result){
                                        var lStock = prd.stock - result.quantity;
                                        var lqty = pi[0].quantity - result.quantity;
                                        var empStock = prd.employeeStock - result.quantity;
                                        ProductInventory.get(pi[0].id).update({quantity: lqty}).then(function(pil){
                                            Product.get(result.productId).update({stock : lStock, employeeStock : empStock}).run().then(function(nPrd){
                                                r.table("inventorylog").insert([
                                                    {employeeId: newLeadInventory.employeeId ,leadId:lead.id,productId: result.productId,quantity: '(-)'+newLeadInventory.quantity,comment:'used in the lead '+lead.leadId+' by '+pi[0].empObj.name,inhouse:nPrd.inHouseStock,emp:nPrd.employeeStock,before:pi[0].quantity,after:lqty,productName:nPrd.name,portfolioId: user.portfolioId,
                                                    createdBy :user.userId,updatedBy :user.userId,createdOn:r.now()}
                                                    ]).run()
                                                res.json({
                                                    result: result
                                                });
                                            }).error(handleError(res));
                                        }).error(handleError(res));    
                                    }).error(handleError(res)); 
                                }else{
                                    newLeadInventory.save().then(function(result) {
                                        var lStock = prd.stock - result.quantity;
                                        var lqty = pi[0].quantity - result.quantity;
                                        var empStock = prd.employeeStock - result.quantity;
                                        ProductInventory.get(pi[0].id).update({quantity: lqty}).then(function(pil){
                                            Product.get(result.productId).update({stock : lStock, employeeStock : empStock}).run().then(function(nPrd){
                                                r.table("inventorylog").insert([
                                                    {employeeId: newLeadInventory.employeeId ,productId: result.productId,quantity: '(-)'+newLeadInventory.quantity,comment:'used in the lead '+lead.leadId+' by '+pi[0].empObj.name,inhouse:nPrd.inHouseStock,emp:nPrd.employeeStock,before:pi[0].quantity,after:lqty,productName:nPrd.name,portfolioId: user.portfolioId,
                                                    createdBy :user.userId,updatedBy :user.userId,createdOn:r.now()}
                                                    ]).run()
                                                res.json({
                                                    result: result
                                                });
                                            }).error(handleError(res));
                                        }).error(handleError(res));
                                   }).error(handleError(res));
                                }
                            }).error(handleError(res)); 
                             
                        } 
                    }
                }).error(handleError(res));
            }).error(handleError(res));
        }
        
    }).error(handleError(res));
};

// update user
exports.updateLeadInventory = function (req, res) {
    var tsk = new LeadInventory(req.body);
    var user = JSON.parse(req.decoded);
    tsk.updatedBy =user.userId;
    tsk.updatedOn =r.now();
    LeadInventory.get(req.params.id).run().then(function(pi) {
        var nQty = pi.quantity - parseInt(tsk.quantity);
        Product.get(tsk.productId).run().then(function(prd){
            if(nQty < 0){
                if(prd.stock < nQty){
                    res.send(500, {error: 'Can not assign more quantity than '+prd.stock });
                }else{
                    var nStock = prd.stock - Math.abs(nQty);
                    LeadInventory.get(tsk.id).update(tsk).then(function(result) {
                        Product.get(prd.id).update({inHouseStock : nStock}).then(function(nPrd){
                            res.json({
                                result: result
                            });
                        }).error(handleError(res));
                    }).error(handleError(res));
                }
            }else{
                var nStock = prd.stock + Math.abs(nQty);
                LeadInventory.get(tsk.id).update(tsk).then(function(result) {
                    Product.get(prd.id).update({inHouseStock : nStock}).then(function(nPrd){
                        res.json({
                            result: result
                        });
                    }).error(handleError(res));
                }).error(handleError(res));
            }    
        });  
    });
};

function handleError(res) {
    return function(error) {
        console.log(error.message);
        return res.send(500, {error: error.message});
    }
}