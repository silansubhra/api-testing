var thinky = require(__dirname+'/../util/thinky.js'),
    r = thinky.r,
    Sms = require(__dirname+'/../model/sms.js'),
	env = require(__dirname+'/../../env'),
	config = require(__dirname+'/../../config/'+ env.name),
	Portfolio = require(__dirname+'/../model/portfolio.js'),
	User = require(__dirname+'/../model/user.js');

//var msg91p = require("msg91")(config.msg91AuthKey, config.msg91SenderId, 1);
//var msg91t = require("msg91")(config.msg91AuthKey, config.msg91SenderId, config.msg91DefaultRoute),
    msg91=require('msg91-sms');
//msg91.sendOne(authkey,number,message,senderid,route,dialcode,function(response){


exports.sendSms = function (req, apiKey, senderId, mobileNo, message) {

	var user = req.decoded;

	var newSms = new Sms({
   		mobile: mobileNo,
	    message: message,
	    sender: senderId,
	    route: config.sms.msg91.defaultRoute,
	    createdOn: r.now(),
	    status: "NEW"
    });

    if(user != undefined && user != null){
    	newSms.portfolioId = user.portfolioId;
	    newSms.createdBy = user.userId;
    }

    if(req.params.key){
    	Portfolio.filter({tpKey:req.params.key}).run().then(function(result){
    		if(result && result.length >0){
    			newSms.portfolioId = result[0].id;
    			var mob =result[0].mobile;
    			User.filter({mobile:result[0].mobile}).run().then(function(user){
    				newSms.createdBy = user[0].id;
    				newSms.save().then(function(result) {
				      	msg91.sendOne(apiKey, mobileNo, message, senderId, 4,91, function(err, response){
					    	if(err == null){
					    		console.log("req:"+response);
					    		Sms.get(result.id).update({requestId:response,status:"SENT"}).execute();
					    	}else{
					    		console.log("err:"+err);
					    	}
						});
				    }).error(function(error){
				    	console.log(error);
				    });
    			});
    		}
    	});
    }else{
		newSms.save().then(function(result) {
	      	msg91.sendOne(apiKey, mobileNo, message, senderId, 4,91, function(err, response){
		    	if(err == null){
		    		console.log("req:"+response);
		    		Sms.get(result.id).update({requestId:response,status:"SENT"}).execute();
		    	}else{
		    		console.log("err:"+err);
		    	}
			});
	    }).error(function(error){
	    	console.log(error);
	    });
	}
};

exports.bulksms = function (req, res) {

	var bulkSms = new Sms(req.body);

	var user = JSON.parse(req.decoded);

	console.log("token---"+JSON.stringify(user));

	console.log('bulksms: '+bulkSms.mobile + ' '+bulkSms.message);
	Portfolio.filter({id:user.portfolioId}).run().then(function(port){
		var portfolio =port[0];
		if(bulkSms){
			if(bulkSms.mobile){
				var mn = ''+bulkSms.mobile;
				var mobileNumbers = mn.split(',');
				for(i=0;i<mobileNumbers.length;i++){
					var newSms = new Sms({
						mobile: mobileNumbers[i],
		    			message: bulkSms.message,
						sender: portfolio.smsProvider.senderId,
					    route: 4,
					    createdOn: r.now(),
					    status: "NEW"
					});

					console.log("sender---"+JSON.stringify(portfolio.smsProvider));

					console.log("portfolio " + user.portfolioId);

					if(user != undefined && user != null){
				    	newSms.portfolioId = user.portfolioId;
					    newSms.createdBy = user.userId;
				    }

					newSms.save().then(function(result) {
						msg91.sendOne(portfolio.smsProvider.authKey, result.mobile, result.message, result.sender, result.route,91, function(response){
							if(response && response != null){
				    			console.log("req:"+response);
				    			Sms.get(result.id).update({requestId:response,status:"SENT"}).execute();
				    			return res.status(200).send({message: "bulk sms processing"});
					    	}else{
					    		return res.status(500).send({message: "something went wrong"});
					    	}
						});
				    }).error(function(error){
				    	console.log(error);
				    });
				}
			}
		}
	});
};


exports.smsDeliveryReport = function (req, res) {
	if(req.body != undefined && req.body != null && req.body.data != undefined && req.body.data != null){
		try{
			var data = JSON.parse(req.body.data);
			//console.log(data);
			if(data.length > 0){

		       	/*
		       	[{
					"senderId": "vjayhs",
					"requestId": "366668724e32363031373933",
					"report": [{
						"date": "2016-06-08 18:41:00",
						"number": "919066364784",
						"status": "1",
						"desc": "DELIVERED"
					}],
					"userId": "98449",
					"campaignName": "API"
				}] */

		       	for(i=0;i<data.length; i++){
		       		var response = data[i];
		       		for(j=0;j<response.report.length; j++){
		       			var report = response.report[j];
		       			//console.log(report.desc);
					  	Sms.filter({requestId:response.requestId}).update({response:JSON.stringify(response),status:report.desc}).execute();
					}
		       	}
		       	return res.status(200).send({message: "acknowledge delivery report"});
	       	}else{
	       		res.status(500).send({error: "invalid request"});
	       	}
		}catch(err) {
			//do nothing here .. just throw 500
			console.log(err);
		}

    }
    return res.status(500).send({error: "invalid request"});
};
