var thinky = require(__dirname+'/../util/thinky.js'),
    r = thinky.r,
    Branch = require(__dirname+'/../model/branch.js');

// list branches
// TODO: all filter, page size and offset, columns, sort
exports.listBranches = function (req, res) {
    var count;
    var pno=1,offset=0,limit=10;

    var tokenObject = JSON.parse(req.decoded);
    var orgId = tokenObject.orgId;


    if(req.query.psize != undefined && req.query.psize != null && !isNaN(req.query.psize)){
        limit = parseInt(req.query.psize);
    }

    if(req.query.pno != undefined && req.query.pno != null && !isNaN(req.query.pno)){
       pno =  parseInt(req.query.pno);
    }

    offset = (pno -1) * limit;

    var sort =req.query.sort;
    var pluck = req.query.pluck;
    
    var token = req.body.token || req.query.token || req.headers['x-access-token'];
    var tokenObject = JSON.parse(req.decoded);
    if(sort == undefined || sort == null ){
        Branch.filter({orgId: tokenObject.orgId}).count().execute().then(function(total) {
                count = total;
                console.log(total);
            });    
        Branch.orderBy(r.desc('createdOn')).filter({orgId: tokenObject.orgId}).skip(offset).limit(limit).run().then(function(branches) {
            res.json({
                data: branches,
                total: count,
                pno: pno,
                psize: limit
            });
        }).error(handleError(res));
        handleError(res);
    }else{
        var result =sort.substring(0, 1);
        sortLength =sort.length;
        if(result ==='-'){
            field=sort.substring(1,sortLength);
            console.log("field--"+field);
            console.log(typeof field);
            console.log("has field--"+Branch.hasFields(field));
            if(Branch.hasFields(field)){
                Branch.filter({portfolioId: tokenObject.portfolioId}).count().execute().then(function(total) {
                    count = total;
                    console.log(total);
                });    
                Branch.orderBy(r.desc(field)).filter({portfolioId: tokenObject.portfolioId}).skip(offset).limit(limit).run().then(function(branches) {
                res.json({
                    data: branches,
                    total: count,
                    pno: pno,
                    psize: limit
                });
                }).error(handleError(res));
                handleError(res);   
            }else{
                res.status(500).send({ message: 'No field exist.'});
            }    
        }else{
            field=sort.substring(0,sortLength);
            console.log("field--"+field);
            console.log("has field--"+Branch.hasFields(field));
            if(Branch.hasFields(field)){
                Branch.filter({portfolioId: tokenObject.portfolioId}).count().execute().then(function(total) {
                    count = total;
                    console.log(total);
                });    
                Branch.orderBy(r.asc(field)).filter({portfolioId: tokenObject.portfolioId}).skip(offset).limit(limit).run().then(function(branches) {
                res.json({
                    data: branches,
                    total: count,
                    pno: pno,
                    psize: limit
                });
                }).error(handleError(res));
                handleError(res);  
            }else{
                res.status(500).send({ message: 'No field exist.'});
            }       
        }
    }    
};
exports.getBranch = function (req, res) {
    var id = req.params.id;
    Branch.get(id).getJoin({org: true}).run().then(function(branch) {
     res.json({
         branch: branch
     });
    }).error(handleError(res));
};


// delete by id
exports.deleteBranch = function (req, res) {
    var id = req.params.id;
    Branch.get(id).delete().run().then(function(org) {
        res.json({
            status: "success"
        });
    }).error(handleError(res));
};

// Add user
exports.addBranch = function (req, res) {
    var newBranch = new Branch(req.body);
    var user = JSON.parse(req.decoded);
    newBranch.createdBy =user.userId;
    newBranch.portfolioId=user.portfolioId;
    newBranch.orgId = user.orgId;
    newBranch.updatedBy =user.userId;
    newBranch.updatedOn =r.now();
    console.log(newBranch);
    newBranch.save().then(function(result) {
        res.json({
            result: result
        });
    }).error(handleError(res));
};

// update user
exports.updateBranch = function (req, res) {
    Branch.get(req.body.id).update(req.body).then(function(result) {
        var user = JSON.parse(req.decoded);
        result.updatedBy =user.userId;
        result.updatedOn =r.now();
        res.json({
            result: result
        });
    }).error(handleError(res));
};

function handleError(res) {
    return function(error) {
        console.log(error.message);
        return res.send(500, {error: error.message});
    }
}