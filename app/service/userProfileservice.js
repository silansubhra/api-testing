var thinky = require(__dirname+'/../util/thinky.js'),
    r = thinky.r,
    env         = require(__dirname+'/../../env'),
    config      = require(__dirname+'/../../config/' + env.name),
    Item = require(__dirname+'/../model/item.js'),
    UserProfile = require(__dirname+'/../model/userProfile.js');
    var WalletTransaction = require(__dirname+'/../model/walletTransaction.js');


// update user
exports.updateUserProfile = function (req, res) {
    var user = JSON.parse(req.decoded);
    if(req.body.shippingAddress){
        console.log('got shippingAddress');
        if(user.shippingAddress){
            console.log('user has shippingAddress');
            var add = req.body.shippingAddress;
            var data = user.shippingAddress;    
            data.push(add);
            var shopAddress = data;

        }else{
            console.log('user dont have shippingAddress');
            var sData =req.body.shippingAddress;
            var shopAddress = [sData];
        }
            UserProfile.get(user.id).update({shippingAddress:shopAddress}).then(function(result) {
                res.json({
                    result: result
                });
            }).error(handleError(res));
    }else{
        console.log('did not got shippingAddress');
        var data = req.body;
        data.updatedBy =user.userId;
        data.updatedOn =r.now();
        UserProfile.get(req.params.id).update(data).then(function(result) {
            res.json({
                result: result
            });
        }).error(handleError(res));
    }
};

exports.getUserProfile = function (req, res) {
    var user = JSON.parse(req.decoded);
    UserProfile.get(user.id).run().then(function(profile) {
     res.json({
         profile: profile
     });
    }).error(handleError(res));
};

exports.listWalletTransaction = function (req, res) {
    var tokenObject = JSON.parse(req.decoded);
    var orgId = tokenObject.orgId;

    var count;
    var pno=1,offset=0,limit=10;
    if(req.query.psize != undefined && req.query.psize != null && !isNaN(req.query.psize)){
        limit = parseInt(req.query.psize);
    }

    if(req.query.pno != undefined && req.query.pno != null && !isNaN(req.query.pno)){
       pno =  parseInt(req.query.pno);
    }

    offset = (pno -1) * limit;

    var filter={
        portfolioId :tokenObject.portfolioId,
        userId: tokenObject.userId
    }
    UserProfile.get(tokenObject.id).run().then(function(up){
        if(up && up !=null){
            WalletTransaction.orderBy({index: r.desc('createdOn')}).filter(filter).getJoin({user: true}).skip(offset).limit(limit).run().then(function(wallet) {
               r.table("walletTransaction").filter(filter).count().run().then(function(total) {
                    res.json({
                        wallet: up.wallet,
                        data: wallet,
                        total: (total!=undefined?total:0),
                        pno: pno,
                        psize: limit
                    });
                });    
            }).error(handleError(res)); 
            
        }
    }).error(handleError(res)); 
    handleError(res);
};

exports.listUserProfileActive = function (req, res) {
    var count;
    var pno=1,offset=0,limit=2000;
    if(req.query.psize != undefined && req.query.psize != null && !isNaN(req.query.psize)){
        limit = parseInt(req.query.psize);
    }

    if(req.query.pno != undefined && req.query.pno != null && !isNaN(req.query.pno)){
       pno =  parseInt(req.query.pno);
    }

    offset = (pno -1) * limit;

    var token = req.body.token || req.query.token || req.headers['x-access-token'];
    var tokenObject = JSON.parse(req.decoded);
    var filter ={};
    if(tokenObject.roles.indexOf('aggregator')>-1){
        filter.mpId = tokenObject.portfolioId;
        filter.role ='vendor';
    }else if(tokenObject.roles.indexOf('super-admin')>-1){
        filter.apiKey = config.onground.apiKey;
    }
    
    if(req.query.sId && req.query.sId !='' && req.query.sId !=null){
        filter.status = req.query.sId; 
    }

    var today = new Date();
    var fromDate = new Date(req.query.fromDate);
    var toDate = new Date(req.query.toDate);
    toDate.setDate(toDate.getDate()+1);

    var priorDate = new Date('2015-11-10');
    today.setDate(today.getDate()+1);
    
    frm=Date.parse(fromDate);
    to=Date.parse(toDate);
    
    console.log(frm==to);

    var tdDate=today.getDate();
    var tdMonth=today.getMonth() +1;
    var tdYear =today.getFullYear();

    var pDate = priorDate.getDate();
    var pMonth = priorDate.getMonth() +1;
    var pYear = priorDate.getFullYear();
    var fDate=fromDate.getDate();
    var fYear=fromDate.getFullYear();
    var fMonth=fromDate.getMonth() + 1;
    if(frm==to){
        var ttDate=toDate.getDate();
        var tYear=toDate.getFullYear();
        var tMonth=toDate.getMonth() + 1; 
    }else{
        var ttDate=toDate.getDate();
        var tYear=toDate.getFullYear();
        var tMonth=toDate.getMonth() + 1;  
    }
     if(fromDate !=undefined && toDate !=undefined && fromDate !=null && toDate !=null && !isNaN(fromDate) && !isNaN(toDate)){
        UserProfile.orderBy({index: r.desc('createdOn')}).filter(filter).getJoin({org: true,portfolio: true}).filter(r.row('createdOn').during(r.time(fYear, fMonth, fDate,"Z"), r.time(tYear, tMonth, ttDate,"Z"), {rightBound: "closed"})).skip(offset).limit(limit).run().then(function(userProfiles) {
           r.table("userProfile").filter(filter).filter(r.row('createdOn').during(r.time(fYear, fMonth, fDate,"Z"), r.time(tYear, tMonth, ttDate,"Z"), {rightBound: "closed"})).count().run().then(function(total) {
            res.json({
                data: userProfiles,
                total: (total!=undefined?total:0),
                pno: pno,
                psize: limit
            });
        });    
        }).error(handleError(res)); 
        handleError(res);
    }else{
        console.log(JSON.stringify(filter));
        UserProfile.orderBy({index: r.desc('createdOn')}).filter(filter).getJoin({org: true,portfolio: true}).skip(offset).limit(limit).run().then(function(userProfiles) {
           r.table("userProfile").filter(filter).count().run().then(function(total) {
            res.json({
                data: userProfiles,
                total: (total!=undefined?total:0),
                pno: pno,
                psize: limit
            });
        });    
        }).error(handleError(res)); 
        handleError(res);
    }
};

exports.trunetoItems = function (req, res) {
    var count;
    var pno=1,offset=0,limit=10;
    if(req.query.psize != undefined && req.query.psize != null && !isNaN(req.query.psize)){
        limit = parseInt(req.query.psize);
    }

    if(req.query.pno != undefined && req.query.pno != null && !isNaN(req.query.pno)){
       pno =  parseInt(req.query.pno);
    }

    offset = (pno -1) * limit;
    var filter = {
        status: 'active',
        type:'service',
        portfolioId:'0e0234a9-ade2-4b07-95be-464371450bd4'
    }
    Item.orderBy({index: r.desc('createdOn')}).filter(filter).getJoin({category :true}).skip(offset).limit(limit).run().then(function(items) {
         Item.filter(filter).count().execute().then(function(total) {
            count = total;
            console.log(total);
            res.json({
                data: items,
                total: count,
                pno: pno,
                psize: limit
            });
        });
    }).error(handleError(res));

    handleError(res);
   
};

exports.getUserProfileByVendor = function (req, res) {
    var user = JSON.parse(req.decoded);
    if(user.roles.indexOf('super-admin')>-1){
        Portfolio.filter({uName:req.params.uname}).run().then(function(portfolio){
            UserProfile.filter({apiKey:config.onground.apiKey,portfolioId:portfolio[0].id,role:'admin'}).run().then(function(profile) {
             if(profile && profile.length>0){
                res.json({
                    roles: profile[0].roles,
                    id:profile[0].id
                });
             }else{
                res.json({
                    roles:[]
                });
             }
            }).error(handleError(res));
        })
    }else{
        return res.send({error:'Do not have permission'});
    }
    
    
};

function handleError(res) {
    return function(error) {
        console.log(error.message);
        return res.send(500, {error: error.message});
    }
}