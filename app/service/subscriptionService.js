var thinky = require(__dirname+'/../util/thinky.js'),
r = thinky.r,
Subscription = require(__dirname+'/../model/subscription.js'),
User = require(__dirname+'/../model/user.js'),
Org = require(__dirname+'/../model/org.js'),
Portfolio = require(__dirname+'/../model/portfolio.js'),
env = require(__dirname+'/../../env'),
config = require(__dirname+'/../../config/'+ env.name);

exports.listSubscriptions = function (req, res) {
    var count;
    var pno=1,offset=0,limit=10;
    if(req.query.psize != undefined && req.query.psize != null && !isNaN(req.query.psize)){
        limit = parseInt(req.query.psize);
    }

    if(req.query.pno != undefined && req.query.pno != null && !isNaN(req.query.pno)){
       pno =  parseInt(req.query.pno);
    }
    offset = (pno -1) * limit;

    var sort =req.query.sort;
    var token = req.body.token || req.query.token || req.headers['x-access-token'];
    var tokenObject = JSON.parse(req.decoded);
    var filter ={
        
        portfolioId: tokenObject.portfolioId,
        status: 'active'
    }
    Subscription.orderBy(r.desc('createdOn')).filter(filter).getJoin({portfolio: true, customer:true, item: true}).skip(offset).limit(limit).run().then(function(subs) {
        Subscription.filter(filter).count().execute().then(function(count) {
            res.json({
                data: subs,
                total: (count!=undefined?count:0),
                pno: pno,
                psize: limit
            });
    });            
    }).error(handleError(res));
    handleError(res);
};


// get by id
exports.getSubscription = function (req, res) {
    var id = req.params.id;
    Subscription.get(id).getJoin({portfolio: true, customer: true, item: true}).run().then(function(main) {
     res.json(main);
    }).error(handleError(res));
};

// delete by id
exports.deleteSubscription = function (req, res) {
    var id = req.params.id;
    Subscription.get(id).delete().run().then(function(branch) {
        res.json({
            status: "success"
        });
    }).error(handleError(res));
}

// add Subscription
exports.addSubscription = function (req, res) {
    var newSubscription = new Subscription(req.body);
    var user = JSON.parse(req.decoded);
    newSubscription.createdBy =user.userId;
    newSubscription.portfolioId=user.portfolioId;
    newSubscription.updatedBy =user.userId;
    newSubscription.updatedOn =r.now();
    var x = new Date(newSubscription.startDate);
    var Service = []
    for (var i = 0; i < newSubscription.noservice; i++) {
        var serv = {
            id:i,
            startDate: x,
            status:'pending'
        }
        Service.push(serv);
        console.log(x);
        if (newSubscription.frequency == "MONTHLY") {
        x = new Date(x.getTime() + 30*24*60*60*1000);

        }else if (newSubscription.frequency == "YEARLY") {
            x = new Date(x.getTime() + 365*24*60*60*1000);
        }else if (newSubscription.frequency == "HALFYEARLY") {
            x = new Date(x.getTime() + 183*24*60*60*1000);
        }else if (newSubscription.frequency == "QUATERLY") {
            x = new Date(x.getTime() + 120*24*60*60*1000);
        }else if (newSubscription.frequency == "WEEKLY") {
            x = new Date(x.getTime() + 7*24*60*60*1000);
        }else if (newSubscription.frequency == "FORTNIGHTLY") {
            x = new Date(x.getTime() + 15*24*60*60*1000);
        }else {
            x = new Date(x.getTime() + 1*24*60*60*1000);
        }
    }
    newSubscription.Service = Service;
    newSubscription.save().then(function(result) {
        res.json({
            subscription: result
        });
    }).error(handleError(res));
};

//update paid status 
exports.updatePaidStatus = function(req, res) {
    var paid = req.body;
    console.log('In paid service');
    var user = JSON.parse(req.decoded);
    paid.updatedBy =user.userId;
    paid.updatedOn =r.now();
    Subscription.get(req.params.id).execute().then(function(result){
        var serv = [];
        result.Service.forEach(function(ser){
            if(ser.id == paid.paidId){
                ser.status = 'paid';
            }
            serv.push(ser);
        });
        Subscription.get(req.params.id).update({service:serv}).then(function(results){
            res.json({
                result:results
            })
        }).error(handleError(res));
    }).error(handleError(res));
};

// update Subscription
exports.updateSubscription = function (req, res) {
    var subs = req.body;
    var user = JSON.parse(req.decoded);
        subs.updatedBy =user.userId;
        subs.updatedOn =r.now();
    Subscription.get(req.params.id).update(subs).then(function(result) {
        res.json({
            result: result
        });
    }).error(handleError(res));
};

function handleError(res) {
    return function(error) {
        console.log(error.message);
        return res.send(500, {error: error.message});
    }
}