var thinky = require(__dirname+'/../util/thinky.js'),
    r = thinky.r,
    Lead = require(__dirname+'/../model/lead.js'),
    env  = require(__dirname+'/../../env'),
    numeral = require('numeral');
    var moment = require('moment');

exports.getDashboardData = function (req,res) {

  var tokenObject = JSON.parse(req.decoded);

    var fromDate =new Date(req.query.fromDate);

    var toDate =new Date(req.query.toDate);

    frm=Date.parse(fromDate);

    to=Date.parse(toDate);

    console.log(frm==to);

    var fDate=fromDate.getDate();

    var fYear=fromDate.getFullYear();

    var fMonth=fromDate.getMonth() + 1;

    if(frm==to){

        var ttDate=toDate.getDate();

        var tYear=toDate.getFullYear();

        var tMonth=toDate.getMonth() + 1; 

    }else{

        var ttDate=toDate.getDate();

        var tYear=toDate.getFullYear();

        var tMonth=toDate.getMonth() + 1;  

    }

    var filter = {
        portfolioId:tokenObject.portfolioId,
        status:'active'
    }
    var filter1 ={
        portfolioId:tokenObject.portfolioId,
        status:'active',
        leadStatus:'CONVERTED'
    }
    if(tokenObject.mpId && tokenObject.mpId != ''){
        filter.mpId = tokenObject.mpId;
        filter1.mpId = tokenObject.mpId;
    }

 var drange = getDates(fromDate,toDate);
  Lead.orderBy({index: r.desc('createdOn')}).filter(filter).filter(r.row('createdOn').during(r.time(fYear, fMonth, fDate,"Z"), r.time(tYear, tMonth, ttDate,"Z"), {rightBound: "closed"})).count().execute().then(function(leads) {

        Lead.orderBy({index: r.desc('createdOn')}).filter(filter).filter(r.row('createdOn').during(r.time(fYear, fMonth, fDate,"Z"), r.time(tYear, tMonth, ttDate,"Z"), {rightBound: "closed"})).run().then(function(totalLeads) {

            Lead.orderBy({index: r.desc('createdOn')}).filter(filter1).filter(r.row('createdOn').during(r.time(fYear, fMonth, fDate,"Z"), r.time(tYear, tMonth, ttDate,"Z"), {rightBound: "closed"})).count().execute().then(function(jobs) {

                Lead.orderBy({index: r.desc('createdOn')}).filter(filter1).filter(r.row('createdOn').during(r.time(fYear, fMonth, fDate,"Z"), r.time(tYear, tMonth, ttDate,"Z"), {rightBound: "closed"})).run().then(function(totalJobs) {


console.log('jobs found----'+totalJobs);

                    Lead.orderBy({index: r.desc('createdOn')}).filter(filter1).hasFields('price').filter(r.row('createdOn').during(r.time(fYear, fMonth, fDate,"Z"), r.time(tYear, tMonth, ttDate,"Z"), {rightBound: "closed"})).sum("price").execute().then(function(sales) {
                        if(jobs <1){
                        var average = sales/1;
                        }else{
                            var average = sales/jobs;
                        }
                        var stats = [];
                        var stats1 = [];
                        var dates = [];
                        for (var i = 0; i < drange.length; i++) {
                            (function(k){
                                var count = 0;
                                var count1 =0;
                                var cdate = drange[i].setHours(0,0,0,0);
                                //console.log(cdate);
                                for (var l = 0; l < totalJobs.length; l++) {
                                     var jdate = totalJobs[l].dueDate.setHours(0,0,0,0);
                                    // console.log(ldate);
                                     if(cdate == jdate){
                                        count1 ++;
                                     } 
                                }
                                for (var j = 0; j < totalLeads.length; j++) {
                                     var ldate = totalLeads[j].dueDate.setHours(0,0,0,0);
                                    // console.log(ldate);
                                     if(cdate == ldate){
                                        count ++;
                                     } 

                                }
                                
                                dates.push(moment(drange[i]).format('DD-MMM'));
                                stats.push(count);
                                stats1.push(count1);

                            })(i);
                            if(i=== drange.length-1){
                                res.json({
                                    'status': 'ok',
                                    'leads': numeral(leads).format('0,0'),
                                    'jobs':  numeral(jobs).format('0,0'),
                                    'sales':  numeral(sales).format('0,0.0'),
                                    'average': numeral(average).format('0,0.0'),
                                    'leadsData':stats,
                                    'jobsData':stats1,
                                    'dates': dates
                                });
                            }
                        }
                    }); 
                });
            });
        });
    });
};


function getDates(startDate, stopDate) {
  console.log(startDate);
  console.log(stopDate);
  var oneDay = 24*3600*1000;
  for (var d=[],ms=startDate*1,last=stopDate*1;ms<=last;ms+=oneDay){
    d.push( new Date(ms) );
  }
  console.log('---'+JSON.stringify(d));
  return d;
}
        
