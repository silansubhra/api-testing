var thinky = require(__dirname+'/../util/thinky.js'),
    r = thinky.r,
    Employee = require(__dirname+'/../model/employee.js'),
    Org = require(__dirname+'/../model/org.js'),
    Branch = require(__dirname+'/../model/branch.js'),
    User = require(__dirname+'/../model/user.js'),
    Department = require(__dirname+'/../model/department.js');  
   // EmpSalary = require(__dirname+'/../model/empSalary.js'),
    //SalaryComponent = require(__dirname+'/../model/salaryComponent.js');
// list employees
// TODO: all filter, page size and offset, columns, sort
exports.listEmployees = function (req, res) {
    var count;
    var pno=1,offset=0,limit=10;
    if(req.query.psize != undefined && req.query.psize != null && !isNaN(req.query.psize)){
        limit = parseInt(req.query.psize);
    }

    if(req.query.pno != undefined && req.query.pno != null && !isNaN(req.query.pno)){
       pno =  parseInt(req.query.pno);
    }

    offset = (pno -1) * limit;

    var departmentId =req.query.departmentId;
    var name = req.query.name;
    var mobile =parseInt(req.query.mobile);
    var designation= req.query.designation;

    var sort =req.query.sort;
    var token = req.body.token || req.query.token || req.headers['x-access-token'];
    var tokenObject = JSON.parse(req.decoded);
    var filter ={
        
        portfolioId: tokenObject.portfolioId,
        status: 'active'
    }

    if(departmentId !=null && departmentId !=undefined){
        filter.departmentId =departmentId;
    }
    if(name !=null && name !=undefined){
        filter.name =name;
    }

    if(mobile !=null && mobile !=undefined && !isNaN(mobile)){
        filter.mobile =mobile;
    }

    if(designation !=null && designation !=undefined){
        filter.designation =designation;
    }

    if(sort == undefined || sort == null ){

        Employee.orderBy(r.desc('createdOn')).filter(filter).getJoin({portfolio: true,department: true,inventorylog:true, productInventory: true,designation:true}).skip(offset).limit(limit).run().then(function(employees) {
            Employee.filter(filter).count().execute().then(function(count) {
                res.json({
                    data: employees,
                    total: (count!=undefined?count:0),
                    pno: pno,
                    psize: limit
                });
        });            
        }).error(handleError(res));
        handleError(res);

    }else{
        var result =sort.substring(0, 1);
        sortLength =sort.length;
        if(result ==='-'){
            field=sort.substring(1,sortLength);
            console.log("field--"+field);
            console.log(typeof field);
            console.log("has field--"+Employee.hasFields(field));
            if(Employee.hasFields(field)){
                Employee.filter(filter).count().execute().then(function(total) {
                    count = total;
                    console.log(total);
                });    
                Employee.orderBy(r.desc(field)).filter(filter).skip(offset).getJoin({portfolio: true,department: true,inventorylog:true, productInventory: true,designation:true}).limit(limit).run().then(function(employees) {
                res.json({
                    data: employees,
                    total: count,
                    pno: pno,
                    psize: limit
                });
                }).error(handleError(res));
                handleError(res);   
            }else{
                res.status(500).send({ message: 'No field exist.'});
            }    
        }else{
            field=sort.substring(0,sortLength);
            console.log("field--"+field);
            console.log("has field--"+Employee.hasFields(field));
            if(Employee.hasFields(field)){
                Employee.filter(filter).count().execute().then(function(total) {
                    count = total;
                    console.log(total);
                });    
                Employee.orderBy(r.asc(field)).filter(filter).getJoin({portfolio: true,department: true,inventorylog:true, productInventory: true}).skip(offset).limit(limit).run().then(function(employees) {
                res.json({
                    data: employees,
                    total: count,
                    pno: pno,
                    psize: limit
                });
                }).error(handleError(res));
                handleError(res);  
            }else{
                res.status(500).send({ message: 'No field exist.'});
            }       
        }
    }    
};
// get by id
exports.getEmployee = function (req, res) {
    var id = req.params.id;
    Employee.get(id).getJoin({designation :true,portfolio :true,department :true,inventorylog:true, productInventory: true}).run().then(function(employee) {
     res.json(employee);
    }).error(handleError(res));
};

// delete by id
exports.deleteEmployee = function (req, res) {
    var id = req.params.id;
    Employee.get(id).delete().run().then(function(branch) {
        res.json({
            status: "success"
        });
    }).error(handleError(res));
};


//bulk delete
exports.bulkDeleteEmployee = function (req, res) {
    console.log("bulk delete called")
    var ids = req.query.ids;
    console.log("ids"+ids);
    if(ids && ids != null){
        var idsArray = ids.split(",");
        for(var i=0;i<idsArray.length;i++){
            var empId =idsArray[i];
            console.log("emp"+empId);
            var token = req.body.token || req.query.token || req.headers['x-access-token'];
            var tokenObject = JSON.parse(req.decoded);
            Employee.filter({portfolioId: tokenObject.portfolioId,id: empId,status: 'active'}).run().then(function(empy){
                if(empy && empy>0){
                    Employee.get(empy.id).update({status:'inactive'}).run().then(function(branch) {
                        
                    }).error(handleError(res));
                }
            });
        }
        res.json({
            status: "success"
        });
    }else{
        res.send(404, {error: 'Select a employee'});
    }  
};

// Add user
exports.addEmployee = function (req, res) {
    var newEmployee = new Employee(req.body);
    var user = JSON.parse(req.decoded);
    newEmployee.createdBy =user.userId;
    newEmployee.portfolioId=user.portfolioId;
    newEmployee.updatedBy =user.userId;
    newEmployee.updatedOn =r.now();
    if(req.body.startDate){
    console.log("date"+req.body.startDate);
    var sDate = new Date(req.body.startDate);
    //var eDate = new Date(req.body.endDate);
    console.log("sdate"+sDate);
   // console.log("edate"+eDate);
    newEmployee.startDate=sDate.setDate(sDate.getDate()+1);
    //newEmployee.endDate=eDate.setDate(eDate.getDate()+1);
    }
    Employee.orderBy({index: r.desc('rank')}).filter({portfolioId: user.portfolioId}).run().then(function(empl){
        if (empl && empl.length>0) {
            emply=empl[0];
            newEmployee.rank=emply.rank+1;
        }else{
            newEmployee.rank=1;
        } 
    
        Org.filter({id:user.orgId}).run().then(function(og){
            var nm =og[0].name;
            newEmployee.cCode =nm.substring(0, 3).toUpperCase();
        
            newEmployee.empId=newEmployee.cCode +' '+ newEmployee.rank;
            console.log("empId" +newEmployee.empId);
            console.log("employee obj startDate"+newEmployee.startDate);
            console.log("employee obj endDate"+newEmployee.endDate);
            newEmployee.save().then(function(result) {
                res.json({
                    employee: result
                });
            }).error(handleError(res));
        });
    }); 
};

// update user
exports.updateEmployee = function (req, res) {
    var emp = new Employee(req.body);
    var user = JSON.parse(req.decoded);
    emp.updatedBy =user.userId;
    emp.updatedOn =r.now();
    Employee.get(req.params.id).update(emp).then(function(result) {
        res.json({
            result: result
        });
    }).error(handleError(res));
};

function handleError(res) {
    return function(error) {
        console.log(error.message);
        return res.send(500, {error: error.message});
    }
}
