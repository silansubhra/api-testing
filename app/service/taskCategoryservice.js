var thinky = require(__dirname+'/../util/thinky.js'),
    r = thinky.r,
    TaskCategory = require(__dirname+'/../model/taskCategory.js'),
    Org = require(__dirname+'/../model/org.js'),
    Branch = require(__dirname+'/../model/branch.js');

// list taskTaskCategories
// TODO: all filter, page size and offset, columns, sort
exports.listTaskCategories = function (req, res) {
    var count;
    var pno=1,offset=0,limit=10;
    if(req.query.psize != undefined && req.query.psize != null && !isNaN(req.query.psize)){
        limit = parseInt(req.query.psize);
    }

    if(req.query.pno != undefined && req.query.pno != null && !isNaN(req.query.pno)){
       pno =  parseInt(req.query.pno);
    }

    offset = (pno -1) * limit;

    var token = req.body.token || req.query.token || req.headers['x-access-token'];
    var tokenObject = JSON.parse(req.decoded);

        TaskCategory.filter({portfolioId: tokenObject.portfolioId}).count().execute().then(function(total) {
            count = total;
            console.log(total);
        });


        TaskCategory.orderBy({index: r.desc('createdOn')}).filter({portfolioId: tokenObject.portfolioId}).skip(offset).limit(limit).run().then(function(taskTaskCategories) {
            res.json({
                data: taskTaskCategories,
                total: count,
                pno: pno,
                psize: limit
            });
        }).error(handleError(res));
    
    handleError(res);
};

// get by id
exports.getTaskCategory = function (req, res) {
    var id = req.params.id;
    TaskCategory.get(id).getJoin({branch :true}).run().then(function(taskCategory) {
     res.json({
         taskCategory: taskCategory
     });
    }).error(handleError(res));
};


// delete by id
exports.deleteTaskCategory = function (req, res) {
    var id = req.params.id;
    TaskCategory.get(id).delete().run().then(function(service) {
        res.json({
            status: "success"
        });
    }).error(handleError(res));
};

// Add user
exports.addTaskCategory = function (req, res) {
    var newTaskCategory = new TaskCategory(req.body);
    var user = JSON.parse(req.decoded);
    newTaskCategory.createdBy =user.userId;
    newTaskCategory.portfolioId=user.portfolioId;
    newTaskCategory.updatedBy =user.userId;
    newTaskCategory.updatedOn =r.now();
    console.log(newTaskCategory);
    newTaskCategory.save().then(function(result) {
        res.json({
            result: result
        });
    }).error(handleError(res));
};

// update user
exports.updateTaskCategory = function (req, res) {
    var tsk = new TaskCategory(req.body);
    var user = JSON.parse(req.decoded);
    tsk.updatedBy =user.userId;
    tsk.updatedOn =r.now();
    TaskCategory.get(tsk.id).update(tsk).then(function(result) {
        
        res.json({
            result: result
        });
    }).error(handleError(res));
};

function handleError(res) {
    return function(error) {
        console.log(error.message);
        return res.send(500, {error: error.message});
    }
}