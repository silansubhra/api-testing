var 
    thinky = require(__dirname+'/../util/thinky.js'),
    r = thinky.r,
    env = require(__dirname+'/../../env'),
    config = require(__dirname+'/../../config/'+ env.name),
    redis = require('redis'),
    redis_cli = redis.createClient(config.redis),
    User = require(__dirname+'/../model/user.js'),
    Portfolio = require(__dirname+'/../model/portfolio.js'),
    User = require(__dirname+'/../model/user.js'),
    Testimony = require(__dirname+'/../model/testimony.js'),
    parser = require('ua-parser-js');

exports.listTestimonies = function (req, res) {
    var count;
    var pno=1,offset=0,limit=10;


    if(req.query.psize != undefined && req.query.psize != null && !isNaN(req.query.psize)){
        limit = parseInt(req.query.psize);
    }

    if(req.query.pno != undefined && req.query.pno != null && !isNaN(req.query.pno)){
       pno =  parseInt(req.query.pno);
    }

    offset = (pno -1) * limit;

    if(req.decoded){
        var tokenObject = JSON.parse(req.decoded);
        Testimony.orderBy(r.desc('createdOn')).filter({portfolioId: tokenObject.portfolioId}).getJoin({items: true}).skip(offset).limit(limit).run().then(function(testimonies) {
            Testimony.filter({portfolioId: tokenObject.portfolioId}).count().execute().then(function(total) {
                count = total;
                console.log(total);
                res.json({
                    data: testimonies,
                    total: count,
                    pno: pno,
                    psize: limit
                });
            }).error(handleError(res));
        }).error(handleError(res));
        handleError(res);
    }else{ 
        var apiKey = req.body.apikey || req.query.apikey || req.headers['x-api-key'];
        console.log("--"+apiKey);
        Portfolio.filter({apiKey: apiKey}).run().then(function(portfolio){
            if(portfolio && portfolio.length >0){
                var portfolioId = portfolio[0].id;
                Testimony.orderBy(r.desc('createdOn')).filter({portfolioId: portfolioId,status:'active'}).getJoin({items: true}).skip(offset).limit(limit).run().then(function(testimonies) {
                    Testimony.filter({portfolioId: portfolioId,status:'active'}).count().execute().then(function(total) {
                        count = total;
                        console.log(total);
                        res.json({
                            data: testimonies,
                            total: count,
                            pno: pno,
                            psize: limit
                        });
                    }).error(handleError(res));
                }).error(handleError(res));
                handleError(res);
            }else{
                res.send(404,{error: 'invalid apikey'})
            }
        }).error(handleError(res));
    }
}; 

exports.addTestimony = function (req, res) {
    var newTestimony = new Testimony(req.body);
    if(req.decoded){
        var user = JSON.parse(req.decoded); 
       if(req.body.name && req.body.name != null && req.body.mobile && req.body.mobile != null){
            newTestimony.name = req.body.name;
        }else{
            newTestimony.name = user.name; 
        }
        newTestimony.createdBy =user.userId;
        newTestimony.portfolioId=user.portfolioId;
        newTestimony.updatedBy =user.userId;
        newTestimony.updatedOn =r.now();
        newTestimony.save().then(function(result) {
            res.status(200).send({'message':'Thank you for your review'});
        }).error(handleError(res))
    }else{
       var key = req.query.apikey;
        Portfolio.filter({apiKey:key}).run().then(function(result){
            if(result && result.length >0){
                newTestimony.portfolioId=result[0].id;
                newTestimony.updatedOn =r.now();
                newTestimony.save().then(function(result) {
                    res.status(200).send({'message':'Thank you for your review'});
                }).error(handleError(res))
            }
        });
    }
};

//get by id
exports.getTestimony = function (req, res) {
    var id = req.params.id;
    if(req.decoded){
        Testimony.filter({id :id}).getJoin({items: true}).run().then(function(testimony) {
            res.json({
                data: testimony[0]
            });
        }).error(handleError(res));
    }
};

exports.updateTestimonies = function (req, res) {
    var testimony = req.body;
    var user = JSON.parse(req.decoded);
    testimony.updatedBy =user.userId;
    testimony.updatedOn =r.now();
    Testimony.get(req.params.id).update(testimony).then(function(result) {
        res.json({
            result: result,
            status: "success"
        });
    }).error(handleError(res));
};

exports.deleteTestimony = function (req, res) {
    var id = req.params.id;
    Testimony.get(id).delete().run().then(function(result) {
        res.json({
            status: "success"
        });
    }).error(handleError(res));
};

function handleError(res) {
    return function(error) {
        console.log(error.message);
        return res.send(500, {error: error.message});
    }
}
