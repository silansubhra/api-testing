var thinky = require(__dirname+'/../util/thinky.js'),
    r = thinky.r,
    ShoppingCartItem = require(__dirname+'/../model/shoppingCartItem.js'),
    Item = require(__dirname+'/../model/item.js'),
    ShoppingCart = require(__dirname+'/../model/shoppingCart.js');

// list shoppingCartItems
// TODO: all filter, page size and offset, columns, sort
exports.listShoppingCartItems = function (req, res) {
    var count;
    var pno=1,offset=0,limit=10;
    if(req.query.psize != undefined && req.query.psize != null && !isNaN(req.query.psize)){
        limit = parseInt(req.query.psize);
    }

    if(req.query.pno != undefined && req.query.pno != null && !isNaN(req.query.pno)){
       pno =  parseInt(req.query.pno);
    }

    offset = (pno -1) * limit;

    var token = req.body.token || req.query.token || req.headers['x-access-token'];
    var tokenObject = JSON.parse(req.decoded);

    ShoppingCartItem.orderBy({index: r.desc('createdOn')}).filter({shoppingCartId: tokenObject.cartId}).hasFields('itemId').getJoin({item: true}).skip(offset).limit(limit).run().then(function(shopItems) {
       r.table("shoppingCartItem").filter({shoppingCartId: tokenObject.cartId}).hasFields('itemId').count().run().then(function(total) {
            res.json({
                data: shopItems,
                total: (count!=undefined?count:0),
                pno: pno,
                psize: limit
            });
        });    
    }).error(handleError(res)); 
    handleError(res);
};

// get by id
exports.getShoppingCartItem = function (req, res) {
    var id = req.params.id;
    ShoppingCartItem.get(id).getJoin({shoppingCartId: true}).run().then(function(shoppingCartItem) {
     res.json({
         shoppingCartItem: shoppingCartItem
     });
    }).error(handleError(res));
};


// delete by id
exports.deleteShoppingCartItem = function (req, res) {
    var id = req.params.id;
    var user = JSON.parse(req.decoded);
    cartId = user.cartId;
    ShoppingCartItem.get(id).run().then(function(shoprCart) {
        var tl=shoprCart[0].total;
        ShoppingCart.get(cartId).run().then(function(cart){
            var subtl = cart[0].subTotal - tl;
            ShoppingCart.get(cartId).update({subTotal: subtl}).run().then(function(newCart){
                ShoppingCartItem.get(id).delete().run().then(function(deleteCart){
                    res.json({
                        cart: newCart,
                        status: "success"
                    });
                }).error(handleError(res));
            }).error(handleError(res));
        }).error(handleError(res));
    }).error(handleError(res));
};

// Add user
exports.addShoppingCartItem = function (req, res) {
    var newShoppingCartItem = new ShoppingCartItem(req.body);
    var user = JSON.parse(req.decoded);
    newShoppingCartItem.userId = user.userId;
    newShoppingCartItem.createdBy =user.userId;
    newShoppingCartItem.portfolioId=user.portfolioId;
    newShoppingCartItem.updatedBy =user.userId;
    newShoppingCartItem.updatedOn =r.now();
    newShoppingCartItem.shoppingCartId = user.cartId;
    var itemId =req.body.itemId;
    var subTotal =0;
    Item.filter({id: itemId}).run().then(function(item){
        var itm =item[0];
        ShoppingCartItem.filter({shoppingCartId: user.cartId,itemId: itemId}).run().then(function(shopItems){
            if(shopItems && shopItems.length >0){
                var cartItem = shopItems[0]
                var nQauntity = cartItem.quantity + req.body.quantity;
                var nPrice = itm.price * nQauntity;
                ShoppingCartItem.get(shopItems[0].id).update({quantity : nQauntity ,total: nPrice}).then(function(shoppingCartItem){
                    ShoppingCartItem.filter({shoppingCartId: user.cartId}).hasFields('itemId').run().then(function(itemCount){
                        if(itemCount && itemCount.length>0){
                            for(var i =0;i< itemCount.length;i++){
                                var totalPrice = itemCount[i].total;
                                subTotal = subTotal+totalPrice;
                            }
                            ShoppingCart.get(user.cartId).update({subTotal: subTotal}).then(function(newCart){
                                res.json({
                                    shoppingCartItem:itemCount,
                                    shoppingCart: newCart,
                                    pno: pno,
                                    psize: limit
                                });
                            }).error(handleError(res));
                        }
                    }).error(handleError(res));
                }).error(handleError(res));
            }else{
                newShoppingCartItem.quantity =req.body.quantity;
                newShoppingCartItem.total = newShoppingCartItem.quantity * itm.price;
                newShoppingCartItem.save().then(function(result) {
                    for(var i =0;i< itemCount.length;i++){
                        var totalPrice = itemCount[i].total;
                        subTotal = subTotal+totalPrice;
                    }
                    ShoppingCart.get(user.cartId).update({subTotal: subTotal}).then(function(newCart){
                        ShoppingCartItem.filter({shoppingCartId: cartId}).run().then(function(newCartItem){
                            res.json({
                                shoppingCartItem:newCartItem,
                                newCart: newCart,
                                pno: pno,
                                psize: limit
                            });
                        }).error(handleError(res));
                    }).error(handleError(res));
                }).error(handleError(res));
            }
        }).error(handleError(res));
    }).error(handleError(res));   
};

// update user
exports.updateShoppingCartItem = function (req, res) {
    var user = JSON.parse(req.decoded);
    cartId = user.userId;
    ShoppingCartItem.get(req.body.id).run().then(function(shopCart){
        var total = shopCart.total;
        itemPrice = total /shopCart.quantity;
        nTotal = itemPrice*req.body.quantity;
        ShoppingCart.get(cartId).run().then(function(cart){
            var sbTotal = cart[0].subTotal;
            var newSbTotal = (sbTotal -total)+nTotal;
            var change = req.body;
            change.updatedBy=user.userId;
            change.updatedOn =r.now();
            change.total = nTotal;
            ShoppingCart.get(cartId).update({subTotal: newSbTotal}).then(function(newCart){
                ShoppingCartItem.get(req.body.id).update(change).then(function(result) {
                    res.json({
                        result: result
                    });  
                }).error(handleError(res));     
            }).error(handleError(res));
        }).error(handleError(res));
    }).error(handleError(res));
};

function handleError(res) {
    return function(error) {
        console.log(error.message);
        return res.send(500, {error: error.message});
    }
}