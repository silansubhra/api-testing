var thinky = require(__dirname+'/../util/thinky.js'),
    r = thinky.r,
    ShoppingCart = require(__dirname+'/../model/shoppingCart.js');

// list shoppingCarts
// TODO: all filter, page size and offset, columns, sort
exports.listShoppingCarts = function (req, res) {
    var count;
    var pno=1,offset=0,limit=10;
    if(req.query.psize != undefined && req.query.psize != null && !isNaN(req.query.psize)){
        limit = parseInt(req.query.psize);
    }

    if(req.query.pno != undefined && req.query.pno != null && !isNaN(req.query.pno)){
       pno =  parseInt(req.query.pno);
    }

    offset = (pno -1) * limit;
    
    var sort =req.query.sort;
    var pluck = req.query.pluck;
    var token = req.body.token || req.query.token || req.headers['x-access-token'];
    var tokenObject = JSON.parse(req.decoded);
    ShoppingCart.orderBy({index: r.desc('createdOn')}).filter({userId: tokenObject.userId}).getJoin({item: true}).skip(offset).limit(limit).run().then(function(shopingCart) {
       r.table("shoppingCart").filter({userId: tokenObject.userId}).count().run().then(function(total) {
            res.json({
                data: shopingCart,
                total: (total!=undefined?total:0),
                pno: pno,
                psize: limit
            });
        });    
    }).error(handleError(res)); 
    handleError(res);
};

// get by id
exports.getShoppingCart = function (req, res) {
    var id = req.params.id;
    ShoppingCart.get(id).getJoin({user: true,item: true}).run().then(function(shoppingCart) {
     res.json({
         shoppingCart: shoppingCart
     });
    }).error(handleError(res));
};


// delete by id
exports.deleteShoppingCart = function (req, res) {
    var id = req.params.id;
    ShoppingCart.get(id).delete().run().then(function(branch) {
        res.json({
            status: "success"
        });
    }).error(handleError(res));
};

// Add user
exports.addShoppingCart = function (req, res) {
    var newShoppingCart = new ShoppingCart(req.body);
    var user = JSON.parse(req.decoded);
    newShoppingCart.userId = user.userId;
    newShoppingCart.createdBy =user.userId;
    newShoppingCart.portfolioId=user.portfolioId;
    newShoppingCart.updatedBy =user.userId;
    newShoppingCart.updatedOn =r.now();
    console.log(newShoppingCart);
    newShoppingCart.save().then(function(result) {
        res.json({
            result: result
        });
    }).error(handleError(res));
};

// update user
exports.updateShoppingCart = function (req, res) {
    var id =req.body.id;
    ShoppingCart.get(id).update(req.body).then(function(result) {
        var user = JSON.parse(req.decoded);
        result.updatedBy =user.userId;
        result.updatedOn =r.now();
        res.json({
            result: result
        });
    }).error(handleError(res));
};

function handleError(res) {
    return function(error) {
        console.log(error.message);
        return res.send(500, {error: error.message});
    }
}