var uuid = require('node-uuid'),
    thinky = require(__dirname+'/../util/thinky.js'),
    r = thinky.r,
    Org = require(__dirname+'/../model/org.js'),
    Media = require(__dirname+'/../model/media.js'),
    MediaService = require(__dirname+'/mediaservice.js'),
    og = require(__dirname+'/../util/og.js'),
    env = require(__dirname+'/../../env'),
    config = require(__dirname+'/../../config/'+ env.name),
    Portfolio = require(__dirname+'/../model/portfolio.js');

// list orgs
// TODO: all filter, page size and offset, columns, sort
exports.listOrgs = function (req, res) {
    //TODO: this needs to be wrapped in single util
    var count;
    var pno=1,offset=0,limit=10;
    if(req.query.psize != undefined && req.query.psize != null && !isNaN(req.query.psize)){
        limit = parseInt(req.query.psize);
    }

    if(req.query.pno != undefined && req.query.pno != null && !isNaN(req.query.pno)){
       pno =  parseInt(req.query.pno);
    }

    offset = (pno -1) * limit;
    // ----------------------------

    var tokenObject = JSON.parse(req.decoded);


    Org.filter({apiKey: tokenObject.apiKey}).count().execute().then(function(total) {
        count = total;
        console.log(count);
    });

    console.log(req.decoded);

    Org.orderBy({index: r.desc('createdOn')}).filter({apiKey: tokenObject.apiKey}).skip(offset).limit(limit).run().then(function(orgs) {
        res.json({
            data: orgs,
            total: count,
            pno: pno,
            psize: limit
        });
    }).error(handleError(res));
};

// get by id
exports.getOrg = function (req, res) {
    var id = req.params.id;
    Org.get(id).getJoin({portfolios: true}).run().then(function(org) {
        console.log(JSON.stringify(org));
        res.json(org);
    }).error(handleError(res));
};


// delete by id
exports.deleteOrg = function (req, res) {
    var id = req.params.id;
    Org.get(id).delete().run().then(function(user) {
        res.json({
            status: "success"
        });
    }).error(handleError(res));
};

// Add user
exports.addOrg = function (req, res) {
    var newOrg = new Org(req.body);
    newOrg.apiKey = uuid.v4();
    newOrg.tpKey = uuid.v4();
    var user = JSON.parse(req.decoded);
    newOrg.createdBy =user.userId;
    newOrg.updatedBy =user.userId;
    newOrg.updatedOn =r.now();
    console.log(newOrg);
    newOrg.save().then(function(result) {
        res.json({
            result: result
        });
    }).error(handleError(res));
};

// update user
exports.updateOrg = function (req, res) {
    var user = JSON.parse(req.decoded);
    var org =req.body;
    org.updatedBy =user.userId;
    org.updatedOn =r.now();
    if(req.files && req.files !=null){
        var file = req.files.fileToUpload;
        if(file && file != null){
            org.picture = file.name;
        }else{
            org.picture = null;
        }
    }

    if (!req.files){
        Org.get(user.orgId).update(org).then(function(result) {
            Portfolio.get(user.portfolioId).update({email:org.email}).then(function(port){
                res.json({
                    result: result
                });
            }).error(handleError(res));
        }).error(handleError(res));
        
    }else{
        console.log(req.files.fileToUpload);
        
        if(file){
            var obj = new Object();
            obj.name = file.name;
            obj.mime = file.mimetype;
            obj.forId = user.orgId;
            obj.userId = user.userId;
            obj.updatedBy =  user.userId;
            obj.portfolioId = user.portfolioId;
            obj.updatedOn = r.now();
            var newMedia = new Media(obj);
        }
        var sendResponse = function(error, data){
            console.log('data --- > ' + JSON.stringify(data));
            if(!error){
                org.coverImage = config.media.upload.s3.accessUrl + '/media/'+ og.getIdPath(data.mediaId) + '/' + file.name;
                org.mediaId = data.mediaId
                console.log(org.mediaId);
                Org.get(user.orgId).update(org).then(function(result) {
                    Portfolio.get(user.portfolioId).update({email:org.email}).then(function(port){
                        res.json({
                            result: result
                        });
                    }).error(handleError(res));
                }).error(handleError(res));
            }else{
                return res.send(500, {error: error.message});
            }
        }

        newMedia.save().then(function(media) {  
            MediaService.uploadMedia(file, media.id, sendResponse);
        });
    }
};

// update org by unmae
exports.updateOrgs = function (req, res) {
    var user = JSON.parse(req.decoded);
    var org =req.body;
    org.updatedBy =user.userId;
    org.updatedOn =r.now();
    Portfolio.filter({uName:req.params.uname}).run().then(function(port){
        if(port && port.length>0){
            var portfolio = port[0];
            Org.get(portfolio.orgId).update(org).then(function(result) {
                Portfolio.get(portfolio.id).update({email:org.email,address:org.address}).then(function(port){
                    res.json({
                        result: result
                    });
                }).error(handleError(res));
            }).error(handleError(res));
        }else{
            return res.send(404, {error: 'Invalid portfolio'});
        }
    });
};

function handleError(res) {
    return function(error) {
        console.log(error.message);
        return res.send(500, {error: error.message});
    }
}
