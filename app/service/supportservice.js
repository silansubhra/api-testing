var thinky = require(__dirname+'/../util/thinky.js'),
    r = thinky.r,
    Support = require(__dirname+'/../model/support.js'),
    Org = require(__dirname+'/../model/org.js'),
    Branch = require(__dirname+'/../model/branch.js'),
    User = require(__dirname+'/../model/user.js'),
    Portfolio = require(__dirname+'/../model/portfolio.js'),
    env = require(__dirname+'/../../env'),
    config = require(__dirname+'/../../config/'+ env.name);

// list supports
// TODO: all filter, page size and offset, columns, sort
exports.listSupports = function (req, res) {
    var count;
    var pno=1,offset=0,limit=10;
    if(req.query.psize != undefined && req.query.psize != null && !isNaN(req.query.psize)){
        limit = parseInt(req.query.psize);
    }

    if(req.query.pno != undefined && req.query.pno != null && !isNaN(req.query.pno)){
       pno =  parseInt(req.query.pno);
    }

    offset = (pno -1) * limit;

    var sort =req.query.sort;
    var pluck = req.query.pluck;
    var token = req.body.token || req.query.token || req.headers['x-access-token'];
    var tokenObject = JSON.parse(req.decoded);
    var portId = config.onground.portfolioId;
    if(portId == tokenObject.portfolioId){
        console.log("super admin");
        Support.filter({forPort: tokenObject.portfolioId}).count().execute().then(function(total) {
            count = total;
            console.log(total);
        });    
        Support.orderBy(r.desc('createdOn')).filter({forPort: tokenObject.portfolioId}).getJoin({portfolio: true,user:true,employee: true,customer:true, item:true}).skip(offset).limit(limit).run().then(function(supports) {
            res.json({
                data: supports,
                total: count,
                pno: pno,
                psize: limit
            });
        }).error(handleError(res));
        handleError(res);
    }else{
        Support.filter({portfolioId: tokenObject.portfolioId}).count().execute().then(function(total) {
            count = total;
            console.log(total);
        });    
        Support.orderBy(r.desc('createdOn')).filter({portfolioId: tokenObject.portfolioId}).getJoin({portfolio: true,user:true,employee: true,customer:true,item:true}).skip(offset).limit(limit).run().then(function(supports) {
            res.json({
                data: supports,
                total: count,
                pno: pno,
                psize: limit
            });
        }).error(handleError(res));
        handleError(res);
    }
};
// get by id
exports.getSupport = function (req, res) {
    var id = req.params.id;
    Support.get(id).getJoin({portfolio: true,customer:true, employee:true, item:true}).run().then(function(support) {
     res.json(support);
    }).error(handleError(res));
};


// delete by id
exports.deleteSupport = function (req, res) {
    var id = req.params.id;
    Support.get(id).delete().run().then(function(branch) {
        res.json({
            status: "success"
        });
    }).error(handleError(res));
};

//bulk delete
exports.bulkDeleteSupport = function (req, res) {
    console.log("bulk delete called")
    var ids = req.query.ids;
    console.log("ids"+ids);
    if(ids && ids != null){
        var idsArray = ids.split(",");
        for(var i=0;i<idsArray.length;i++){
            var aggId =idsArray[i];
            console.log("agg"+aggId);
            var token = req.body.token || req.query.token || req.headers['x-access-token'];
            var tokenObject = JSON.parse(req.decoded);
            Support.filter({portfolioId: tokenObject.portfolioId,id: aggId,status: 'active'}).run().then(function(aggt){
                if(aggt && aggt>0){
                    Support.get(aggt.id).update({status:'inactive'}).run().then(function(branch) {
                        res.json({
                            status: "success"
                        });
                    }).error(handleError(res));
                }else{
                    res.send(404, {error: 'Support does not exist'});
                }
            });
        }
    }else{
        res.send(404, {error: 'Select a support'});
    }  
};

// Add user
exports.addSupport = function (req, res) {
    var newSupport = new Support(req.body);
    var user = JSON.parse(req.decoded);
    newSupport.createdBy =user.userId;
    newSupport.updatedBy=user.userId;
    newSupport.updatedOn=r.now();
    newSupport.portfolioId = user.portfolioId;
    newSupport.forPort = config.onground.portfolioId;
    console.log("portId"+config.onground.portfolioId);
    Support.orderBy({index: r.desc('rank')}).filter({forPort: config.onground.portfolioId}).run().then(function(sp){
        if(sp && sp.length>0){
           var spp = sp[0];
            newSupport.rank=spp.rank+1;
        }else{
            newSupport.rank=1;
        }
        Portfolio.get(config.onground.portfolioId).run().then(function(port){
            var name = port.name;
            var cCode =name.substring(0, 3).toUpperCase(); 
            newSupport.tokenId=cCode +' '+ newSupport.rank;
            console.log("tokenId" +newSupport.tokenId);
            newSupport.save().then(function(result) {
             /*if(portfolio.enableEmail==true){
                    console.log("port email"+portfolio.email);
                    if(portfolio.email != undefined && portfolio.email != null){
                        console.log("mail to portfolio");
                        emailService.sendEmail(config.smtp.gmail, {
                            portfolioId: portfolio.id, 
                            from: '"Zinetgo" <support@zinetgo.com>', // sender address
                            to: portfolio.email, // list of receivers
                            subject: 'You have received a new lead', // Subject line
                            text: 'Dear '+portfolio.name +'! You have received a new lead .Please find below details '+'.<br><br>'+'Service Name : '+serviceName +'.<br>'+'From: '+lead.name +'.<br>'+'Mobile: ' + lead.mobile+'.<br>'+'Email: '+(lead.email != undefined ? lead.email: '') +'.<br>'+'Date: '+(lead.dueDate != undefined ? lead.dueDate: '')+'.<br>'+'Address: '+(lead.address ? lead.address : '' )+'.<br>'+'Description: '+(lead.description ? lead.description: '')+'. \n\nBest,\nTeam OnGround', // plain text
                            html: '<b>Dear '+portfolio.name +'</b>!<br><br>You have received a new lead.Please find below details '+'.<br><br>'+'Service Name : '+serviceName +'.<br>'+'From: '+lead.name +'.<br>'+'Mobile: ' + lead.mobile+'.<br>'+'Email: '+(lead.email != undefined ? lead.email: '') +'.<br>'+'Date: '+(lead.dueDate != undefined ? lead.dueDate: '')+'.<br>'+'Address: '+(lead.address ? lead.address : '' )+'.<br>'+'Description: '+(lead.description ? lead.description: '')+'.<br><br>Best,<br>Team OnGround' // html body
                        });
                    }
                    var portfolioEmailId,portfolioSenderName;

                    if(portfolio.emailProvider){
                      portfolioEmailId= portfolio.emailProvider.auth.user;
                      portfolioSenderName= portfolio.emailProvider.auth.name;
                    }
                    if(!portfolioEmailId){
                      portfolioEmailId = portfolio.email;
                    }
                    if(!portfolioSenderName){
                      portfolioSenderName = portfolio.name;
                    }

                    console.log('portfolio.emailProvider: '+ portfolio.emailProvider);

                    console.log("lead email"+lead.email);

                    if(lead.email != undefined && lead.email != null){
                        emailService.sendEmail(portfolio.emailProvider, {
                            from: '"'+portfolioSenderName +'" <'+portfolioEmailId+'>', // sender address
                            to: lead.email, // list of receivers
                            subject: 'We have received your request for '+serviceName+' Refernce number is :' + lead.leadId, // Subject line
                            text: 'Dear '+lead.name +'! We have received your request for '+serviceName+'. Your reference number is '+ lead.leadId+'.\n\nBest,\n'+portfolio.name, // plain text
                            html: '<b>Dear '+lead.name +'</b>!<br><br>We have received your request for '+serviceName+'. Your reference number is '+ lead.leadId+'.<br><br>Best,<br>'+portfolio.name
                        });
                    }
                }*/
            res.json({
                result: result
            });
            }).error(handleError(res));    
            handleError(res);
        });        
    });    
};


// update user
exports.updateSupport = function (req, res) {
    var supp = req.body;
    var user = JSON.parse(req.decoded);
        supp.updatedBy =user.userId;
        supp.updatedOn =r.now();
    Support.get(req.params.id).update(supp).then(function(result) {
        res.json({
            result: result
        });
    }).error(handleError(res));
};

function handleError(res) {
    return function(error) {
        console.log(error.message);
        return res.send(500, {error: error.message});
    }
}
