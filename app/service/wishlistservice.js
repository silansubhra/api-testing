var thinky = require(__dirname+'/../util/thinky.js'),
    r = thinky.r,
    Wishlist = require(__dirname+'/../model/wishlist.js'),
    Org = require(__dirname+'/../model/org.js'),
    Branch = require(__dirname+'/../model/branch.js');

// list wishlists
// TODO: all filter, page size and offset, columns, sort
exports.listWishlists = function (req, res) {
    var count;
    var pno=1,offset=0,limit=10;
    if(req.query.psize != undefined && req.query.psize != null && !isNaN(req.query.psize)){
        limit = parseInt(req.query.psize);
    }

    if(req.query.pno != undefined && req.query.pno != null && !isNaN(req.query.pno)){
       pno =  parseInt(req.query.pno);
    }

    offset = (pno -1) * limit;

    var token = req.body.token || req.query.token || req.headers['x-access-token'];
    var tokenObject = JSON.parse(req.decoded);

        Wishlist.filter({createdBy: tokenObject.userId}).count().execute().then(function(total) {
            count = total;
            console.log(total);
            Wishlist.orderBy({index: r.desc('createdOn')}).filter({createdBy: tokenObject.userId}).getJoin({event :true,item: true}).skip(offset).limit(limit).run().then(function(wishlists) {
                res.json({
                    data: wishlists,
                    total: count,
                    pno: pno,
                    psize: limit
                });
            }).error(handleError(res));
        }).error(handleError(res));        
    
    handleError(res);
};

// get by id
exports.getWishlist = function (req, res) {
    var id = req.params.id;
    Wishlist.get(id).getJoin({event :true,item: true}).run().then(function(wishlist) {
     res.json({
         wishlist: wishlist
     });
    }).error(handleError(res));
};


// delete by id
exports.deleteWishlist = function (req, res) {
    var id = req.params.id;
    Wishlist.get(id).delete().run().then(function(wishlist) {
        res.json({
            status: "success"
        });
    }).error(handleError(res));
};

// Add user
exports.addWishlist = function (req, res) {
    var newWishlist = new Wishlist(req.body);
    var user = JSON.parse(req.decoded);
    newWishlist.createdBy =user.userId;
    newWishlist.portfolioId=user.portfolioId;
    newWishlist.updatedBy =user.userId;
    newWishlist.updatedOn =r.now();
    newWishlist.itemId =req.query.itemId;
    newWishlist.userId =user.userId
    console.log('itemId----'+newWishlist.itemId);
    newWishlist.save().then(function(result) {
        res.json({
            result: result
        });
    }).error(handleError(res));
};

// update user
exports.updateWishlist = function (req, res) {
    var id =req.body.id;
    Wishlist.get(id).update(req.body).then(function(result) {
        var user = JSON.parse(req.decoded);
        result.updatedBy =user.userId;
        result.updatedOn =r.now();
        res.json({
            result: result
        });
    }).error(handleError(res));
};

function handleError(res) {
    return function(error) {
        console.log(error.message);
        return res.send(500, {error: error.message});
    }
}