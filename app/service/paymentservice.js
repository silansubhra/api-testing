var thinky = require(__dirname+'/../util/thinky.js'),
    r = thinky.r,
    Payment = require(__dirname+'/../model/payment.js'),
    //Job = require(__dirname+'/../model/job.js'),
    Preference = require(__dirname+'/../model/preference.js'),
    Order =require(__dirname+'/../model/order.js'),
    smsService = require(__dirname+'/smsservice.js'),
    emailService = require(__dirname+'/emailservice.js'),
    UserProfile = require(__dirname+'/../model/userProfile.js'),
    Item = require(__dirname+'/../model/item.js'),
    Portfolio = require(__dirname+'/../model/portfolio.js'),
    env = require(__dirname+'/../../env'),
    Lead   = require(__dirname+'/../model/lead.js'),
    config = require(__dirname+'/../../config/'+ env.name);


// list payments
// TODO: all filter, page size and offset, columns, sort
exports.listPayments = function (req, res) {
    var count;
    var pno=1,offset=0,limit=10;
    if(req.query.psize != undefined && req.query.psize != null && !isNaN(req.query.psize)){
        limit = parseInt(req.query.psize);
    }

    if(req.query.pno != undefined && req.query.pno != null && !isNaN(req.query.pno)){
       pno =  parseInt(req.query.pno);
    }

    offset = (pno -1) * limit;

    var token = req.body.token || req.query.token || req.headers['x-access-token'];
    var tokenObject = JSON.parse(req.decoded);

        Payment.filter({portfolioId: tokenObject.portfolioId}).count().execute().then(function(total) {
            count = total;
            console.log(total);
        });


        Payment.orderBy({index: r.desc('createdOn')}).filter({portfolioId: tokenObject.portfolioId}).skip(offset).limit(limit).run().then(function(payments) {
            res.json({
                data: payments,
                total: count,
                pno: pno,
                psize: limit
            });
        }).error(handleError(res));
    
    handleError(res);
};

// get by id
exports.getPayment = function (req, res) {
    var id = req.params.id;
    Payment.get(id).getJoin({branch: true,job: true,order: true}).run().then(function(payment) {
     res.json({
         payment: payment
     });
    }).error(handleError(res));
};


// delete by id
exports.deletePayment = function (req, res) {
    var id = req.params.id;
    Payment.get(id).delete().run().then(function(branch) {
        res.json({
            status: "success"
        });
    }).error(handleError(res));
};

// Add user
exports.addPayment = function (req, res) {
    var newPayment = new Payment(req.body);
    var user = JSON.parse(req.decoded);
    var portfolio = JSON.parse(req.decoded);
    newPayment.portfolioId=user.portfolioId;
    newPayment.createdBy =user.userId;
    newPayment.updatedBy =user.userId;
    newPayment.updatedOn =r.now();
    console.log(newPayment);
    newPayment.save().then(function(result) {
        res.json({
            result: result
        });
    }).error(handleError(res));
};

// update user
exports.updatePayment = function (req, res) {
    console.log("id---"+req.params.id);
    var record = req.body;
    record.updatedOn =r.now();
    Payment.get(req.params.id).update(record).then(function(result) {
        console.log("phone==="+req.body.buyer_phone);
        var mob= req.body.buyer_phone.substr(3, req.body.buyer_phone.length-1);
        console.log("mobile--"+mob);
        console.log("purpose--"+req.body.purpose);
        var mobile=parseInt(mob);
        if(result.walletUsed){
            UserProfile.get(result.profileId).run().then(function(up){
                var nwallet = parseInt(up.wallet) - parseInt(result.walletUsed);
                UserProfile.update({wallet:nwallet}).then(function(updated){

                });
            });
        }
        Order.filter({orderId: req.body.purpose}).run().then(function(order){
            var paymentId = req.body.payment_id;
            if(order && order.length>0){
               var order1=order[0];

                Order.get(order1.id).update({orderStatus: 'confirm','paymentId':paymentId}).then(function(updatedOrder){
                    Object.keys(order1.cart.items).forEach(function(key, index) {
                        // key: the name of the object key
                        // index: the ordinal position of the key within the object
                        var product = order1.cart.items[key];
                        Item.get(product.item.id).run().then(function(item){
                            var nStock = item.stock-product.qty;
                            Item.get(product.item.id).update({stock:nStock}).then(function(itm){
                            });
                        });
                        
                    });
                    Portfolio.get(order[0].portfolioId).run().then(function(port){
                        var portfolio =port;
                        Preference.filter({portfolioId:portfolio.id}).run().then(function(eConfig){
                            if(eConfig && eConfig.length>0){
                                var mailConf = eConfig[0];
                                var portfolioEmailId,portfolioSenderName;
                                portfolioEmailId= mailConf.emailConfig.gmail.auth.user;
                                portfolioSenderName= mailConf.emailConfig.gmail.auth.name;
                                var sender = mailConf.emailConfig.gmail;
                                if(!portfolioEmailId){
                                  portfolioEmailId = portfolio.email;
                                }
                                if(!portfolioSenderName){
                                  portfolioSenderName = portfolio.name;
                                }
                            }else{
                                var portfolioEmailId,portfolioSenderName;
                                portfolioEmailId= config.smtp.gmail.auth.user;
                                portfolioSenderName= config.smtp.senderName;
                                var sender = config.smtp.gmail;
                            }
                            var portfolioSmsId = mailConf.smsConfig.smsProvider.senderId;
                            if(portfolioSmsId == undefined || portfolioSmsId == null || portfolioSmsId === ""){
                               portfolioSmsId = config.sms.msg91.senderId;
                            }
                            var authKey = mailConf.smsConfig.smsProvider.authKey;
                            if(authKey == undefined || authKey == null || authKey === ""){
                               authKey = config.sms.msg91.authKey;
                            }

                            if(portfolio.mobile != undefined && portfolio.mobile != null){
                             smsService.sendSms(req, config.sms.msg91.authKey,config.sms.msg91.senderId, portfolio.mobile,'You have received a new order '+'\n\n orderId:'+req.body.purpose +'.\n\nBest,\n'+ portfolio.name);
                             smsService.sendSms(req, authKey, portfolioSmsId, mobile,'Your Order with ' +portfolio.name+' '+req.body.purpose+' has been successfully placed\n\nBest,\n'+ portfolio.name);
                            }

                            if(req.body.buyer != undefined && req.body.buyer != null){
                              emailService.sendEmail(sender, {
                                    from: '"'+portfolioSenderName +'" <'+portfolioEmailId+'>', // sender address
                                    to: req.body.buyer, // list of receivers
                                    subject: 'Order Confirmation - Your Order with ' +portfolio.name+' '+req.body.purpose+' has been successfully placed', // Subject line
                                    text: 'Dear '+req.body.buyer_name +',<br><br>We are pleased to confirm your order no '+req.body.purpose+'.<br><br>Thank you for shopping with '+portfolio.name , // plain text
                                    html: 'Dear '+req.body.buyer_name +',<br><br>We are pleased to confirm your order no '+req.body.purpose+'.<br><br>Thank you for shopping with '+portfolio.name , // plain text
                                });
                            }
                            if(portfolio.email != undefined && portfolio.email != null){
                                emailService.sendEmail(config.smtp.gmail, {
                                    portfolioId: portfolio.id,
                                    from: '"Zinetgo Support" <support@zinetgo.com>', // sender address
                                    to: portfolio.email, // list of receivers
                                    subject: 'You have received a new order '+req.body.purpose, // Subject line
                                    text: 'Dear '+portfolio.name +'<br><br>You have received a new order ' +'\n\n orderId:'+req.body.purpose+'\n\n For more details log on to your zinetgo account'+ '.\n\nBest,\nTeam Zinetgo', // plain text
                                    html: '<b>Dear '+portfolio.name +'</b>!<br><br>You have received a new order ' +'<br><br> orderId:'+req.body.purpose+'<br><br> For more details log on to your zinetgo account'+'.\n\nBest,\nTeam Zinetgo' // html body
                                });
                            }
                            res.json({
                                result: result
                            });
                        });
                    });
                }); 
            }else {
                Lead.filter({leadId: req.body.purpose}).run().then(function(leads){
                    if(leads && leads.length >0){
                        for (var i = 0; i <leads.length; i++) {
                            var lead=leads[i];
                            Lead.get(lead.id).update({leadStatus: 'PAID','paymentId':paymentId}).then(function(updatedLead){
                            });
                        }
                        res.json({
                            result: result
                        });
                    }else{
                        return res.send(500, {error: 'Invalid request'});
                    }
                    
                });
            }
        });
    }).error(handleError(res));
};


function handleError(res) {
    return function(error) {
        console.log(error.message);
        return res.send(500, {error: error.message});
    }
}