var thinky = require(__dirname+'/../util/thinky.js'),
    r = thinky.r,
    Visitor = require(__dirname+'/../model/visitor.js'),
    Org = require(__dirname+'/../model/org.js'),
    Branch = require(__dirname+'/../model/branch.js'),
    Task = require(__dirname+'/../model/task.js'),
    Portfolio = require(__dirname+'/../model/portfolio.js'),
    smsService  = require(__dirname+'/veriformmservice.js'),
    emailService  = require(__dirname+'/emailservice.js'),
    EmailTemplate = require('email-templates').EmailTemplate,
    path = require('path'),
    templatesDir = path.resolve(__dirname, '..', 'templates'),
    fs = require('fs'),
    Template = require(__dirname+'/../model/template.js'),
    Preference = require(__dirname+'/../model/preference.js'),
    User = require(__dirname+'/../model/user.js');

// list visitors
// TODO: all filter, page size and offset, columns, sort
exports.listVisitors = function (req, res) {
    var count;
    var pno=1,offset=0,limit=10;
    if(req.query.psize != undefined && req.query.psize != null && !isNaN(req.query.psize)){
        limit = parseInt(req.query.psize);
    }
    if(req.query.pno != undefined && req.query.pno != null && !isNaN(req.query.pno)){
       pno =  parseInt(req.query.pno);
    }
    offset = (pno -1) * limit;

    var sort =req.query.sort;
    var pluck = req.query.pluck;
    var token = req.body.token || req.query.token || req.headers['x-access-token'];
    var tokenObject = JSON.parse(req.decoded);

    var address =req.query.address;
    var name = req.query.name;
    var mobile =req.query.mobile;

    var filter ={
        portfolioId: tokenObject.portfolioId,
        status: 'active'
    }
    var input =req.query.q;
    if(input && input!=null && input!=''){
    var q1 ="(?i)"+input;
    }else{
        var q1 ='';
    }

    if(address !=null && address !=undefined){
        filter.address =address;
    }
    if(name !=null && name !=undefined){
        filter.name =name;
    }

    if(mobile !=null && mobile !=undefined){
        filter.mobile =mobile;
    }
    if(req.query.fromDate && req.query.toDate && req.query.fromDate !=null && req.query.toDate !=null && req.query.toDate !='' && req.query.fromDate !=''){
        var fromDate = new Date(new Date(req.query.fromDate).setHours(0,0,0,0));
        var toDate = new Date(new Date(req.query.toDate).setHours(0,0,0,0));

        toDate.setDate(toDate.getDate()+1);

        
        frm=Date.parse(fromDate);
        to=Date.parse(toDate);

        var fDate=fromDate.getDate();
        var fYear=fromDate.getFullYear();
        var fMonth=fromDate.getMonth() + 1;
        if(frm==to){
            var ttDate=toDate.getDate();
            var tYear=toDate.getFullYear();
            var tMonth=toDate.getMonth() + 1; 
        }else{
            var ttDate=toDate.getDate();
            var tYear=toDate.getFullYear();
            var tMonth=toDate.getMonth() + 1;  
        }
    }

    if(fromDate !=undefined && toDate !=undefined && fromDate !=null && toDate !=null && !isNaN(fromDate) && !isNaN(toDate) && toDate !='' && fromDate !=''){
        Visitor.orderBy({index: r.desc('vDate')}).filter(filter).filter(function(doc){
                return doc('name').match(q1).default(false).
        or(doc('mobile').match(q1).default(false)).
        or(doc('salesStage').match(q1).default(false));}).getJoin({portfolio: true,employee:true }).filter(r.row('vDate').during(r.time(fYear, fMonth, fDate,"Z"), r.time(tYear, tMonth, ttDate,"Z"), {leftBound: "closed",rightBound: "closed"})).skip(offset).limit(limit).run().then(function(visitors) {
           r.table("visitor").filter(filter).filter(function(doc){
                 return doc('name').match(q1).default(false).
        or(doc('mobile').match(q1).default(false)).
        or(doc('salesStage').match(q1).default(false));}).filter(r.row('vDate').during(r.time(fYear, fMonth, fDate,"Z"), r.time(tYear, tMonth, ttDate,"Z"), {leftBound: "closed",rightBound: "closed"})).count().run().then(function(total) {
            res.json({
                data: visitors,
                total: (total!=undefined?total:0),
                pno: pno,
                psize: limit
            });
        });    
        }).error(handleError(res)); 
        handleError(res);
    }else{
        Visitor.orderBy({index: r.desc('vDate')}).filter(filter).filter(function(doc){
                return doc('name').match(q1).default(false).
        or(doc('mobile').match(q1).default(false)).
        or(doc('salesStage').match(q1).default(false));}).getJoin({portfolio: true,employee:true}).skip(offset).limit(limit).run().then(function(visitors) {
           r.table("visitor").filter(filter).filter(function(doc){
                return doc('name').match(q1).default(false).
        or(doc('mobile').match(q1).default(false)).
        or(doc('salesStage').match(q1).default(false));}).count().run().then(function(total) {
            res.json({
                data: visitors,
                total: (total!=undefined?total:0),
                pno: pno,
                psize: limit
            });
        });    
        }).error(handleError(res)); 
        handleError(res);
    }
};


exports.listcsvVisitors = function (req, res) {

    var token = req.body.token || req.query.token || req.headers['x-access-token'];
    var tokenObject = JSON.parse(req.decoded);

   var filter = {
        portfolioId: tokenObject.portfolioId,
        status: 'active'
    }
    if(tokenObject.apiKey !==config.onground.apiKey){
        filter.mpId = tokenObject.mpId;
    }
    
    var input =req.query.q;
    if(input && input!=null){
    var q1 ="(?i)"+input;
    }else{
        var q1 ='';
    }

    if(req.query.fromDate && req.query.toDate && req.query.fromDate !=null && req.query.toDate !=null && req.query.toDate !='' && req.query.fromDate !=''){
        var fromDate = new Date(new Date(req.query.fromDate).setHours(0,0,0,0));
        var toDate = new Date(new Date(req.query.toDate).setHours(0,0,0,0));
        toDate.setDate(toDate.getDate()+1);
        
        frm=Date.parse(fromDate);
        to=Date.parse(toDate);

        var fDate=fromDate.getDate();
        var fYear=fromDate.getFullYear();
        var fMonth=fromDate.getMonth() + 1;
        if(frm==to){
            var ttDate=toDate.getDate();
            var tYear=toDate.getFullYear();
            var tMonth=toDate.getMonth() + 1; 
        }else{
            var ttDate=toDate.getDate();
            var tYear=toDate.getFullYear();
            var tMonth=toDate.getMonth() + 1;  
        }
    }
    if(fromDate !=undefined && toDate !=undefined && fromDate !=null && toDate !=null && !isNaN(fromDate) && !isNaN(toDate) && toDate !='' && fromDate !=''){
        Visitor.orderBy({index: r.desc('vDate')}).filter(filter).filter(function(doc){
                return doc('name').match(q1).default(false).
        or(doc('mobile').match(q1).default(false)).
        or(doc('salesStage').match(q1).default(false));}).getJoin({customer: true,item: true,employee:true}).filter(r.row('vDate').during(r.time(fYear, fMonth, fDate,"Z"), r.time(tYear, tMonth, ttDate,"Z"), {leftBound: "closed",rightBound: "closed"})).run().then(function(leads) {
           res.json({
                data: leads
            });
        }).error(handleError(res)); 
        handleError(res);
    }else{
        Visitor.orderBy({index: r.desc('vDate')}).filter(filter).filter(function(doc){
                return doc('name').match(q1).default(false).
        or(doc('mobile').match(q1).default(false)).
        or(doc('salesStage').match(q1).default(false));}).getJoin({customer: true,item: true,employee:true}).run().then(function(leads) {

            res.json({
                data: leads
            });
        }).error(handleError(res)); 
        handleError(res);
    }


    //var fields = ['createdOn','leadId', 'customer.name','customer.mobile','item.name','dueDate','employee.name','address','leadStatus'];


   /* var csv = json2csv({ data: leads, fields: fields });
    fs.writeFile('file.csv', csv, function(err) {
      if (err) throw err;
      console.log('file saved');
      res.end();
    });*/  
};
// get by id
exports.getVisitor = function (req, res) {
    var id = req.params.id;
    Visitor.get(id).getJoin({portfolioId: true,employee:true}).run().then(function(visitor) {
     res.json(visitor);
    }).error(handleError(res));
};


// delete by id
exports.deleteVisitor = function (req, res) {
    var id = req.params.id;
    Visitor.get(id).delete().run().then(function(branch) {
        res.json({
            status: "success"
        });
    }).error(handleError(res));
};

//bulk delete
exports.bulkDeleteVisitor = function (req, res) {
    var ids = req.query.ids;
    console.log("ids"+ids);
    if(ids && ids != null){
        var idsArray = ids.split(",");
        for(var i=0;i<idsArray.length;i++){
            var visitId =idsArray[i];
            console.log("visit"+visitId);
            var token = req.body.token || req.query.token || req.headers['x-access-token'];
            var tokenObject = JSON.parse(req.decoded);
            Visitor.filter({portfolioId: tokenObject.portfolioId,id: visitId,status: 'active'}).run().then(function(visitr){
                if(visitr && visitr>0){
                    Visitor.get(visitr.id).update({status:'inactive'}).run().then(function(branch) {
                        
                    }).error(handleError(res));
                }
            });
        }
        res.json({
            status: "success"
        });
    }else{
        res.send(404, {error: 'Select a visitor'});
    }  
};


// Add visitor
exports.addVisitor = function (req, res) {
    var newVisitor = new Visitor(req.body);
    var user = JSON.parse(req.decoded);
    newVisitor.createdBy =user.userId;
    newVisitor.portfolioId=user.portfolioId;
    newVisitor.updatedBy =user.userId;
    newVisitor.updatedOn =r.now();
    console.log(newVisitor);
    newVisitor.save().then(function(result) {
        if(result.fDate && result.fDate != null && result.fDate !=''){
            var newTask = new Task({
                portfolioId :result.portfolioId,
                name: 'follow up '+result.name,
                employeeId :result.employeeId,
                startDate: result.fDate,
                tStatus:'To-do',
                url:'/visitors/'+result.id
            });
            newTask.save().then(function(tasked){
                res.json({
                    result: result
                });
            }).error(handleError(res));
        }else{
            res.json({
                result: result
            });
        }
    }).error(handleError(res));
};

// update visitor
exports.updateVisitor = function (req, res) {
    var visit = new Visitor(req.body);
    var user = JSON.parse(req.decoded);
    visit.updatedBy =user.userId;
    visit.updatedOn =r.now();
    if(visit.vDate){
        visit.vDate = new Date(visit.vDate);
    }
    Visitor.get(req.params.id).update(visit).then(function(result) {
        if(visit.fDate && visit.fDate != null && visit.fDate !=''){
            var newTask = new Task({
                portfolioId :result.portfolioId,
                name: 'follow up '+result.name,
                employeeId :result.employeeId,
                startDate: visit.fDate,
                tStatus:'To-do',
                url:'/visitors/'+result.id
            });
            newTask.save().then(function(tasked){
                res.json({
                    result: result
                });
            }).error(handleError(res));
        }else{
            res.json({
                result: result
            });
        }
    }).error(handleError(res));
};

exports.sendSmsVisitor = function (req, res) {
    var visit = new Visitor(req.body);
    var user = JSON.parse(req.decoded);
    Visitor.get(req.params.id).getJoin({portfolioId: true,employee:true}).run().then(function(visitor){
        Preference.filter({portfolioId:user.portfolioId}).run().then(function(eConfig){
            Portfolio.get(user.portfolioId).run().then(function(portfolio){

                var portfolioEmailId,portfolioSenderName;
                if(eConfig && eConfig.length>0){
                    var mailConf = eConfig[0];
                   /* portfolioEmailId= mailConf.emailConfig.gmail.auth.user;
                    portfolioSenderName= mailConf.emailConfig.gmail.auth.name;
                    var sender = mailConf.emailConfig.gmail;*/
                }else{
                    var sender = config.smtp.gmail;
                }
                /*if(!portfolioEmailId){
                  portfolioEmailId = portfolio.email;
                }
                if(!portfolioSenderName){
                  portfolioSenderName = portfolio.name;
                }*/
                Template.filter({portfolioId: user.portfolioId}).run().then(function(temp){
                    if(temp && temp.length>0){
                        if(visitor.salesStage === 'visit'){
                            if(temp[0].visitor.new.text !=""){
                                if(temp[0].visitor.new.html ===''){
                                    var html =  temp[0].visitor.new.text;
                                }else{
                                    var html = temp[0].visitor.new.html
                                }
                                if(temp[0].visitor.new.subject ===''){
                                    var subject =  temp[0].visitor.new.text;
                                }else{
                                    var subject =  temp[0].visitor.new.subject;
                                }
                                fs.appendFile(path.join(templatesDir ,'visitor-new/html.hbs'), html, function (err) {
                                  if (!err){
                                  } 
                                });
                                fs.appendFile(path.join(templatesDir ,'visitor-new/text.hbs'), temp[0].visitor.new.text, function (err) {
                                  if (!err){
                                  } 
                                });
                                fs.appendFile(path.join(templatesDir ,'visitor-new/subject.hbs'), subject, function (err) {
                                  if (!err){
                                  } 
                                });
                                var template2 = new EmailTemplate(path.join(templatesDir, 'visitor-new'));
                                fs.writeFile(path.join(templatesDir ,'visitor-new/html.hbs'), '', function(){
                                });
                                fs.writeFile(path.join(templatesDir ,'visitor-new/text.hbs'), '', function(){
                                });
                                fs.writeFile(path.join(templatesDir ,'visitor-new/subject.hbs'), '', function(){
                                });
                            }else{
                                var template2 = new EmailTemplate(path.join(templatesDir, 'visitor-new-default'));
                            }
                        }else if(visitor.salesStage === 'measurement'){
                            if(temp[0].visitor.measure.text !=""){

                                if(temp[0].visitor.measure.html ===''){
                                    var html =  temp[0].visitor.measure.text;
                                }else{
                                    var html = temp[0].visitor.measure.text
                                }
                                if(temp[0].visitor.measure.subject ===''){
                                    var subject =  temp[0].visitor.measure.text;
                                }else{
                                    var subject =  temp[0].visitor.measure.text;
                                }

                                fs.appendFile(path.join(templatesDir ,'visitor-measure/html.hbs'), html, function (err) {
                                  if (!err){
                                  } 
                                });
                                fs.appendFile(path.join(templatesDir ,'visitor-measure/text.hbs'), temp[0].visitor.measure.text, function (err) {
                                  if (!err){
                                  } 
                                });
                                fs.appendFile(path.join(templatesDir ,'visitor-measure/subject.hbs'), subject, function (err) {
                                  if (!err){
                                  } 
                                });
                                var template2 = new EmailTemplate(path.join(templatesDir, 'visitor-measure'));
                                fs.writeFile(path.join(templatesDir ,'visitor-measure/html.hbs'), '', function(){
                                });
                                fs.writeFile(path.join(templatesDir ,'visitor-measure/text.hbs'), '', function(){
                                });
                                fs.writeFile(path.join(templatesDir ,'visitor-measure/subject.hbs'), '', function(){
                                });
                            }else{
                                var template2 = new EmailTemplate(path.join(templatesDir, 'visitor-measure-default'));
                            }
                            
                        }else if(visitor.salesStage === 'quotation'){
                            if(temp[0].visitor.quote.text !=""){
                                if(temp[0].visitor.quote.html ===''){
                                    var html =  temp[0].visitor.quote.text;
                                }else{
                                    var html = temp[0].visitor.quote.text
                                }
                                if(temp[0].visitor.quote.subject ===''){
                                    var subject =  temp[0].visitor.quote.text;
                                }else{
                                    var subject =  temp[0].visitor.quote.text;
                                }
                                fs.appendFile(path.join(templatesDir ,'visitor-quote/html.hbs'), html, function (err) {
                                  if (!err){
                                  } 
                                });
                                fs.appendFile(path.join(templatesDir ,'visitor-quote/text.hbs'), temp[0].visitor.quote.text, function (err) {
                                  if (!err){
                                  } 
                                });
                                fs.appendFile(path.join(templatesDir ,'visitor-quote/subject.hbs'), subject, function (err) {
                                  if (!err){
                                  } 
                                });
                                var template2 = new EmailTemplate(path.join(templatesDir, 'visitor-quote'));
                                fs.writeFile(path.join(templatesDir ,'visitor-quote/html.hbs'), '', function(){
                                });
                                fs.writeFile(path.join(templatesDir ,'visitor-quote/text.hbs'), '', function(){
                                });
                                fs.writeFile(path.join(templatesDir ,'visitor-quote/subject.hbs'), '', function(){
                                });
                            }else{
                                var template2 = new EmailTemplate(path.join(templatesDir, 'visitor-quote-default'));
                            }
                        }
                    }else{
                        if(visitor.salesStage === 'visit'){
                            var template2 = new EmailTemplate(path.join(templatesDir, 'visitor-new-default'));
                        }else if(visitor.salesStage === 'quotation'){
                             var template2 = new EmailTemplate(path.join(templatesDir, 'visitor-quote-default'));
                        }else if(visitor.salesStage === 'measurement'){
                            var template2 = new EmailTemplate(path.join(templatesDir, 'visitor-measure-default'));
                        }
                    }
                    
                    var leads = {};
                    leads.employee = visitor.employee.name;
                    leads.employee_mob = visitor.employee.mobile;
                    leads.pName = portfolio.name;
                    leads.name = visitor.name;
                    leads.mob = portfolio.mobile;
                    leads.website = portfolio.website;
                    leads.address = visitor.address;
                    leads.mobile= visitor.mobile;
                    leads.vDate= visitor.vDate;
                    console.log(JSON.stringify(leads));
                    template2.render(leads, function (err, tmp2) {
                        if (err) {
                        return console.error(err)
                        }
                        /*if(mailConf.emailConfig.enableEmail==true){
                            if(leads.email != undefined && lead.email != null){
                                emailService.sendEmail(sender, {
                                    from: '"'+portfolioSenderName +'" <'+portfolioEmailId+'>', // sender address
                                    to: lead.email, // list of receivers
                                    subject: tmp2.subject,
                                    text: tmp2.text,
                                    html: tmp2.html
                                });
                            }
                        }*/
                        if(mailConf.smsConfig.enableSms==true){
                            var portfolioSmsId = mailConf.smsConfig.smsProvider.senderId;
                            if(portfolioSmsId == undefined || portfolioSmsId == null || portfolioSmsId === ""){
                                portfolioSmsId = config.sms.msg91.senderId;
                            }

                            var authKey = mailConf.smsConfig.smsProvider.authKey;
                            if(authKey == undefined || authKey == null || authKey === ""){
                                authKey = config.sms.msg91.authKey;
                            }
                            console.log('portfolio.authKey-----------: '+ authKey);
                            console.log('portfolio portfolioSmsId-----------: '+ portfolioSmsId);
                            
                            if(visitor.mobile != undefined && visitor.mobile != null){
                                smsService.sendSms(req, authKey, portfolioSmsId, visitor.mobile,tmp2.text);
                            }
                        }
                        res.json({
                            result: {'status':'success','message':'message sent'}
                        });
                    }).error(handleError(res)); 
                }).error(handleError(res)); 
            }).error(handleError(res)); 
        }).error(handleError(res)); 
    }).error(handleError(res)); 
};

function handleError(res) {
    return function(error) {
        console.log(error.message);
        return res.send(500, {error: error.message});
    }
}
