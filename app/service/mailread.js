var MailListener = require("mail-listener2");
var thinky = require(__dirname+'/../util/thinky.js'),
    r = thinky.r,
    Portfolio = require(__dirname+'/../model/portfolio.js'),
    Customer = require(__dirname+'/../model/customer.js'),
    env         = require(__dirname+'/../../env'),
    config      = require(__dirname+'/../../config/' + env.name),
    Item  = require(__dirname+'/../model/item.js'),
    smsService  = require(__dirname+'/veriformmservice.js'),
    emailService  = require(__dirname+'/emailservice.js'),
    Lead   = require(__dirname+'/../model/lead.js');
    var h2p = require('html2plaintext')

exports.readMail = function () {
  var mailListener = new MailListener({
    username: "leads@zinetgo.com",
    password: "Z!n$tg0@2016",
    host: "imap.gmail.com",
    port: 993, // imap port 
    tls: true,
    //connTimeout: null, // Default by node-imap 
    //authTimeout: 5000, // Default by node-imap, 
    debug: console.log, // Or your custom function with only one incoming argument. Default: null 
    tlsOptions: { rejectUnauthorized: false },
    mailbox: "INBOX", // mailbox to monitor 
    searchFilter: ["UNSEEN"], // the search filter being used after an IDLE notification has been retrieved 
    markSeen: true, // all fetched email willbe marked as seen and not fetched next time 
    fetchUnreadOnStart: true, // use it only if you want to get all unread email on lib start. Default is `false`, 
    mailParserOptions: {streamAttachments: true}, // options to be passed to mailParser lib. 
    attachments: true, // download attachments as they are encountered to the project directory 
    attachmentOptions: { directory: "attachments/" } // specify a download directory for attachments 
  });
   
  mailListener.start(); // start listening 
   
  // stop listening 
  //mailListener.stop(); 
   
  mailListener.on("server:connected", function(){
    console.log("imapConnected");
  });
   
  mailListener.on("server:disconnected", function(){
    console.log("imapDisconnected");
  });
   
  mailListener.on("error", function(err){
    console.log(err);
  });
   
  mailListener.on("mail", function(mail, seqno, attributes){
    Portfolio.filter({email:mail.headers.to,status:'active'}).run().then(function(portfolio){
      if(portfolio && portfolio.length >0){
        mail.text = h2p(mail.html);
        if(mail.text.includes('yet5')){
          if(mail.text.includes('Course Interested in') && mail.text.includes('Contact No')){
            var namepos = mail.text.indexOf('Name :');
            var namen = mail.text.indexOf(" City", namepos);

            var mobilepos = mail.text.indexOf('Contact No.:');
            var mobilen = mail.text.indexOf(" Course", mobilepos);

            var emailpos = mail.text.indexOf('Email :');
            var emailn = mail.text.indexOf(" Comments:", emailpos);

            var addresspos = mail.text.indexOf('Area :');
            var addressn = mail.text.indexOf(" Contact No", addresspos);

            var itempos = mail.text.indexOf('Course Interested in:');
            var itemn = mail.text.indexOf(' Email :',itempos);

            var decpos = mail.text.indexOf('Comments:');
            var descn = mail.text.indexOf('The copy',decpos);

            var leadIdPos = mail.text.indexOf('Lead unique ID:');
            var leadn = mail.text.indexOf(" your have", leadIdPos);

            var date = new Date (mail.headers.date.substring(5,25));

            console.log(date);

            var ndate = date;
            var ddate = ndate;

            var name = mail.text.substring(namepos+7,namen);
            var mobile = parseInt(mail.text.substring(mobilepos+13,mobilen));
            var email = mail.text.substring(emailpos+8,emailn);

            var address = mail.text.substring(addresspos+7,addressn);
            var item = mail.text.substring(itempos+22,itemn);

            var description = mail.text.substring(decpos+10,descn);

            var leadId = mail.text.substring(leadIdPos+15,leadn);

            var leadSource  = 'YET5';
	          console.log(address);
            addLead(name,mobile,email,address,item,description,ddate,ndate,leadId,leadSource,portfolio[0].id);

          }else{
            var namepos = mail.text.indexOf('Name');
            var namen = mail.text.indexOf("\n\n", namepos);
            var mobilepos = mail.text.indexOf('Mobile No.:');
            var mobilen = mail.text.indexOf("\n\n", mobilepos);
            var emailpos = mail.text.indexOf('Email ID:');
            var emailn = mail.text.indexOf("\n\n", emailpos);

            var addresspos = mail.text.indexOf('City & Area:');
            var addressn = mail.text.indexOf("Course:", addresspos);

            var itempos = mail.text.indexOf('Course:');
            var itemn = mail.text.indexOf('\n',itempos);
            var decpos = mail.text.indexOf('Comments');
            var descn = mail.text.indexOf('\n\n',decpos);
            var leadIdPos = mail.text.indexOf('[ID:');
            var leadn = mail.text.indexOf("]", leadIdPos);

            var date = new Date (mail.headers.date.substring(5,25));

            console.log(date);
            var ndate = date;
            var ddate = ndate;

            var name = mail.text.substring(namepos+6,namen);
            var mobile = parseInt(mail.text.substring(mobilepos+12,mobilen));

            var email = mail.text.substring(emailpos+10,emailn);
            var address = mail.text.substring(addresspos+13,addressn);
            var item = mail.text.substring(itempos+8,itemn);
            var description = mail.text.substring(decpos+10,descn);
            var leadId = mail.text.substring(leadIdPos+5,leadn);
            var leadSource  = 'YET5';

            addLead(name,mobile,email,address,item,description,ddate,ndate,leadId,leadSource,portfolio[0].id);
          }
        }else if(mail.text.includes('Bro4u')){
          var leadIdPos = mail.text.indexOf('Booking ID:');
          var leadn = mail.text.indexOf("\n", leadIdPos);
          var namepos = mail.text.indexOf('Customer Name:');
          var namen = mail.text.indexOf("\n", namepos);
          var mobilepos = mail.text.indexOf('Mobile Number:');
          var mobilen = mail.text.indexOf("\n", mobilepos);
          var itempos = mail.text.indexOf('Service Type:: [');
          var itemn = mail.text.indexOf(' ]',itempos);
          var addresspos = mail.text.indexOf('Address:');
          var addressn = mail.text.indexOf("\n", addresspos);
          var decpos = mail.text.indexOf('Custom Message:');
          var descn = mail.text.indexOf('\n\n',decpos);

          var dpos = mail.text.indexOf('Order Placed On:');
          var dn = mail.text.indexOf('\n\n',dpos);
          var date = mail.text.substring(dpos+17,dn);

          var ddpos = mail.text.indexOf('Service Delivery Date:');
          var ddn = mail.text.indexOf('\n',ddpos);
          var dddate = mail.text.substring(ddpos+23,ddn);
          var dstring = dddate.substring(8,12)+'/'+dddate.substring(3,6)+'/'+dddate.substring(0,2)+' 9:00';

          console.log(date);
          if(date.includes('PM')){
            if(date.substring(15,16) ==':'){
              var time = parseInt(date.substring(14,15))+12;
              var second = date.substring(15,18);
              var ntime = time+second;
            }else{
               var time = parseInt(date.substring(14,16))+12; 
              var second = date.substring(16,20);
              var ntime = time+second;
            }
          } else{
            if(date.substring(15,16) ==':'){
              var ntime = date.substring(14,19);
            }else{
              var ntime = date.substring(14,20);
            }
          }            

          var dd = date.substring(8,12)+'/'+date.substring(3,6)+'/'+date.substring(0,2)+' '+ntime;

          var ndate = new Date(dd);
          var ddate = new Date(dstring);
          var mobile = parseInt(mail.text.substring(mobilepos+15,mobilen));
          var name = mail.text.substring(namepos+16,namen);
          var item = mail.text.substring(itempos+16,itemn);
          var leadId = mail.text.substring(leadIdPos+12,leadn);
          var address = mail.text.substring(addresspos+9,addressn);
          var description = mail.text.substring(decpos+16,descn);

          var leadSource  = 'BRO4U';
          

          addLead(name,mobile,email,address,item,description,ddate,ndate,leadId,leadSource,portfolio[0].id);
        }else if(mail.text.includes('justdial')){
          Lead.filter({leadSource:'JUSTDAIL',portfolioId:portfolio[0].id}).count().execute().then(function(leadcount){
            if(mail.text.includes('Caller Name:')){
              var namepos = mail.text.indexOf('Caller Name:');
              var namen = mail.text.indexOf(" from", namepos);

              var mobilepos = mail.text.indexOf('Caller Phone:');
              var mobilen = mail.text.indexOf("\n", mobilepos);

              if(mail.text.indexOf('Caller Email:') > -1){
                var emailpos = mail.text.indexOf('Caller Email:');
                var emailn = mail.text.indexOf("\nSend", emailpos);
              }
              var addressn = mail.text.indexOf("\nCaller Requirement:");
              var itempos = mail.text.indexOf('Caller Requirement:');
              var itemn = mail.text.indexOf('\nCall Date & Time:',itempos);


              var dpos = mail.text.indexOf('Call Date & Time: ');
              var dn = mail.text.indexOf('\nBranch Info',dpos);
              var date = mail.text.substring(dpos+23,dn);
              var ntime = date.substring(12,dn);
              var dd = date.substring(7,11)+'/'+date.substring(3,6)+'/'+date.substring(0,2)+' '+ntime;
              var ddate = new Date(dd);
              var name = mail.text.substring(namepos+13,namen);

              if(emailpos) {
                var email = mail.text.substring(emailpos+14,emailn);
              }else{
                var email = '';
              }

              var mobile = parseInt(mail.text.substring(mobilepos+17,mobilen));
              var address = mail.text.substring(namen+6,addressn);
              var item = mail.text.substring(itempos+20,itemn);
              var description = '';
              var ndate = ddate;
            }else{
              var namepos = mail.text.indexOf('User Name:');
              var namen = mail.text.indexOf(" from", namepos);

              var emailpos = mail.text.indexOf('User Email:');
              var emailn = mail.text.indexOf("\nSend", emailpos);

              var addressn = mail.text.indexOf("\nUser Requirement");

              var itempos = mail.text.indexOf('User Requirement:');
              var itemn = mail.text.indexOf('\nSearch Date',itempos);


              var dpos = mail.text.indexOf('Search Date & Time: ');
              var dn = mail.text.indexOf('\nBranch Info',dpos);

              var date = mail.text.substring(dpos+20,dn);
              
              var ntime = date.substring(17,dn);
                       
              var dd = date.substring(12,16)+'/'+date.substring(5,7)+'/'+date.substring(8,11)+' '+ntime;
              var ddate = new Date(dd);

              var name = mail.text.substring(namepos+11,namen);
              var email = mail.text.substring(emailpos+12,emailn);
              var ndate = ddate;
              var address = mail.text.substring(namen+6,addressn);

              var mobile = parseInt('0000000000'+(leadcount+1));

              var item = mail.text.substring(itempos+18,itemn);
              var description = '';
              var mobile ='';
            }
              var leadSource  = 'JUSTDAIL';
              var leadId = leadSource+(leadcount+1);
              addLead(name,mobile,email,address,item,description,ddate,ndate,leadId,leadSource,portfolio[0].id);
          })
        }else if(mail.text.includes('HomeTriangle')){

          var namepos = mail.text.indexOf('Customer Name:');
          var namen = mail.text.indexOf("\n*", namepos);

          var mobilepos = mail.text.indexOf('Customer Mobile:*');
          var mobilen = mail.text.indexOf("\n*", mobilepos);

          var addresspos = mail.text.indexOf('Customer Address:*');
          var addressn = mail.text.indexOf("\n\nSend", addresspos);

          var itempos = mail.text.indexOf('Service/Package:*');
          var itemn = mail.text.indexOf('\n*',itempos);

          var leadIdPos = mail.text.indexOf('New Request #');
          var leadn = mail.text.indexOf("\nHi", leadIdPos);

          var date = new Date (mail.headers.date.substring(5,25));

          console.log(date);

          var ndate = date;
          var ddate = ndate;

          var name = mail.text.substring(namepos+17,namen);
          var mobile = parseInt(mail.text.substring(mobilepos+18,mobilen));
          var email = '';

          var address = mail.text.substring(addresspos+20,addressn);

          var item = mail.text.substring(itempos+19,itemn);
          var description = '';
          var leadId = mail.text.substring(leadIdPos+13,leadn);
          var leadSource  = 'HOMETRIANGLE';
          addLead(name,mobile,email,address,item,description,ddate,ndate,leadId,leadSource,portfolio[0].id);
        }else{
          return;
        }
      }else{
        return;
      }
    });
  });
   
  mailListener.on("attachment", function(attachment){
    console.log(attachment.path);
  });
}

var checkCustomer = function(portfolioId,mobile,name,email,address,callback){
  Customer.filter({portfolioId:portfolioId,mobile:mobile}).then(function(customer){
      if(customer && customer.length>0){
        callback(null,customer[0].id);
      }else{
          var newCustomer = new Customer({
            name:name,
            mobile: mobile,
            address: address,
            email: email,
            status:'active',
            portfolioId:portfolioId
          });
          newCustomer.save().then(function(customer){
            callback(null,customer.id);
          })
      }
  })

}

var addLead = function(name,mobile,email,address,item,description,ddate,ndate,leadId,leadSource,portfolioId){

      Lead.filter({leadId:leadId,portfolioId:portfolioId}).run().then(function(leadf){
        if(leadf && leadf.length >0){
          return;
        }else{
          var newLead = new Lead({
            portfolioId : portfolioId,
            status: 'active',
            leadStatus:'NEW',
            leadSource : leadSource
          });
          var callback = function(error,data){
              newLead.customerId = data;
              newLead.address = address;
              newLead.leadId = leadId;
              newLead.description = description;
              newLead.createdOn = ndate;
              newLead.dueDate = ddate;
              var mecallback = function(error,data1){
                newLead.itemId = data1;
                newLead.save().then(function(result) {
                 });
              }
              checkService(portfolioId,item,mecallback);
          }
          checkCustomer(portfolioId,mobile,name,email,address,callback);
        }
      })
}

var checkService = function(portfolioId,item,callback){
  Item.filter({portfolioId:portfolioId,name:item}).then(function(service){
      if(service && service.length>0){
        callback(null,service[0].id);
      }else{
          var newItem = new Item({
            name:item,
            status:'active',
            portfolioId:portfolioId
          });
          newItem.save().then(function(service){
            callback(null,service.id);
          })
      }
  })

}
