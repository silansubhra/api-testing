var thinky = require(__dirname+'/../util/thinky.js'),
    r = thinky.r,
    Lead = require(__dirname+'/../model/lead.js'),
    Customer = require(__dirname+'/../model/customer.js'),
    env         = require(__dirname+'/../../env'),
    numeral = require('numeral'),
    Org = require(__dirname+'/../model/org.js'),
    UserProfile = require(__dirname+'/../model/userProfile.js');

exports.getDashboardData = function (req,res) {
    var count;
    var pno=1,offset=0,limit=10;
    if(req.query.psize != undefined && req.query.psize != null && !isNaN(req.query.psize)){
        limit = parseInt(req.query.psize);
    }

    if(req.query.pno != undefined && req.query.pno != null && !isNaN(req.query.pno)){
       pno =  parseInt(req.query.pno);
    }

    offset = (pno -1) * limit;

  var tokenObject = JSON.parse(req.decoded);
  var orgId = tokenObject.orgId;

  var today =new Date();
    var fromDate =new Date(req.query.fromDate);
    var toDate =new Date(req.query.toDate);
    toDate.setDate(toDate.getDate()+1);
    var priorDate = new Date(new Date().setDate(today.getDate() - 30));
    today.setDate(today.getDate()+1);
    
    frm=Date.parse(fromDate);
    to=Date.parse(toDate);
    
    console.log(frm==to);

    var tdDate=today.getDate();
    var tdMonth=today.getMonth() +1;
    var tdYear =today.getFullYear();

    var pDate = priorDate.getDate();
    var pMonth = priorDate.getMonth() +1;
    var pYear = priorDate.getFullYear();
    var fDate=fromDate.getDate();
    var fYear=fromDate.getFullYear();
    var fMonth=fromDate.getMonth() + 1;
    if(frm==to){
        var ttDate=toDate.getDate();
        console.log("ttDate"+ttDate);
        var tYear=toDate.getFullYear();
        var tMonth=toDate.getMonth() + 1; 
    }else{
        var ttDate=toDate.getDate();
        var tYear=toDate.getFullYear();
        var tMonth=toDate.getMonth() + 1;  
    }
    UserProfile.filter({id:tokenObject.id}).run().then(function(profile){
        if(profile && profile.length>0){
            var role = profile[0].roles;
            if(role == 'user'){
                var filter ={
                    portfolioId: tokenObject.portfolioId,
                    status :'active',
                    customerId: tokenObject.userId
                }

                if(fromDate !=undefined && toDate !=undefined && fromDate !=null && toDate !=null && !isNaN(fromDate) && !isNaN(toDate)){
                    Lead.orderBy({index: r.desc('createdOn')}).filter(filter).getJoin({portfolio: true,customer: true,item: true}).filter(r.row('createdOn').during(r.time(fYear, fMonth, fDate,"Z"), r.time(tYear, tMonth, ttDate,"Z"), {rightBound: "closed"})).getJoin({slot: true}).skip(offset).limit(limit).run().then(function(leads) {
                       r.table("lead").filter(filter).filter(r.row('createdOn').during(r.time(fYear, fMonth, fDate,"Z"), r.time(tYear, tMonth, ttDate,"Z"), {rightBound: "closed"})).count().run().then(function(total) {
                        res.json({
                            data: leads,
                            total: (total!=undefined?total:0),
                            pno: pno,
                            psize: limit
                        });
                    });    
                    }).error(handleError(res)); 
                    handleError(res);
                }else{
                    Lead.orderBy({index: r.desc('createdOn')}).filter(filter).getJoin({portfolio: true,customer: true,item: true}).filter(r.row('createdOn').during(r.time(pYear, pMonth, pDate,"Z"), r.time(tdYear, tdMonth, tdDate,"Z"))).getJoin({slot: true}).skip(offset).limit(limit).run().then(function(leads) {
                       r.table("lead").filter(filter).filter(r.row('createdOn').during(r.time(pYear, pMonth, pDate,"Z"), r.time(tdYear, tdMonth, tdDate,"Z"))).count().run().then(function(total) {
                        res.json({
                            data: leads,
                            total: (total!=undefined?total:0),
                            pno: pno,
                            psize: limit
                        });
                    });    
                    }).error(handleError(res)); 
                    handleError(res);
                }           
            }
        }
    });
};

function handleError(res) {
    return function(error) {
        console.log(error.message);
        return res.send(500, {error: error.message});
    }
}
