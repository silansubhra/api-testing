var thinky = require(__dirname+'/../util/thinky.js'),
    r = thinky.r,
    HomeCarosal = require(__dirname+'/../model/homeCarosal.js'),
    MediaService = require(__dirname+'/mediaservice.js'),
    og = require(__dirname+'/../util/og.js'),
    env = require(__dirname+'/../../env'),
    Media = require(__dirname+'/../model/media.js'),
    config = require(__dirname+'/../../config/'+ env.name),
    UserProfile = require(__dirname+'/../model/userProfile.js'),
    Org = require(__dirname+'/../model/org.js');

// list homeCarosals
// TODO: all filter, page size and offset, columns, sort
exports.listHomeCarosals = function (req, res) {
    var count;
    var pno=1,offset=0,limit=10;
    if(req.query.psize != undefined && req.query.psize != null && !isNaN(req.query.psize)){
        limit = parseInt(req.query.psize);
    }

    if(req.query.pno != undefined && req.query.pno != null && !isNaN(req.query.pno)){
       pno =  parseInt(req.query.pno);
    }

    offset = (pno -1) * limit;

    var sort =req.query.sort;
    var pluck = req.query.pluck;
    var filter={
        status:'active'
    }

    if(req.decoded && req.decoded !=null){
        var token = req.body.token || req.query.token || req.headers['x-access-token'];
        var tokenObject = JSON.parse(req.decoded);
        if(tokenObject.centerId && tokenObject.centerId !=null){
            filter.centerId = tokenObject.centerId;
        }else{
            filter.portfolioId = tokenObject.portfolioId;
        }
        HomeCarosal.orderBy(r.asc('position')).filter(filter).skip(offset).limit(limit).run().then(function(homeCarosals) {
            HomeCarosal.filter(filter).count().execute().then(function(total) {
                res.json({
                    data: homeCarosals,
                    total: total,
                    pno: pno,
                    psize: limit
                });
            });
        }).error(handleError(res));
        handleError(res);
    }else{
        var apiKey = req.apikey;
        UserProfile.filter({apiKey :apiKey}).run().then(function(up){
            if(up && up!=null){
                if(up[0].centerId){
                    filter.centerId = up[0].centerId;
                }else{
                    filter.portfolioId = up[0].portfolioId;
                }
                HomeCarosal.orderBy(r.asc('position')).filter(filter).skip(offset).limit(limit).run().then(function(homeCarosals) {
                    HomeCarosal.filter(filter).count().execute().then(function(total) {
                        res.json({
                            data: homeCarosals,
                            total: total,
                            pno: pno,
                            psize: limit
                        });
                    });
                }).error(handleError(res));
                handleError(res);
            }else{
                res.send(401, {error: 'Unauthorized'});
            }
        }).error(handleError(res));
    }
};
// get by id
exports.getHomeCarosal = function (req, res) {
    var id = req.params.id;
    HomeCarosal.get(id).getJoin({org: true}).run().then(function(homeCarosal) {
     res.json({
         homeCarosal: homeCarosal
     });
    }).error(handleError(res));
};


// delete by id
exports.deleteHomeCarosal = function (req, res) {
    var id = req.params.id;
    HomeCarosal.get(id).delete().run().then(function(org) {
        res.json({
            status: "success"
        });
    }).error(handleError(res));
};

//bulk delete
exports.bulkDeleteHomeCarosal = function (req, res) {
    var ids = req.query.ids;
    console.log("ids"+ids);
    if(ids && ids != null){
        var idsArray = ids.split(",");
        for(var i=0;i<idsArray.length;i++){
            var deptId =idsArray[i];
            console.log("dept"+deptId);
            var token = req.body.token || req.query.token || req.headers['x-access-token'];
            var tokenObject = JSON.parse(req.decoded);
            HomeCarosal.filter({portfolioId: tokenObject.portfolioId,id: deptId,status: 'active'}).run().then(function(deptt){
                if(deptt && deptt>0){
                    HomeCarosal.get(deptt.id).update({status:'inactive'}).run().then(function(branch) {

                    }).error(handleError(res));
                }
            });
        }
        res.json({
            status: "success"
        });
    }else{
        res.send(404, {error: 'Select a homeCarosal'});
    }
};

// Add user
exports.addHomeCarosal = function (req, res) {
    var newHomeCarosal = new HomeCarosal(req.body);
    var user = JSON.parse(req.decoded);
    newHomeCarosal.portfolioId=user.portfolioId;
    newHomeCarosal.createdBy =user.userId;
    newHomeCarosal.updatedBy =user.userId;
    newHomeCarosal.centerId = user.userId;
    console.log(newHomeCarosal.position);
    console.log("centerID--->"+user.centerId);
    if (user.centerId !=null || user.centerId !=undefined || !isNaN(user.centerId)) {
        newHomeCarosal.centerId = user.centerId;
        console.log("centerId");
    };
   // newHomeCarosal.updatedOn =r.now();

    if(req.files && req.files !=null){
        var file = req.files.fileToUpload;
        if(file && file != null){
            newHomeCarosal.picture = file.name;
        }else{
            newHomeCarosal.picture = null;
        }
    }
    console.log(newHomeCarosal);
    newHomeCarosal.saveAll().then(function(result) {
        console.log('saved');

        if (!req.files){
            res.json({
                result: result
            });
        }else{

            console.log(req.files.fileToUpload);


            if(file){
                var obj = new Object();
                obj.name = file.name;
                obj.mime = file.mimetype;
                obj.forId = result.id;
                obj.userId = user.userId;
                obj.updatedBy =  user.userId;
                obj.portfolioId = user.portfolioId;
                obj.updatedOn = r.now();
                var newMedia = new Media(obj);
            }

            var sendResponse = function(error, data){
                console.log('data --- > ' + JSON.stringify(data));
                if(!error){


                    var coverImage = config.media.upload.s3.accessUrl + '/media/'+ og.getIdPath(data.mediaId) + '/' + file.name;

                    HomeCarosal.get(result.id).update({mediaId: data.mediaId, coverImage: coverImage}).then(function(r1) {

                        res.json({
                            result: r1
                        });
                    }).error(handleError(res));
                    // res.json({
                    //     result: data
                    // });
                }else{
                    return res.send(500, {error: error.message});
                }
            }

            newMedia.save().then(function(media) {
                MediaService.uploadMedia(file, media.id, sendResponse);
            });
        }

    }).error(handleError(res));
};

// update user
exports.updateHomeCarosal = function (req, res) {
    var dep = new HomeCarosal(req.body);
    var user = JSON.parse(req.decoded);
    dep.updatedBy =user.userId;
    dep.updatedOn =r.now();
    HomeCarosal.get(dep.id).update(dep).then(function(result) {

        res.json({
            result: result
        });
    }).error(handleError(res));
};

function handleError(res) {
    return function(error) {
        console.log("error====");
        return res.send(500, {error: error.message});
    }
}
