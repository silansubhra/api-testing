var	env = require(__dirname+'/../../env'),
	config = require(__dirname+'/../../config/'+ env.name),
    nodemailer = require('nodemailer'),
    fs = require('fs'),
    Email = require(__dirname+'/../model/email.js'),
    EmailTracker = require(__dirname+'/../model/emailTracker.js'),
    transporter = nodemailer.createTransport(config.smtp.gmail);
   
 
exports.sendEmail = function(smtp, mailOptions){
	var mail = new Email(mailOptions);
	mail.status='NEW';
	//save the email
	mail.save().then(function(savedMail) {
		//add a tracking pixel to email

		var trkCode = '<img alt="" src="'+ getTrackingCode(savedMail.id,'open') +'" width="1" height="1" border="0" />';
		mailOptions.html = trkCode + mailOptions.html;
		//console.log(mailOptions.html);

		if(smtp){
			transporter = nodemailer.createTransport(smtp);
		}
	    transporter.sendMail(mailOptions, function(error, info){
	        if(error){
	        	Email.get(savedMail.id).update({status:'FAILED'}).execute();
	            return console.log(error);
	        }
	        Email.get(savedMail.id).update({status:'SENT'}).execute();
	        console.log('Message sent: ' + info.response);
	    });
	}).error(function(error){
		console.log('email save error');
	});
};

var getTrackingCode = function(id,action){
	return config.url.url+ '/trk/em/' +id +'/open';
	//var trkCode = '<img alt="" src="'+ config.environment.url+ '/trk/em?id=' +id +'?action=open" width="1" height="1" border="0" />';
};

exports.trackEmail = function(req,res){
	var id,action;
	id = req.params.id;
	action = req.params.action.toUpperCase();
	console.log('id action : '+ id + ' '+ action);
	Email.get(id).update({status:action}).execute();
	var trk = new EmailTracker({
		emailId: id,
		action: action
	});
	trk.save().then(function(result){
		console.log('email tracked '+id);
	}).error(function(err){
		console.log(err.Message);
	});

	var img = fs.readFileSync(__dirname+'/../../1x1.png');
	res.writeHead(200, {'Content-Type': 'image/png' });
	res.end(img, 'binary');
};