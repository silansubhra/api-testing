var thinky = require(__dirname+'/../util/thinky.js'),
    r = thinky.r,
    Portfolio = require(__dirname+'/../model/portfolio.js'),
    Branch = require(__dirname+'/../model/branch.js'),
    env         = require(__dirname+'/../../env'),
    config      = require(__dirname+'/../../config/' + env.name),
    Item  = require(__dirname+'/../model/item.js'),
    smsService  = require(__dirname+'/veriformmservice.js'),
    emailService  = require(__dirname+'/emailservice.js'),
    Lead   = require(__dirname+'/../model/lead.js'),
    MediaService = require(__dirname+'/mediaservice.js'),
    Media = require(__dirname+'/../model/media.js'),
    og = require(__dirname+'/../util/og.js'),
    Customer = require(__dirname+'/../model/customer.js'),
    emailConfig = require(__dirname+'/../model/mailConfig.js'),
    path = require('path'),
    UserProfile = require(__dirname+'/../model/userProfile.js');
    ProductInventory = require(__dirname+'/../model/productInventory.js'),
    InventoryLog = require(__dirname+'/../model/inventorylog.js'),
    LeadInventory = require(__dirname+'/../model/leadInventory.js'),
    templatesDir = path.resolve(__dirname, '..', 'templates'),
    csv=require('csvtojson'),
    fs = require('fs'),
    sync = require('synchronize'),
    Template = require(__dirname+'/../model/template.js'),
    Preference = require(__dirname+'/../model/preference.js'),
    Insta = require('instamojo-nodejs'),
    Bitly = require('bitly'),
    moment = require('moment'),
    Payment = require(__dirname+'/../model/payment.js'),
    WalletTransaction = require(__dirname+'/../model/walletTransaction.js');

    var Handlebars = require('handlebars');    

// list leads
exports.listLeads = function (req, res) {
    var count;
    var pno=1,offset=0,limit=10;
    if(req.query.psize != undefined && req.query.psize != null && !isNaN(req.query.psize)){
        limit = parseInt(req.query.psize);
    }
    if(req.query.pno != undefined && req.query.pno != null && !isNaN(req.query.pno)){
       pno =  parseInt(req.query.pno);
    }
    offset = (pno -1) * limit;

    var sort =req.query.sort;
    var pluck = req.query.pluck;
    var token = req.body.token || req.query.token || req.headers['x-access-token'];
    var tokenObject = JSON.parse(req.decoded);

    var address =req.query.address;
    var name = req.query.name;
    var mobile =req.query.mobile;

   var filter = {
        portfolioId: tokenObject.portfolioId,
        status: 'active'
    }
    if(tokenObject.apiKey !==config.onground.apiKey){
        filter.mpId = tokenObject.mpId;
    }
    if(tokenObject.roles.indexOf('user')>-1){
        filter.customerId = tokenObject.userId;
    }else if(tokenObject.roles.indexOf('vendor')>-1){
        filter.mpId = tokenObject.mpId;
    }
    
    var input =req.query.q;
    if(input && input!=null&& input!=''){
    var q1 ="(?i)"+input;
    }else{
        var q1 ='';
    }

    if(address !=null && address !=undefined){
        filter.address =address;
    }
    if(name !=null && name !=undefined){
        filter.name =name;
    }

    if(mobile !=null && mobile !=undefined){
        filter.mobile =mobile;
    }
    if(req.query.fromDate && req.query.toDate && req.query.fromDate !=null && req.query.toDate !=null && req.query.toDate !='' && req.query.fromDate !=''){
        var fromDate = new Date(new Date(req.query.fromDate).setHours(0,0,0,0));
        var toDate = new Date(new Date(req.query.toDate).setHours(0,0,0,0));
        toDate.setDate(toDate.getDate()+1);

        
        frm=Date.parse(fromDate);
        to=Date.parse(toDate);

        var fDate=fromDate.getDate();
        var fYear=fromDate.getFullYear();
        var fMonth=fromDate.getMonth() + 1;
        if(frm==to){
            var ttDate=toDate.getDate();
            var tYear=toDate.getFullYear();
            var tMonth=toDate.getMonth() + 1; 
        }else{
            var ttDate=toDate.getDate();
            var tYear=toDate.getFullYear();
            var tMonth=toDate.getMonth() + 1;  
        }
    }

    if(fromDate !=undefined && toDate !=undefined && fromDate !=null && toDate !=null && !isNaN(fromDate) && !isNaN(toDate) && toDate !='' && fromDate !=''){
        Lead.orderBy({index: r.desc('createdOn')}).filter(filter).filter(function(doc){
                return doc('name').match(q1).default(false).
        or(doc('leadId').match(q1).default(false)).or(doc('leadStatus').match(q1).default(false));}).getJoin({portfolio: true,customer: true,inventorylog:true,aggregator: true,item: true,employee:true , leadInventory: true,proLead:true}).filter(r.row('createdOn').during(r.time(fYear, fMonth, fDate,"Z"), r.time(tYear, tMonth, ttDate,"Z"), {leftBound: "closed",rightBound: "closed"})).skip(offset).limit(limit).run().then(function(leads) {
           r.table("lead").filter(filter).filter(function(doc){
                 return doc('name').match(q1).default(false).
        or(doc('leadId').match(q1).default(false)).or(doc('leadStatus').match(q1).default(false));}).filter(r.row('createdOn').during(r.time(fYear, fMonth, fDate,"Z"), r.time(tYear, tMonth, ttDate,"Z"), {leftBound: "closed",rightBound: "closed"})).count().run().then(function(total) {
            res.json({
                data: leads,
                total: (total!=undefined?total:0),
                pno: pno,
                psize: limit
            });
        });    
        }).error(handleError(res)); 
        handleError(res);
    }else{
        Lead.orderBy({index: r.desc('createdOn')}).filter(filter).filter(function(doc){
                return doc('name').match(q1).default(false).
        or(doc('leadId').match(q1).default(false)).or(doc('leadStatus').match(q1).default(false));}).getJoin({portfolio: true,customer: true,inventorylog:true,aggregator: true,item: true,employee:true , leadInventory: true,proLead:true}).skip(offset).limit(limit).run().then(function(leads) {
           r.table("lead").filter(filter).filter(function(doc){
                return doc('name').match(q1).default(false).
        or(doc('leadId').match(q1).default(false)).or(doc('leadStatus').match(q1).default(false));}).count().run().then(function(total) {
            res.json({
                data: leads,
                total: (total!=undefined?total:0),
                pno: pno,
                psize: limit
            });
        });    
        }).error(handleError(res)); 
        handleError(res);
    }
};

exports.listLeadsByVendor = function (req, res) {
    var count;
    var pno=1,offset=0,limit=10;
    if(req.query.psize != undefined && req.query.psize != null && !isNaN(req.query.psize)){
        limit = parseInt(req.query.psize);
    }

    if(req.query.pno != undefined && req.query.pno != null && !isNaN(req.query.pno)){
       pno =  parseInt(req.query.pno);
    }

    if(req.query.type != undefined && req.query.type != null && !isNaN(req.query.type)){
       var type =  req.query.pno;
    }

    var token = req.body.token || req.query.token || req.headers['x-access-token'];
    var tokenObject = JSON.parse(req.decoded);
    var today = new Date();
    
    var fromDate = new Date(new Date(req.query.fromDate).setHours(0,0,0,0));
    var toDate = new Date(new Date(req.query.toDate).setHours(0,0,0,0));

    toDate.setDate(toDate.getDate()+1);
    var priorDate = new Date(new Date().setDate(today.getDate() - 365));
    priorDate = new Date(priorDate.setHours(0,0,0,0));


    today.setDate(today.getDate()+1);

    var customerId = req.query.customerId;
    var leadId = req.query.leadId;
    var itemId = req.query.itemId;
    var aggregatorId = req.query.aggregatorId;
    
    frm=Date.parse(fromDate);
    to=Date.parse(toDate);

    var tdDate=today.getDate();
    var tdMonth=today.getMonth() +1;
    var tdYear =today.getFullYear();

    var pDate = priorDate.getDate();
    var pMonth = priorDate.getMonth() +1;
    var pYear = priorDate.getFullYear();
    var fDate=fromDate.getDate();
    var fYear=fromDate.getFullYear();
    var fMonth=fromDate.getMonth() + 1;
    if(frm==to){
        var ttDate=toDate.getDate();
        var tYear=toDate.getFullYear();
        var tMonth=toDate.getMonth() + 1; 
    }else{
        var ttDate=toDate.getDate();
        var tYear=toDate.getFullYear();
        var tMonth=toDate.getMonth() + 1;  
    }

    var filter = {
        status: 'active'
    }
    if(customerId !=null && customerId !=undefined){
        filter.customerId =customerId;
    }

    if(aggregatorId !=null && aggregatorId !=undefined){
        filter.aggregatorId =aggregatorId;
    }

    if(itemId !=null && itemId !=undefined){
        filter.itemId =itemId;
    }
    if(type && type !=null && type !=''){
        filter.type =type;
    }
    if(tokenObject.roles.indexOf('vendor')>-1){
        filter.mpId = tokenObject.mpId,
       filter.portfolioId = tokenObject.portfolioId;
        console.log('-------- portfolioId'+JSON.stringify(filter));
        if(fromDate !=undefined && toDate !=undefined && fromDate !=null && toDate !=null && !isNaN(fromDate) && !isNaN(toDate)){
            Lead.orderBy({index: r.desc('createdOn')}).filter(filter).filter(function (user) {
             return (user('leadStatus').ne('CONVERTED'))}).getJoin({portfolio: true,customer: true,inventorylog:true,aggregator: true,item: true,employee:true ,leadInventory: true,proLead:true}).filter(r.row('createdOn').during(r.time(fYear, fMonth, fDate,"Z"), r.time(tYear, tMonth, ttDate,"Z"), {rightBound: "closed"})).getJoin({slot: true}).skip(offset).limit(limit).run().then(function(leads) {
               r.table("lead").filter(filter).filter(function (user) {
                    return (user('leadStatus').ne('CONVERTED'))}).filter(r.row('createdOn').during(r.time(fYear, fMonth, fDate,"Z"), r.time(tYear, tMonth, ttDate,"Z"), {rightBound: "closed"})).count().run().then(function(total) {
                    Lead.orderBy({index: r.desc('createdOn')}).filter(filter).filter(function (user) {
                        return (user('leadStatus').eq('CONVERTED'))}).getJoin({portfolio: true,customer: true,inventorylog:true,aggregator: true,item: true,employee:true ,leadInventory: true,proLead:true}).filter(r.row('createdOn').during(r.time(fYear, fMonth, fDate,"Z"), r.time(tYear, tMonth, ttDate,"Z"), {rightBound: "closed"})).getJoin({slot: true}).skip(offset).limit(limit).run().then(function(jobs) {
                        r.table("lead").filter(filter).filter(function (user) {
                            return (user('leadStatus').eq('CONVERTED'))}).filter(r.row('createdOn').during(r.time(fYear, fMonth, fDate,"Z"), r.time(tYear, tMonth, ttDate,"Z"), {rightBound: "closed"})).count().run().then(function(total1) {
                            res.json({
                                data: leads,
                                data1:jobs,
                                total: (total!=undefined?total:0),
                                total1: (total1!=undefined?total1:0),
                                pno: pno,
                                psize: limit
                            });
                        });    
                    }).error(handleError(res)); 
                });    
            }).error(handleError(res));
            handleError(res);
        }else{
            Lead.orderBy({index: r.desc('createdOn')}).filter(filter).filter(function (user) {
             return (user('leadStatus').ne('CONVERTED'))}).getJoin({portfolio: true,customer: true,inventorylog:true,aggregator: true,item: true,employee:true , leadInventory: true,proLead:true}).filter(r.row('createdOn').during(r.time(pYear, pMonth, pDate,"Z"), r.time(tdYear, tdMonth, tdDate,"Z"))).getJoin({slot: true}).skip(offset).limit(limit).run().then(function(leads) {
               r.table("lead").filter(filter).filter(function (user) {
                    return (user('leadStatus').ne('CONVERTED'))}).filter(r.row('createdOn').during(r.time(pYear, pMonth, pDate,"Z"), r.time(tdYear, tdMonth, tdDate,"Z"))).count().run().then(function(total) {
                    Lead.orderBy({index: r.desc('createdOn')}).filter(filter).filter(function (user) {
                     return (user('leadStatus').eq('CONVERTED'))}).getJoin({portfolio: true,customer: true,inventorylog:true,aggregator: true,item: true,employee:true , leadInventory: true,proLead:true}).filter(r.row('createdOn').during(r.time(pYear, pMonth, pDate,"Z"), r.time(tdYear, tdMonth, tdDate,"Z"))).getJoin({slot: true}).skip(offset).limit(limit).run().then(function(jobs) {
                        r.table("lead").filter(filter).filter(function (user) {
                            return (user('leadStatus').eq('CONVERTED'))}).filter(r.row('createdOn').during(r.time(pYear, pMonth, pDate,"Z"), r.time(tdYear, tdMonth, tdDate,"Z"))).count().run().then(function(total1) {
                        
                            res.json({
                                data: leads,
                                data1:jobs,
                                total: (total!=undefined?total:0),
                                total1: (total1!=undefined?total1:0),
                                pno: pno,
                                psize: limit
                            });
                        });    
                    }).error(handleError(res)); 
                });    
            }).error(handleError(res)); 
            handleError(res);
        }   
    }else if(tokenObject.roles.indexOf('aggregator')>-1){
        filter.mpId = tokenObject.portfolioId,
        Portfolio.filter({uName: req.query.sId}).run().then(function(port){
            if(port && port.length>0){
                var portfolio = port[0];
                filter.portfolioId = portfolio.id;
                console.log('aggregator----'+JSON.stringify(filter));
                if(fromDate !=undefined && toDate !=undefined && fromDate !=null && toDate !=null && !isNaN(fromDate) && !isNaN(toDate)){
                    Lead.orderBy({index: r.desc('createdOn')}).filter(filter).filter(function (user) {
                     return (user('leadStatus').ne('CONVERTED'))}).getJoin({portfolio: true,customer: true,inventorylog:true,aggregator: true,item: true,employee:true ,leadInventory: true,proLead:true}).filter(r.row('createdOn').during(r.time(fYear, fMonth, fDate,"Z"), r.time(tYear, tMonth, ttDate,"Z"), {rightBound: "closed"})).getJoin({slot: true}).skip(offset).limit(limit).run().then(function(leads) {
                       r.table("lead").filter(filter).filter(function (user) {
                            return (user('leadStatus').ne('CONVERTED'))}).filter(r.row('createdOn').during(r.time(fYear, fMonth, fDate,"Z"), r.time(tYear, tMonth, ttDate,"Z"), {rightBound: "closed"})).count().run().then(function(total) {
                            Lead.orderBy({index: r.desc('createdOn')}).filter(filter).filter(function (user) {
                                return (user('leadStatus').eq('CONVERTED'))}).getJoin({portfolio: true,customer: true,inventorylog:true,aggregator: true,item: true,employee:true ,leadInventory: true,proLead:true}).filter(r.row('createdOn').during(r.time(fYear, fMonth, fDate,"Z"), r.time(tYear, tMonth, ttDate,"Z"), {rightBound: "closed"})).getJoin({slot: true}).skip(offset).limit(limit).run().then(function(jobs) {
                                r.table("lead").filter(filter).filter(function (user) {
                                    return (user('leadStatus').eq('CONVERTED'))}).filter(r.row('createdOn').during(r.time(fYear, fMonth, fDate,"Z"), r.time(tYear, tMonth, ttDate,"Z"), {rightBound: "closed"})).count().run().then(function(total1) {
                                    res.json({
                                        data: leads,
                                        data1:jobs,
                                        total: (total!=undefined?total:0),
                                        total1: (total1!=undefined?total1:0),
                                        pno: pno,
                                        psize: limit
                                    });
                                });    
                            }).error(handleError(res)); 
                        });    
                    }).error(handleError(res));
                    handleError(res);
                }else{
                    Lead.orderBy({index: r.desc('createdOn')}).filter(filter).filter(function (user) {
                     return (user('leadStatus').ne('CONVERTED'))}).getJoin({portfolio: true,customer: true,inventorylog:true,aggregator: true,item: true,employee:true , leadInventory: true,proLead:true}).filter(r.row('createdOn').during(r.time(pYear, pMonth, pDate,"Z"), r.time(tdYear, tdMonth, tdDate,"Z"))).getJoin({slot: true}).skip(offset).limit(limit).run().then(function(leads) {
                       r.table("lead").filter(filter).filter(function (user) {
                            return (user('leadStatus').ne('CONVERTED'))}).filter(r.row('createdOn').during(r.time(pYear, pMonth, pDate,"Z"), r.time(tdYear, tdMonth, tdDate,"Z"))).count().run().then(function(total) {
                            Lead.orderBy({index: r.desc('createdOn')}).filter(filter).filter(function (user) {
                             return (user('leadStatus').eq('CONVERTED'))}).getJoin({portfolio: true,customer: true,inventorylog:true,aggregator: true,item: true,employee:true , leadInventory: true,proLead:true}).filter(r.row('createdOn').during(r.time(pYear, pMonth, pDate,"Z"), r.time(tdYear, tdMonth, tdDate,"Z"))).getJoin({slot: true}).skip(offset).limit(limit).run().then(function(jobs) {
                                r.table("lead").filter(filter).filter(function (user) {
                                    return (user('leadStatus').eq('CONVERTED'))}).filter(r.row('createdOn').during(r.time(pYear, pMonth, pDate,"Z"), r.time(tdYear, tdMonth, tdDate,"Z"))).count().run().then(function(total1) {
                                
                                    res.json({
                                        data: leads,
                                        data1:jobs,
                                        total: (total!=undefined?total:0),
                                        total1: (total1!=undefined?total1:0),
                                        pno: pno,
                                        psize: limit
                                    });
                                });    
                            }).error(handleError(res)); 
                        });    
                    }).error(handleError(res)); 
                    handleError(res);
                }  
            }
        }).error(handleError(res)); 
    }else if(tokenObject.roles.indexOf('super-admin')>-1){
        Portfolio.filter({uName: req.query.sId}).run().then(function(port){
            if(port && port.length>0){
                var portfolio = port[0];
                filter.portfolioId = portfolio.id;
                console.log('aggregator----'+JSON.stringify(filter));
                if(fromDate !=undefined && toDate !=undefined && fromDate !=null && toDate !=null && !isNaN(fromDate) && !isNaN(toDate)){
                    Lead.orderBy({index: r.desc('createdOn')}).filter(filter).filter(function (user) {
                     return (user('leadStatus').ne('CONVERTED'))}).getJoin({portfolio: true,customer: true,inventorylog:true,aggregator: true,item: true,employee:true ,leadInventory: true,proLead:true}).filter(r.row('createdOn').during(r.time(fYear, fMonth, fDate,"Z"), r.time(tYear, tMonth, ttDate,"Z"), {rightBound: "closed"})).getJoin({slot: true}).skip(offset).limit(limit).run().then(function(leads) {
                       r.table("lead").filter(filter).filter(function (user) {
                            return (user('leadStatus').ne('CONVERTED'))}).filter(r.row('createdOn').during(r.time(fYear, fMonth, fDate,"Z"), r.time(tYear, tMonth, ttDate,"Z"), {rightBound: "closed"})).count().run().then(function(total) {
                            Lead.orderBy({index: r.desc('createdOn')}).filter(filter).filter(function (user) {
                                return (user('leadStatus').eq('CONVERTED'))}).getJoin({portfolio: true,customer: true,inventorylog:true,aggregator: true,item: true,employee:true ,leadInventory: true,proLead:true}).filter(r.row('createdOn').during(r.time(fYear, fMonth, fDate,"Z"), r.time(tYear, tMonth, ttDate,"Z"), {rightBound: "closed"})).getJoin({slot: true}).skip(offset).limit(limit).run().then(function(jobs) {
                                r.table("lead").filter(filter).filter(function (user) {
                                    return (user('leadStatus').eq('CONVERTED'))}).filter(r.row('createdOn').during(r.time(fYear, fMonth, fDate,"Z"), r.time(tYear, tMonth, ttDate,"Z"), {rightBound: "closed"})).count().run().then(function(total1) {
                                    res.json({
                                        data: leads,
                                        data1:jobs,
                                        total: (total!=undefined?total:0),
                                        total1: (total1!=undefined?total1:0),
                                        pno: pno,
                                        psize: limit
                                    });
                                });    
                            }).error(handleError(res)); 
                        });    
                    }).error(handleError(res));
                    handleError(res);
                }else{
                    Lead.orderBy({index: r.desc('createdOn')}).filter(filter).filter(function (user) {
                     return (user('leadStatus').ne('CONVERTED'))}).getJoin({portfolio: true,customer: true,inventorylog:true,aggregator: true,item: true,employee:true , leadInventory: true,proLead:true}).filter(r.row('createdOn').during(r.time(pYear, pMonth, pDate,"Z"), r.time(tdYear, tdMonth, tdDate,"Z"))).getJoin({slot: true}).skip(offset).limit(limit).run().then(function(leads) {
                       r.table("lead").filter(filter).filter(function (user) {
                            return (user('leadStatus').ne('CONVERTED'))}).filter(r.row('createdOn').during(r.time(pYear, pMonth, pDate,"Z"), r.time(tdYear, tdMonth, tdDate,"Z"))).count().run().then(function(total) {
                            Lead.orderBy({index: r.desc('createdOn')}).filter(filter).filter(function (user) {
                             return (user('leadStatus').eq('CONVERTED'))}).getJoin({portfolio: true,customer: true,inventorylog:true,aggregator: true,item: true,employee:true , leadInventory: true,proLead:true}).filter(r.row('createdOn').during(r.time(pYear, pMonth, pDate,"Z"), r.time(tdYear, tdMonth, tdDate,"Z"))).getJoin({slot: true}).skip(offset).limit(limit).run().then(function(jobs) {
                                r.table("lead").filter(filter).filter(function (user) {
                                    return (user('leadStatus').eq('CONVERTED'))}).filter(r.row('createdOn').during(r.time(pYear, pMonth, pDate,"Z"), r.time(tdYear, tdMonth, tdDate,"Z"))).count().run().then(function(total1) {
                                
                                    res.json({
                                        data: leads,
                                        data1:jobs,
                                        total: (total!=undefined?total:0),
                                        total1: (total1!=undefined?total1:0),
                                        pno: pno,
                                        psize: limit
                                    });
                                });    
                            }).error(handleError(res)); 
                        });    
                    }).error(handleError(res)); 
                    handleError(res);
                }  
            }
        }).error(handleError(res)); 
    }else{
        return res.send(401,{error:'do not have permission'})
    }         
};

// get by id
exports.getLead = function (req, res) {
    var id = req.params.id;
    Lead.get(id).getJoin({portfolio: true,customer: true,item: true ,employee: true,inventorylog:true, leadInventory: true,leadAssignment: true,proLead: true}).run().then(function(lead) {
     res.json(lead);
    }).error(handleError(res));
};

//converted leads
exports.convertedLeads = function(req, res){
    var key = req.query.apikey;
    Portfolio.filter({apiKey:key}).run().then(function(port){
        if(port && port.length >0){
            var portfolio = port[0];
            var portId = portfolio.id;
            Lead.orderBy({index: r.desc('createdOn')}).filter({portfolioId:portId,leadStatus:'CONVERTED'}).count().execute().then(function(jobs) {
                res.json({
                    'leads': jobs
                });  
            }).error(handleError(res));
        }else{
            res.status(500).send({error:'Profile not found'});
        }
    }).error(handleError(res));
};


//converted leads
exports.sendConfirmation = function(req, res){
    var user = JSON.parse(req.decoded);
    Lead.get(req.params.id).getJoin({portfolio: true,customer: true,item: true ,employee: true}).run().then(function(lead){
        Preference.filter({portfolioId:user.portfolioId}).run().then(function(eConfig){
            var portfolioEmailId,portfolioSenderName;
            if(eConfig && eConfig.length>0){
                var mailConf = eConfig[0];
                Template.filter({portfolioId: user.portfolioId}).run().then(function(temp){
                    if(temp && temp.length>0 && temp[0].lead.confirm !=""){
                        if(temp[0].lead.confirm.html ===''){
                            var html =  'Your lead with reference no {{ leadId }} has been confirmed by {{ pName }} on {{ dueDate }}';
                        }else{
                            var html = temp[0].lead.confirm.html;
                        }
                        if(temp[0].lead.confirm.subject ===''){
                            var subject =  'Your lead with reference no {{ leadId }} has been confirmed by {{ pName }}';
                        }else{
                            var subject =  temp[0].lead.confirm.subject;
                        }

                        if(temp[0].lead.confirm.text ===''){
                            var text =  'Your lead with reference no {{ leadId }} has been confirmed by {{ pName }} on {{ dueDate }}';
                        }else{
                            var text =  temp[0].lead.confirm.text;
                        }

                    }else{
                        var html =  'Your lead with reference no {{ leadId }} has been confirmed by {{ pName }} on {{ dueDate }}';
                        var subject =  'Your lead with reference no {{ leadId }} has been confirmed by {{ pName }}';
                        var text =  'Your lead with reference no {{ leadId }} has been confirmed by {{ pName }} on {{ dueDate }}';
                    }
                    
                    var html1 = Handlebars.compile(html);
                    var subject1 = Handlebars.compile(subject);
                    var text1 = Handlebars.compile(text);

                    var leadData = {};

                    leadData.dueDate = moment(lead.dueDate).format("DD-MM-YYYY,h:mm a");

                    leadData.pName = lead.portfolio.name;
                    leadData.sName = lead.item.name;
                    leadData.mob = lead.portfolio.mobile;
                    leadData.website = lead.portfolio.website || '';
                    leadData.address = lead.address;
                    leadData.mobile = lead.customer.mobile;
                    leadData.name = lead.customer.name;
                    leadData.price = lead.price || 0;
                    leadData.ename = lead.employee.name || '';
                    leadData.eno = lead.employee.mobile || '';
                    leadData.leadId = lead.leadId;

                    var resulthtml = html1(leadData);
                    var resulttext = text1(leadData);
                    var resultsubject = subject1(leadData);

                        if(mailConf.lead && mailConf.lead.confirm.enableSms === true){
                            if(!mailConf.smsConfig && mailConf.smsConfig == {}){
                                return res.status(500).send({error:'Please provide your sms provider details'});
                            }
                        }
                        if(mailConf.lead && mailConf.lead.confirm.enableEmail === true){
                            if(!mailConf.emailConfig && mailConf.emailConfig == {}){
                                return res.status(500).send({error:'Please provide your email provider details'});
                            }
                        }
                        var portfolioSmsId = mailConf.smsConfig.smsProvider.senderId;
                        

                        portfolioEmailId= mailConf.emailConfig.gmail.auth.user;
                        portfolioSenderName= mailConf.emailConfig.gmail.auth.name || user.name;
                        var sender = mailConf.emailConfig.gmail;
                        var authKey = mailConf.smsConfig.smsProvider.authKey;

                        if(mailConf.lead && mailConf.lead.confirm.enableEmail === true){
                            if(lead.customer.email != undefined && lead.customer.email != null){
                                emailService.sendEmail(sender, {
                                    from: '"'+portfolioSenderName +'" <'+portfolioEmailId+'>', // sender address
                                    to: lead.customer.email, // list of receivers
                                    subject: resultsubject,
                                    text: resulttext,
                                    html: resulthtml
                                });
                            }else{
                                return res.status(500).send({error:'please update customer email email id'});
                            }
                        }

                        if(mailConf.lead && mailConf.lead.confirm.enableSms === true){

                            smsService.sendSms(req, authKey, portfolioSmsId, lead.customer.mobile,resulttext); 
                        }
                        res.status(200).send({message:'SMS sent successfully'});
                            
                });

            }else{
               return res.status(500).send({error:'Please provide your sms providee details'});
            }
        });

    })
    
    
};

// delete by id
exports.deleteLead = function (req, res) {
    var id = req.params.id;
    Lead.get(id).delete().run().then(function(branch) {
        res.json({
            status: "success"
        });
    }).error(handleError(res));
};

exports.bulkDeleteLead = function (req, res) {
    var ids = req.query.ids;
    if(ids && ids != null){
        var idsArray = ids.split(",");
        for(var i=0;i<idsArray.length;i++){
            var ldId =idsArray[i];
            var token = req.body.token || req.query.token || req.headers['x-access-token'];
            var tokenObject = JSON.parse(req.decoded);
            Lead.filter({portfolioId: tokenObject.portfolioId,id: ldId,status: 'active'}).run().then(function(ldr){
                if(ldr && ldr>0){
                    Lead.get(ldr.id).update({status:'inactive'}).run().then(function(branch) {
                        
                    }).error(handleError(res));
                }
            });
        }
        res.json({
            status: "success"
        });
    }else{
        res.send(404, {error: 'Select a lead'});
    }  
};


// Add user
exports.addLead = function (req, res) {
    var newLead = new Lead(req.body);
    var user = JSON.parse(req.decoded);
    var portfolio = JSON.parse(req.decoded);
    newLead.createdBy =user.userId;
    newLead.portfolioId=user.portfolioId;
    newLead.updatedBy =user.userId;
    newLead.updatedOn =r.now();
    var dueDate = new Date(req.body.dueDate);
    if(req.body.createdOn  && req.body.createdOn  !=null){
        newLead.createdOn = new Date(req.body.createdOn)
    }
    newLead.dueDate=dueDate;
    Lead.orderBy({index: r.desc('rank')}).filter({portfolioId: user.portfolioId}).run().then(function(ld){
        if (ld && ld.length>0) {
            led=ld[0];
            newLead.rank=led.rank+1;
        }else{
            newLead.rank=1;
        }
        Portfolio.filter({id: user.portfolioId}).run().then(function(port){
            if(port && port.length>0){
                var nm =port[0].name;
                var cCode =nm.substring(0, 3).toUpperCase();
                var str = "" + newLead.rank;
                var pad = "0000";
                Preference.filter({portfolioId:user.portfolioId}).run().then(function(eConfig){
                    if(eConfig && eConfig.length>0){
                        var mailConf = eConfig[0];
                        if(mailConf.lead.prefix && mailConf.lead.prefix !=''){
                            if(mailConf.lead.sequence && mailConf.lead.prefix !='' && newLead.rank < parseInt(mailConf.lead.sequence)){
                                newLead.leadId= mailConf.lead.prefix +'-'+mailConf.lead.sequence;
                            }else{
                                newLead.leadId= mailConf.lead.prefix +'-'+newLead.rank;
                            }
                        }else{
                            newLead.leadId=cCode +'-'+'LD'+'-'+ pad.substring(0, pad.length - str.length) + str;
                        }
                    }else{
                        newLead.leadId=cCode +'-'+'LD'+'-'+ pad.substring(0, pad.length - str.length) + str;
                    }
                    newLead.save().then(function(result) {
                        res.json({
                            result: result
                        });
                    }).error(handleError(res));  
                }).error(handleError(res));
            }
        }).error(handleError(res));
    }).error(handleError(res));        
};

exports.sendPaymentLink = function (req, res) {
    var leadId = req.params.id;
    Lead.get(leadId).run().then(function(lead){
        if(lead && parseInt(lead.price) >9){
            var user = JSON.parse(req.decoded);
            Portfolio.get(user.portfolioId).run().then(function(portfolio){
                if(portfolio){
                    Customer.get(lead.customerId).run().then(function(customer){
                      var iapikey = portfolio.instamojo.apiKey;
                      var authtoken = portfolio.instamojo.authtoken;
                      Insta.setKeys(iapikey, authtoken);
                      var data = new Insta.PaymentData();
                      var amnt = parseInt(req.body.amount);
                      data.purpose = lead.leadId;            // REQUIRED 
                      data.amount = parseInt(lead.price);   // REQUIRED 
                      data.redirect_url= portfolio.website;
                      data.currency                = 'INR';
                      data.buyer_name              = customer.name;
                      data.email                   = customer.email;
                      data.phone                   = customer.mobile;
                      data.send_sms                = false;
                      data.send_email              = false;
                      data.allow_repeated_payments = false;
                       var newPayment = new Payment({
                            orgId: portfolio.orgId,
                            portfolioId: portfolio.id,
                            orderId: data.purpose,
                            amount: data.amnt,
                            paymentDate: new Date(),
                            paymentMode: 'Credit/Debit Card'
                          });
                          newPayment.save().then(function(payment){
                            data.webhook = 'https://api.zinetgo.com/api/payment/'+ payment.id + '?apikey='+config.onground.apiKey,
                            Insta.createPayment(data, function(error, response) {
                              if (error) {
                                // some error 
                                res.status(500).send({ error: error });
                              } else {
                                // Payment redirection link at response.payment_request.longurl 
                                response.paymentId = payment.id;
                                Preference.filter({portfolioId:user.portfolioId}).run().then(function(eConfig){
                                    if(eConfig && eConfig.length>0){
                                        var mailConf = eConfig[0];
                                        if(mailConf.smsConfig.enableSms==true){
                                            var portfolioSmsId = mailConf.smsConfig.smsProvider.senderId;
                                            if(portfolioSmsId == undefined || portfolioSmsId == null || portfolioSmsId === ""){
                                                portfolioSmsId = config.sms.msg91.senderId;
                                            }

                                            var authKey = mailConf.smsConfig.smsProvider.authKey;
                                            if(authKey == undefined || authKey == null || authKey === ""){
                                                authKey = config.sms.msg91.authKey;
                                            }
                                            var bitly = new Bitly('6074d0c97bc006eb9a7356fecf13f135fac08767');
                                            var pay = JSON.parse(response);
                                            console.log(pay.payment_request);
                                            bitly.shorten(pay.payment_request.longurl)
                                          .then(function(response1) {
                                            console.log(JSON.stringify(response1));
                                            var short_url = response1.data.url
                                            if(customer.mobile != undefined && customer.mobile != null){
                                                smsService.sendSms(req, authKey, portfolioSmsId, customer.mobile,'Dear '+customer.name+',\nThank you for choosing '+portfolio.name+'. You can make the payment after the service or use below link to pay online.\n'+short_url);
                                            }
                                          }, function(error) {
                                            throw error;
                                          });
                                        } 
                                    }
                                    res.json({
                                       result:JSON.parse(response)
                                    });
                                });
                              } 
                            });
                        });
                    });
                }else{
                  return res.send(500, {error: 'Invalid request'}); 
                }
            });
        }else{
            return res.send(500, {error: 'Invalid lead or price'});
        }
    })      
};

exports.assignLead = function (req, res) {
    var user = JSON.parse(req.decoded);
    var newLead = req.body;
    Lead.filter({proLeadId: newLead.proLeadId, portfolioId: newLead.portfolioId,status: 'active'}).run().then(function(exitst){
        if(exitst && exitst.length >0){
            res.send(500, {error: 'You have already assigned this lead to this vendor'});
        }else{
            console.log(req.body.dueDate);
            var obj = new Object();
            var dueDate = new Date(req.body.dueDate);
            obj.dueDate=dueDate;
            console.log(obj.dueDate);
            obj.portfolioId= newLead.portfolioId;
            obj.itemId = newLead.itemId;
            obj.proLeadId= newLead.proLeadId;
            obj.createdOn = new Date();
            obj.status = 'active';
            obj.leadId = newLead.leadId;
            obj.address = newLead.address;
            obj.mpId = user.portfolioId;
            obj.price = newLead.price;
            var assignedLead = new Lead(obj);
            Portfolio.get(user.portfolioId).run().then(function(tport){
                Item.get(newLead.itemId).run().then(function(aItem){
                    Lead.get(newLead.proLeadId).run().then(function(plead){
                        Item.get(plead.itemId).run().then(function(pitem){
                            Portfolio.get(newLead.portfolioId).getJoin({org: true}).run().then(function(portfolio){
                                var leadType = portfolio.org.vendorType;
        			             console.log(leadType);
                                if(leadType === 'converstion'){
                                    var per = portfolio.org.leadPercentage;
        				            console.log(newLead.price);
        				             console.log(portfolio.org.leadPercentage);
        				             console.log(leadType);
                                    var lprice =( parseInt(newLead.price)||0) * (parseInt(portfolio.org.leadPercentage)/100);
                                    var damount = lprice;
        				            console.log(damount);
                                    var leadst = 'CONVERTED';

                                    /*if(parseInt(lprice) > portfolio.trunetoWallet){
                                        res.send(500, {error: 'In-sufficient balance in wallet'});
                                    }*/
                                }else if(leadType === 'leadModel'){
                                    console.log(pitem.lprice);
                                    console.log(pitem.id);
                                    var damount = pitem.lprice;
                                    if(parseInt(pitem.lprice) > portfolio.trunetoWallet){
                                        res.send(500, {error: 'In-sufficient balance in wallet'});
                                    }
                                     var leadst = 'TRANSFERRED';
                                }
                                var portfolioEmailId,portfolioSenderName;
                                emailConfig.filter({portfolioId:user.portfolioId}).run().then(function(eConfig){
                                    if(eConfig && eConfig.length>0){
                                        var mailConf = eConfig[0];
                                        portfolioEmailId= mailConf.gmail.auth.user;
                                        portfolioSenderName= mailConf.gmail.auth.name;
                                        var sender = mailConf.gmail;
                                    }else{
                                        var sender = config.smtp.gmail;
                                    }
                                    if(!portfolioEmailId){
                                      portfolioEmailId = portfolio.email;
                                    }
                                    if(!portfolioSenderName){
                                      portfolioSenderName = portfolio.name;
                                    } 

                                    console.log("sender---=="+JSON.stringify(sender));
                                    assignedLead.vendorName = portfolio.name;
                                    assignedLead.vendorMobile = portfolio.mobile;
                                    assignedLead.vendorEmail = portfolio.email;
                                    var namount = portfolio.trunetoWallet - parseInt(damount);
                                    var newWalletTransaction = new WalletTransaction({
                                        portfolioId:portfolio.id,
                                        beforeBal: portfolio.trunetoWallet || 0,
                                        afterBal: parseInt(namount),
                                        withdrawl: parseInt(damount),
                                        mpId: tport.id,
                                        comments: 'Paid '+damount+' for leadId: '+obj.leadId
                                    });
                                    newWalletTransaction.save().then(function(walletTran){
                                        var nbal = parseInt(tport.wallet) + parseInt(walletTran.withdrawl);
                                        var newWalletTransaction1 = new WalletTransaction({
                                            portfolioId:tport.id,
                                            beforeBal: tport.wallet || 0,
                                            afterBal: parseInt(nbal),
                                            deposite: parseInt(walletTran.withdrawl),
                                            comments: 'Received '+damount+' for leadId: '+obj.leadId + ' from '+portfolio.name,
                                            transactionType: leadType
                                        });
                                        newWalletTransaction1.save().then(function(walletTran1){
                                            Portfolio.get(newLead.portfolioId).update({trunetoWallet :namount }).then(function(portfoliow){
                                                Portfolio.get(tport.id).update({wallet :nbal }).then(function(portfoliow){
                                                    Lead.get(plead.id).update({leadStatus:leadst}).then(function(tlead){
                                                        Customer.filter({portfolioId: newLead.portfolioId,mobile: parseInt(newLead.cMob)}).run().then(function(cus){
                                                            if(cus && cus.length>0){
                                                                assignedLead.customerId = cus[0].id;
                                                                assignedLead.save().then(function(result) {
                                                                    if(portfolio.email != undefined && portfolio.email != null){
                                                                        emailService.sendEmail(sender, {
                                                                            from: '"'+portfolioSenderName +'" <'+portfolioEmailId+'>', // sender address
                                                                            to: portfolio.email, // list of receivers
                                                                            subject: 'New lead from '+tport.name,
                                                                            text: 'Dear '+portfolio.name +','+'<br>'+'A new lead has been assigned to you by '+tport.name+'.<br>Please find below details.'+'\n Service Name :'+aItem.name+'\nFrom:'+cus[0].name +'\nMobile: '+cus[0].mobile+'\nEmail: '+cus[0].email || ''+'\nDate: '+result.dueDate +'\nAddress: '+result.address +'\n\nBest,\nTeam '+ tport.name,
                                                                            html: 'Dear '+portfolio.name +','+'</b><br><br>A new lead has been assigned to you by '+tport.name+'.<br>Please find below details.'+'<br> Service Name :'+aItem.name+'<br>From:'+cus[0].name +'<br>Mobile: '+cus[0].mobile +'<br>Email: '+cus[0].email || ''+'<br>Date: '+result.dueDate +'<br>Address: '+result.address +'<br><br>Best,<br>Team '+ tport.name,
                                                                        });
                                                                    }
                                                                    if(tport.enableSms==true){
                                                                        var portfolioSmsId = tport.smsProvider.senderId;
                                                                        if(portfolioSmsId == undefined || portfolioSmsId == null || portfolioSmsId === ""){
                                                                            portfolioSmsId = config.sms.msg91.senderId;
                                                                        }

                                                                        console.log('portfolio.portfolioSmsId: '+ portfolioSmsId);

                                                                        var authKey = tport.smsProvider.authKey;
                                                                        if(authKey == undefined || authKey == null || authKey === ""){
                                                                            authKey = config.sms.msg91.authKey;
                                                                        }
                                                                        console.log('portfolio.authKey: '+ authKey);
                                                                        if(portfolio.mobile != undefined && portfolio.mobile != null){
                                                                            smsService.sendSms(req, authKey, portfolioSmsId, portfolio.mobile,'Dear '+portfolio.name +'\n! A new lead has been assigned to you by '+tport.name+'.\nPlease find below details.'+'\n Service Name :'+aItem.name+'\nFrom:'+cus[0].name +'\nMobile: '+cus[0].mobile+'\nEmail: '+cus[0].email+'\nDate: '+result.dueDate +'\nAddress: '+result.address +'\n\nBest,\nTeam'+ tport.name);
                                                                        }
                                                                    } 
                                                                    res.json({
                                                                        result: 'Lead assigned successfully'
                                                                    });
                                                                }).error(handleError(res));
                                                            }else{
                                                                var cust = new Customer({
                                                                    mobile: parseInt(newLead.cMob),
                                                                    name: newLead.cName,
                                                                    email: newLead.cEmail,
                                                                    status: 'active',
                                                                    portfolioId: newLead.portfolioId
                                                                });
                                                                cust.save().then(function(newCust){
                                                                    assignedLead.customerId = newCust.id;
                                                                    assignedLead.save().then(function(result) {
                                                                        if(portfolio.email != undefined && portfolio.email != null){
                                                                            emailService.sendEmail(sender, {
                                                                                from: '"'+portfolioSenderName +'" <'+portfolioEmailId+'>', // sender address
                                                                                to: portfolio.email, // list of receivers
                                                                                subject: 'New lead from '+tport.name,
                                                                                text: 'Dear '+portfolio.name +','+'<br>'+'A new lead has been assigned to you by '+tport.name+'.\nPlease find below details.'+'\n Service Name :'+aItem.name+'\nFrom:'+newCust.name +'\nMobile: '+newCust.mobile+'\nEmail: '+newCust.email+'\nDate: '+result.dueDate +'\nAddress: '+result.address +'\n\nBest,\nTeam '+ tport.name,
                                                                                html: 'Dear '+portfolio.name +','+'</b><br><br>A new lead has been assigned to you by '+tport.name+'.<br>Please find below details.'+'<br> Service Name :'+aItem.name+'<br>From:'+newCust.name +'<br>Mobile: '+newCust.mobile +'<br>Email: '+newCust.email +'<br>Date: '+result.dueDate +'<br>Address: '+result.address +'<br><br>Best,<br>Team '+ tport.name,
                                                                            });
                                                                        }
                                                                        if(tport.enableSms==true){
                                                                            var portfolioSmsId = tport.smsProvider.senderId;
                                                                            if(portfolioSmsId == undefined || portfolioSmsId == null || portfolioSmsId === ""){
                                                                                portfolioSmsId = config.sms.msg91.senderId;
                                                                            }

                                                                            console.log('portfolio.portfolioSmsId: '+ portfolioSmsId);

                                                                            var authKey = tport.smsProvider.authKey;
                                                                            if(authKey == undefined || authKey == null || authKey === ""){
                                                                                authKey = config.sms.msg91.authKey;
                                                                            }
                                                                            console.log('portfolio.authKey: '+ authKey);
                                                                            if(portfolio.mobile != undefined && portfolio.mobile != null){
                                                                                smsService.sendSms(req, authKey, portfolioSmsId, portfolio.mobile,'Dear '+portfolio.name +',\n A new lead has been assigned to you by '+tport.name+'\n Service Name :'+aItem.name+'\nFrom:'+newCust.name +'\nMobile: '+newCust.mobile+'\nEmail: '+newCust.email+'\nDate: '+result.dueDate +'\nAddress: '+result.address +'\n\nBest,\nTeam '+ tport.name);
                                                                            }
                                                                        } 
                                                                        res.json({
                                                                            result: 'Lead assigned successfully'
                                                                        });
                                                                    }).error(handleError(res));
                                                                }).error(handleError(res));
                                                            }
                                                        }).error(handleError(res));
                                                    }).error(handleError(res));
                                                }).error(handleError(res));
                                            }).error(handleError(res));
                                        }).error(handleError(res));
                                    }).error(handleError(res));
                                }).error(handleError(res));
                            }).error(handleError(res));
                        })
                    })
                }).error(handleError(res));
            }).error(handleError(res));
        }
    }).error(handleError(res));
};


exports.addExternalLead = function (req, res) {
    var key = req.params.key;
    if(key == undefined || key == null){
        return res.send(500, {error: 'API key not set'});
    }
    var data, newLead;
    if(req.query.data){
        //do nothing
        data = JSON.parse(req.query.data);
        newLead =  new Lead(data);
    }else{
       newLead = new Lead(req.body);
    }
    if(newLead == undefined || key == null){
        return res.send(500, {error: 'Not data passed to API'});
    }else{
        if(newLead.dueDate && newLead.dueDate!=null){
            var dDate = new Date(newLead.dueDate);
        newLead.dueDate = dDate;
        }else{
            newLead.dueDate = new Date();
        }
        
        var address = newLead.address;
        var description = newLead.description;
        var email = newLead.email;
        if(newLead.mobile == undefined || newLead.mobile == null || newLead.mobile == ""){
            return res.send(500, {error: 'Missing required field mobile'});
        }
        if(newLead.name == undefined || newLead.name == null || newLead.name == ""){
            return res.send(500, {error: 'Missing required field name'});
        }
        if(newLead.itemId == undefined || newLead.itemId == null || newLead.itemId == ""){
            return res.send(500, {error: 'Missing required field itemId'});
        }
        if(address ==undefined || address == null || address == ""){
             newLead.address = '';
        }
        if(description ==undefined || description == null || description == ""){
            newLead.description = '';
        }
        if(email ==undefined || email == null || email == ""){
            newLead.email = '';
        }
        Portfolio.filter({tpKey:key}).run().then(function(result){
            if(result && result.length >0){
                var portfolio = result[0];
                if(portfolio.isMarketPlace === true){
                    newLead.mpId = portfolio.id;
                }
                Lead.orderBy({index: r.desc('rank')}).filter({portfolioId: result[0].id}).run().then(function(ld){
                    if (ld && ld.length>0) {
                        led=ld[0];
                        newLead.rank=led.rank+1;
                    }else{
                        newLead.rank=1;
                    } 
                    Item.filter({id:newLead.itemId}).run().then(function(item) {
                        if(item && item.length > 0){
                            serviceName = item[0].name;
                            newLead.portfolioId = result[0].id;
                            newLead.referralType ='Direct';
                            newLead.status='active';
                            newLead.channel='WEBSITE';
                            var mob = parseInt(newLead.mobile);
                            Preference.filter({portfolioId:newLead.portfolioId}).run().then(function(eConfig){
                                var portfolioEmailId,portfolioSenderName;
                                if(eConfig && eConfig.length>0){
                                    var mailConf = eConfig[0];
                                    if(mailConf.lead.prefix && mailConf.lead.prefix !=''){
                                        if(mailConf.lead.sequence && mailConf.lead.prefix !='' && newLead.rank < parseInt(mailConf.lead.sequence)){
                                            newLead.leadId= mailConf.lead.prefix +'-'+mailConf.lead.sequence;
                                            newLead.rank=parseInt(mailConf.lead.sequence)+1;
                                        }else{
                                            newLead.leadId= mailConf.lead.prefix +'-'+newLead.rank;

                                        }
                                    }else{
                                        var nm =result[0].name;
                                        var cCode =nm.substring(0, 3).toUpperCase();
                                        var str = "" + newLead.rank;
                                        var pad = "0000";
                                        newLead.leadId=cCode +'-'+'LD'+'-'+ pad.substring(0, pad.length - str.length) + str;
                                        var sender = config.smtp.gmail;
                                    }
                                    
                                    portfolioEmailId= mailConf.emailConfig.gmail.auth.user;
                                    portfolioSenderName= mailConf.emailConfig.gmail.auth.name;
                                    var sender = mailConf.emailConfig.gmail;
                                }else{
                                    var nm =result[0].name;
                                    var cCode =nm.substring(0, 3).toUpperCase();
                                    var str = "" + newLead.rank;
                                    var pad = "0000";
                                    newLead.leadId=cCode +'-'+'LD'+'-'+ pad.substring(0, pad.length - str.length) + str;
                                    var sender = config.smtp.gmail;
                                }
                                if(!portfolioEmailId){
                                  portfolioEmailId = portfolio.email;
                                }
                                if(!portfolioSenderName){
                                  portfolioSenderName = portfolio.name;
                                }
                                if(req.files && req.files !=null){
                                    var file = req.files.fileToUpload;
                                    if(file && file != null){
                                        newLead.picture = file.name;
                                    }else{
                                        newLead.picture = null;
                                    }
                                }
                                Template.filter({portfolioId: newLead.portfolioId}).run().then(function(temp){
                                    if(temp && temp.length>0 && temp[0].lead.new !=""){
                                        var html1 = Handlebars.compile(temp[0].lead.new.html !='' ? temp[0].lead.new.html :'<p>Dear {{ name }}!<br><br>We have received your request for {{ sName }}. Your reference number is {{ leadId }}. For any queries please call us at {{ mob }} or visit website {{ website }}<br><br><br>Best,<br><br>{{ pName }}</p>');
                                        var subject1 = Handlebars.compile(temp[0].lead.new.subject !=''? temp[0].lead.new.subject : 'We have received your request for {{ sName }}  Refernce number is :{{ leadId }}.');
                                        var text1 = Handlebars.compile(temp[0].lead.new.text !=''?temp[0].lead.new.text : 'Dear {{ name }}! \n We have received your request for {{ sName }}. Your reference number is {{ leadId }}. Call us at {{ mob }} or visit {{ website }} for any queries.');
                                    }else{
                                        var html1 = Handlebars.compile('<p>Dear {{ name }}!<br><br>We have received your request for {{ sName }}. Your reference number is {{ leadId }}. For any queries please call us at {{ mob }} or visit website {{ website }}<br><br><br>Best,<br><br>{{ pName }}</p>');
                                        var subject1 = Handlebars.compile('We have received your request for {{ sName }}  Refernce number is :{{ leadId }}.');
                                        var text1 = Handlebars.compile('Dear {{ name }}! \n We have received your request for {{ sName }}. Your reference number is {{ leadId }}. Call us at {{ mob }} or visit {{ website }} for any queries.');
                                    }

                                    var html2 = Handlebars.compile(fs.readFileSync(path.join(templatesDir, 'lead-self/html.hbs'), 'utf8'));
                                    var subject2 = Handlebars.compile(fs.readFileSync(path.join(templatesDir, 'lead-self/subject.hbs'), 'utf8'));
                                    var text2 = Handlebars.compile(fs.readFileSync(path.join(templatesDir, 'lead-self/text.hbs'), 'utf8'));

                                
                                    fs.writeFile(path.join(templatesDir ,'lead-cust/html.hbs'), '', function(){
                                    });
                                    fs.writeFile(path.join(templatesDir ,'lead-cust/text.hbs'), '', function(){
                                    });
                                    fs.writeFile(path.join(templatesDir ,'lead-cust/subject.hbs'), '', function(){
                                    });
                                    Customer.filter({portfolioId:newLead.portfolioId,mobile:mob}).run().then(function(cust) {   
                                       if(cust && cust.length > 0){
                                                //exists
                                            newLead.customerId = cust[0].id;
                                        
                                          if(cust[0].id){
                                                var stats = cust[0].stats;
                                                if(stats && stats!=null){
                                                  var sts =stats ;
                            
                                                  if(sts.leads && sts.leads !=null){
                                                     sts.leads = sts.leads + 1;
                                                  }else{
                                                    sts.leads =1;
                                                  }
                                                }else{
                                                    var sts= {
                                                      leads: 1
                                                    }
                                                }
                                                Customer.get(cust[0].id).update({stats:sts}).then(function(cust1) {
                                                });//.error(handleError(res));
                                            }
                                            newLead.save().then(function(lead) {
                                                UserProfile.filter({userId: lead.customerId,portfolioId: lead.portfolioId}).run().then(function(up){
                                                    if(up && up.length>0){
                                                        UserProfile.get(up[0].id).update({leadAddress:lead.address}).run().then(function(newAddress){

                                                        });
                                                    }
                                                })

                                                // get the sms api key
                                                // get the sender id
                                                var leads = lead;
                                                console.log('--'+JSON.stringify(leads));
                                                leads.dueDate = moment(leads.dueDate).format("DD-MM-YYYY,h:mm a");
                                                leads.pName = portfolio.name;
                                                leads.sName = serviceName;
                                                leads.mob = portfolio.mobile;
                                                leads.website = portfolio.website;
                                                leads.address = newLead.address;

                                                var resulthtml1 = html1(leads);
                                                var resulttext1 = text1(leads);
                                                var resultsubject1 = subject1(leads);

                                                var resulthtml2 = html2(leads);
                                                var resulttext2 = text2(leads);
                                                var resultsubject2 = subject2(leads);
                                                    
                                                if(portfolio.email != undefined && portfolio.email != null){
                                                    emailService.sendEmail(config.smtp.gmail, {
                                                        from: '"Zinetgo" <support@zinetgo.com>', // sender address
                                                        to: portfolioEmailId, // list of receivers
                                                        subject: resultsubject2, // Subject line
                                                        text: resulttext2,
                                                        html: resulthtml2
                                                    });
                                                }
                                                if(mailConf && mailConf.emailConfig.enableEmail==true){
                                                
                                                    if(leads.email != undefined && lead.email != null){
                                                        emailService.sendEmail(sender, {
                                                            from: '"'+portfolioSenderName +'" <'+portfolioEmailId+'>', // sender address
                                                            to: lead.email, // list of receivers
                                                            subject: resultsubject1,
                                                            text: resulttext1,
                                                            html: resulthtml1
                                                        });
                                                    }
                                                }

                                                if(portfolio.mobile != undefined && portfolio.mobile != null){
                                                    var authKey1 = config.sms.msg91.authKey;
                                                    var portfolioSmsId1 = config.sms.msg91.senderId;
                                                   smsService.sendSms(req, authKey1, portfolioSmsId1, portfolio.mobile,resulttext2);
                                                }
                                                if(mailConf && mailConf.smsConfig.enableSms==true){
                                                    var portfolioSmsId = mailConf.smsConfig.smsProvider.senderId;
                                                    if(portfolioSmsId == undefined || portfolioSmsId == null || portfolioSmsId === ""){
                                                        portfolioSmsId = config.sms.msg91.senderId;
                                                    }

                                                    var authKey = mailConf.smsConfig.smsProvider.authKey;
                                                    if(authKey == undefined || authKey == null || authKey === ""){
                                                        authKey = config.sms.msg91.authKey;
                                                    }
                                                    console.log('portfolio.authKey-----------: '+ authKey);
                                                    console.log('portfolio portfolioSmsId-----------: '+ portfolioSmsId);
                                                    
                                                    if(lead.mobile != undefined && lead.mobile != null){
                                                        smsService.sendSms(req, authKey, portfolioSmsId, lead.mobile,resulttext1);
                                                    }
                                                } 
                                                if (!req.files){
                                                    res.json({
                                                        result: {'status':'success','message':'We have received your request. Your reference number is '+ lead.leadId,'leadId':lead.leadId}
                                                    });
                                                }else{
                                                    if(file){
                                                        var obj = new Object();
                                                        obj.name = file.name;
                                                        obj.mime = file.mimetype;
                                                        obj.forId = lead.id;
                                                        obj.userId = user.userId;
                                                        obj.updatedBy =  user.userId;
                                                        obj.portfolioId = user.portfolioId;
                                                        obj.updatedOn = r.now();
                                                        var newMedia = new Media(obj);
                                                    }

                                                    var sendResponse = function(error, data){
                                                        if(!error){
                                                            var coverImage = config.media.upload.s3.accessUrl + '/media/'+ og.getIdPath(data.mediaId) + '/' + file.name;

                                                            Product.get(lead.id).update({mediaId: data.mediaId, leadImage: coverImage}).then(function(r1) {

                                                                res.json({
                                                                    result: {'status':'success','message':'We have received your request. Your reference number is '+ lead.leadId,'leadId':lead.leadId}
                                                                });
                                                            }).error(handleError(res));
                                                            // res.json({
                                                            //     result: data
                                                            // });
                                                        }else{
                                                            return res.send(500, {error: error.message});
                                                        }
                                                    }

                                                    newMedia.save().then(function(media) {  
                                                        MediaService.uploadMedia(file, media.id, sendResponse);
                                                    });
                                                }  
                                            }).error(handleError(res)); 
                                        }else{
                                            var newCustomer = new Customer({
                                                name: newLead.name,
                                                //address: newLead.address,
                                                status: "active",
                                                mobile: newLead.mobile,
                                                email: newLead.email,
                                                stats: {
                                                  leads: 1
                                                },
                                                portfolioId: newLead.portfolioId,
                                                branchId: newLead.branchId,
                                                createdOn: newLead.createdOn
                                            });
                                            newCustomer.save().then(function(result) {
                                              newLead.customerId = result.id;

                                              newLead.save().then(function(lead) { 
                                                   var leads = lead;
                                                    leads.pName = portfolio.name;
                                                    leads.sName = serviceName;
                                                    leads.mob = portfolio.mobile;
                                                    leads.website = portfolio.website;

                                                    var resulthtml1 = html1(leads);
                                                    var resulttext1 = text1(leads);
                                                    var resultsubject1 = subject1(leads);

                                                    var resulthtml2 = html2(leads);
                                                    var resulttext2 = text2(leads);
                                                    var resultsubject2 = subject2(leads);
                                                    
                                                    if(portfolio.email != undefined && portfolio.email != null){
                                                        emailService.sendEmail(config.smtp.gmail, {
                                                            from: '"Zinetgo" <support@zinetgo.com>', // sender address
                                                            to: portfolioEmailId, // list of receivers
                                                            subject: resultsubject2, // Subject line
                                                            text: resulttext2,
                                                            html: resulthtml2
                                                        });
                                                    }
                                                    if(mailConf && mailConf.emailConfig.enableEmail==true){
                                                    
                                                        if(leads.email != undefined && lead.email != null){
                                                            emailService.sendEmail(sender, {
                                                                from: '"'+portfolioSenderName +'" <'+portfolioEmailId+'>', // sender address
                                                                to: lead.email, // list of receivers
                                                                subject: resultsubject1,
                                                                text: resulttext1,
                                                                html: resulthtml1
                                                            });
                                                        }
                                                    }

                                                    if(portfolio.mobile != undefined && portfolio.mobile != null){
                                                        var authKey1 = config.sms.msg91.authKey;
                                                        var portfolioSmsId1 = config.sms.msg91.senderId;
                                                       smsService.sendSms(req, authKey1, portfolioSmsId1, portfolio.mobile,resulttext2);
                                                    }
                                                    if(mailConf && mailConf.smsConfig.enableSms==true){
                                                        var portfolioSmsId = mailConf.smsConfig.smsProvider.senderId;
                                                        if(portfolioSmsId == undefined || portfolioSmsId == null || portfolioSmsId === ""){
                                                            portfolioSmsId = config.sms.msg91.senderId;
                                                        }

                                                        var authKey = mailConf.smsConfig.smsProvider.authKey;
                                                        if(authKey == undefined || authKey == null || authKey === ""){
                                                            authKey = config.sms.msg91.authKey;
                                                        }
                                                        console.log('portfolio.authKey-----------: '+ authKey);
                                                        console.log('portfolio portfolioSmsId-----------: '+ portfolioSmsId);
                                                        
                                                        if(lead.mobile != undefined && lead.mobile != null){
                                                            smsService.sendSms(req, authKey, portfolioSmsId, lead.mobile,resulttext1);
                                                        }
                                                    } 
                                                    if (!req.files){
                                                        res.json({
                                                            result: {'status':'success','message':'We have received your request. Your reference number is '+ lead.leadId,'leadId':lead.leadId}
                                                        });
                                                    }else{
                                                        if(file){
                                                            var obj = new Object();
                                                            obj.name = file.name;
                                                            obj.mime = file.mimetype;
                                                            obj.forId = lead.id;
                                                            obj.userId = user.userId;
                                                            obj.updatedBy =  user.userId;
                                                            obj.portfolioId = user.portfolioId;
                                                            obj.updatedOn = r.now();
                                                            var newMedia = new Media(obj);
                                                        }

                                                        var sendResponse = function(error, data){
                                                            if(!error){
                                                                var coverImage = config.media.upload.s3.accessUrl + '/media/'+ og.getIdPath(data.mediaId) + '/' + file.name;

                                                                Lead.get(lead.id).update({mediaId: data.mediaId, leadImage: coverImage}).then(function(r1) {

                                                                    res.json({
                                                                        result: {'status':'success','message':'We have received your request. Your reference number is '+ lead.leadId,'leadId':lead.leadId}
                                                                    });
                                                                }).error(handleError(res));
                                                                // res.json({
                                                                //     result: data
                                                                // });
                                                            }else{
                                                                return res.send(500, {error: error.message});
                                                            }
                                                        }

                                                        newMedia.save().then(function(media) {  
                                                            MediaService.uploadMedia(file, media.id, sendResponse);
                                                        });
                                                    }
                                                }).error(handleError(res)); 
                                            }).error(handleError(res)); 
                                        }  
                                    }).error(handleError(res));
                                }).error(handleError(res));  
                            }).error(handleError(res));          
                        }else{
                            return res.send(500, {error: 'Invalid itemId'});
                        }
                    }).error(function(error){
                        return res.send(500, {error: 'Invalid itemId'});
                    }); 
                }).error(function(error){
                //
                });
            }else{
                return res.send(500, {error: 'Invalid api key'});
            }
        }).error(function(error){
            //
        });
    }
};

// update user
exports.updateLead = function (req, res) {
    var id = req.params.id;
    var ld =req.body;
    var user = JSON.parse(req.decoded);
    console.log('-------'+ld.dueDate);
    ld.updatedBy =user.userId;
    ld.updatedOn =r.now();
    if(ld.dueDate && ld.dueDate !=null){
        ld.dueDate = new Date(ld.dueDate);
    }
    Lead.get(id).update(ld).then(function(result) {
        
        res.json({
            result: result
        });
    }).error(handleError(res));
};


exports.bulkUpload = function (req, res) {
    if (!req.files) {
        res.status(500).send({error: 'Please input a CSV file to upload.'});
        return;
    }

    var tokenObject = JSON.parse(req.decoded);

    var file = req.files.fileToUpload;
    var fileName = file.name;
    var mime = file.mimetype;
    var dir = __dirname;
    var csvFileName = dir + '/' + file.name;
    var pr = [];
    
    if(fileName.endsWith(".csv") || fileName.endsWith(".CSV")){
          file.mv(csvFileName, function(err) {
                csv()
                .fromFile(csvFileName)
                .on('json',(jsonObj)=>{
                    var totalAmount =  0;
                    var prod = [];
                    var probj = jsonObj[Object.keys(jsonObj)[9]];
                    if(probj && probj.length>0){
                        var prods = probj.split('/');
                        for(var i = 0;i< prods.length;i++){
                            var position = prods[i].indexOf('-');
                            var aPosition =  prods[i].indexOf('-',position+1);
                            var pName = prods[i].substring(0,position).trim();
                            var pQty = parseInt(prods[i].substring(position+1, aPosition).trim());
                            var amount = parseInt(prods[i].substring(aPosition+1, prods[i].length).trim());
                            var p1 = {
                                productName : pName,
                                quantity : pQty,
                                price:  amount
                            }
                            totalAmount += pQty * amount;
                            prod.push(p1);
                        }
                    }
                    var spGst = parseInt(jsonObj.spareGst) || 0;
                    var srGst = parseInt(jsonObj.serviceGst) || 0;
                    console.log(spGst);
                    console.log(srGst);
                    console.log(totalAmount);
                    console.log(jsonObj.serviceCharge);
                    jsonObj.total = totalAmount + spGst + srGst + parseInt(jsonObj.serviceCharge);
                    jsonObj.product = prod;
                    pr.push(jsonObj);
                })
                .on('done',(error)=>{
                    var count = 0;
                    var length = pr.length;
                    var meCallback = function(error,data) {
                        count++;
                        if(count === length){
                           res.json({
                                result: {'status':'success','message':'Bulk uploaded successfully'}
                            });
                       }
                    }
                    for(var i=0;i<pr.length;i++){
                        (function(j){
                            var cust = {};
                            cust.name =pr[i].customerName;
                            cust.mobile=pr[i].customerMobile;
                            cust.email = pr[i].customerEmail;
                            var obj = pr[i];
                            createCustomer(cust,tokenObject,obj,meCallback);
                        })(i)    
                    }
                });
          });
    }else{
      res.status(500).send({error: 'Only  CSV file supported.'});
    }
};

var createCustomer = function(cust,user,obj,callback){
    var emp = {};
    emp.name = obj.employeeName;
    emp.mobile = obj.employeeMobile;
    emp.email = obj.employeeEmail || '';
    if(cust && cust.mobile && cust.mobile !=null && cust.mobile != ''){
        Customer.filter({mobile : parseInt(cust.mobile),portfolioId: user.portfolioId}).run().then(function(ce){
            if(ce && ce.length>0){
                var sts = ce[0].stats;
                if(sts.leads){
                    sts.leads=sts.leads+1;
                }else{
                    sts.leads=0;
                }
                Customer.get(ce[0].id).update({stats:sts,status:'active'}).run().then(function(cus){
                    var idd = cus.id;
                    emp.customerId = idd;
                    var meCallback = function(error,data) {
                        callback(null,idd);
                    };
                    createEmployee(emp,user,obj,meCallback);
                });
            }else{
                var newCus = new Customer({
                    portfolioId:user.portfolioId, 
                    name: cust.name,
                    mobile: cust.mobile,
                    email: cust.email,
                    status:'active',
                    stats:{lead:0},
                    createdOn: new Date()
                });
                newCus.save().then(function(customet){
                    var idd = customet.id;
                    emp.customerId = idd;
                    var meCallback = function(error,data) {
                        callback(null,idd);
                    };
                    createEmployee(emp,user,obj,meCallback);
                    
                });
            }
        });
    }else{
        emp.customerId = null;
        var meCallback = function(error,data) {
            callback(null,data);
        };
        createEmployee(emp,user,obj,meCallback);
    }
}

var createEmployee = function(emp,user,obj,callback){
    var service = {};
    service.name= obj.serviceName;
    service.customerId = emp.customerId;
    service.leadId = obj.billNo;
    service.address = obj.address;
    service.product = obj.product;
    service.qty = obj.quantity;
    service.servicePrice = obj.serviceCharge || 0;
    service.totalPrice = obj.total || 0;
    service.paid = obj.paid || service.totalPrice;
    service.dueAmount = (obj.total - service.paid ) || 0;
    service.remarks = obj.remarks || '';
    service.spareGst = obj.spareGst || 0;
    service.serviceGst = obj.serviceGst || 0;

    var li = obj[Object.keys(obj)[8]];
    if(li && li !=''){
        var jD = new Date(li);
        if(jD && !isNaN( jD.getTime())){
            service.jDate = jD;
        }else{
            service.jDate = new Date();
        }
    }else{
        service.jDate = new Date();
    }

    var lstatus = obj[Object.keys(obj)[14]];
    if(lstatus && lstatus !=''){
        lstatus = lstatus.toUpperCase();
        if(lstatus === 'CONFIRM'){
            service.leadStatus = 'CONVERTED';
        }else{
            service.leadStatus = 'NEW'
        }
    }else{
        service.leadStatus = 'NEW'
    }

    var ed = obj[Object.keys(obj)[0]];
    if(ed && ed !=''){
        var cd = new Date(ed);
        if(cd && !isNaN( cd.getTime())){
            service.createdOn = cd;
        }else{
            service.createdOn = new Date();
        }
    }else{
        service.createdOn = new Date();
    }
    if(emp && emp.mobile && emp.mobile !=null && emp.mobile != ''){
        Employee.filter({mobile : parseInt(emp.mobile),portfolioId: user.portfolioId}).run().then(function(ce){
            
            if(ce && ce.length>0){
                var idd = ce[0].id;
                service.employeeId = idd;
                service.empName = ce[0].name;
                service.empObj = ce[0];
                var mmCallback = function(error,data){
                    callback(null,data);
                };
                createService(service,user,mmCallback);
            }else{
                var eEmail = emp.email || '';
                var newEmp = new Employee({
                    portfolioId:user.portfolioId, 
                    name: emp.name,
                    mobile: emp.mobile,
                    email: eEmail,
                    status:'active'
                });
                newEmp.save().then(function(employee){
                    var idd = employee.id;
                    service.employeeId = idd;
                    service.empName = employee.name;
                    service.empObj = employee;
                    var mmCallback = function(error,data){
                        callback(null,data);
                    };
                    createService(service,user,mmCallback);
                    
                });
            }
        });
    }else{
        service.employeeId = '';
        var mmCallback = function(error,data){
            callback(null,data);
        };
        service.empObj = null;
        createService(service,user,mmCallback);
    }
}


var createService = function(service,user,callback){
    var lead = {};
    lead.customerId = service.customerId;
    lead.employeeId = service.employeeId;
    lead.leadId = service.leadId;
    lead.address = service.address;
    lead.empObj = service.empObj;
    lead.product = service.product;
    lead.servicePrice = service.servicePrice || 0;
    lead.totalPrice = service.totalPrice || 0;
    lead.paid = service.paid || 0;
    lead.dueAmount = service.dueAmount || 0;
    lead.remarks = service.remarks || '';
    lead.jDate = service.jDate;
    lead.createdOn = service.createdOn;
    lead.spareGst = service.spareGst || 0;
    lead.serviceGst = service.serviceGst || 0;
    lead.leadStatus = service.leadStatus;
    if(service && service.name && service.name !=null && service.name != ''){
        Item.filter({name : service.name,portfolioId: user.portfolioId,type:'service'}).run().then(function(ce){
            if(ce && ce.length>0){
                var idd = ce[0].id;
                lead.itemId = idd;
                var sCallback = function(error,data){
                    callback(null,data);
                };
                createLead(lead,user,sCallback);
            }else{
                var newitem = new Item({
                    portfolioId:user.portfolioId, 
                    name: service.name,
                    type:'service',
                    status:'active'
                });
                newitem.save().then(function(item){
                    var idd = item.id;
                    lead.itemId = idd;
                    var sCallback = function(error,data){
                        callback(null,data);
                    };
                    createLead(lead,user,sCallback);
                    
                });
            }
        });
    }else{
        lead.itemId = null;
        var sCallback = function(error,data){
            callback(null,data);
        };
        createLead(lead,user,sCallback);
    }
}



var createLead = function(obj,user,callback){
    Portfolio.get(user.portfolioId).run().then(function(result){
        Preference.filter({portfolioId:user.portfolioId}).run().then(function(eConfig){
            Lead.filter({portfolioId:user.portfolioId}).count().execute().then(function(leads){
                if(obj.leadId && obj.leadId != null && obj.leadId !=''){
                    var leadId=obj.leadId;
                }else{
                    
                        if(eConfig && eConfig.length>0){
                            var mailConf = eConfig[0];
                            if(mailConf.lead.prefix && mailConf.lead.prefix !=''){
                                if(mailConf.lead.sequence && mailConf.lead.prefix !='' && leads < parseInt(mailConf.lead.sequence)){
                                    var leadId= mailConf.lead.prefix +'-'+mailConf.lead.sequence;
                                    //var rank = parseInt(mailConf.lead.sequence)+1
                                }else{
                                    var leadId= mailConf.lead.prefix +'-'+leads;
                                }
                            }else{
                                var nm =result.name;
                                var cCode =nm.substring(0, 3).toUpperCase();
                                if(leads> 0){
                                    var str = "" + leads;
                                }else{
                                    var str = "" + 1;
                                }
                                var pad = "0000";
                                var leadId=cCode +'-'+'LD'+'-'+ pad.substring(0, pad.length - str.length) + str;
                            }
                        }else{
                            var nm =result.name;
                            var cCode =nm.substring(0, 3).toUpperCase();
                            if(leads> 0){
                                var str = "" + leads;
                            }else{
                                var str = "" + 1;
                            }
                            var pad = "0000";
                            var leadId=cCode +'-'+'LD'+'-'+ pad.substring(0, pad.length - str.length) + str;
                        }
                }
                var newLead = new Lead({
                    portfolioId: user.portfolioId,
                    customerId: obj.customerId,
                    employeeId: obj.employeeId,
                    itemId: obj.itemId,
                    leadStatus:obj.leadStatus,
                    serviceCharge: obj.servicePrice,
                    paid: obj.paid,
                    dueAmount: obj.dueAmount,
                    remarks: obj.description,
                    price:obj.totalPrice,
                    dueDate:obj.jDate,
                    createdOn: obj.createdOn,
                    address : obj.address,
                    spareGst : obj.spareGst,
                    serviceGst :obj.serviceGst,
                    leadId : leadId
                });
                newLead.save().then(function(lead){
                    var ld={};
                    ld.lead = lead;
                    ld.product = obj.product;
                    ld.empObj = obj.empObj;
                    var sCallback = function(error,data){
                        callback(null,data);
                    };
                    createProduct(ld,user,sCallback);
                });
            });    
        });    
    });
}


var createProduct = function(product1,user,callback){
    // get the products they want to add. 
    for(var i =0; i< product1.product.length;i++){
        (function(j){
        var product = product1.product[i];
            if(product && product.productName && product.productName !=null && product.productName != ''){
                Item.filter({name : product.productName,portfolioId: user.portfolioId,type:'product'}).run().then(function(ce){
                   
                    if(ce && ce.length>0){
                        if(product1.empObj && product1.empObj != null){
                            var eId = product1.empObj.id;
                            var eName = product1.empObj.name

                        }else{
                            var eId = '';
                            var eName = '';
                        }
                        var idd = ce[0].id;
                        if(ce[0].employeeStock > product.quantity){
                            var nest = ce[0].employeeStock - product.quantity;
                        }else{
                            var nest = ce[0].employeeStock;
                        }
                        if(ce[0].stock > product.quantity){

                            var nStock=ce[0].stock -product.quantity;
                            var nihs = ce[0].inHouseStock-product.quantity;
                        }else{
                            var newinvlog = new InventoryLog({
                                employeeId: eId ,
                                productId: ce[0].id,
                                quantity: ''+product.quantity,
                                price: product.price,
                                comment:'invetory updated added quantity '+product.quantity,
                                inhouse:ce[0].inHouseStock,
                                emp:ce[0].employeeStock,
                                before:ce[0].employeeStock,
                                after:nest,
                                productName:ce[0].name,
                                portfolioId: user.portfolioId,
                                createdBy :user.userId,
                                updatedBy :user.userId,
                                createdOn:product1.lead.dueDate

                            });
                            newinvlog.save().then(function(logsvd){

                            });
                            var nStock=ce[0].stock;
                            var nihs = ce[0].inHouseStock;
                        }

                        var inlog = new InventoryLog({
                            employeeId: eId ,
                            productId: ce[0].id,
                             price: product.price,
                            quantity: ''+product.quantity,
                            comment:'assigned to employee '+eName,
                            inhouse:ce[0].inHouseStock,
                            emp:ce[0].employeeStock,
                            before:ce[0].employeeStock,
                            after:nest,
                            productName:ce[0].name,
                            portfolioId: user.portfolioId,
                            createdBy :user.userId,
                            updatedBy :user.userId,
                            createdOn:product1.lead.dueDate
                        });

                        inlog.save().then(function(invsved){
                            ProductInventory.filter({portfolioId:user.portfolioId,employeeId:product1.lead.employeeId,productId:ce[0].id}).run().then(function(inventory){
                                if(inventory && inventory.length>0){
                                    if(inventory[0].quantity < product.quantity){
                                        var pqty = inventory[0].quantity;
                                    }else{
                                        var pqty = inventory[0].quantity - product.quantity;
                                    }
                                    ProductInventory.get(inventory[0].id).update({quantity: pqty}).then(function(updateDqty){

                                    });
                                }else{
                                    var prinvent = new ProductInventory({
                                        empObj: product1.empObj,
                                        employeeId: eId ,
                                        productId: ce[0].id,
                                        portfolioId: user.portfolioId,
                                        createdBy :user.userId,
                                        updatedBy :user.userId,
                                        quantity: 0,
                                        price: 0,
                                        productObj: ce[0],
                                        createdOn:product1.lead.dueDate

                                    });
                                    prinvent.save().then(function(pi){

                                    });
                                }
                            });
                        });
                        Item.get(ce[0].id).update({stock:nStock,inHouseStock:nihs,employeeStock:nest}).run().then(function(nitem){

                            var newLeadInventory = new LeadInventory({
                                employeeId: product1.lead.employeeId ,
                                productId: ce[0].id,
                                price: product.price,
                                quantity: product.quantity,
                                leadId: product1.lead.id,
                                productName:ce[0].name,
                                portfolioId: user.portfolioId,
                                createdBy :user.userId,
                                updatedBy :user.userId,
                                createdOn:product1.lead.dueDate,
                                productObj:ce[0]
                            });
                            newLeadInventory.save().then(function(li){
                                var inventlog = new InventoryLog({
                                    employeeId: product1.lead.employeeId,
                                    leadId:product1.lead.id,
                                    productId: ce[0].id,
                                    price: product.price,
                                    quantity: '(-)'+product.quantity,
                                    comment:'used in the lead '+product1.lead.leadId+' by '+eName,
                                    inhouse:nitem.inHouseStock,
                                    emp:nitem.employeeStock,
                                    before:ce[0].stock,
                                    after:nitem.stock,
                                    productName:product.productName,
                                    portfolioId: user.portfolioId,
                                    createdBy :user.userId,
                                    updatedBy :user.userId,
                                    createdOn:product1.lead.dueDate
                                });
                                inventlog.save().then(function(inventorysvaed){
                                }); 
                            });
                        });    
                    }else{
                        var uName = product.productName.toLowerCase().split(' ').join('-');
                        var newitem = new Item({
                            portfolioId:user.portfolioId, 
                            name: product.productName,
                            type:'product',
                            status:'active',
                            stock:0,
                            price:product.price,
                            uName: uName,
                            inHouseStock: 0,
                            employeeStock:0
                        });
                        newitem.save().then(function(item){
                            var invelog = new InventoryLog({
                                productId: item.id,
                                quantity: ''+product.quantity,
                                comment:'product added to inventory',
                                inhouse:product.quantity,
                                emp:0,
                                portfolioId: user.portfolioId,
                                createdBy :user.userId,
                                updatedBy :user.userId,
                                createdOn:product1.lead.dueDate
                            });
                            invelog.save().then(function(invsaved){
                                if(product1.empObj && product1.empObj != null){
                                    var eId = product1.empObj.id;
                                    var eName = product1.empObj.name

                                }else{
                                    var eId = '';
                                    var eName = '';
                                }
                                var prinvent = new ProductInventory({
                                    empObj: product1.empObj,
                                    employeeId: eId ,
                                    productId: item.id,
                                    portfolioId: user.portfolioId,
                                    createdBy :user.userId,
                                    updatedBy :user.userId,
                                    quantity: item.employeeStock,
                                    productObj: item,
                                    createdOn:product1.lead.dueDate
                                });
                                prinvent.save().then(function(pi){
                                    var invenlog = new InventoryLog({
                                        employeeId: eId ,
                                        productId: item.id,
                                        quantity: ''+product.quantity,
                                        comment:'assigned to employee '+eName,
                                        inhouse:item.inHouseStock,
                                        emp:product.quantity,
                                        before:0,
                                        after:product.quantity,
                                        productName:item.name,
                                        portfolioId: user.portfolioId,
                                        createdBy :user.userId,
                                        updatedBy :user.userId,
                                        createdOn:product1.lead.dueDate
                                    });
                                    invenlog.save().then(function(invenlogsaved){
                                        var newLeadInventory = new LeadInventory({
                                            employeeId: product1.lead.employeeId ,
                                            productId: item.id,
                                            quantity: product.quantity,
                                            leadId: product1.lead.id,
                                            productName:item.name,
                                            portfolioId: user.portfolioId,
                                            createdBy :user.userId,
                                            updatedBy :user.userId,
                                            createdOn:product1.lead.dueDate,
                                            productObj:item
                                        });
                                        newLeadInventory.save().then(function(li){
                                            var inventlog = new InventoryLog({
                                                employeeId: product1.lead.employeeId,
                                                leadId:product1.lead.id,
                                                productId: item.id,
                                                quantity: '(-)'+product.quantity,
                                                comment:'used in the lead '+product1.lead.leadId+' by '+eName,
                                                inhouse:item.inHouseStock,
                                                emp:item.employeeStock,
                                                before:item.stock,
                                                after:item.stock,
                                                productName:product.productName,
                                                portfolioId: user.portfolioId,
                                                createdBy :user.userId,
                                                updatedBy :user.userId,
                                                createdOn:product1.lead.dueDate
                                            });
                                            inventlog.save().then(function(inventorysvaed){
                                                
                                            });

                                        });
                                    });
                                });
                            });
                        });
                    }
                });
            }else{
                callback(null,product1);
            }
        })(i)    
    }
    callback(null,'added');
}


exports.listcsvLeads = function (req, res) {

    var token = req.body.token || req.query.token || req.headers['x-access-token'];
    var tokenObject = JSON.parse(req.decoded);

   var filter = {
        portfolioId: tokenObject.portfolioId,
        status: 'active'
    }
    if(tokenObject.apiKey !==config.onground.apiKey){
        filter.mpId = tokenObject.mpId;
    }
    
    var input =req.query.q;
    if(input && input!=null){
    var q1 ="(?i)"+input;
    }else{
        var q1 ='';
    }

    if(req.query.fromDate && req.query.toDate && req.query.fromDate !=null && req.query.toDate !=null && req.query.toDate !='' && req.query.fromDate !=''){
        var fromDate = new Date(req.query.fromDate);
        var toDate = new Date(req.query.toDate);
        toDate.setDate(toDate.getDate()+1);

        
        frm=Date.parse(fromDate);
        to=Date.parse(toDate);

        var fDate=fromDate.getDate();
        var fYear=fromDate.getFullYear();
        var fMonth=fromDate.getMonth() + 1;
        if(frm==to){
            var ttDate=toDate.getDate();
            var tYear=toDate.getFullYear();
            var tMonth=toDate.getMonth() + 1; 
        }else{
            var ttDate=toDate.getDate();
            var tYear=toDate.getFullYear();
            var tMonth=toDate.getMonth() + 1;  
        }
    }
    if(fromDate !=undefined && toDate !=undefined && fromDate !=null && toDate !=null && !isNaN(fromDate) && !isNaN(toDate) && toDate !='' && fromDate !=''){
        Lead.orderBy({index: r.desc('createdOn')}).filter(filter).filter(function(doc){
                return doc('name').match(q1).default(false).
        or(doc('leadId').match(q1).default(false)).or(doc('leadStatus').match(q1).default(false));}).getJoin({customer: true,item: true,employee:true}).filter(r.row('createdOn').during(r.time(fYear, fMonth, fDate,"Z"), r.time(tYear, tMonth, ttDate,"Z"), {leftBound: "closed",rightBound: "closed"})).run().then(function(leads) {
           res.json({
                data: leads
            });
        }).error(handleError(res)); 
        handleError(res);
    }else{
        Lead.orderBy({index: r.desc('createdOn')}).filter(filter).filter(function(doc){
                return doc('name').match(q1).default(false).
        or(doc('leadId').match(q1).default(false)).or(doc('leadStatus').match(q1).default(false));}).getJoin({customer: true,item: true,employee:true}).run().then(function(leads) {

            res.json({
                data: leads
            });
        }).error(handleError(res)); 
        handleError(res);
    }


    //var fields = ['createdOn','leadId', 'customer.name','customer.mobile','item.name','dueDate','employee.name','address','leadStatus'];


   /* var csv = json2csv({ data: leads, fields: fields });
    fs.writeFile('file.csv', csv, function(err) {
      if (err) throw err;
      console.log('file saved');
      res.end();
    });*/  
};

function handleError(res) {
    return function(error) {
        console.log(error.message);
        return res.send(500, {error: error.message});
    }
}
