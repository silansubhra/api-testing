var thinky = require(__dirname+'/../util/thinky.js'),
    r = thinky.r,
    JobOpening = require(__dirname+'/../model/jobOpening.js'),
    Portfolio= require(__dirname+'/../model/portfolio.js');

// list jobopenings
// TODO: all filter, page size and offset, columns, sort
exports.listJobOpenings = function (req, res) {
    var count;
    var pno=1,offset=0,limit=10;


    if(req.query.psize != undefined && req.query.psize != null && !isNaN(req.query.psize)){
        limit = parseInt(req.query.psize);
    }

    if(req.query.pno != undefined && req.query.pno != null && !isNaN(req.query.pno)){
       pno =  parseInt(req.query.pno);
    }

    offset = (pno -1) * limit;

    if(req.decoded){
        var tokenObject = JSON.parse(req.decoded);
          
        JobOpening.orderBy(r.desc('createdOn')).filter({portfolioId: tokenObject.portfolioId}).skip(offset).limit(limit).run().then(function(jobopenings) {
            JobOpening.filter({portfolioId: tokenObject.portfolioId}).count().execute().then(function(total) {
                count = total;
                console.log(total);
                res.json({
                    data: jobopenings,
                    total: count,
                    pno: pno,
                    psize: limit
                });
            }).error(handleError(res));
        }).error(handleError(res));
        handleError(res);
    }else{ 
        var apiKey = req.body.apiKey || req.query.apikey || req.headers['x-api-key'];
        console.log("--"+apiKey);
        Portfolio.filter({apiKey: apiKey}).run().then(function(portfolio){
            if(portfolio && portfolio.length >0){
                var portfolioId = portfolio[0].id;
                JobOpening.orderBy(r.desc('createdOn')).filter({portfolioId: portfolioId}).skip(offset).limit(limit).run().then(function(jobopenings) {
                    JobOpening.filter({portfolioId: portfolioId}).count().execute().then(function(total) {
                        count = total;
                        console.log(total);
                        res.json({
                            data: jobopenings,
                            total: count,
                            pno: pno,
                            psize: limit
                        });
                    }).error(handleError(res));
                }).error(handleError(res));
                handleError(res);
            }else{
                res.send(404,{error: 'invalid apikey'})
            }
        }).error(handleError(res));
    }
}; 
exports.getJobOpenings = function (req, res) {
    var id = req.params.id;
    JobOpening.get(id).run().then(function(jobopening) {
     res.json({
         jobopening: jobopening
     });
    }).error(handleError(res));
};

// delete by id 
exports.deleteJobOpenings = function (req, res) {
    var id = req.params.id;
    JobOpening.get(id).delete().run().then(function(jobopenings) {
        res.json({
            status: "success"
        });
    }).error(handleError(res)); 
};

// Add user
exports.addJobOpenings = function (req, res) {
    var newJobOpening = new JobOpening(req.body);
    var user = JSON.parse(req.decoded);
    newJobOpening.createdBy =user.userId;
    newJobOpening.portfolioId=user.portfolioId;
    newJobOpening.orgId = user.orgId;
    newJobOpening.updatedBy =user.userId;
    newJobOpening.updatedOn =r.now();
    console.log(newJobOpening);
    newJobOpening.save().then(function(result) {
        res.json({
            result: result
        });
    }).error(handleError(res));
};

// update user
exports.updateJobOpenings = function (req, res) {
    var job = req.body;
    var user = JSON.parse(req.decoded);
    job.updatedBy =user.userId;
    job.updatedOn =r.now();
    JobOpening.get(job.id).update(job).then(function(result) {
        res.json({
            result: result
        });
    }).error(handleError(res));
};

function handleError(res) {
    return function(error) {
        console.log(error.message);
        return res.send(500, {error: error.message});
    }
}