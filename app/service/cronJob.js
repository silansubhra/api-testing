var cronJob = require('node-cron');
var thinky = require(__dirname+'/../util/thinky.js'),
    r = thinky.r,
    Portfolio = require(__dirname+'/../model/portfolio.js'),
    Branch = require(__dirname+'/../model/branch.js'),
    env         = require(__dirname+'/../../env'),
    config      = require(__dirname+'/../../config/' + env.name),
    emailService  = require(__dirname+'/emailservice.js'),
    Lead   = require(__dirname+'/../model/lead.js'),
    cJob   = require(__dirname+'/../model/cronjob.js'),
    EmailTemplate = require('email-templates').EmailTemplate,
    path = require('path'),
    templatesDir = path.resolve(__dirname, '..', 'templates'),
    Template = require(__dirname+'/../model/template.js'),
    moment = require('moment'),
    smsService  = require(__dirname+'/veriformmservice.js');
    var Handlebars = require('handlebars');    


 
exports.setCron = function (req,mobile,email,date,name,sd) {
	var d = date.getDate();
	var m = date.getMonth()+1;
	var y = date.getYear();
	var h = date.getHours();
	var min = date.getMinutes();

	console.log(date);
	console.log(sd);
	var obj = '1 '+min+' '+h+' '+d+' '+m+' *';

	var realHour = sd.getHours();
	var realMinutes = sd.getMinutes();

	console.log(obj);
	var newcJob = new cJob({
		mobile: mobile,
	    scheduleTime: obj,
	    message: name+' at '+realHour+':'+realMinutes+' .Please visit zinetgo.com for more details',
	    reqobj: null
	});
	newcJob.save().then(function(cj){
		var myJob = cronJob.schedule(cj.scheduleTime, function(){
			sendsmsjob(cj.reqobj, config.sms.msg91.authKey, config.sms.msg91.senderId, cj.mobile,cj.message,cj.id);
		});
		myJob.start();

	});
};

exports.sendWeeklyReport = function () {
	console.log('weekly-report');
	var filter = {
		status:'active'
	}
	var today = new Date();
	var priorDate = new Date(new Date().setDate(today.getDate() - 7));
	var previous = new Date(priorDate.getTime() - 7*24*60*60*1000);

	var tdDate=today.getDate();
    var tdMonth=today.getMonth() +1;
    var tdYear =today.getFullYear();

    var pDate = priorDate.getDate();
    var pMonth = priorDate.getMonth() +1;
    var pYear = priorDate.getFullYear();

    var lDate = previous.getDate();
    var lMonth = previous.getMonth() +1;
    var lYear = previous.getFullYear();

    var dt = moment(today).format("DD-MMM");
    var dt1 = moment(priorDate).format("DD-MMM");
    var date = dt1+' to '+dt;

    var date1 = {
    	pDate :pDate,
    	pMonth : pMonth,
    	pYear :pYear,

    	tdDate :tdDate,
    	tdMonth :tdMonth,
    	tdYear :tdYear
    }

    var date2 = {

    	tdDate :pDate,
    	tdMonth :pMonth,
    	tdYear :pYear,

    	pDate :lDate,
    	pMonth : lMonth,
    	pYear :lYear
    }

    console.log(JSON.stringify(date2));

    //var obj = '0 0 9 1 * monday';

    var obj = '0 0 9 * * monday';
    var myJob = cronJob.schedule(obj, function(){
    	UserProfile.filter({apiKey:'6ec1c63b-08a6-444f-ae6e-72a97d47c079',role:'admin',status: "active"}).hasFields('email').run().then(function(up){
    		if(up && up.length >0){
    			var tab = "";
    			var q1 = "";
    			var q2 = "";
    			var f = "";
    			for(var i =0; i<= up.length;i++){
    				(function(j){
	    				if(up[i].roles.indexOf('job-mgmt')>-1){
	    					var portfolio = up[i];
	    					var email = up[i].email;
	    						var ld = {};
		    					var psCallback = function(error,data){
		    						ld.psale = data;
		    						if(ld.plead === 0){
						        		ld.lper = ld.leads*100;
						  				ld.jper = ld.jobs*100;
						  				ld.sper = ld.sales*100;
						  			}else if(ld.leads === 0){
						  				ld.lper = '-'+(ld.plead*100);
						  				if(ld.jobs === 0){
						  				  ld.jper = '-'+(ld.pjob*100);
						  				}else{
						  					ld.jper = Math.round(((ld.jobs - ld.pjob)/jobs)*100);
						  				}if(ld.sales === 0){
						  					ld.sper = '-'+(ld.psale*100);
						  				}else{
						  					ld.sper = Math.round(((ld.sales - ld.psale)/ld.sales)*100);
						  				}
						        	}else{
						  				if(ld.jobs === 0){
						  				  ld.jper = '-'+(ld.pjob*100);
						  				}else{
						  					ld.jper = Math.round(((ld.jobs - ld.pjob)/ld.jobs)*100);
						  				}if(ld.sales === 0){
						  					ld.sper = '-'+(ld.psale*100);
						  				}else{
						  					ld.sper = Math.round(((ld.sales - ld.psale)/ld.sales)*100);
						  				}
						        		ld.lper = Math.round(((ld.leads - ld.plead)/ld.leads)*100);
						        	}
						        	ld.date = date;

						        	var html1 = Handlebars.compile(fs.readFileSync(path.join(templatesDir, 'weekly-report/html.hbs'), 'utf8'));
                                    var subject1 = Handlebars.compile(fs.readFileSync(path.join(templatesDir, 'weekly-report/subject.hbs'), 'utf8'));
                                    var text1 = Handlebars.compile(fs.readFileSync(path.join(templatesDir, 'weekly-report/text.hbs'), 'utf8'));

                                    var html = html1(ld);
                                    var text = text1(ld);
                                    var subject = subject1(ld);
						        	
						        	emailService.sendEmail(config.smtp.gmail, {
		                                from: '"Zinetgo" <support@zinetgo.com>', // sender address
		                                to: email, // list of receivers
		                                subject: subject, // Subject line
		                                text: text,
		                                html: html
		                            });
		    					}

		    					var plCallback = function(error,data){
		    						filter.portfolioId = portfolio.portfolioId;
		    						ld.plead = data;
		    						countPrice(filter,date2,psCallback)
		    					}
		    					var pjCallback = function(error,data){
		    						filter.portfolioId = portfolio.portfolioId;
		    						ld.pjob = data;
		    						countleads(filter,date2,plCallback)
		    					}
		    					var lCallback = function(error,data){
		    						filter.portfolioId = portfolio.portfolioId;
		    						ld.sales = data;
		    						countjobs(filter,date2,pjCallback)
		    					}
		    					var pCallback = function(error,data){
		    						filter.portfolioId = portfolio.portfolioId;
		    						ld.leads = data;
		    						countPrice(filter,date1,lCallback)
		    					}
		    					var meCallback = function(error,data){
		    						filter.portfolioId = portfolio.portfolioId;
		    						ld.jobs = data;
		    						countleads(filter,date1,pCallback)
		    					}
		    					filter.portfolioId = portfolio.portfolioId;
		    					countjobs(filter,date1,meCallback);
			        		
				        	/*ld.date = date;
				        	ld.leads = leads;
				        	ld.job = jobs;
				        	ld.sales = sales;
				        	ld.plead = lleads;
				        	ld.pjob = ljobs;
				        	ld.psale = lsales;*/
				        	
	    					/*tab = "lead";
	    					q1 = "CONVERTED";
	    					q2 = "PAID";
	    					f = "leadStatus";*/
	    				}/*else if(up[i].indexOf('order-mgmt')>-1){
    					tab = "order";
    					q1 = "open";
    					q2 = "delivered";
    					f="orderStatus"
	    				}else{
	    					tab = "blog";
	    					q1 = "Pending";
	    					q2 = "delivered";
	    				}*/
	    			})(i);
    			};
		    }else{
		    	return;
		    }
		});
	  //smsService.sendSms(req, config.sms.msg91.authKey, config.sms.msg91.senderId, mobile,name+' at '+h+':'+min+' .Please visit zinetgo.com for more details');
	});
	myJob.start();
	console.log('done at '+obj);
};

exports.sendMonthlyReport = function () {
	console.log('Monthly-report');
	var filter = {
		status:'active'
	}
	var today = new Date();
	
	var priorDate = new Date(new Date().setDate(today.getDate() - 30));

    priorDate = new Date(priorDate.setHours(0,0,0,0));

	var previous = new Date(priorDate.getTime() - 30*24*60*60*1000);

	var tdDate=today.getDate();
    var tdMonth=today.getMonth() +1;
    var tdYear =today.getFullYear();

    var pDate = priorDate.getDate();
    var pMonth = priorDate.getMonth() +1;
    var pYear = priorDate.getFullYear();

    var lDate = previous.getDate();
    var lMonth = previous.getMonth() +1;
    var lYear = previous.getFullYear();

    var dt = moment(today).format("DD-MMM");
    var dt1 = moment(priorDate).format("DD-MMM");
    var date = dt1+' to '+dt;

    var date1 = {
    	pDate :pDate,
    	pMonth : pMonth,
    	pYear :pYear,

    	tdDate :tdDate,
    	tdMonth :tdMonth,
    	tdYear :tdYear
    }

    var date2 = {

    	tdDate :pDate,
    	tdMonth :pMonth,
    	tdYear :pYear,

    	pDate :lDate,
    	pMonth : lMonth,
    	pYear :lYear
    }

    var obj = '0 0 9 1 * *';
    var myJob = cronJob.schedule(obj, function(){
    	UserProfile.filter({apiKey:'6ec1c63b-08a6-444f-ae6e-72a97d47c079',role:'admin',status: "active"}).hasFields('email').run().then(function(up){
    		if(up && up.length >0){
    			var tab = "";
    			var q1 = "";
    			var q2 = "";
    			var f = "";
    			for(var i =0; i< up.length;i++){
    				(function(j){
	    				if(up[i].roles.indexOf('job-mgmt')>-1){
	    					var portfolio = up[i];
	    					var email = up[i].email;
	    					console.log(email);
	    					var ld = {};
	    					var psCallback = function(error,data){
	    						ld.psale = data;
	    						if(ld.plead === 0){
					        		ld.lper = ld.leads*100;
					  				ld.jper = ld.jobs*100;
					  				ld.sper = ld.sales*100;
					  			}else if(ld.leads === 0){
					  				ld.lper = '-'+(ld.plead*100);
					  				if(ld.jobs === 0){
					  				  ld.jper = '-'+(ld.pjob*100);
					  				}else{
					  					ld.jper = Math.round(((ld.jobs - ld.pjob)/jobs)*100);
					  				}if(ld.sales === 0){
					  					ld.sper = '-'+(ld.psale*100);
					  				}else{
					  					ld.sper = Math.round(((ld.sales - ld.psale)/ld.sales)*100);
					  				}
					        	}else{
					  				if(ld.jobs === 0){
					  				  ld.jper = '-'+(ld.pjob*100);
					  				}else{
					  					ld.jper = Math.round(((ld.jobs - ld.pjob)/ld.jobs)*100);
					  				}if(ld.sales === 0){
					  					ld.sper = '-'+(ld.psale*100);
					  				}else{
					  					ld.sper = Math.round(((ld.sales - ld.psale)/ld.sales)*100);
					  				}
					        		ld.lper = Math.round(((ld.leads - ld.plead)/ld.leads)*100);
					        	}
					        	ld.date = date;
					        	
					        	var html1 = Handlebars.compile(fs.readFileSync(path.join(templatesDir, 'monthly-report/html.hbs'), 'utf8'));
                                var subject1 = Handlebars.compile(fs.readFileSync(path.join(templatesDir, 'monthly-report/subject.hbs'), 'utf8'));
                                var text1 = Handlebars.compile(fs.readFileSync(path.join(templatesDir, 'monthly-report/text.hbs'), 'utf8'));

                                var html = html1(ld);
                                var text = text1(ld);
                                var subject = subject1(ld);
					        	
					        	emailService.sendEmail(config.smtp.gmail, {
	                                from: '"Zinetgo" <support@zinetgo.com>', // sender address
	                                to: email, // list of receivers
	                                subject: subject, // Subject line
	                                text: text,
	                                html: html
	                            });
	    					}

	    					var plCallback = function(error,data){
	    						filter.portfolioId = portfolio.portfolioId;
	    						ld.plead = data;
	    						countPrice(filter,date2,psCallback)
	    					}
	    					var pjCallback = function(error,data){
	    						filter.portfolioId = portfolio.portfolioId;
	    						ld.pjob = data;
	    						countleads(filter,date2,plCallback)
	    					}
	    					var lCallback = function(error,data){
	    						filter.portfolioId = portfolio.portfolioId;
	    						ld.sales = data;
	    						countjobs(filter,date2,pjCallback)
	    					}
	    					var pCallback = function(error,data){
	    						filter.portfolioId = portfolio.portfolioId;
	    						ld.leads = data;
	    						countPrice(filter,date1,lCallback)
	    					}
	    					var meCallback = function(error,data){
	    						filter.portfolioId = portfolio.portfolioId;
	    						ld.jobs = data;
	    						countleads(filter,date1,pCallback)
	    					}
	    					filter.portfolioId = portfolio.portfolioId;
	    					countjobs(filter,date1,meCallback);
	    				}
	    			})(i);
    			};
		    }else{
		    	return;
		    }
		});
	  //smsService.sendSms(req, config.sms.msg91.authKey, config.sms.msg91.senderId, mobile,name+' at '+h+':'+min+' .Please visit zinetgo.com for more details');
	});
	myJob.start();
	console.log('done at '+obj);
};

var countjobs = function(filter,date1,callback){
	console.log(JSON.stringify(date1));
	r.table("lead").orderBy({index: r.desc('createdOn')}).filter(filter).filter(function(doc){
        return doc('leadStatus').match('CONVERTED').default(false).or(doc('leadStatus').match('PAID').default(false));}).filter(r.row('createdOn').during(r.time(date1.pYear, date1.pMonth, date1.pDate,"Z"), r.time(date1.tdYear, date1.tdMonth, date1.tdDate,"Z"), {leftBound: "closed",rightBound: "closed"})).count().run().then(function(jobs) {
        	console.log(jobs);
        	callback(null,jobs);
    })
}

var countleads = function(filter,date1,callback){
	console.log(JSON.stringify(date1));
	r.table("lead").orderBy({index: r.desc('createdOn')}).filter(filter).filter(r.row('createdOn').during(r.time(date1.pYear, date1.pMonth, date1.pDate,"Z"), r.time(date1.tdYear, date1.tdMonth, date1.tdDate,"Z"), {leftBound: "closed",rightBound: "closed"})).count().run().then(function(leads) {
        console.log(leads);
		callback(null,leads);
    })
}

var countPrice = function(filter,date1,callback){
	console.log(JSON.stringify(date1));
	r.table("lead").orderBy({index: r.desc('createdOn')}).filter(filter).filter(function(doc){
        return doc('leadStatus').match('CONVERTED').default(false).or(doc('leadStatus').match('PAID').default(false));}).filter(r.row('createdOn').during(r.time(date1.pYear, date1.pMonth, date1.pDate,"Z"), r.time(date1.tdYear, date1.tdMonth, date1.tdDate,"Z"), {leftBound: "closed",rightBound: "closed"})).sum('price').run().then(function(sales) {
        	console.log(sales);
        	callback(null,sales);
    })
}

exports.setCronJob = function(){
	cJob.filter({}).run().then(function(jobs){
		if(jobs && jobs.length>0){
			for (var i = 0; i <jobs.length; i++) {
				(function(j){
					var reobj = jobs[i].reqobj;
					var mob = jobs[i].mobile;
					var id= jobs[i].id;
					var message = jobs[i].message;
					var schedule = jobs[i].scheduleTime;
					var myJob = cronJob.schedule(schedule, function(){
						sendsmsjob(reobj, config.sms.msg91.authKey, config.sms.msg91.senderId, mob,message,id);
					});
					myJob.start();
				})(i);
			}
		}else{
			return;
		}
	})
}

var sendsmsjob = function(req,authKey,senderId,mobile,message,id){
	smsService.sendSms(req, authKey, senderId, mobile,message);
	cJob.get(id).delete().run().then(function(result){

	});
}
