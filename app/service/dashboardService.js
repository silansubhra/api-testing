var thinky = require(__dirname+'/../util/thinky.js'),

    r = thinky.r,

    Order = require(__dirname+'/../model/order.js'),

    Customer = require(__dirname+'/../model/customer.js'),

    env  = require(__dirname+'/../../env'),

    Blog = require(__dirname+'/../model/blog.js'),

    numeral = require('numeral'),

    Org = require(__dirname+'/../model/org.js'),

    Product = require(__dirname+'/../model/item.js');


exports.getDashboardData = function (req,res) {

  var tokenObject = JSON.parse(req.decoded);

  var orgId = tokenObject.orgId;


    var today =new Date();

    var fromDate = new Date(new Date(req.query.fromDate).setHours(0,0,0,0));
    var toDate = new Date(new Date(req.query.toDate).setHours(0,0,0,0));

    toDate.setDate(toDate.getDate()+1);
    var priorDate = new Date(new Date().setDate(today.getDate() - 30));
    priorDate = new Date(priorDate.setHours(0,0,0,0));

    today.setDate(today.getDate()+1);

    

    frm=Date.parse(fromDate);

    to=Date.parse(toDate);

    

    console.log(frm==to);


    var tdDate=today.getDate();

    var tdMonth=today.getMonth() +1;

    var tdYear =today.getFullYear();


    var pDate = priorDate.getDate();

    var pMonth = priorDate.getMonth() +1;

    var pYear = priorDate.getFullYear();

    var fDate=fromDate.getDate();

    var fYear=fromDate.getFullYear();

    var fMonth=fromDate.getMonth() + 1;

    if(frm==to){

        var ttDate=toDate.getDate();

        var tYear=toDate.getFullYear();

        var tMonth=toDate.getMonth() + 1; 

    }else{

        var ttDate=toDate.getDate();

        var tYear=toDate.getFullYear();

        var tMonth=toDate.getMonth() + 1;  

    }

    
  Org.filter({id:tokenObject.orgId}).run().then(function(og){

        if(og && og.length>0){

            var orgType = og[0].orgType;

            if(orgType == 'business'){
                var roles =tokenObject.roles;

                if(roles.indexOf('job-mgmt')>-1){
                    var filter = {
                        portfolioId:tokenObject.portfolioId,
                        status:'active'
                    }
                    var filter1 ={
                        portfolioId:tokenObject.portfolioId,
                        status:'active',
                        leadStatus:'CONVERTED'
                    }
                    if(tokenObject.mpId && tokenObject.mpId != ''){
                        filter.mpId = tokenObject.mpId;
                        filter1.mpId = tokenObject.mpId;
                    }
                    if(fromDate !=undefined && toDate !=undefined && fromDate !=null && toDate !=null && !isNaN(fromDate) && !isNaN(toDate)){
                        Lead.orderBy({index: r.desc('createdOn')}).filter(filter).filter(r.row('createdOn').during(r.time(fYear, fMonth, fDate,"Z"), r.time(tYear, tMonth, ttDate,"Z"), {rightBound: "closed"})).count().execute().then(function(leads) {

                            Product.orderBy({index: r.desc('createdOn')}).filter({portfolioId: tokenObject.portfolioId,type: 'product'}).count().execute().then(function(products) {

                                Lead.orderBy({index: r.desc('createdOn')}).filter(filter1).filter(r.row('createdOn').during(r.time(fYear, fMonth, fDate,"Z"), r.time(tYear, tMonth, ttDate,"Z"), {rightBound: "closed"})).count().execute().then(function(jobs) {

                                    Lead.orderBy({index: r.desc('createdOn')}).filter(filter1).hasFields('price').filter(r.row('createdOn').during(r.time(fYear, fMonth, fDate,"Z"), r.time(tYear, tMonth, ttDate,"Z"), {rightBound: "closed"})).sum("price").execute().then(function(sales) {
                                        if(jobs <1){
                                        var average = sales/1;
                                        }else{
                                            var average = sales/jobs;
                                        }
                                        res.json({
                                            'status': 'ok',
                                            'leads': numeral(leads).format('0,0'),
                                            'products': numeral(products).format('0,0'),
                                            'jobs':  numeral(jobs).format('0,0'),
                                            'sales':  numeral(sales).format('0,0.0'),
                                            'average': numeral(average).format('0,0.0')
                                        });   
                                    }); 
                                });
                            });
                        });
                    }else{  

                        Lead.orderBy({index: r.desc('createdOn')}).filter(filter).filter(r.row('createdOn').during(r.time(pYear, pMonth, pDate,"Z"), r.time(tdYear, tdMonth, tdDate,"Z"), {rightBound: "closed"})).count().execute().then(function(leads) {

                            Product.orderBy({index: r.desc('createdOn')}).filter({portfolioId: tokenObject.portfolioId,type: 'product'}).count().execute().then(function(products) {

                                Lead.orderBy({index: r.desc('createdOn')}).filter(filter1).filter(r.row('createdOn').during(r.time(pYear, pMonth, pDate,"Z"), r.time(tdYear, tdMonth, tdDate,"Z"), {rightBound: "closed"})).count().execute().then(function(jobs) {

                                    Lead.orderBy({index: r.desc('createdOn')}).filter(filter1).hasFields('price').filter(r.row('createdOn').during(r.time(pYear, pMonth, pDate,"Z"), r.time(tdYear, tdMonth, tdDate,"Z"), {rightBound: "closed"})).sum("price").execute().then(function(sales) {

                                        if(jobs <1){
                                            var average = sales/1;
                                        }else{
                                            var average = sales/jobs;
                                        }
                                        res.json({

                                            'status': 'ok',

                                            'leads': numeral(leads).format('0,0'),

                                            'products': numeral(products).format('0,0'),

                                            'jobs':  numeral(jobs).format('0,0'),

                                            'sales':  numeral(sales).format('0,0.0'),

                                            'average': numeral(average).format('0,0.0')

                                        });
                                    }); 
                                });
                            });
                        });
                    }
                }else if(roles.indexOf('order-mgmt')>-1){

                    var filter = {
                        portfolioId:tokenObject.portfolioId,
                        status:'active'
                    }
                    var filter1 ={
                        portfolioId:tokenObject.portfolioId,
                        status:'active',
                        leadStatus:'CONVERTED'
                    }
                    if(tokenObject.mpId && tokenObject.mpId != ''){
                        filter.mpId = tokenObject.mpId;
                        filter1.mpId = tokenObject.mpId;
                    }


                    if(fromDate !=undefined && toDate !=undefined && fromDate !=null && toDate !=null && !isNaN(fromDate) && !isNaN(toDate)){

                        Order.orderBy({index: r.desc('orderDate')}).filter(filter).filter(r.row('orderDate').during(r.time(fYear, fMonth, fDate,"Z"), r.time(tYear, tMonth, ttDate,"Z"), {rightBound: "closed"})).count().execute().then(function(orders) {

                          Order.orderBy({index: r.desc('orderDate')}).filter(filter).filter(r.row('orderDate').during(r.time(fYear, fMonth, fDate,"Z"), r.time(tYear, tMonth, ttDate,"Z"), {rightBound: "closed"})).sum("grossTotal").execute().then(function(sales) {

                            console.log('orders: '+ orders);
                            console.log('sales: '+ sales);

                            if(orders <1){

                            var average = sales/1;

                            }else{
                                var average = sales/orders;
                            }
                            res.json({
                                'status': 'ok',
                                'orders': numeral(orders).format('0,0'),
                                'sales':  numeral(sales).format('0,0.0'),
                                'average': numeral(average).format('0,0.0')
                            });
                          });
                        });
                    }else{  

                        Order.orderBy({index: r.desc('orderDate')}).filter(filter).filter(r.row('orderDate').during(r.time(pYear, pMonth, pDate,"Z"), r.time(tdYear, tdMonth, tdDate,"Z"), {rightBound: "closed"})).count().execute().then(function(orders) {

                          Order.orderBy({index: r.desc('orderDate')}).filter(filter).filter(r.row('orderDate').during(r.time(pYear, pMonth, pDate,"Z"), r.time(tdYear, tdMonth, tdDate,"Z"), {rightBound: "closed"})).sum("grossTotal").execute().then(function(sales) {

                                if(orders <1){

                                var average = sales/1;

                                }else{
                                    var average = sales/orders;
                                }
                                console.log('average: '+ average);
                                res.json({
                                    'status': 'ok',
                                    'orders': numeral(orders).format('0,0'),
                                    'sales':  numeral(sales).format('0,0.0'),
                                    'average': numeral(average).format('0,0.0')
                                });
                            });
                        });
                    }
                }else{
                    res.json({
                        'status': 'ok',
                        'leads': '0,0',
                        'products': '0,0',
                        'jobs':  '0,0',
                        'sales':  '0,0.0',
                        'average':'0,0.0'
                    }); 
                }
            }else if (orgType =='blog'){
                var roles =tokenObject.roles;
                var filter = {
                    portfolioId: tokenObject.portfolioId
                };
                var filter1 = {
                   portfolioId: tokenObject.portfolioId,
                   approvedStatus:'Approved'
                };
                if(roles.indexOf('Author')>-1){
                    filter.createdBy = tokenObject.userId;
                    filter1.createdBy = tokenObject.userId;
                }
                if(fromDate !=undefined && toDate !=undefined && fromDate !=null && toDate !=null && !isNaN(fromDate) && !isNaN(toDate)){

                    Blog.orderBy({index: r.desc('createdOn')}).filter(filter).filter(r.row('createdOn').during(r.time(fYear, fMonth, fDate,"Z"), r.time(tYear, tMonth, ttDate,"Z"), {rightBound: "closed"})).count().execute().then(function(blogs) {

                      Blog.orderBy({index: r.desc('createdOn')}).filter(filter1).filter(r.row('createdOn').during(r.time(fYear, fMonth, fDate,"Z"), r.time(tYear, tMonth, ttDate,"Z"), {rightBound: "closed"})).count().execute().then(function(approved) {
                        res.json({
                            'status': 'ok',
                            'blogs': blogs,
                            'approved': approved
                        });
                      });
                    });
                }else {
                    Blog.orderBy({index: r.desc('createdOn')}).filter(filter).filter(r.row('createdOn').during(r.time(pYear, pMonth, pDate,"Z"), r.time(tdYear, tdMonth, tdDate,"Z"), {rightBound: "closed"})).count().execute().then(function(blogs) {
                      Blog.orderBy({index: r.desc('createdOn')}).filter(filter1).filter(r.row('createdOn').during(r.time(pYear, pMonth, pDate,"Z"), r.time(tdYear, tdMonth, tdDate,"Z"), {rightBound: "closed"})).count().execute().then(function(approved) {
                            res.json({
                                'status': 'ok',
                                'blogs': blogs,
                                'approved': approved
                            });

                        });

                    });
                }
            }else{
                return res.send(500,{error:'not a valid user'})
            }
        }
    });
};
