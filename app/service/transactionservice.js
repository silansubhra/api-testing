var thinky = require(__dirname+'/../util/thinky.js'),
    r = thinky.r,
    Transaction = require(__dirname+'/../model/transaction.js'),
    Payment = require(__dirname+'/../model/payment.js');

// list transactions
// TODO: all filter, page size and offset, columns, sort
exports.listTransactions = function (req, res) {
    var count;
    var pno=1,offset=0,limit=10;
    if(req.query.psize != undefined && req.query.psize != null && !isNaN(req.query.psize)){
        limit = parseInt(req.query.psize);
    }

    if(req.query.pno != undefined && req.query.pno != null && !isNaN(req.query.pno)){
       pno =  parseInt(req.query.pno);
    }

    offset = (pno -1) * limit;

    var token = req.body.token || req.query.token || req.headers['x-access-token'];
    var tokenObject = JSON.parse(req.decoded);

        Transaction.filter({portfolioId: tokenObject.portfolioId}).count().execute().then(function(total) {
            count = total;
            console.log(total);
        });


        Transaction.orderBy({index: r.desc('createdOn')}).filter({portfolioId: tokenObject.portfolioId}).skip(offset).limit(limit).run().then(function(transactions) {
            res.json({
                data: transactions,
                total: count,
                pno: pno,
                psize: limit
            });
        }).error(handleError(res));
   
    handleError(res);
};

// get by id
exports.getTransaction = function (req, res) {
    var id = req.params.id;
    Transaction.get(id).getJoin({branch: true,payment: true}).run().then(function(transaction) {
     res.json(transaction);
    }).error(handleError(res));
};


// delete by id
exports.deleteTransaction = function (req, res) {
    var id = req.params.id;
    Transaction.get(id).delete().run().then(function(branch) {
        res.json({
            status: "success"
        });
    }).error(handleError(res));
};

// Add user
exports.addTransaction = function (req, res) {
    var paymentId=req.params.paymentId;
    if(paymentId && paymentId!=null){
        var newTransaction = new Transaction(req.body);
        var user = JSON.parse(req.decoded);
    newTransaction.portfolioId=user.portfolioId;
    newTransaction.createdBy =user.userId;
    newTransaction.updatedBy =user.userId;
    newTransaction.updatedOn =r.now();
    console.log(newTransaction);
    newTransaction.save().then(function(result) {
        res.json({
            result: result
        });
    }).error(handleError(res));
}else{
    return res.send(500, {error: "invalid payment"});
}

};

// update user
exports.updateTransaction = function (req, res) {
    Transaction.get(req.body.id).update(req.body).then(function(result) {
        var user = JSON.parse(req.decoded);
        result.updatedBy =user.userId;
        result.updatedOn =r.now();
        res.json({
            result: result
        });
    }).error(handleError(res));
};

function handleError(res) {
    return function(error) {
        console.log(error.message);
        return res.send(500, {error: error.message});
    }
}
