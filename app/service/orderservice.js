var thinky = require(__dirname+'/../util/thinky.js'),
    r = thinky.r,
    Order = require(__dirname+'/../model/order.js'),
    Org = require(__dirname+'/../model/org.js'),
    Branch = require(__dirname+'/../model/branch.js'),
    Customer = require(__dirname+'/../model/customer.js'),
    UserProfile = require(__dirname+'/../model/userProfile.js'),
    Converter = require("csvtojson").Converter,
    converter = new Converter({}),
    fs = require('fs'),
    sync = require('synchronize'),
    Portfolio = require(__dirname+'/../model/portfolio.js'),
    Payment = require(__dirname+'/../model/payment.js'),
    emailService = require(__dirname+'/emailservice.js'),
    Preference = require(__dirname+'/../model/preference.js'),
    smsService  = require(__dirname+'/veriformmservice.js'),
    env = require(__dirname+'/../../env'),
    config = require(__dirname+'/../../config/'+ env.name),
    Insta = require('instamojo-nodejs');
    
// list order
// TODO: all filter, page size and offset, columns, sort
exports.listOrders = function (req, res) {
  var count;
  var pno=1,offset=0,limit=10;
  if(req.query.psize != undefined && req.query.psize != null && !isNaN(req.query.psize)){
      limit = parseInt(req.query.psize);
  }

  if(req.query.pno != undefined && req.query.pno != null && !isNaN(req.query.pno)){
     pno =  parseInt(req.query.pno);
  }

  offset = (pno -1) * limit;

  var token = req.body.token || req.query.token || req.headers['x-access-token'];
  var tokenObject = JSON.parse(req.decoded);
  var today =new Date();
  var fromDate =new Date(req.query.fromDate);
  var toDate =new Date(req.query.toDate);
  var priorDate = new Date(new Date().setDate(today.getDate() - 30));
  today.setDate(today.getDate()+1);
  var customerId = req.query.customerId;
  
  frm=Date.parse(fromDate);
  to=Date.parse(toDate);
  
  console.log(frm==to);

  var tdDate=today.getDate();
  var tdMonth=today.getMonth() +1;
  var tdYear =today.getFullYear();

  var pDate = priorDate.getDate();
  var pMonth = priorDate.getMonth() +1;
  var pYear = priorDate.getFullYear();
  var fDate=fromDate.getDate();
  var fYear=fromDate.getFullYear();
  var fMonth=fromDate.getMonth() + 1;
  if(frm==to){
      var ttDate=toDate.getDate();
      console.log("ttDate"+ttDate);
      var tYear=toDate.getFullYear();
      var tMonth=toDate.getMonth() + 1; 
  }else{
      toDate.setDate(toDate.getDate()+1);
      var ttDate=toDate.getDate();
      var tYear=toDate.getFullYear();
      var tMonth=toDate.getMonth() + 1;  
  }
  if(tokenObject.roles.indexOf('admin')>-1){
    var filter ={
        portfolioId: tokenObject.portfolioId,
        status: 'active'
    }
  }else if(tokenObject.roles.indexOf('user')>-1){
    var filter ={
      createdBy: tokenObject.userId,
        status: 'active',
        portfolioId: tokenObject.portfolioId
    }
  }

  if(customerId !=null && customerId !=undefined){
      filter.customerId =customerId;
  }
  
  console.log("filter"+JSON.stringify(filter));

  

 if(fromDate !=undefined && toDate !=undefined && fromDate !=null && toDate !=null && !isNaN(fromDate) && !isNaN(toDate)){
  Order.orderBy({index: r.desc('orderDate')}).filter(filter).getJoin({portfolio: true,customer: true,item: true,payment: true}).filter(r.row('orderDate').date().during(r.time(fYear, fMonth, fDate,"Z"), r.time(tYear, tMonth, ttDate,"Z"), {rightBound: "closed"})).skip(offset).limit(limit).run().then(function(orders) {
   r.table("order").filter(filter).filter(r.row('orderDate').date().during(r.time(fYear, fMonth, fDate,"Z"), r.time(tYear, tMonth, ttDate,"Z"), {rightBound: "closed"})).count().run().then(function(total) {
    res.json({
        data: orders,
        total: (total!=undefined?total:0),
        pno: pno,
        psize: limit
    });
  });    
  }).error(handleError(res)); 
  handleError(res);
  }else{
    Order.orderBy({index: r.desc('orderDate')}).filter(filter).getJoin({portfolio: true,customer: true,item: true,payment: true}).filter(r.row('orderDate').date().during(r.time(pYear, pMonth, pDate,"Z"), r.time(tdYear, tdMonth, tdDate,"Z"), {rightBound: "closed"})).skip(offset).limit(limit).run().then(function(orders) {
       r.table("order").filter(filter).filter(r.row('orderDate').date().during(r.time(pYear, pMonth, pDate,"Z"), r.time(tdYear, tdMonth, tdDate,"Z"), {rightBound: "closed"})).count().run().then(function(total) {
        res.json({
            data: orders,
            total: (total!=undefined?total:0),
            pno: pno,
            psize: limit
        });
    });    
    }).error(handleError(res)); 
    handleError(res);
  } 
        
};

// get by id
exports.getOrder = function (req, res) {
    var id = req.params.id;
    Order.get(id).getJoin({item: true,payment: true,customer:true}).run().then(function(order) {
     res.json(order);
    }).error(handleError(res));
};


// delete by id
exports.deleteOrder = function (req, res) {
    var id = req.params.id;
    Order.get(id).delete().run().then(function(order) {
        res.json({
            status: "success"
        });
    }).error(handleError(res));
};

exports.bulkDeleteOrder = function (req, res) {
    var ids = req.query.ids;
    console.log("ids"+ids);
    if(ids && ids != null){
        var idsArray = ids.split(",");
        for(var i=0;i<idsArray.length;i++){
            var ordId =idsArray[i];
            console.log("ord"+ordId);
            var token = req.body.token || req.query.token || req.headers['x-access-token'];
            var tokenObject = JSON.parse(req.decoded);
            Order.filter({portfolioId: tokenObject.portfolioId,id: ordId,status: 'active'}).run().then(function(ordr){
                if(ordr && ordr>0){
                    Order.get(ordr.id).update({status:'inactive'}).run().then(function(branch) {
                        
                    }).error(handleError(res));
                }
            });
        }
        res.json({
              status: "success"
          });
    }else{
        res.send(404, {error: 'Select a order'});
    }  
};


// Add order
exports.addOrder = function (req, res) {

  var tokenObject = JSON.parse(req.decoded);
  var model = new Order(req.body);
  model.createdOn = {$reql_type$: "TIME",epoch_time: Math.round(new Date().getTime()/1000.0),timezone:"+05:30"};
  model.orgId = tokenObject.orgId;
  model.branchId = tokenObject.branchId;
  model.createdBy =tokenObject.userId;
  model.portfolioId=tokenObject.portfolioId;
  model.updatedBy =tokenObject.userId;
  model.updatedOn =r.now();
  /*var orderDate = new Date(req.body.orderDate);
  model.orderDate=orderDate.setDate(orderDate.getDate()+1);*/
  var left =req.body.grossTotal - req.body.amountPaid;
  if(left>0){
    model.amountDue = left;
  };
  Order.orderBy({index: r.desc('rank')}).filter({portfolioId: tokenObject.portfolioId}).run().then(function(ord){
    if(ord && ord.length>0){
      nRank = ord[0].rank;
      model.rank = nRank+1;
    }else{
      model.rank =1;
    }
  
    var str = "" + model.rank;
    var pad = "0000";
    Portfolio.get(tokenObject.portfolioId).run().then(function(portfolio){
      pName= portfolio.name;
      var code=pName.substring(0, 3).toUpperCase();
      console.log("code--"+code);
      model.orderId =code+'-'+'OD'+'-'+pad.substring(0, pad.length - str.length) + str;
      
      var custId = null;
      if(req.body.customerId && req.body.customerId !=null){
        custId = req.body.customerId;
      }else{
        custId = tokenObject.userId;
      }
      model.customerId =custId;
        console.log("customerId-----"+custId);
        console.log("portfolioId-----"+tokenObject.portfolioId); 
      Customer.filter({portfolioId:tokenObject.portfolioId,id:custId}).run().then(function(cust) {
          if(cust && cust.length > 0){  
            //exists
            if(model.mobile && model.mobile !=null && !isNaN(model.mobile)){
              var mob = parseInt(model.mobile);
              model.mobile = mob;
            }else{
              var mob = parseInt(cust[0].mobile);
              model.mobile = mob;
            }
            model.save().then(function(result) {
              console.log("cust[0].id"+cust[0].id);
              if(cust[0].id){
                cust[0].createdBy=tokenObject.userId;
                cust[0].portfolioId=tokenObject.portfolioId;


                var stats = cust[0].stats;
                if(stats && stats != null){
                  if(stats.gmv && stats.gmv != null){
                    stats.gmv = stats.gmv + model.grossTotal;
                  }else{
                    stats.gmv = stats.gmv + model.grossTotal;
                  }

                  if(stats.orders && stats.orders != null){
                    stats.orders = stats.orders + 1;
                  }else{
                    stats.orders = 1;
                  }
                }else{
                  stats = {
                    gmv : model.grossTotal,
                    orders : 1
                  }
                }
                
                Customer.get(cust[0].id).update({stats:stats}).then(function(cust1) {
                  console.log("1---" + cust1);
                });//.error(handleError(res));
              }
                var newPayment = new Payment({
                  orgId: result.orgId,
                  portfolioId: result.portfolioId,
                  createdBy: result.createdBy,
                  updatedBy :result.createdBy,
                  orderId: result.orderId,
                  amount: result.grossTotal,
                  paymentDate: result.orderDate,
                  paymentMode: result.paymentMode,
                  profileId: tokenObject.id
                });
                if(model.usedWalletBal){
                  newPayment.walletUsed = parseInt(model.usedWalletBal);
                }
                newPayment.save().then(function(payment){
                  if(result.paymentMode ==='COD' || result.paymentMode ==='Neft'){
                    if(model.usedWalletBal){
                      UserProfile.get(tokenObject.id).run().then(function(up){
                        var newWallet = parseInt(up.wallet) - parseInt(model.usedWalletBal);
                        UserProfile.update({wallet: newWallet}).then(function(updated){
                          Preference.filter({portfolioId:portfolio.id}).run().then(function(eConfig){
                            var portfolioEmailId,portfolioSenderName;
                            if(eConfig && eConfig.length>0){
                              var mailConf = eConfig[0];
                              portfolioEmailId= mailConf.emailConfig.gmail.auth.user;
                              portfolioSenderName= mailConf.emailConfig.gmail.auth.name;
                              var sender = mailConf.emailConfig.gmail;
                              if(!portfolioEmailId){
                                portfolioEmailId = portfolio.email;
                              }
                              if(!portfolioSenderName){
                                portfolioSenderName = portfolio.name;
                              }
                              if(mailConf.smsConfig.enableSms==true){
                                var portfolioSmsId = mailConf.smsConfig.smsProvider.senderId;
                                if(portfolioSmsId == undefined || portfolioSmsId == null || portfolioSmsId === ""){
                                    portfolioSmsId = config.sms.msg91.senderId;
                                }

                                var authKey = mailConf.smsConfig.smsProvider.authKey;
                                if(authKey == undefined || authKey == null || authKey === ""){
                                    authKey = config.sms.msg91.authKey;
                                }
                                smsService.sendSms(req, config.sms.msg91.authKey,config.sms.msg91.senderId, portfolio.mobile,'You have received a new order '+'\n\n orderId:'+result.orderId +'\n\n payment mode:'+result.paymentMode +'.\n\nBest,\n'+ portfolio.name);
                                smsService.sendSms(req, authKey, portfolioSmsId, mob,'Your order has been successfully placed '+'\n\n order id:'+result.orderId +'\n\n via :'+result.paymentMode +' \n\nBest,\n'+ portfolio.name);
                              } 
                              if(portfolio.email != undefined && portfolio.email != null){
                                  emailService.sendEmail(config.smtp.gmail, {
                                      portfolioId: portfolio.id,
                                      from: '"Zinetgo Support" <support@zinetgo.com>', // sender address
                                      to: portfolio.email, // list of receivers
                                      subject: 'You have received a new order '+' '+result.orderId, // Subject line
                                      text: 'Dear '+portfolio.name +'! You have received a new order ' +'\n\n orderId:'+result.orderId+'\n\n From more details log on to your zinetgo account'+ '.\n\nBest,\n'+ portfolio.name, // plain text
                                      html: '<b>Dear '+portfolio.name +'</b>!<br><br>You have received a new order ' +'<br><br> orderId:'+result.orderId+'<br><br> From more details log on to your zinetgo account'+'.\n\nBest,\n'+ portfolio.name // html body
                                  });
                              }
                              if(result.billingAddress.email != undefined && result.billingAddress.email != null){
                                emailService.sendEmail(sender, {
                                      from: '"'+portfolioSenderName +'" <'+portfolioEmailId+'>', // sender address
                                      to: result.billingAddress.email, // list of receivers
                                      subject: 'Your order has been successfully placed'+'\n\n order id:'+result.orderId, // Subject line
                                      text: 'Dear '+result.billingAddress.name +'! Your order has been successfully placed '+'\n\n order id:'+result.orderId+'.\n\nBest,\n'+portfolio.name, // plain text
                                      html: '<b>Dear '+result.billingAddress.name +'</b>!<br><br>! Your order has been successfully placed '+'\n\n order id:'+result.orderId+'.<br><br>Best,<br>'+portfolio.name
                                  });
                              }
                            }
                          });
                        })
                      })
                    }
                    
                  }
                res.json({
                  customer:cust[0],
                  result: result,
                  paymentId: payment.id
                });
              }).error(handleError(res));
            }).error(handleError(res));
          }
      }).error(handleError(res));
    });
  });
};

exports.bulkUpload = function (req, res) {
    if (!req.files) {
        res.status(500).send({error: 'Please input a CSV file to upload.'});
        return;
    }

    var tokenObject = JSON.parse(req.decoded);

    var file = req.files.file;
    var fileName = file.name;
    var mime = file.mimetype;
    var dir = __dirname;

    if(fileName.endsWith(".csv") || fileName.endsWith(".CSV")){
      //delete the file
          file.mv(dir + '/' + file.name, function(err) {
            converter.fromFile(dir + '/' + file.name,function(err,result){
              if(err){
                console.log(err);
                res.status(500).send({error: 'Internal server error.'});
              }else{
                console.log(result.length);
                for(var i=0;i<result.length;i++){
                  var model = new Order(result[i]);
                  var id = create(model,tokenObject);
                }
                res.json({
                    status: "success",
                    message: "Data uploaded successfully"
                });
              }
            });
          });
          fs.exists(dir + '/' + file.name, (exists) => {
            console.log(exists ? 'it\'s there' : 'it\'s not there');
            //fs.unlink('/tmp/' + file.name, (err) => {
              //if (err) throw err;
              //console.log('successfully deleted /tmp/' + file.name);
            //});
        });
    }else{
      res.status(500).send({error: 'Only  CSV file supported.'});
    }
};



// update order
exports.updateOrder = function (req, res) {
  var od = req.body;
  var user = JSON.parse(req.decoded);
  od.updatedBy =user.userId;
  od.updatedOn =r.now();
  if(req.params.id && req.params.id !=null){
    var id =req.params.id;
  }else{
    var id =req.body.id;
  }
  Portfolio.get(user.portfolioId).run().then(function(port){
  if(od.orderDate && od.orderDate !=null){
        od.orderDate = new Date(od.orderDate);
    }
    var portfolio =port;
    Order.get(id).then(function(od1) {
      if(od1.paymentMode === 'Credit/Debit Card' && od.orderStatus ==='cancelled' && (od1.orderStatus ==='confirm' || od1.orderStatus ==='delivered')){
        Payment.filter({purpose: od1.orderId}).then(function(pay){
          if(pay && pay.length>0){
            Insta.setKeys('346152816498133c4e68755a2aec12ad', '85265f2ccbd0e0f5a41752c3734374b3');
            var refund = new Insta.RefundRequest();
            refund.payment_id = pay[0].payment_id;     // This is the payment_id, NOT payment_request_id 
            refund.type       = od.type;     // Available : ['RFD', 'TNR', 'QFL', 'QNR', 'EWN', 'TAN', 'PTH'] 
            refund.body       = od.reason || '';     // Reason for refund 
            refund.setRefundAmount(od1.grossTotal);  // Optional, if you want to refund partial amount 
            console.log(refund);
            Insta.createRefund(refund, function(error, response) {
               console.log(error);
              if(!error && response.statusCode == 201){
                console.log(response);
                Order.get(id).update(od).then(function(result) {
                  var portfolioSmsId = portfolio.smsProvider.senderId;
                  if(portfolioSmsId == undefined || portfolioSmsId == null || portfolioSmsId === ""){
                     portfolioSmsId = config.sms.msg91.senderId;
                  }
                  console.log('portfolio.portfolioSmsId: '+ portfolioSmsId);

                  var authKey = portfolio.smsProvider.authKey;
                  if(authKey == undefined || authKey == null || authKey === ""){
                     authKey = config.sms.msg91.authKey;
                  }
                  console.log('portfolio.authKey: '+ authKey);
                  var mob = result.shippingAddress.mobile
                  /*if(portfolio.mobile != undefined && portfolio.mobile != null){
                   smsService.sendSms(req, config.sms.msg91.authKey,config.sms.msg91.senderId, portfolio.mobile,'A order has been cancelled. Find below details '+'\n\n orderId:'+result.orderId +'.\n\nBest,\n'+ portfolio.name);
                   smsService.sendSms(req, authKey, portfolioSmsId, mob,'A order has been cancelled. Find below details '+'\n\n order id:'+result.orderId +' \n\nBest,\n'+ portfolio.name);
                  }
                  if(portfolio.email != undefined && portfolio.email != null){
                    emailService.sendEmail(config.smtp.gmail, {
                        portfolioId: portfolio.id,
                        from: '"Zinetgo Support" <support@zinetgo.com>', // sender address
                        to: portfolio.email, // list of receivers
                        subject: 'Cancel of order'+' '+result.orderId, // Subject line
                        text: 'Dear '+portfolio.name +'! A order has been cancelled. Find below details ' +'\n\n orderId:'+result.orderId+'\n\n For more details log on to your zinetgo account'+ '.\n\nBest,\n'+ portfolio.name, // plain text
                        html: '<b>Dear '+portfolio.name +'</b>!<br><br>A order has been cancelled. Find below details ' +'<br><br> orderId:'+result.orderId+'<br><br> For more details log on to your zinetgo account'+'.\n\nBest,\n'+ portfolio.name // html body
                    });
                  }*/

                  var portfolioEmailId,portfolioSenderName;

                  if(portfolio.emailProvider){
                    portfolioEmailId= portfolio.emailProvider.auth.user;
                    portfolioSenderName= portfolio.emailProvider.auth.name;
                  }
                  if(!portfolioEmailId){
                    portfolioEmailId = portfolio.email;
                  }
                  if(!portfolioSenderName){
                    portfolioSenderName = portfolio.name;
                  }

                  console.log('portfolio.emailProvider: '+ portfolio.emailProvider);

                  if(result.shippingAddress.email != undefined && result.shippingAddress.email != null){
                    emailService.sendEmail(portfolio.emailProvider, {
                          from: '"'+portfolioSenderName +'" <'+portfolioEmailId+'>', // sender address
                          to: result.shippingAddress.email, // list of receivers
                          subject: 'Your order has been cancelled', // Subject line
                          text: 'Dear '+result.shippingAddress.name +'! Your order has been cancelled of '+'\n\n order id:'+result.orderId+'.\n\nBest,\n'+portfolio.name, // plain text
                          html: '<b>Dear '+result.shippingAddress.name +'</b>!<br><br>! Your order has been cancelled of '+'\n\n order id:'+result.orderId+'.<br><br>Best,<br>'+portfolio.name
                      });
                  }
                  res.json({
                      result: result
                  });
                }).error(handleError(res));
              }else{
                res.send(500, {error: 'invalid payment details'});
              }
            });
          }
        }).error(handleError(res));
      }else{
        Order.get(id).update(od).then(function(result) {

          res.json({
              result: result
          });
        }).error(handleError(res));
      }
    }).error(handleError(res));
  }).error(handleError(res));
};

var create = function (model, user) {
  if(model.orderDate && model.orderDate != null){
    model.orderDate = {$reql_type$: "TIME",epoch_time: model.orderDate,timezone:"+05:30"};
  }else{
    model.orderDate = {$reql_type$: "TIME",epoch_time: Math.round(new Date().getTime()/1000.0),timezone:"+05:30"};
  }
  model.createdOn = {$reql_type$: "TIME",epoch_time: Math.round(new Date().getTime()/1000.0),timezone:"+05:30"};
  model.orgId = user.orgId;
  model.branchId = user.branchId;
  model.updatedBy =user.userId;
  model.updatedOn =r.now();
  model.createdBy = user.userId;

  Order.filter({orgId:user.orgId,mobile:model.mobile}).execute().then(function(cust) {
      if(cust && cust.length > 0){
        //exists
        model.orderId = cust[0].id;
        model.save().then(function(result) {
          if(cust[0].id){
            var stats = cust[0].stats;
            stats.gmv = stats.gmv + model.grossTotal;
            stats.orders = stats.orders + 1;
            Order.get(cust[0].id).update({stats:stats}).then(function(cust1) {
              console.log("1---" + cust1);
            });//.error(handleError(res));
            console.log("2---" + result);
          }
          return result.id;
        });//.error(err(error));
      }else{
        //not exist create new
        var newOrder = new Order({
            name: model.name,
            address: model.address,
            status: "active",
            mobile: model.mobile,
            mobile2: model.mobile2,
            email: model.email,
            stats: {
              orders: 1,
              gmv: model.grossTotal || 0
            },
            orgId: model.orgId,
            branchId: model.branchId,
            createdOn: model.createdOn
        });
        newOrder.save().then(function(result) {
          model.orderId = result.id;
          model.save().then(function(result) {
            console.log("3---" + result.id);
            return result.id;
          });//.error(err(error));
        });
      }
  });//.error(err(error));
  return null;
}

function err(error){
  return error;
}

function handleError(res) {
    return function(error) {
        console.log(error.message);
        return res.send(500, {error: error.message});
    }
}
