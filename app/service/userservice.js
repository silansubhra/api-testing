var uuid = require('node-uuid'),
    crypto = require('crypto'),
    jwt    = require('jsonwebtoken'),
    thinky = require(__dirname+'/../util/thinky.js'),
    r = thinky.r,
    env = require(__dirname+'/../../env'),
    config = require(__dirname+'/../../config/'+ env.name),
    redis = require('redis'),
    redis_cli = redis.createClient(config.redis),
    User = require(__dirname+'/../model/user.js'),
    UserProfile = require(__dirname+'/../model/userProfile.js'),
    Org = require(__dirname+'/../model/org.js'),
    Portfolio = require(__dirname+'/../model/portfolio.js'),
    ShoppingCartItem = require(__dirname+'/../model/shoppingCart.js'),
    Item = require(__dirname+'/../model/item.js'),
    smsService  = require(__dirname+'/veriformmservice.js'),
    emailService = require(__dirname+'/emailservice.js'),
    mediaService = require(__dirname+'/mediaservice.js'),
    Customer = require(__dirname+'/../model/customer.js'),
    parser = require('ua-parser-js'),
    og = require(__dirname+'/../util/og.js'),
    otp = require('otplib/lib/authenticator'),
    Mport = require(__dirname+'/../model/mport.js'),
    Preference = require(__dirname+'/../model/preference.js');
    //emailConfig = require(__dirname+'/../model/preference.js');
    var NodeGeocoder = require('node-geocoder');
    var distance = require('google-distance');
    var geolib = require('geolib');


// list users
// TODO: all filter, page size and offset, columns, sort
exports.listUsers = function (req, res) {
    var tokenObject = JSON.parse(req.decoded);
    var orgId = tokenObject.orgId;

    var count;
    var pno=1,offset=0,limit=10;
    if(req.query.psize != undefined && req.query.psize != null && !isNaN(req.query.psize)){
        limit = parseInt(req.query.psize);
    }

    if(req.query.pno != undefined && req.query.pno != null && !isNaN(req.query.pno)){
       pno =  parseInt(req.query.pno);
    }

    offset = (pno -1) * limit;

    var filter={
        portfolioId :tokenObject.portfolioId,
        status:'active',
    }

    if(req.query.userType && req.query.userType!=null) {
        filter.userType=req.query.userType;
    }
    User.orderBy({index: r.desc('createdOn')}).filter(filter).hasFields('blogUserType').getJoin({portfolio: true}).skip(offset).limit(limit).run().then(function(users) {
       r.table("user").filter(filter).hasFields('blogUserType').count().run().then(function(total) {
        res.json({
            data: users,
            total: (total!=undefined?total:0),
            pno: pno,
            psize: limit
        });
    });    
    }).error(handleError(res)); 
    handleError(res);
};

// get by id
exports.getUser = function (req, res) {
    var id = req.params.id;
    var user = JSON.parse(req.decoded);
    if(user.roles.indexOf('admin')>-1 || user.createdBy === id){
        User.get(id).getJoin({org: true}).run().then(function(usr) {
             res.json({
            result: usr
            })
        }).error(handleError(res));
    }else{
     res.status(401).send({ error: 'Do not have permission' });
    }
};


//get user by mobile
exports.checkUser = function (req, res) {
    var apiKey = req.query.apikey;
    var mobile = req.query.mobile;
    Portfolio.filter({apiKey:apiKey}).run().then(function(portFolio){
        if(portFolio && portFolio.length >0 ) {
            var portF = portFolio[0];
            var id = portF.id;
            UserProfile.filter({portfolioId:id, mobile:parseInt(mobile)}).run().then(function(userProfile){
                if(userProfile && userProfile.length >0) {
                    res.status(200).send({message:'exist'});
                }else {
                    res.status(200).send({message:'Not exist'});
                }
            });
        }else {
            res.send(500, {error: error.message});
        }
    });
};

// delete by id
exports.deleteUser = function (req, res) {
    var id = req.params.id;
    User.get(id).delete().execute().then(function() {
        res.json({
            status: "success"
        });
    }).error(handleError(res));
};

// Add user
exports.addUser = function (req, res) {
    var newUser = new User(req.body);
    var user = JSON.parse(req.decoded);
    //generate a token with email id
    if(newUser.email != undefined && newUser.email != null){
        var verificationToken = jwt.sign({email: newUser.email}, config.secret,{expiresIn:"7d"});
        //set the apiKey with token value
        newUser.verificationToken = verificationToken;
    }
    console.log("password===="+newUser.password);
    var passwd = newUser.password;
    newUser.password = crypto.createHash('md5').update(newUser.password).digest("hex");
    newUser.createdBy =user.userId;
    newUser.updatedBy =user.userId;
    newUser.updatedOn =r.now();
    newUser.createdOn =r.now();
    newUser.portfolioId = user.portfolioId;
    newUser.orgId = user.orgId;
    newUser.save().then(function(result) {
        if(result.blogUserType && result.blogUserType !=null){
            if(result.blogUserType === 'Author'){
               var per = {'/api/blogs/':["GET"],'/api/blog/':["GET","PUT","POST","DELETE"],'/api/blog-categories/':["GET"],'/api/blog-category/':["GET"]}

            }else if(result.blogUserType === 'Editor'){
                var per = {'/api/blogs/':["GET"],'/api/blog/':["GET","PUT","POST","DELETE"],'/api/blog-categories/':["GET"],'/api/blog-category/':["GET"]}

            }else if(result.blogUserType === 'admin'){
                var per = {'/api/blogs/':["GET"],'/api/blog/':["GET","PUT","POST","DELETE"],'/api/blog-categories/':["GET"],'/api/users/':["GET"],'/api/user/':["GET","PUT","POST","DELETE"]}
            }
            r.table("userProfile").insert([
            {apiKey:user.apiKey,orgId: result.orgId,portfolioId:result.portfolioId,name:result.name ,mobile:result.mobile,email: result.email,password:result.password ,createdBy :result.createdBy,updatedBy :result.updatedBy,createdOn:r.now(),userId:result.id,mobile: result.mobile,permissions:per,roles:["blog",result.blogUserType]
           }
            ]).run()
        }else{
            r.table("userProfile").insert([
            {apiKey:user.apiKey,orgId: result.orgId,portfolioId:result.portfolioId,name:result.name ,mobile:result.mobile,email: result.email,password:result.password ,createdBy :result.createdBy,updatedBy :result.updatedBy,createdOn:r.now(),userId:result.id,mobile: result.mobile,permissions:{'/api/employees/':["GET"],'/api/employee/':["GET","PUT","POST","DELETE"],'/api/designations/':["GET"],'/api/departments/':["GET"]},roles:["blog",result.userType]
           }
           ]).run()
        }
        Portfolio.get(result.portfolioId).run().then(function(port){
            if(port.enableEmail === true){
                Preference.filter({portfolioId:result.portfolioId}).run().then(function(eConfig){
                    var portfolioEmailId,portfolioSenderName;
                    if(eConfig && eConfig.length>0){
                        var mailConf = eConfig[0];
                        portfolioEmailId= mailConf.emailConfig.gmail.auth.user;
                        portfolioSenderName= mailConf.emailConfig.gmail.auth.name;
                        var sender =mailConf.emailConfig.gmail;
                    }else{
                        var sender =config.smtp.gmail;
                    }
                    console.log("sender-----"+JSON.stringify(sender));
                    
                    if(newUser.email != undefined && newUser.email != null){
                        emailService.sendEmail(sender,{
                            portfolioId: user.portfolioId,
                            createdBy: user.userId,
                            updatedBy: user.userId,
                            from: '"Zinetgo Support" <support@zinetgo.com>', // sender address
                            to: newUser.email, // list of receivers
                            subject: 'Zinetgo registeration successful', // Subject line
                            text: 'Dear '+newUser.name +'! Your Zinetgo account is successfully created. Please confirm your email by <a href="">clicking this link</a>.  \nYour login credentials are \n\n Mobile: '+ newUser.mobile + '\nPassword: '+ passwd + '\n\nBest,\nTeam Zinetgo', // plaintext body
                            html: '<b>Dear '+newUser.name +'</b>!<br><br>Your Zinetgo account is successfully created. Please confirm your email by <a href="'+ config.url+ '/verify/' + verificationToken+'">clicking this link</a>. <br>Your login credentials are <br> Mobile: '+ newUser.mobile + '<br>Password: '+ passwd + '<br><br>Best,<br>Team Zinetgo' // html body
                        });
                    }
                });
            }

            if(eConfig.smsConfig.enableSms==true){
                if(mailConf.smsConfig.smsProvider == undefined || mailConf.smsConfig.smsProvider == null || mailConf.smsConfig.smsProvider === ""){
                    var authKey = mailConf.smsConfig.smsProvider.authKey;
                    var portfolioSmsId = mailConf.smsConfig.smsProvider.senderId;
                }else{
                    var authKey = config.sms.msg91.authKey;
                    var portfolioSmsId = config.sms.msg91.senderId;

                }
                console.log('portfolio.authKey: '+ authKey);
                if(portfolio.mobile != undefined && portfolio.mobile != null){
                     smsService.sendSms(req, newUser.mobile,'Dear' +result.name+'!'+' '+ 'You have been registered with Zinetgo. Your login credentials are \n\n Mobile: '+ newUser.mobile + '\nPassword: '+ passwd + '\n\nBest,\nTeam Zinetgo');
                }
            } 
        });
        res.json({
            result: result
        });
    }).error(handleError(res));
};

exports.uploadAvtar = function (req, res) {

    mediaService.addMedia(req,res);
}

// update user
exports.updateUser = function (req, res) {
    var usr = new User(req.body);
    var user = JSON.parse(req.decoded);
    usr.updatedBy =user.userId;
    usr.updatedOn =r.now();
    usr.mobile = parseInt(usr.mobile);
    if(usr.password){
        usr.password = crypto.createHash('md5').update(usr.password).digest("hex");
    }
    roles = [
    'blog',
    usr.blogUserType
    ]
    User.get(req.params.id).update(usr).then(function(result) {
        UserProfile.filter({userId : result.id}).update({email: result.email,password: result.password,mobile: result.mobile,name: result.name,roles:roles}).then(function(usp){
            console.log(usp);
            res.json({
                result: result
            });
        }).error(handleError(res));
    }).error(handleError(res));
};



exports.signUp = function (req, res) {
    var mobile = parseInt(req.body.mobile), password=req.body.password, name=req.body.name, apiKey=req.body.apiKey;
    if(mobile ==undefined || mobile ==null || password == undefined || password == null
    || name ==undefined || name ==null || apiKey == undefined || apiKey == null){
    return res.status(500).send({error: 'Invalid request: mobile,name, password or api key is not set'});
    }
    var data, newUser;
    if(req.query.data){
        //do nothing
        data = JSON.parse(req.query.data);
        var newUser =  new UserProfile(data);
    }else{
       var data = {};
       data.mobile = mobile;
       data.name = name;
       data.email = req.body.email;
       data.password = password;
       data.apiKey =apiKey;
       var newUser =  new UserProfile(data);
    }

    if(newUser == undefined || apiKey == null){
        return res.send(500, {error: 'Not data passed to API'});
    }else{
        if(newUser.mobile == undefined || newUser.mobile == null || newUser.mobile == ""){
            return res.send(500, {error: 'Missing required field mobile'});
        }
        if(newUser.name == undefined || newUser.name == null || newUser.name == ""){
            return res.send(500, {error: 'Missing required field name'});
        }
        if(newUser.password == undefined || newUser.password == null || newUser.password == ""){
            return res.send(500, {error: 'Missing required field password'});
        } 
        Portfolio.filter({apiKey:apiKey}).run().then(function(result){
           if(result && result.length >0){
                var portfolio = result[0];

                newUser.portfolioId = portfolio.id;
                newUser.mpId = portfolio.id;

                var mob = parseInt(newUser.mobile);
                newUser.password = crypto.createHash('md5').update(newUser.password).digest("hex");
                var passwd = newUser.password;

                UserProfile.filter({portfolioId:portfolio.id,mobile:mob,apiKey:apiKey,role:'user'}).run().then(function(user) {
                  if(user && user.length > 0){
                    return res.send(500, {error: 'Mobile no already exists'});
                    }else{
                        Preference.filter({portfolioId:portfolio.id}).run().then(function(eConfig){
                            var portfolioEmailId,portfolioSenderName;
                            if(eConfig && eConfig.length>0){
                                var mailConf = eConfig[0];
                                portfolioEmailId= mailConf.emailConfig.gmail.auth.user;
                                portfolioSenderName= mailConf.emailConfig.gmail.auth.name;
                                var sender = mailConf.emailConfig.gmail;
                            }else{
                                var sender = config.smtp.gmail;
                            }
                            if(!portfolioEmailId){
                              portfolioEmailId = portfolio.email;
                            }
                            if(!portfolioSenderName){
                              portfolioSenderName = portfolio.name;
                            }
                            var portfolioSmsId = mailConf.smsConfig.smsProvider.senderId;
                            if(portfolioSmsId == undefined || portfolioSmsId == null || portfolioSmsId === ""){
                                portfolioSmsId = config.sms.msg91.senderId;
                            }

                            var authKey = mailConf.smsConfig.smsProvider.authKey;
                            if(authKey == undefined || authKey == null || authKey === ""){
                                authKey = config.sms.msg91.authKey;
                            }
                            console.log('portfolio.authKey-----------: '+ authKey);
                            console.log('portfolio portfolioSmsId-----------: '+ portfolioSmsId);
                            Customer.filter({portfolioId: portfolio.id,mobile: mob}).run().then(function(cus){
                                if(!cus || cus.length<1){
                                    var newUser1 = new User({
                                        mobile: mob,
                                        password : passwd,
                                        name : newUser.name,
                                        email: newUser.email
                                    });
                                    newUser1.save().then(function(usrp){
                                        var newShoppingCart = new ShoppingCartItem({
                                            orgId: result[0].orgId,
                                            userId: usrp.id,
                                            portfolioId: result[0].id,
                                            createdBy: usrp.id,
                                            updatedBy :usrp.id
                                        });
                                        newShoppingCart.save().then(function(cart){
                                            newUser.orgId= result[0].orgId,
                                            newUser.userId= usrp.id,
                                            newUser.createdBy= usrp.id,
                                            newUser.updatedBy =usrp.id,
                                            newUser.role = 'user';
                                            newUser.roles= ["user"],
                                            newUser.cartId= cart.id,
                                            newUser.wallet=0,
                                            newUser.permissions= { "/api/change-password/": ["POST"] ,"/api/order/": ["GET","PUT","POST","DELETE"] ,"/api/orders/": ["GET"] ,"/api/user/": ["GET","PUT","POST","DELETE"] ,"/api/users/": ["GET"] ,"/api/wishlist/": ["GET","PUT","POST" ,"DELETE"] ,"/api/wishlists/": ["GET"] ,"/api/userProfile/": ["GET","PUT"]}
                                            newUser.save().then(function(usr) {
                                                r.table("customer").insert([
                                                {id:usr.userId,name: usr.name,mobile:mob,email:usr.email,orgId: usr.orgId,portfolioId: usr.portfolioId,createdBy :usr.userId,updatedBy :usr.userId,createdOn:r.now(),status:"active"}
                                                ]).run()
                                                
                                                if(portfolio.mobile != undefined && portfolio.mobile != null){
                                                 //smsService.sendSms(req, config.sms.msg91.authKey,config.sms.msg91.senderId, portfolio.mobile,'You have received a new user request' + 'from '+newUser.name + ' ' + newUser.mobile +'.\n\nBest,\n'+ portfolio.name);
                                                 if(mailConf.smsConfig.enableSms==true){
                                                        smsService.sendSms(req, authKey, portfolioSmsId, newUser.mobile,'You have been successfully registered with  '+portfolio.name +' \n\nBest,\n'+ portfolio.name);
                                                    } 
                                                }
                                                if(newUser.email != undefined && newUser.email != null && mailConf.emailConfig.enableEmail==true){
                                                  emailService.sendEmail(sender, {
                                                        from: '"'+portfolioSenderName +'" <'+portfolioEmailId+'>', // sender address
                                                        to: newUser.email, // list of receivers
                                                        subject: 'You have been registered with '+portfolio.name+' with mobile number :' + newUser.mobile, // Subject line
                                                        text: 'Dear '+newUser.name +'! You have been registered with '+portfolio.name+' with Mobile number :' + newUser.mobile+'.\n\nBest,\n'+portfolio.name, // plain text
                                                        html: '<b>Dear '+newUser.name +'</b>!<br><br>! You have been registered with '+portfolio.name+' with Mobile number :' + newUser.mobile+'.<br><br>Best,<br>'+portfolio.name
                                                    });
                                                }  
                                                res.json({success: true,'message':'! You have been registered with '+portfolio.name+' ' +'in Mobile number is :' + newUser.mobile});  
                                            });
                                       }).error(handleError(res));
                                    }).error(handleError(res));
                                }else{
                                    var cust = cus[0];
                                    var newShoppingCart = new ShoppingCartItem({
                                        orgId: result[0].orgId,
                                        userId: cust.id,
                                        portfolioId: result[0].id,
                                        createdBy: cust.id,
                                        updatedBy :cust.id
                                    });
                                    newShoppingCart.save().then(function(cart){

                                            newUser.orgId= result[0].orgId;
                                            newUser.userId= cust.id;
                                            newUser.createdBy= cust.id;
                                            newUser.updatedBy =cust.id;
                                            newUser.role = 'user';
                                            newUser.roles= ["user"];
                                            newUser.cartId= cart.id;
                                            newUser.wallet=0;
                                            newUser.permissions= { "/api/change-password/": ["POST"] ,"/api/order/": ["GET","PUT","POST","DELETE"] ,"/api/orders/": ["GET"] ,"/api/user/": ["GET","PUT","POST","DELETE"] ,"/api/users/": ["GET"] ,"/api/wishlist/": ["GET","PUT","POST" ,"DELETE"] ,"/api/wishlists/": ["GET"] ,"/api/userProfile/": ["GET","PUT"]}
                                        
                                        newUser.save().then(function(userProfile){
                                            r.table("user").insert([
                                            {id:cust.id,name: userProfile.name,mobile:mob,email:userProfile.email,password:passwd, orgId: userProfile.orgId,portfolioId: userProfile.portfolioId,createdBy :cust.id,updatedBy :cust.id,createdOn:r.now(),status:"active"}
                                            ]).run()

                                            if(portfolio.mobile != undefined && portfolio.mobile != null){
                                             //smsService.sendSms(req, config.sms.msg91.authKey,config.sms.msg91.senderId, portfolio.mobile,'You have received a new user request' + 'from '+cust.name + ' ' + cust.mobile +'.\n\nBest,\n'+ portfolio.name);
                                             if(mailConf.smsConfig.enableSms==true){
                                                    smsService.sendSms(req, authKey, portfolioSmsId, newUser.mobile,'You have been successfully registered with  '+portfolio.name +' \n\nBest,\n'+ portfolio.name);
                                                } 
                                            }
                                            console.log('portfolio.emailProvider: '+ portfolio.emailProvider);
                                            if(userProfile.email != undefined && userProfile.email != null && mailConf.emailConfig.enableEmail==true){
                                              emailService.sendEmail(sender, {
                                                    from: '"'+portfolioSenderName +'" <'+portfolioEmailId+'>', // sender address
                                                    to: userProfile.email, // list of receivers
                                                    subject: 'You have been registered with '+portfolio.name+' with mobile number :' + userProfile.mobile, // Subject line
                                                    text: 'Dear '+userProfile.name +'! You have been registered with '+portfolio.name+' with Mobile number :' + userProfile.mobile+'.\n\nBest,\n'+portfolio.name, // plain text
                                                    html: '<b>Dear '+userProfile.name +'</b>!<br><br>! You have been registered with '+portfolio.name+' with Mobile number :' + userProfile.mobile+'.<br><br>Best,<br>'+portfolio.name
                                                });
                                            }
                                        }).error(function(error) {
                                        console.log(error.message);
                                        return res.status(500).send({error: error.message});
                                       });
                                    }).error(handleError(res));   
                                    res.json({success: true,'message':'! You have been registered with '+' '+portfolio.name+' ' +'in Mobile number is :' + cust.mobile});
                                }
                            }).error(handleError(res));
                        }).error(handleError(res));
                    }
               }).error(handleError(res));
            }else{
                return res.send(500, {error: 'Invalid user'});
            }
       }).error(function(error){
        });
    }
};



exports.login = function(req, res) {
    var mobile = req.body.mobile || req.body.email, password=req.body.password, apiKey=req.body.apiKey;
    console.log(apiKey);
    console.log(mobile);
   console.log(password);
    if(mobile ==undefined || mobile ==null || password == undefined || password == null
        || apiKey == undefined || apiKey == null){
        return res.status(500).send({error: 'Invalid request: mobile, password or api key is not set'});
    }
    var pwd = crypto.createHash('md5').update(password).digest("hex");
    var filter = {
        "password":pwd ,
        "apiKey": apiKey
    }
    if(!isNaN(parseInt(mobile))){
        filter.mobile = parseInt(mobile)
    }else{
        filter.email = mobile
    }

   console.log(JSON.stringify(filter));
    UserProfile.filter(filter).run().then(function(users) {
        if(users && users.length > 0){
            var user = users[0];
            if(user.status=='active'){
                var token = uuid.v4();
                og.saveToken(token,JSON.stringify(user));
                var ogId = user.orgId;
                Portfolio.get(user.portfolioId).run().then(function(port){
                    Org.get(ogId).run().then(function(og){
                        var data={
                            apiKey: user.apiKey,
                            orgId: user.orgId,
                            orgType: og.orgType,
                            portfolioId: user.portfolioId,
                            roles: user.roles,
                            permissions: user.permissions
                        }  
                        if(og.coverImage){
                            var coverImage = og.coverImage;
                        }else{
                            var coverImage = '';
                        }
                        if(user.cartId && user.cartId!=null){
                            data.shippingAddress = user.shippingAddress;
                            var shopCartId= user.cartId;
                            ShoppingCartItem.filter({portfolioId: user.portfolioId,shoppingCartId: shopCartId}).hasFields('itemId').count().execute().then(function(totalItem){
                                res.json({success: true,id: user.userId,coverImage: coverImage,token: token,mobile: user.mobile,orgType: og.orgType,email: user.email,profile:user,name: user.name,itemTotal: totalItem});
                            }).error(handleError(res));
                        }else{
                            res.json({success: true,coverImage: coverImage,id: user.userId,token: token,uname:port.uName,letterHead:port.letterheadheader,mobile: mobile,orgType: og.orgType,email: user.email,profile:user,name: user.name});
                        }
                    });
                });
            }else if(user.status=='approval pending'){
                res.json({ success: false,user: user,message: 'Your account verification pending'});
            }else if(user.status=='suspended'){
                res.json({ success: false, message: 'Your have payment due.Please make payment for uninterrepted Service'});
            }
        }else{
            res.json({ success: false, message: 'Authentication failed. Invalid mobile number or password' });
        }
    }).error(function(res){
        res.json({ success: false, message: 'Authentication failed. Invalid mobile number or password.' });
    });
};


exports.forgot= function(req, res) {
  var mobile = parseInt(req.body.mobile),apiKey=req.body.apiKey || req.query.apikey;
  if(mobile ==undefined || mobile ==null || apiKey == undefined || apiKey == null ){
    return res.status(500).send({error: 'Invalid request: mobile or api key is not set'});
  }     
    var filter = {
        mobile: mobile, 
        apiKey: apiKey
    }
    if(req.body.role && req.body.role === 'vendor'){
        filter.role = 'vendor';
    }
    console.log('filter value'+JSON.stringify(filter));
    UserProfile.filter(filter).run().then(function(userProf){
        if(!userProf || userProf.length < 1){
            console.log('user not found');
            return res.send(500, {error: "User doesn't exists."});
        }else{
            console.log('got port');
            console.log('user  found');
            var secret = otp.generateSecret();
            var code = otp.generate(secret);
            var time = Date.now() + 1200000; // 20 min
            /*console.log('user=='+user.length);
            console.log('user--'+JSON.stringify(user[0]));*/
            var value = {'expTime': time,
                'userId':userProf[0].id,
                'mobile':userProf[0].mobile,
                'email': userProf[0].email,
                'name': userProf[0].name,
            };
            console.log('token value'+JSON.stringify(value));
            og.saveToken(code,JSON.stringify(value));
            //var email =user[0].email;
            console.log(userProf[0].portfolioId);
            var pfilter = {};
            if(req.body.role && req.body.role === 'vendor'){
                pfilter.portfolioId = userProf[0].mpId;
            }else{
                pfilter.portfolioId = userProf[0].portfolioId;
            }
            Preference.filter(pfilter).run().then(function(eConfig){
                console.log(eConfig);
                console.log(eConfig.length);
                console.log(apiKey !== config.onground.apiKey);
                if(eConfig && eConfig.length >0 && apiKey !== config.onground.apiKey && eConfig[0].smsConfig.smsProvider){
                    var mailConf = eConfig[0];
                    var portfolioSmsId = mailConf.smsConfig.smsProvider.senderId;
                    var authKey = mailConf.smsConfig.smsProvider.authKey;
                }else{
                    var portfolioSmsId = config.sms.msg91.senderId;
                    var authKey = config.sms.msg91.authKey;
                }
                console.log('portfolio.portfolioSmsId: '+ portfolioSmsId);
            
                console.log('portfolio.authKey: '+ authKey);
                if(mobile != undefined && mobile != null){
                    smsService.sendSms(req, authKey, portfolioSmsId, mobile,'Reset password request otp is  '+' ' +code);
                }
                res.json({
                    status: 'success'
                });   
            }).error(handleError(res)); 
        }
    }).error(handleError(res));
};

exports.reset =function(req,res){
    var apiKey=req.body.apiKey ;
    if(apiKey == undefined || apiKey == null){
        return res.status(500).send({error: 'Invalid request: api key is not set'});
    }
    var otp = req.body.otp;
    if (otp) {
        redis_cli.get(otp, function(err, reply) {
            if (reply && reply != null) {
                console.log('exists -- '+ reply);
                otpValue = JSON.parse(reply);
                var now = Date.parse(new Date());
                var mobile = otpValue.mobile;
                if(otpValue.expTime >= now){
                    var newPassword = crypto.createHash('md5').update(req.body.newPassword).digest("hex");
                    
                    UserProfile.get(otpValue.userId).update({password: newPassword}).then(function(userPwd){
                        User.get(userPwd.userId).update({password: newPassword}).then(function(uPwd){
                            og.deleteToken(otp);
                            if(apiKey === config.onground.apiKey){
                                var portfolioSmsId = config.sms.msg91.senderId;
                                var authKey = config.sms.msg91.authKey;
                                smsService.sendSms(req, authKey, portfolioSmsId, mobile,'This is a confirmation that the password for your account ' + mobile + ' has just been changed.\n'+' \n\nBest,\nZinetgo support');
                                var emailProvider = config.smtp.gmail;
                                emailService.sendEmail(emailProvider, {
                                    from: '"'+config.smtp.senderName +'" <'+config.smtp.gmail.user+'>', // sender address
                                    to: otpValue.email, // list of receivers
                                    subject: 'Your password has been successfully changed', // Subject line
                                    text: 'Dear user! Your password has been successfully changed'+'.\n\nBest,\nZinetgo support', // plain text
                                    html: '<b>Dear user </b>!<br><br>! Your password has been successfully changed.\n'+'.<br><br>Best,<br>Zinetgo support'
                                });
                            }else{
                                var pfilter={};
                                if(userPwd.role === 'vendor'){
                                    pfilter.portfolioId =userPwd.mpId;
                                }else{
                                    pfilter.portfolioId = userPwd.portfolioId;
                                }
                                Preference.filter(pfilter).run().then(function(eConfig){
                                    var portfolioEmailId,portfolioSenderName;
                                    if(eConfig && eConfig.length>0){
                                        var mailConf = eConfig[0];
                                        portfolioEmailId= mailConf.emailConfig.gmail.auth.user;
                                        portfolioSenderName= mailConf.emailConfig.gmail.auth.name;
                                        var sender = mailConf.emailConfig.gmail;
                                    }else{
                                        var sender = config.smtp.gmail;
                                    }
                                    if(!portfolioEmailId){
                                      portfolioEmailId = config.smtp.gmail.auth.user;
                                    }
                                    if(!portfolioSenderName){
                                      portfolioSenderName = portfolioEmailId;
                                    }
                                    var portfolioSmsId = mailConf.smsConfig.smsProvider.senderId;
                                    if(portfolioSmsId == undefined || portfolioSmsId == null || portfolioSmsId === ""){
                                        portfolioSmsId = config.sms.msg91.senderId;
                                    }

                                    var authKey = mailConf.smsConfig.smsProvider.authKey;
                                    if(authKey == undefined || authKey == null || authKey === ""){
                                        authKey = config.sms.msg91.authKey;
                                    }
                                    console.log('portfolio.authKey-----------: '+ authKey);
                                    console.log('portfolio portfolioSmsId-----------: '+ portfolioSmsId);

                                        smsService.sendSms(req, authKey, portfolioSmsId, mobile,'This is a confirmation that the password for your account ' + mobile + ' has just been changed successfully.');
                                        emailService.sendEmail(sender, {
                                            from: '"'+portfolioSenderName +'" <'+portfolioEmailId+'>', // sender address
                                            to: otpValue.email, // list of receivers
                                            subject: 'Your password has been successfully changed', // Subject line
                                            text: 'Dear '+otpValue.name +'! Your password has been successfully changed', // plain text
                                            html: '<b>Dear '+otpValue.name +'</b>!<br><br>! Your password has been successfully changed.'
                                        });
                                }).error(handleError(res));
                            }
                            res.json({ success: true, message: 'Password changed successful' });
                        }).error(handleError(res));
                    }).error(handleError(res));
                }else{
                    og.deleteToken(otp);
                    return res.send(401, {error: 'time out'});
                }    
            } else {
                console.log('doesn\'t exist');
                return res.send(401, {error: 'Otp does not exists'});
            }
        });
    }else{
        return res.send(401, {error: 'Otp not found'});
    }
};


exports.logout = function(req,res){
    var token = req.body.token || req.query.token || req.headers['x-access-token'];
    if(token){
        if(og.deleteToken(token)){
            res.json({ success: true, message: 'Logout successful' });
        }else{
            return res.status(500).send({error: 'Couldn\'t logout please again'});
        }
    }else{
        return res.status(500).send({error: 'Couldn\'t logout please again'});
    }
};


exports.changePassword = function(req,res){
    var tokenObject = JSON.parse(req.decoded);
    UserProfile.get(tokenObject.id).run().then(function(usr){
        var password =usr.password;
        var oldPassword = crypto.createHash('md5').update(req.body.oldPassword).digest("hex");
        var newPassword = crypto.createHash('md5').update(req.body.newPassword).digest("hex");
        if(oldPassword === password){
            UserProfile.get(usr.id).update({password:newPassword}).then(function(result){
                if(tokenObject.apiKey === config.onground.apiKey){
                    var portfolioSmsId = config.sms.msg91.senderId;
                    var authKey = config.sms.msg91.authKey;
                    smsService.sendSms(req, authKey, portfolioSmsId, usr.mobile,'This is a confirmation that the password for your account ' + usr.mobile + ' has just been changed.\n'+' \n\nBest,\nZinetgo support');
                    var emailProvider = config.smtp.gmail;
                    emailService.sendEmail(config.smtp.gmail, {
                        from: '"'+config.smtp.senderName +'" <'+config.smtp.gmail.user+'>', // sender address
                        to: usr.email, // list of receivers
                        subject: 'Password changed ', // Subject line
                        text: 'Dear user ! Your password has been successfully changed'+'. \n\nBest,'+'\n\nBest,\nZinetgo support', // plain text
                        html: '<b>Dear user </b>!<br><br>Your password has been successfully changed.'+'<br><br>Best,<br>Zinetgo support' // html body
                   });
                }else{
                    var pfilter = {};
                    if(result.role === 'vendor'){
                        pfilter.portfolioId = result.mpId;
                    }else{
                        pfilter.portfolioId = result.portfolioId;
                    }
                    Preference.filter(pfilter).run().then(function(eConfig){
                        var portfolioEmailId,portfolioSenderName;
                        if(eConfig && eConfig.length>0){
                            var mailConf = eConfig[0];
                            portfolioEmailId= mailConf.emailConfig.gmail.auth.user;
                            portfolioSenderName= mailConf.emailConfig.gmail.auth.name;
                            var sender = mailConf.emailConfig.gmail;
                        }else{
                            var sender = config.smtp.gmail;
                        }
                        if(!portfolioEmailId){
                          portfolioEmailId = portfolio.email;
                        }
                        if(!portfolioSenderName){
                          portfolioSenderName = portfolioEmailId;
                        }
                        var portfolioSmsId = mailConf.smsConfig.smsProvider.senderId;
                        if(portfolioSmsId == undefined || portfolioSmsId == null || portfolioSmsId === ""){
                            portfolioSmsId = config.sms.msg91.senderId;
                        }

                        var authKey = mailConf.smsConfig.smsProvider.authKey;
                        if(authKey == undefined || authKey == null || authKey === ""){
                            authKey = config.sms.msg91.authKey;
                        }
                        console.log('portfolio.authKey-----------: '+ authKey);
                        console.log('portfolio portfolioSmsId-----------: '+ portfolioSmsId);

                            //smsService.sendSms(req, config.sms.msg91.authKey,config.sms.msg91.senderId, portfolio.mobile,'! Password of  ' +mobile+ ' ' +' email'+ email + ' '+'has been changed'+'. \n\nBest,\nHurreh.com');
                            smsService.sendSms(req, authKey, portfolioSmsId, usr[0].mobile,'This is a confirmation that the password for your account has just been changed successfully.\n'+' \n\nBest,\n'+ portfolio.name);
                          
                         emailService.sendEmail(sender, {
                                from: '"'+portfolioSenderName +'" <'+portfolioEmailId+'>', // sender address
                                to: usr.email, // list of receivers
                                subject: 'Password changed ', // Subject line
                                text: 'Dear user! Your password has been successfully changed', // plain text
                                html: '<b>Dear user </b>!<br><br>Your password has been successfully changed.'
                           });
                    });
                }
            });
            res.json({ success: true, message: 'Password successfully changed' });
        }else{
            res.status(500).send({error: 'password did not matched'});
        }
    });
};


exports.verifyEmail = function(req, res){
    var token = req.params.token;
    if (token!=undefined && token != null) {
        // 1. decode the token for basic validity
        jwt.verify(token, config.secret, function(err, decoded) {
          if (err || decoded.email == undefined || decoded.email == null) {
            return res.json({ success: false, message: err|| "Invalid token" });
          } else {
            // 2. get the user with the verification token
            User.filter({verificationToken:token}).run().then(function(result) {
                if(result.length > 0){
                    var user = result[0];
                    // 3. validate the email in token and in user table
                    if(user.email == decoded.email){
                        // valid token and update user
                        User.get(user.id).update({emailValidate:true,verificationToken:null}).execute();
                        res.json({
                             message: 'Email verified successfully'
                         });
                    }
                }
            });
          }
        });
    } else{
        return res.status(403).send({
            success: false,
            message: 'Invalid url or token'
        });
    }
};

exports.vSignUp = function (req, res) {
    var mobile = req.body.mobile, password=req.body.password, name=req.body.name, apiKey=req.body.apiKey;
    if(mobile ==undefined || mobile ==null || password == undefined || password == null
    || name ==undefined || name ==null || apiKey == undefined || apiKey == null){
    return res.status(500).send({error: 'Invalid request: mobile,name, password or api key is not set'});
    }
    var data, newUser;
    if(req.query.data){
        //do nothing
        data = JSON.parse(req.query.data);
        var newUser =  new UserProfile(data);
    }else{
       var data = {};
       data.mobile = mobile;
       data.name = name;
       data.email = req.body.email;
       data.password = password;
       data.apiKey =apiKey;
       data.address = req.body.address;
       var newUser =  new UserProfile(data);
    }
    newUser.password = crypto.createHash('md5').update(newUser.password).digest("hex");
    var passwd = newUser.password;
    Portfolio.filter({apiKey : apiKey}).run().then(function(aPort){
        if(aPort && aPort.length>0){
            Preference.filter({portfolioId:aPort[0].id}).run().then(function(eConfig){
                if(eConfig && eConfig.length>0){
                    var mailConf = eConfig[0];
                    portfolioEmailId= mailConf.emailConfig.gmail.auth.user;
                    portfolioSenderName= mailConf.emailConfig.gmail.auth.name;
                    var sender = mailConf.emailConfig.gmail;
                }else{
                    var sender = config.smtp.gmail;
                }
                if(!portfolioEmailId){
                  portfolioEmailId = aPort[0].email;
                }
                if(!portfolioSenderName){
                  portfolioSenderName = aPort[0].name;
                }
                Portfolio.filter({mobile: parseInt(mobile)}).run().then(function(port){
                    if(port && port.length>0){
                        Mport.filter({portfolioId: port[0].id,mpId:aPort[0].id}).run().then(function(emport){
                            if(emport && emport.length>0){
                                return res.send(500, {error: 'Already registered with '+aPort[0].name});
                            }else{
                                var portfolio = port[0];
                                newUser.portfolioId = portfolio.id;
                                newUser.role = 'vendor';
                                newUser.roles = ['vendor'];
                                newUser.status = 'approval pending';
                                newUser.orgId = portfolio.orgId;     
                                newUser.createdOn = r.now();
                                newUser.mpId = aPort[0].id;
                                newUser.permissions= { "/api/change-password/": ["POST"] ,"/api/order/": ["GET","PUT","POST","DELETE"] ,"/api/orders/": ["GET"] ,"/api/user/": ["GET","PUT","POST","DELETE"] ,"/api/users/": ["GET"] ,"/api/wishlist/": ["GET","PUT","POST" ,"DELETE"] ,"/api/wishlists/": ["GET"] ,"/api/userProfile/": ["GET","PUT"]};
                                console.log(newUser);
                                newUser.save().then(function(usr){
                                    if(portfolio.mpId){
                                        var nmId = mpId.push(aPort[0].id);
                                    }else{
                                        nmId = [aPort[0].id];
                                    }
                                    var newPort = new Mport({
                                        createdOn : r.now(),
                                        portfolioId : portfolio.id,
                                        mpId : aPort[0].id
                                    });
                                    console.log(newPort);
                                    newPort.save().then(function(nwport){
                                        var portfolioSmsId = aPort[0].smsProvider.senderId;
                                        if(portfolioSmsId == undefined || portfolioSmsId == null || portfolioSmsId === ""){
                                           portfolioSmsId = config.sms.msg91.senderId;
                                        }

                                        console.log('aPort[0].portfolioSmsId: '+ portfolioSmsId);

                                        var authKey = aPort[0].smsProvider.authKey;
                                        if(authKey == undefined || authKey == null || authKey === ""){
                                           authKey = config.sms.msg91.authKey;
                                        }
                                        console.log('aPort[0].authKey: '+ authKey);

                                        if(portfolio.mobile != undefined && portfolio.mobile != null){
                                         //smsService.sendSms(req, config.sms.msg91.authKey,config.sms.msg91.senderId, portfolio.mobile,'You have received a new user request' + 'from '+newUser.name + ' ' + newUser.mobile +'.\n\nBest,\n'+ portfolio.name);
                                         smsService.sendSms(req, authKey, portfolioSmsId, newUser.mobile,'You have been successfully registered with  '+aPort[0].name +' \n\nRegards,\nTeam '+ aPort[0].name);
                                        }
                                        var portfolioEmailId,portfolioSenderName;

                                        if(aPort[0].emailProvider){
                                          portfolioEmailId= aPort[0].emailProvider.auth.user;
                                          portfolioSenderName= aPort[0].emailProvider.auth.name;
                                        }
                                        if(!portfolioEmailId){
                                          portfolioEmailId = aPort[0].email;
                                        }
                                        if(!portfolioSenderName){
                                          portfolioSenderName = aPort[0].name;
                                        }
                                        console.log('aPort[0].emailProvider: '+ aPort[0].emailProvider);

                                        if(newUser.email != undefined && newUser.email != null){
                                          emailService.sendEmail(sender, {
                                                from: '"'+portfolioSenderName +'" <'+portfolioEmailId+'>', // sender address
                                                to: newUser.email, // list of receivers
                                                subject: 'You have been registered with'+aPort[0].name+' in mobile number is :' + newUser.mobile, // Subject line
                                                text: 'Dear '+newUser.name +' You have been registered with '+aPort[0].name+' in Mobile number is :' + newUser.mobile+'.\n\nBest,\n'+aPort[0].name, // plain text
                                                html: '<b>Dear '+newUser.name +'</b>!<br><br> You have been registered with '+aPort[0].name+' '+'in Mobile number is :' + newUser.mobile+'.<br><br>Regards,<br>Team '+aPort[0].name
                                            });
                                        } 
                                        var obj ={ 
                                            mpId: aPort[0].id
                                        }
                                        if(!portfolio.wallet){
                                            obj.trunetoWallet= 0
                                        } 
                                        Portfolio.get(portfolio.id).update(obj).run().then(function(wallet){

                                        });
                                        res.json({success: true,'message':'! You have been registered with '+' '+aPort[0].name+' ' +'in Mobile number is :' + newUser.mobile});  

                                    }).error(handleError(res));
                                    
                                }).error(handleError(res)); 
                            }
                        })
                    }else{
                        var uName = '';
                        var un = newUser.name.toLowerCase().split(' ');
                        for(var i=0;i<un.length;i++){
                            uName+= un[i].charAt(0);
                        }
                        if(newUser.address && newUser.address!= '' && newUser.address != null){
                            var add = newUser.address;
                        }else{
                            var add = 'Bangalore';
                        }
                        var vmobile = ''+newUser.mobile;
                        uName += '-'+(vmobile.substring(0, 4));
                        var meCallback = function(error,lat){
                            if(lat && lat!=null ){
                                var lati = lat[0].latitude;
                                var longi = lat[0].longitude;
                            }else{
                                var lati = null;
                                var longi = null;
                            }
                            var newOrg = new Org({
                             email: newUser.email,
                             mobile: newUser.mobile,
                             name: newUser.name,
                             address: newUser.address,
                             createdOn : r.now(),
                             orgType: 'business'
                            });
                            newOrg.save().then(function(org) {
                                var newPortfolio = new Portfolio({
                                    orgId:org.id,
                                    name: org.name,
                                    email: newUser.email,
                                    mobile:newUser.mobile,
                                    apiKey : uuid.v4(),
                                    wallet: 0,
                                    trunetoWallet: 0,
                                    tpKey : uuid.v4(),
                                    uName: uName,
                                    mpId: aPort[0].id,
                                    lat:lati,
                                    long:longi,
                                    enableEmail: false,
                                    createdOn : r.now(),
                                    enableSms: false
                                });
                                newPortfolio.save().then(function(portfolio) {
                                    var newUser1 = new User({
                                        createdOn : r.now(),
                                        mobile: newUser.mobile,
                                        name: newUser.name,
                                        password : passwd,
                                        address : newUser.address,
                                        email : newUser.email,
                                        status : 'approval pending'
                                    });
                                    newUser1.save().then(function(nwuser){
                                       newUser.status = 'approval pending';
                                       newUser.portfolioId = portfolio.id;
                                       newUser.orgId = org.id;
                                       newUser.userId = nwuser.id;
                                       newUser.createdOn = r.now(); 
                                       newUser.role = 'vendor';
                                       newUser.roles = ['vendor'];
                                       newUser.mpId = aPort[0].id;
                                       newUser.permissions= { "/api/change-password/": ["POST"] ,"/api/order/": ["GET","PUT","POST","DELETE"] ,"/api/orders/": ["GET"] ,"/api/user/": ["GET","PUT","POST","DELETE"] ,"/api/users/": ["GET"] ,"/api/wishlist/": ["GET","PUT","POST" ,"DELETE"] ,"/api/wishlists/": ["GET"] ,"/api/userProfile/": ["GET","PUT"]};
                                       newUser.save().then(function(saveUser){
                                            var newPort = new Mport({
                                                createdOn : r.now(),
                                                portfolioId : portfolio.id,
                                                mpId : aPort[0].id
                                            });
                                            newPort.save().then(function(nwport){
                                                var portfolioSmsId = aPort[0].smsProvider.senderId;
                                                if(portfolioSmsId == undefined || portfolioSmsId == null || portfolioSmsId === ""){
                                                   portfolioSmsId = config.sms.msg91.senderId;
                                                }

                                                console.log('aPort[0].portfolioSmsId: '+ portfolioSmsId);

                                                var authKey = aPort[0].smsProvider.authKey;
                                                if(authKey == undefined || authKey == null || authKey === ""){
                                                   authKey = config.sms.msg91.authKey;
                                                }
                                                console.log('aPort[0].authKey: '+ authKey);

                                                if(portfolio.mobile != undefined && portfolio.mobile != null){
                                                 //smsService.sendSms(req, config.sms.msg91.authKey,config.sms.msg91.senderId, portfolio.mobile,'You have received a new user request' + 'from '+newUser.name + ' ' + newUser.mobile +'.\n\nBest,\n'+ portfolio.name);
                                                 smsService.sendSms(req, authKey, portfolioSmsId, newUser.mobile,'You have been successfully registered with  '+aPort[0].name +' \n\nBest,\n'+ aPort[0].name);
                                                }
                                                var portfolioEmailId,portfolioSenderName;

                                                if(aPort[0].emailProvider){
                                                  portfolioEmailId= aPort[0].emailProvider.auth.user;
                                                  portfolioSenderName= aPort[0].emailProvider.auth.name;
                                                }
                                                if(!portfolioEmailId){
                                                  portfolioEmailId = aPort[0].email;
                                                }
                                                if(!portfolioSenderName){
                                                  portfolioSenderName = aPort[0].name;
                                                }
                                                console.log('aPort[0].emailProvider: '+ aPort[0].emailProvider);

                                                if(newUser.email != undefined && newUser.email != null){
                                                  emailService.sendEmail(sender, {
                                                        from: '"'+portfolioSenderName +'" <'+portfolioEmailId+'>', // sender address
                                                        to: newUser.email, // list of receivers
                                                        subject: 'You have been registered with'+aPort[0].name+' in mobile number is :' + newUser.mobile, // Subject line
                                                        text: 'Dear '+newUser.name +'! You have been registered with '+aPort[0].name+' in Mobile number is :' + newUser.mobile+'.\n\nBest,\n'+aPort[0].name, // plain text
                                                        html: '<b>Dear '+newUser.name +'</b>!<br><br>! You have been registered with '+aPort[0].name+' '+'in Mobile number is :' + newUser.mobile+'.<br><br>Best,<br>'+aPort[0].name
                                                    });
                                                }  
                                                res.json({success: true,'message':'! You have been registered with '+' '+aPort[0].name+' ' +'in Mobile number is :' + newUser.mobile});  
                                            }).error(handleError(res));  

                                        }).error(handleError(res));  
                                    }).error(handleError(res));   
                                }).error(handleError(res));
                            }).error(handleError(res));
                        }
                        getLat(add,meCallback);
                        
                    }
                }).error(handleError(res));
            }).error(handleError(res));
        }else{
            return res.status(500).send({error: 'Invalid request'});
        }
     }).error(handleError(res));
};


exports.vLogin = function(req, res) {
    var mobile = req.body.mobile, password=req.body.password, apiKey=req.body.apiKey;
    console.log(apiKey);
    if(mobile ==undefined || mobile ==null || password == undefined || password == null
        || apiKey == undefined || apiKey == null){
        return res.status(500).send({error: 'Invalid request: mobile, password or api key is not set'});
    }
	var pwd = crypto.createHash('md5').update(password).digest("hex");
    var filter = {
        "password":pwd ,
        "apiKey": apiKey,
        role:'vendor'
    }
    if(!isNaN(parseInt(mobile))){
        filter.mobile = parseInt(mobile)
    }else{
        filter.email = mobile
    }
   console.log(JSON.stringify(filter));
    UserProfile.filter(filter).run().then(function(users) {
      console.log(users.length);
        if(users && users.length > 0){
            var user = users[0];
            if(user.status=='active'){
                var token = uuid.v4();
                og.saveToken(token,JSON.stringify(user));
                var ogId = user.orgId;
                Org.get(ogId).run().then(function(og){
                    Portfolio.get(user.portfolioId).run().then(function(port){
                     console.log(port.uName);
                        var data={
                            apiKey: user.apiKey,
                            orgId: user.orgId,
                            orgType: og.orgType,
                            portfolioId: user.portfolioId,
                            roles: user.roles,
                            permissions: user.permissions
                        }  
                        if(og.coverImage){
                            var coverImage = og.coverImage;
                        }else{
                            var coverImage = null;
                        }
                        res.json({success: true,coverImage: coverImage,id: user.userId,token: token,mobile: mobile,uname:port.uName,orgType: og.orgType,email: user.email,profile:user,name: user.name,wallet: port.trunetoWallet});
                    });
                });
            }else if(user.status=='approval pending'){
                var token = uuid.v4();
                og.saveToken(token,JSON.stringify(user));
                var ogId = user.orgId;
                Org.get(ogId).run().then(function(og){
                    Portfolio.get(user.portfolioId).run().then(function(port){
                        var data={
                            apiKey: user.apiKey,
                            orgId: user.orgId,
                            orgType: og.orgType,
                            portfolioId: user.portfolioId,
                            roles: user.roles,
                            permissions: user.permissions
                        }  
                        if(og.coverImage){
                            var coverImage = og.coverImage;
                        }else{
                            var coverImage = null;
                        }
                        res.json({success: true,coverImage: coverImage,id: user.userId,token: token,mobile: mobile,uname:port.uName,orgType: og.orgType,email: user.email,profile:user,wallet: port.trunetoWallet,name: user.name,message: 'Your account verification pending'});
                    }).error(handleError(res));
                }).error(handleError(res));
            }else if(user.status=='suspended'){
                res.json({ success: false, message: 'Your have payment due.Please make payment for uninterrepted Service'});
            }
        }else{
            res.json({ success: false, message: 'Authentication failed. Invalid mobile number or password' });
        }
    });
};

var getLat = function(address,callback){
    var geocoder = NodeGeocoder();
    var obj = geocoder.geocode(address, function(err, lat) {
        console.log('lat long---'+JSON.stringify(lat));
        if(!err){
            callback(null,lat);
        }else{
            callback(err,null);
        }
        
    });
}

exports.getd = function(req,res){
    var geocoder = NodeGeocoder();
     geocoder.geocode('Hurreh Technologies Pvt Ltd', function(err, lat1) {
        console.log('lat long---1'+JSON.stringify(lat1));
        geocoder.geocode('BSF Campus', function(err, lat2) {
            console.log('lat long---2'+JSON.stringify(lat2));
            var d = geolib.getDistance(
                {latitude: lat1[0].latitude, longitude: lat1[0].longitude},
                {latitude: lat2[0].latitude, longitude: lat2[0].longitude}
            );
            var km = d/1000;
            res.send(200,{message:km});
        });
    });
}

function handleError(res) {
    return function(error) {
        console.log(error.message);
        return res.status(500).send({error: error.message});
    }
}

