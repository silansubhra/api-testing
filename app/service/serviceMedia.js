var thinky = require(__dirname+'/../util/thinky.js'),
    r = thinky.r,
    ServiceMedia = require(__dirname+'/../model/serviceMedia.js'),
    Service = require(__dirname+'/../model/service.js'),
    Media = require(__dirname+'/../model/media.js');

// list serviceMedias
// TODO: all filter, page size and offset, columns, sort
exports.listServiceMedias = function (req, res) {
    var count;
    var pno=1,offset=0,limit=10;
    if(req.query.psize != undefined && req.query.psize != null && !isNaN(req.query.psize)){
        limit = parseInt(req.query.psize);
    }

    if(req.query.pno != undefined && req.query.pno != null && !isNaN(req.query.pno)){
       pno =  parseInt(req.query.pno);
    }

    offset = (pno -1) * limit;

    ServiceMedia.count().execute().then(function(total) {
        count = total;
        console.log(count);
    });

    console.log(req.decoded);

    ServiceMedia.orderBy({index: r.desc('createdOn')}).skip(offset).limit(limit).run().then(function(serviceMedias) {
        res.json({
            data: serviceMedias,
            total: count,
            pno: pno,
            psize: limit
        });
    }).error(handleError(res));
};

// get by id
exports.getServiceMedia = function (req, res) {
    var id = req.params.id;
    ServiceMedia.get(id).getJoin({service: true,media :true}).run().then(function(serviceMedia) {
     res.json({
         serviceMedia: serviceMedia
     });
    }).error(handleError(res));
};


// delete by id
exports.deleteServiceMedia = function (req, res) {
    var id = req.params.id;
    ServiceMedia.get(id).delete().run().then(function(service) {
        res.json({
            status: "success"
        });
    }).error(handleError(res));
};

// Add user
exports.addServiceMedia = function (req, res) {
    var newServiceMedia = new ServiceMedia(req.body);
    var user = JSON.parse(req.decoded);
    newServiceMedia.portfolioId=user.portfolioId;
    newServiceMedia.createdBy =user.userId;
    newServiceMedia.updatedBy =user.userId;
    newServiceMedia.updatedOn =r.now();
    console.log(newServiceMedia);

    newServiceMedia.save().then(function(result) {
        res.json({
            result: result
        });
    }).error(handleError(res));
};

// update user
exports.updateServiceMedia = function (req, res) {
    ServiceMedia.get(req.body.id).update(req.body).then(function(result) {
        res.json({
            result: result
        });
    }).error(handleError(res));
};

function handleError(res) {
    return function(error) {
        console.log(error.message);
        return res.send(500, {error: error.message});
    }
}