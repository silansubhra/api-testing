var thinky = require(__dirname+'/../util/thinky.js'),
    r = thinky.r,
    Task = require(__dirname+'/../model/task.js'),
    Org = require(__dirname+'/../model/org.js'),
    Branch = require(__dirname+'/../model/branch.js'),
    cron = require(__dirname+'/cronJob.js');

// list tasks
// TODO: all filter, page size and offset, columns, sort
exports.listTasks = function (req, res) {
    var count;
    var pno=1,offset=0,limit=10;
    if(req.query.psize != undefined && req.query.psize != null && !isNaN(req.query.psize)){
        limit = parseInt(req.query.psize);
    }

    if(req.query.pno != undefined && req.query.pno != null && !isNaN(req.query.pno)){
       pno =  parseInt(req.query.pno);
    }

    offset = (pno -1) * limit;

    var token = req.body.token || req.query.token || req.headers['x-access-token'];
    var tokenObject = JSON.parse(req.decoded);
    var filter ={
        portfolioId: tokenObject.portfolioId,
        status: 'active'
    }

    if(req.query.tStatus && req.query.tStatus !='' && req.query.tStatus !=null){
        filter.tStatus = req.query.tStatus; 
    }

    var today = new Date();

    var fromDate = new Date(new Date(req.query.fromDate).setHours(0,0,0,0));
    var toDate = new Date(new Date(req.query.toDate).setHours(0,0,0,0));

    toDate.setDate(toDate.getDate()+1);
    var priorDate = new Date(new Date().setDate(today.getDate() - 30));
    priorDate = new Date(priorDate.setHours(0,0,0,0));

    
    frm=Date.parse(fromDate);
    to=Date.parse(toDate);
    
    console.log(frm==to);

    var tdDate=today.getDate();
    var tdMonth=today.getMonth() +1;
    var tdYear =today.getFullYear();

    var pDate = priorDate.getDate();
    var pMonth = priorDate.getMonth() +1;
    var pYear = priorDate.getFullYear();
    var fDate=fromDate.getDate();
    var fYear=fromDate.getFullYear();
    var fMonth=fromDate.getMonth() + 1;
    if(frm==to){
        var ttDate=toDate.getDate();
        var tYear=toDate.getFullYear();
        var tMonth=toDate.getMonth() + 1; 
    }else{
        var ttDate=toDate.getDate();
        var tYear=toDate.getFullYear();
        var tMonth=toDate.getMonth() + 1;  
    }

     if(fromDate !=undefined && toDate !=undefined && fromDate !=null && toDate !=null && !isNaN(fromDate) && !isNaN(toDate)){
        Task.orderBy({index: r.desc('startDate')}).filter(filter).getJoin({category: true,employee:true}).filter(r.row('startDate').during(r.time(fYear, fMonth, fDate,"Z"), r.time(tYear, tMonth, ttDate,"Z"), {rightBound: "closed"})).skip(offset).limit(limit).run().then(function(tasks) {
           r.table("task").filter(filter).filter(r.row('startDate').during(r.time(fYear, fMonth, fDate,"Z"), r.time(tYear, tMonth, ttDate,"Z"), {rightBound: "closed"})).count().run().then(function(total) {
            res.json({
                data: tasks,
                total: (total!=undefined?total:0),
                pno: pno,
                psize: limit
            });
        });    
        }).error(handleError(res)); 
        handleError(res);
    }else{
        Task.orderBy({index: r.desc('startDate')}).filter(filter).getJoin({category: true,employee:true}).skip(offset).limit(limit).run().then(function(tasks) {
           r.table("task").filter(filter).count().run().then(function(total) {
            res.json({
                data: tasks,
                total: (total!=undefined?total:0),
                pno: pno,
                psize: limit
            });
        });    
        }).error(handleError(res)); 
        handleError(res);
    }
};

exports.todayTask = function (req, res) {
    var count;
    var pno=1,offset=0,limit=10;
    if(req.query.psize != undefined && req.query.psize != null && !isNaN(req.query.psize)){
        limit = parseInt(req.query.psize);
    }

    if(req.query.pno != undefined && req.query.pno != null && !isNaN(req.query.pno)){
       pno =  parseInt(req.query.pno);
    }

    offset = (pno -1) * limit;

    var token = req.body.token || req.query.token || req.headers['x-access-token'];
    var tokenObject = JSON.parse(req.decoded);

    var filter ={
        portfolioId: tokenObject.portfolioId,
        status: 'active'
    }

    if(req.query.tStatus && req.query.tStatus !='' && req.query.tStatus !=null){
        filter.tStatus = req.query.tStatus; 
    }

    var tFilter = {
         portfolioId: tokenObject.portfolioId,
         status: 'active',
         tStatus:'To-do'
    }
    var cFilter = {
         portfolioId: tokenObject.portfolioId,
         status: 'active',
         tStatus:'Completed'
    }

    var pFilter ={
        portfolioId: tokenObject.portfolioId,
        status: 'active',
        tStatus:'In-Progress'
    }

    var fromDate = new Date();
    var toDate = new Date();

    var fDate=fromDate.getDate();
    var fYear=fromDate.getFullYear();
    var fMonth=fromDate.getMonth() + 1;

    toDate.setDate(toDate.getDate()+1);

    var ttDate=toDate.getDate();
    var tYear=toDate.getFullYear();
    var tMonth=toDate.getMonth() + 1;


    Task.orderBy({index: r.desc('startDate')}).filter(filter).getJoin({employee:true}).filter(r.row('startDate').during(r.time(fYear, fMonth, fDate,"Z"), r.time(tYear, tMonth, ttDate,"Z"), {rightBound: "closed"})).skip(offset).limit(limit).run().then(function(tasks) {
       r.table("task").filter({portfolioId: tokenObject.portfolioId,status: 'active'}).filter(r.row('startDate').during(r.time(fYear, fMonth, fDate,"Z"), r.time(tYear, tMonth, ttDate,"Z"), {rightBound: "closed"})).count().run().then(function(total) {
            r.table("task").filter(tFilter).filter(r.row('startDate').during(r.time(fYear, fMonth, fDate,"Z"), r.time(tYear, tMonth, ttDate,"Z"), {rightBound: "closed"})).count().run().then(function(ttotal) {
                r.table("task").filter(cFilter).filter(r.row('startDate').during(r.time(fYear, fMonth, fDate,"Z"), r.time(tYear, tMonth, ttDate,"Z"), {rightBound: "closed"})).count().run().then(function(ctotal) {
                    r.table("task").filter(pFilter).filter(r.row('startDate').during(r.time(fYear, fMonth, fDate,"Z"), r.time(tYear, tMonth, ttDate,"Z"), {rightBound: "closed"})).count().run().then(function(ptotal) {
                        
                        res.json({
                            data: tasks,
                            ctotal: (ctotal!=undefined?ctotal:0),
                            ttotal: (ttotal!=undefined?ttotal:0),
                            ptotal: (ptotal!=undefined?ptotal:0),
                            total: (total!=undefined?total:0),
                            pno: pno,
                            psize: limit
                        });
                    });    
                });
            });        
        });    
    }).error(handleError(res)); 
    handleError(res);
};

// get by id
exports.getTask = function (req, res) {
    var id = req.params.id;
    Task.get(id).getJoin({category: true,employee:true}).run().then(function(task) {
     res.json({
         task: task
     });
    }).error(handleError(res));
};


// delete by id
exports.deleteTask = function (req, res) {
    var id = req.params.id;
    Task.get(id).delete().run().then(function(service) {
        res.json({
            status: "success"
        });
    }).error(handleError(res));
};

// Add user
exports.addTask = function (req, res) {
    var newTask = new Task(req.body);
    var user = JSON.parse(req.decoded);
    newTask.createdBy =user.userId;
    newTask.portfolioId=user.portfolioId;
    newTask.updatedBy =user.userId;
    newTask.updatedOn =r.now();

    var sd = new Date(newTask.startDate);
    var dd = new Date(newTask.dueDate);
    var twentyMinutesBefore = new Date(sd.getTime() - (10 * 60 * 1000));
     if(Date.parse(twentyMinutesBefore) < Date.parse(new Date())){
         twentyMinutesBefore = new Date(sd.getTime() - (1 * 60 * 1000));
     }
    newTask.startDate = sd;
    newTask.dueDate =  dd;
    Portfolio.get(user.portfolioId).run().then(function(portfolio){
        newTask.save().then(function(result) {
            name = result.name;
            cron.setCron(req,portfolio.mobile,portfolio.email,twentyMinutesBefore,name,sd);
            res.json({
                result: result
            });
        }).error(handleError(res));
    }).error(handleError(res));
};

// update user
exports.updateTask = function (req, res) {
    var tsk = new Task(req.body);
    var user = JSON.parse(req.decoded);
    tsk.updatedBy =user.userId;
    tsk.updatedOn =r.now();
    if(tsk.startDate){
        var sd = new Date(tsk.startDate);
        tsk.startDate = sd;
    };
    if(tsk.dueDate){
        var dd = new Date(tsk.dueDate);
        tsk.dueDate = dd;
    };
    
    Portfolio.get(user.portfolioId).run().then(function(portfolio){
        Task.get(req.params.id).update(tsk).then(function(result) {
            if(tsk.startDate){
                var sd = new Date(req.body.startDate);
                tsk.startDate = sd;
                var twentyMinutesBefore = new Date(sd.getTime() - (10 * 60 * 1000));
                cron.setCron(req,portfolio.mobile,portfolio.email,twentyMinutesBefore,result.name);
            };
            res.json({
                result: result
            });
        }).error(handleError(res));
    });
};

function handleError(res) {
    return function(error) {
        console.log(error.message);
        return res.send(500, {error: error.message});
    }
}