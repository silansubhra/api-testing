//var json2csv = require('json2csv');
var fs = require('fs');

exports.csvData = function (req, res) {
  
  var fields = ['car', 'price', 'color'];
  var myCars = [
    {
      "car": "Audi",
      "price": 40000,
      "color": "blue"
    }, {
      "car": "BMW",
      "price": 35000,
      "color": "black"
    }, {
      "car": "Porsche",
      "price": 60000,
      "color": "green"
    }
  ];
  var csv = json2csv({ data: myCars, fields: fields });
  var path=''+Date.now()+'.csv'; 
  fs.writeFile(path, csv, function(err,data) {
      console.log(path);
      if (err) {
        console.log(err);
        throw err;
      }else{ 
        console.log(path);
        res.download(path); // This is what you need
      }
  });
}