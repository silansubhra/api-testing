var thinky = require(__dirname+'/../util/thinky.js'),
    r = thinky.r,
    MailConfig = require(__dirname+'/../model/mailConfig.js');

// list mailConfigs
// TODO: all filter, page size and offset, columns, sort
exports.listMailConfigs = function (req, res) {
    var count;
    var pno=1,offset=0,limit=10;

    var tokenObject = JSON.parse(req.decoded);
    var orgId = tokenObject.orgId;


    if(req.query.psize != undefined && req.query.psize != null && !isNaN(req.query.psize)){
        limit = parseInt(req.query.psize);
    }

    if(req.query.pno != undefined && req.query.pno != null && !isNaN(req.query.pno)){
       pno =  parseInt(req.query.pno);
    }

    offset = (pno -1) * limit;

    var sort =req.query.sort;
    var pluck = req.query.pluck;
    
    var token = req.body.token || req.query.token || req.headers['x-access-token'];
    var tokenObject = JSON.parse(req.decoded);
        MailConfig.filter({orgId: tokenObject.orgId}).count().execute().then(function(total) {
                count = total;
                console.log(total);
            });    
        MailConfig.orderBy(r.desc('createdOn')).filter({portfolioId: tokenObject.portfolioId}).skip(offset).limit(limit).run().then(function(mailConfigs) {
            res.json({
                data: mailConfigs,
                total: count,
                pno: pno,
                psize: limit
            });
        }).error(handleError(res));
        handleError(res);    
};
exports.getMailConfig = function (req, res) {
    var id = req.params.id;
    MailConfig.get(id).getJoin({portfolio: true}).run().then(function(mailConfig) {
     res.json({
         mailConfig: mailConfig
     });
    }).error(handleError(res));
};


// delete by id
exports.deleteMailConfig = function (req, res) {
    var id = req.params.id;
    MailConfig.get(id).delete().run().then(function(org) {
        res.json({
            status: "success"
        });
    }).error(handleError(res));
};

// Add user
exports.addMailConfig = function (req, res) {
    var newMailConfig = new MailConfig(req.body);
    var user = JSON.parse(req.decoded);
    newMailConfig.createdBy =user.userId;
    newMailConfig.portfolioId=user.portfolioId;
    newMailConfig.orgId = user.orgId;
    newMailConfig.updatedBy =user.userId;
    newMailConfig.updatedOn =r.now();
    console.log(newMailConfig);
    newMailConfig.save().then(function(result) {
        res.json({
            result: result
        });
    }).error(handleError(res));
};

// update user
exports.updateMailConfig = function (req, res) {
    var cet = new MailConfig(req.body);
    var user = JSON.parse(req.decoded);
    cet.updatedBy =user.userId;
    cet.updatedOn =r.now();
    MailConfig.get(cet).update(cet).then(function(result) {
        res.json({
            result: result
        });
    }).error(handleError(res));
};

function handleError(res) {
    return function(error) {
        console.log(error.message);
        return res.send(500, {error: error.message});
    }
}