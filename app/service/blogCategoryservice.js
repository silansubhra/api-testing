var thinky = require(__dirname+'/../util/thinky.js'),
    r = thinky.r,
    BlogCategory = require(__dirname+'/../model/blogCategory.js'),
    Org = require(__dirname+'/../model/org.js'),
    Branch = require(__dirname+'/../model/branch.js');
    Portfolio = require(__dirname+'/../model/portfolio.js');

// list blogBlogCategories
// TODO: all filter, page size and offset, columns, sort
exports.listBlogCategories = function (req, res) {
    var count;
    var pno=1,offset=0,limit=10;
    if(req.query.psize != undefined && req.query.psize != null && !isNaN(req.query.psize)){
        limit = parseInt(req.query.psize);
    }

    if(req.query.pno != undefined && req.query.pno != null && !isNaN(req.query.pno)){
       pno =  parseInt(req.query.pno);
    }

    offset = (pno -1) * limit;

    var filter ={
        status: 'active'
    }

    var token = req.body.token || req.query.token || req.headers['x-access-token'];
    if(req.decoded !=undefined && req.decoded !=null){
        var tokenObject = JSON.parse(req.decoded);
        filter.portfolioId= tokenObject.portfolioId;
        BlogCategory.orderBy({index: r.desc('createdOn')}).filter(filter).skip(offset).limit(limit).run().then(function(blogCategory) {
            r.table("blogCategory").filter(filter).count().run().then(function(total) {
                res.json({
                    data: blogCategory,
                    total: (total!=undefined?total:0),
                    pno: pno,
                    psize: limit
                });
            });
        }).error(handleError(res)); 
    }else{
        var apiKey = req.apikey ;
        Portfolio.filter({apiKey: apiKey}).run().then(function(portfolio){
            if(portfolio && portfolio.length >0){
                var portId = portfolio[0].id;
                filter.portfolioId = portId;
                BlogCategory.orderBy({index: r.desc('createdOn')}).filter(filter).skip(offset).limit(limit).run().then(function(blogCategory) {
                    r.table("blogCategory").filter(filter).count().run().then(function(total) {
                        res.json({
                            data: blogCategory,
                            total: (total!=undefined?total:0),
                            pno: pno,
                            psize: limit
                        });
                    });
                }).error(handleError(res)); 
            }else{
                res.send(404);
            } 
        }).error(handleError(res));       
    }
};

// get by id
exports.getBlogCategory = function (req, res) {
    var id = req.params.id;
    BlogCategory.get(id).getJoin({branch :true}).run().then(function(blogCategory) {
     res.json({
         blogCategory: blogCategory
     });
    }).error(handleError(res));
};


// delete by id
exports.deleteBlogCategory = function (req, res) {
    var id = req.params.id;
    BlogCategory.get(id).delete().run().then(function(service) {
        res.json({
            status: "success"
        });
    }).error(handleError(res));
};

// Add user
exports.addBlogCategory = function (req, res) {
    var newBlogCategory = new BlogCategory(req.body);
    var user = JSON.parse(req.decoded);
    newBlogCategory.createdBy =user.userId;
    newBlogCategory.portfolioId=user.portfolioId;
    newBlogCategory.updatedBy =user.userId;
    newBlogCategory.updatedOn =r.now();
    newBlogCategory.uName = req.body.name.toLowerCase().split(' ').join('-');
    newBlogCategory.save().then(function(result) {
        res.json({
            result: result
        });
    }).error(handleError(res));
};

// update user
exports.updateBlogCategory = function (req, res) {
    var cat = new BlogCategory(req.body);
    var user = JSON.parse(req.decoded); 
    cat.updatedBy =user.userId;
    cat.updatedOn =r.now();
    BlogCategory.get(cat.id).update(cat).then(function(result) {
        res.json({
            result: result
        });
    }).error(handleError(res));
};

function handleError(res) {
    return function(error) {
        console.log(error.message);
        return res.send(500, {error: error.message});
    }
}