var thinky = require(__dirname+'/../util/thinky.js'),
    r = thinky.r,
    Category = require(__dirname+'/../model/category.js'),
    Org = require(__dirname+'/../model/org.js'),
    MediaService = require(__dirname+'/mediaservice.js'),
    og = require(__dirname+'/../util/og.js'),
    env = require(__dirname+'/../../env'),
    config = require(__dirname+'/../../config/'+ env.name),
    Branch = require(__dirname+'/../model/branch.js');

// list categories
// TODO: all filter, page size and offset, columns, sort
exports.listCategories = function (req, res) {
    var count;
    var pno=1,offset=0,limit=10;
    if(req.query.psize != undefined && req.query.psize != null && !isNaN(req.query.psize)){
        limit = parseInt(req.query.psize);
    }

    if(req.query.pno != undefined && req.query.pno != null && !isNaN(req.query.pno)){
       pno =  parseInt(req.query.pno);
    }

    offset = (pno -1) * limit;
    var filter ={
        status: 'active'
    }
    var input =req.query.q;
    if(input && input!=null){
    var q1 ="(?i)"+input;
    }else{
        var q1 ='';
    }
    console.log("filter="+JSON.stringify(filter));
    if(req.decoded !=undefined && req.decoded !=null){
        var tokenObject = JSON.parse(req.decoded);
        filter.portfolioId =tokenObject.portfolioId;
       Category.orderBy({index: r.asc('rank')}).filter(filter).getJoin({items: true}).filter(function(doc){
                return doc('name').match(q1).default(false).
        or(doc('uName').match(q1).default(false));}).skip(offset).limit(limit).run().then(function(categories) {
            r.table("category").filter(filter).filter(function(doc){
                return doc('name').match(q1).default(false).or(doc('uName').match(q1).default(false));}).count().run().then(function(total) {
                res.json({
                    data: categories,
                    total: (count!=undefined?count:0),
                    pno: pno,
                    psize: limit
                });
            });
        }).error(handleError(res)); 
        handleError(res);
    }else{
        var apiKey = req.apikey ;
        Portfolio.filter({apiKey: apiKey}).run().then(function(portfolio){
            var portId = portfolio[0].id;
            filter.portfolioId = portId
            console.log("filter="+JSON.stringify(filter));
            Category.orderBy({index: r.asc('rank')}).filter(filter).getJoin({items: true}).filter(function(doc){
                return doc('name').match(q1).default(false).or(doc('uName').match(q1).default(false)).or(doc('uName').match(q1).default(false));}).skip(offset).limit(limit).run().then(function(categories) {
                r.table("category").filter(filter).filter(function(doc){
                return doc('name').match(q1).default(false).or(doc('uName').match(q1).default(false));}).count().run().then(function(total) {
                    res.json({
                        data: categories,
                        total: (count!=undefined?count:0),
                        pno: pno,
                        psize: limit
                    });
                });
            }).error(handleError(res));
            handleError(res);
        });    
    } 
};

// get by id
exports.getCategory = function (req, res) {
    var id = req.params.id;
    Category.get(id).getJoin({branch :true}).run().then(function(category) {
     res.json({
         category: category
     });
    }).error(handleError(res));
};

// get by uName
exports.getCategoryByUname = function (req, res) {
    var filter = {};
    filter.uName = req.params.uName;
    if(req.decoded !=undefined && req.decoded !=null){
        var tokenObject = JSON.parse(req.decoded);
        filter.portfolioId = tokenObject.portfolioId;
        Category.filter(filter).getJoin({items :true}).run().then(function(category) {
         res.json({
             category: category[0]
         });
        }).error(handleError(res));
    }else if(req.query.apikey && req.query.apikey !=null){
        Portfolio.filter({apiKey: req.query.apikey}).run().then(function(portfolio){
            var portId = portfolio[0].id;
            filter.portfolioId = portId;
            Category.filter(filter).getJoin({items :true}).run().then(function(category) {
             res.json({
                 category: category[0]
             });
            }).error(handleError(res));
        }).error(handleError(res));
    }
    
};

// delete by id
exports.deleteCategory = function (req, res) {
    var id = req.params.id;
    Category.get(id).delete().run().then(function(service) {
        res.json({
            status: "success"
        });
    }).error(handleError(res));
};

// Add user
exports.addCategory = function (req, res) {
    var newCategory = new Category(req.body);
    var user = JSON.parse(req.decoded);
    newCategory.createdBy =user.userId;
    newCategory.portfolioId=user.portfolioId;
    newCategory.updatedBy =user.userId;
    newCategory.updatedOn =r.now();
    var nam = req.body.name;
    var nm =nam.toLowerCase();
    var newchar = '-'
    nm = nm.split(' ').join(newchar);
    newCategory.uName = nm;
    console.log(newCategory);
    Category.orderBy({index: r.desc('rank')}).filter({portfolioId: user.portfolioId}).run().then(function(ld){
        if (ld && ld.length>0) {
            led=ld[0];
            newCategory.rank = led.rank+1;
        }else{
            newCategory.rank=1;
        } 
        if(req.files && req.files !=null){
            var file = req.files.fileToUpload;
            if(file && file != null){
                newCategory.picture = file.name;
            }else{
                newCategory.picture = null;
            }
        }
        newCategory.save().then(function(result) {
            if (!req.files){
                res.json({
                    result: result
                });
            }else{
                console.log(req.files.fileToUpload);
                if(file){
                    var obj = new Object();
                    obj.name = file.name;
                    obj.mime = file.mimetype;
                    obj.forId = result.id;
                    obj.userId = user.userId;
                    obj.updatedBy =  user.userId;
                    obj.portfolioId = user.portfolioId;
                    obj.updatedOn = r.now();
                    var newMedia = new Media(obj);
                }

                var sendResponse = function(error, data){
                    console.log('data --- > ' + JSON.stringify(data));
                    if(!error){
                        if(config.media.upload.to == 'local'){
                            var coverImage = config.media.upload.local.path+'/'+ og.getIdPath(data.mediaId) + '/' + file.name;
                        }else{
                            var coverImage = config.media.upload.s3.accessUrl + '/media/'+ og.getIdPath(data.mediaId) + '/' + file.name;
                        }
                        

                        Category.get(result.id).update({mediaId: data.mediaId, coverImage: coverImage}).then(function(r1) {

                            res.json({
                                result: r1
                            });
                        }).error(handleError(res));
                        // res.json({
                        //     result: data
                        // });
                    }else{
                        return res.send(500, {error: error.message});
                    }
                }

                newMedia.save().then(function(media) {  
                    MediaService.uploadMedia(file, media.id, sendResponse);
                });
            }
        }).error(handleError(res));
    }).error(handleError(res));
};

// update user
exports.updateCategory = function (req, res) {
    var cat = new Category(req.body);
    var user = JSON.parse(req.decoded);
    cat.updatedBy =user.userId;
    cat.updatedOn =r.now();
    Category.get(req.params.id).update(cat).then(function(result) {
        
        res.json({
            result: result
        });
    }).error(handleError(res));
};

function handleError(res) {
    return function(error) {
        console.log(error.message);
        return res.send(500, {error: error.message});
    }
}