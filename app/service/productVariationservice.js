var thinky = require(__dirname+'/../util/thinky.js'),
    r = thinky.r,
    ProductVariation = require(__dirname+'/../model/productVariation.js'),
    Org = require(__dirname+'/../model/org.js'),
    Branch = require(__dirname+'/../model/branch.js');

// list productVariations
// TODO: all filter, page size and offset, columns, sort
exports.listProductVariations = function (req, res) {
    var count;
    var pno=1,offset=0,limit=10;
    if(req.query.psize != undefined && req.query.psize != null && !isNaN(req.query.psize)){
        limit = parseInt(req.query.psize);
    }

    if(req.query.pno != undefined && req.query.pno != null && !isNaN(req.query.pno)){
       pno =  parseInt(req.query.pno);
    }

    offset = (pno -1) * limit;

    var token = req.body.token || req.query.token || req.headers['x-access-token'];
    var tokenObject = JSON.parse(req.decoded);
    var productId = req.params.id;
    ProductVariation.orderBy(r.desc('createdOn')).filter({portfolioId: tokenObject.portfolioId,productId:productId}).getJoin({product: true}).skip(offset).limit(limit).run().then(function(productVariations) {
        ProductVariation.filter({portfolioId: tokenObject.portfolioId,productId:productId}).count().execute().then(function(count) {
            res.json({
                data: productVariations,
                total: (count!=undefined?count:0),
                pno: pno,
                psize: limit
            });
        });            
    }).error(handleError(res));
    handleError(res);
};

// get by id
exports.getProductVariation = function (req, res) {
    var id = req.params.id;
    ProductVariation.get(id).getJoin({product :true}).run().then(function(productVariation) {
     res.json({
         productVariation: productVariation
     });
    }).error(handleError(res));
};


// delete by id
exports.deleteProductVariation = function (req, res) {
    var id = req.params.id;
    ProductVariation.get(id).delete().run().then(function(service) {
        res.json({
            status: "success"
        });
    }).error(handleError(res));
};

// Add user
exports.addProductVariation = function (req, res) {
    var newProductVariation = new ProductVariation(req.body);
    var user = JSON.parse(req.decoded);
    newProductVariation.createdBy =user.userId;
    newProductVariation.portfolioId=user.portfolioId;
    newProductVariation.updatedBy =user.userId;
    newProductVariation.updatedOn =r.now();
    newProductVariation.productId = req.params.id;
    console.log(newProductVariation);
    newProductVariation.save().then(function(result) {
        res.json({
            result: result
        });
    }).error(handleError(res));
};

// update user
exports.updateProductVariation = function (req, res) {
    var tsk = new ProductVariation(req.body);
    var user = JSON.parse(req.decoded);
    tsk.updatedBy =user.userId;
    tsk.updatedOn =r.now();
    ProductVariation.get(tsk.id).update(tsk).then(function(result) {
        res.json({
            result: result
        });
    }).error(handleError(res));
};

function handleError(res) {
    return function(error) {
        console.log(error.message);
        return res.send(500, {error: error.message});
    }
}