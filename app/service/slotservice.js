var thinky = require(__dirname+'/../util/thinky.js'),
    r = thinky.r,
    Slot = require(__dirname+'/../model/slot.js'),
    Org = require(__dirname+'/../model/org.js'),
    Branch = require(__dirname+'/../model/branch.js'),
    Portfolio= require(__dirname+'/../model/portfolio.js')
    Lead = require(__dirname+'/../model/lead.js')
;

// list slots
// TODO: all filter, page size and offset, columns, sort
exports.listSlots = function (req, res) {
    var count;
    var pno=1,offset=0,limit=10;
    if(req.query.psize != undefined && req.query.psize != null && !isNaN(req.query.psize)){
        limit = parseInt(req.query.psize);
    }
    if(req.query.pno != undefined && req.query.pno != null && !isNaN(req.query.pno)){
       pno =  parseInt(req.query.pno);
    }

    offset = (pno -1) * limit;
    var sort =req.query.sort;
    var pluck = req.query.pluck;

    var token = req.body.token || req.query.token || req.headers['x-access-token'];

    var serviceId = req.query.serviceId;

    if(req.decoded){
        var tokenObject = JSON.parse(req.decoded);
        Slot.orderBy(r.desc('createdOn')).filter({portfolioId: tokenObject.portfolioId, serviceId: serviceId}).skip(offset).limit(limit).run().then(function(slots) {
            Slot.filter({portfolioId: tokenObject.portfolioId, serviceId: serviceId}).count().execute().then(function(total) {
            res.json({
                data: slots,
                total: count,
                pno: pno,
                psize: limit
              });
            });
        }).error(handleError(res));
        handleError(res);
    }else{
        var apiKey = req.body.apiKey || req.query.apikey || req.headers['x-api-key'];
        Portfolio.filter({apiKey: apiKey}).run().then(function(portfolio){
          console.log(JSON.stringify(portfolio));
            var portfolioId =  portfolio[0].id;
            //slot date
            if(req.query.sDate && req.query.sDate !== ''){
              var ssDate = new Date(req.query.sDate);
              console.log("\n" +ssDate);
                //selected date
            }else{
              var ssDate = new Date();
              console.log(ssDate);
            }
            if(new Date().getDate() == ssDate.getDate()){
                var fr = parseFloat(new Date().getHours()+'.'+new Date().getMinutes())+0.30;
                Slot.orderBy(r.desc('createdOn')).filter(function(user) {
                    return user("from").ge(fr) }).filter({portfolioId: portfolioId,  serviceId: serviceId}).skip(offset).limit(limit).run().then(function(slots){

                Slot.filter(function(user) {
                    return user("from").ge(fr) }).filter({portfolioId: portfolioId,  serviceId: serviceId}).count().execute().then(function(total){
                           res.json({
                             data: slots,
                             total: count,
                             pno: pno,
                             psize: limit
                           });
                         });
                       }).error(handleError(res));
                       handleError(res);
              }else{
                Slot.orderBy(r.desc('createdOn')).filter({portfolioId: portfolioId,  serviceId: serviceId}).skip(offset).limit(limit).run().then(function(slots) {
                  Slot.filter({portfolioId: portfolioId,  serviceId: serviceId}).count().execute().then(function(total) {
                    res.json({
                      data: slots,
                      total: count,
                      pno: pno,
                      psize: limit
                    });
                  });
                }).error(handleError(res));
                handleError(res);

            }
        });
    }
};

// get by id
exports.getSlot = function (req, res) {
    var id = req.params.id;
    Slot.get(id).getJoin({branch :true}).run().then(function(slot) {
     res.json({
         slot: slot
     });
    }).error(handleError(res));
};


// delete by id
exports.deleteSlot = function (req, res) {
    var id = req.params.id;
    Slot.get(id).delete().run().then(function(service) {
        res.json({
            status: "success"
        });
    }).error(handleError(res));
};

// Add user
exports.addSlot = function (req, res) {
    var newSlot = new Slot(req.body);
    var user = JSON.parse(req.decoded);
    newSlot.portfolioId=user.portfolioId;
    newSlot.createdBy =user.userId;
    newSlot.updatedBy =user.userId;
    newSlot.serviceId = req.params.id;
    newSlot.updatedOn =r.now();
    console.log(newSlot.fromTime);
    newSlot.timing = newSlot.fromTime +'-'+newSlot.toTime;
    console.log(newSlot.timing);
    var f = newSlot.fromTime.split(":");
    newSlot.from = parseFloat(f[0]+"."+f[1]);
    console.log(newSlot.from);

    newSlot.save().then(function(result) {
        res.json({
            result: result
        });
    }).error(handleError(res));
};

// update user
exports.updateSlot = function (req, res) {
    var sl = new Slot(req.body);
    var user = JSON.parse(req.decoded);
    sl.updatedBy = user.userId;
    sl.updatedOn = r.now();
    Slot.get(sl.id).update(sl).then(function(result) {
        res.json({
            result: result
        });
    }).error(handleError(res));
};

function handleError(res) {
    return function(error) {
        console.log(error.message);
        return res.send(500, {error: error.message});
    }
}
