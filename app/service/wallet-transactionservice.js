var thinky = require(__dirname+'/../util/thinky.js'),
    r = thinky.r,
    Portfolio = require(__dirname+'/../model/portfolio.js'),
    env         = require(__dirname+'/../../env'),
    config      = require(__dirname+'/../../config/' + env.name),
    smsService  = require(__dirname+'/veriformmservice.js'),
    emailService  = require(__dirname+'/emailservice.js'),
    og = require(__dirname+'/../util/og.js'),
    Customer = require(__dirname+'/../model/customer.js'),
    WalletTransaction = require(__dirname+'/../model/walletTransaction.js');

// list walletTransactions
exports.listWallets = function (req, res) {
    var count;
    var pno=1,offset=0,limit=10;
    if(req.query.psize != undefined && req.query.psize != null && !isNaN(req.query.psize)){
        limit = parseInt(req.query.psize);
    }
    if(req.query.pno != undefined && req.query.pno != null && !isNaN(req.query.pno)){
       pno =  parseInt(req.query.pno);
    }
    offset = (pno -1) * limit;

    var sort =req.query.sort;
    var pluck = req.query.pluck;
    var token = req.body.token || req.query.token || req.headers['x-access-token'];
    var tokenObject = JSON.parse(req.decoded);

    var today = new Date();
    var fromDate = new Date(req.query.fromDate);
    var toDate = new Date(req.query.toDate);
    toDate.setDate(toDate.getDate()+1);
    var priorDate = new Date('2015-11-10');
    today.setDate(today.getDate()+1);

    
    frm=Date.parse(fromDate);
    to=Date.parse(toDate);

    var tdDate=today.getDate();
    var tdMonth=today.getMonth() +1;
    var tdYear =today.getFullYear();

    var pDate = priorDate.getDate();
    var pMonth = priorDate.getMonth() +1;
    var pYear = priorDate.getFullYear();
    var fDate=fromDate.getDate();
    var fYear=fromDate.getFullYear();
    var fMonth=fromDate.getMonth() + 1;
    if(frm==to){
        var ttDate=toDate.getDate();
        var tYear=toDate.getFullYear();
        var tMonth=toDate.getMonth() + 1; 
    }else{
        var ttDate=toDate.getDate();
        var tYear=toDate.getFullYear();
        var tMonth=toDate.getMonth() + 1;  
    }

    var filter = {
        portfolioId: tokenObject.portfolioId,
        status: 'active'
    }
    if(tokenObject.apiKey !==config.onground.apiKey){
        filter.mpId = tokenObject.mpId;
    }
    if(tokenObject.roles.indexOf('user')>-1){
        filter.customerId = tokenObject.userId;
    }else if(tokenObject.roles.indexOf('vendor')>-1){
        filter.mpId = tokenObject.mpId;
    }
   if(req.query.mpId && req.query.mpId != null && req.query.mpId !=''){
       filter.mpId =  req.query.mpId;
    }
    console.log(JSON.stringify(filter));
    if(fromDate !=undefined && toDate !=undefined && fromDate !=null && toDate !=null && !isNaN(fromDate) && !isNaN(toDate)){
        WalletTransaction.orderBy({index: r.desc('createdOn')}).filter(filter).getJoin({portfolio: true}).filter(r.row('createdOn').during(r.time(fYear, fMonth, fDate,"Z"), r.time(tYear, tMonth, ttDate,"Z"), {rightBound: "closed"})).getJoin({slot: true}).skip(offset).limit(limit).run().then(function(walletTransactions) {
           r.table("walletTransaction").filter(filter).filter(r.row('createdOn').during(r.time(fYear, fMonth, fDate,"Z"), r.time(tYear, tMonth, ttDate,"Z"), {rightBound: "closed"})).count().run().then(function(total) {
            res.json({
                data: walletTransactions,
                total: (total!=undefined?total:0),
                pno: pno,
                psize: limit
            });
        });    
        }).error(handleError(res)); 
        handleError(res);
    }else{
        WalletTransaction.orderBy({index: r.desc('createdOn')}).filter(filter).getJoin({portfolio: true}).filter(r.row('createdOn').during(r.time(pYear, pMonth, pDate,"Z"), r.time(tdYear, tdMonth, tdDate,"Z"))).getJoin({slot: true}).skip(offset).limit(limit).run().then(function(walletTransactions) {
           r.table("walletTransaction").filter(filter).filter(r.row('createdOn').during(r.time(pYear, pMonth, pDate,"Z"), r.time(tdYear, tdMonth, tdDate,"Z"))).count().run().then(function(total) {
            res.json({
                data: walletTransactions,
                total: (total!=undefined?total:0),
                pno: pno,
                psize: limit
            });
        });    
        }).error(handleError(res)); 
        handleError(res);
    }               
};

exports.listWalletsByVendor = function (req, res) {
    var count;
    var pno=1,offset=0,limit=10;
    if(req.query.psize != undefined && req.query.psize != null && !isNaN(req.query.psize)){
        limit = parseInt(req.query.psize);
    }
    if(req.query.pno != undefined && req.query.pno != null && !isNaN(req.query.pno)){
       pno =  parseInt(req.query.pno);
    }
    offset = (pno -1) * limit;

    var sort =req.query.sort;
    var pluck = req.query.pluck;
    var token = req.body.token || req.query.token || req.headers['x-access-token'];
    var tokenObject = JSON.parse(req.decoded);

    var today = new Date();
    var fromDate = new Date(req.query.fromDate);
    var toDate = new Date(req.query.toDate);
    toDate.setDate(toDate.getDate()+1);
    var priorDate = new Date('2015-11-10');
    today.setDate(today.getDate()+1);

    
    frm=Date.parse(fromDate);
    to=Date.parse(toDate);

    var tdDate=today.getDate();
    var tdMonth=today.getMonth() +1;
    var tdYear =today.getFullYear();

    var pDate = priorDate.getDate();
    var pMonth = priorDate.getMonth() +1;
    var pYear = priorDate.getFullYear();
    var fDate=fromDate.getDate();
    var fYear=fromDate.getFullYear();
    var fMonth=fromDate.getMonth() + 1;
    if(frm==to){
        var ttDate=toDate.getDate();
        var tYear=toDate.getFullYear();
        var tMonth=toDate.getMonth() + 1; 
    }else{
        var ttDate=toDate.getDate();
        var tYear=toDate.getFullYear();
        var tMonth=toDate.getMonth() + 1;  
    }

    var filter = {
        status: 'active'
    }

    var id = req.params.id;
    
    Portfolio.filter({uName:id,status:'active'}).run().then(function(port) {
        if(port && port.length>0){
            var portfolio = port[0];
        }else{
            return res.send(401,{error:'not found'});
        }
        if(tokenObject.roles.indexOf('aggregator')>-1){
            filter.mpId = tokenObject.portfolioId;
            filter.portfolioId = portfolio.id;
        }else if(tokenObject.roles.indexOf('super-admin')>-1){
            filter.portfolioId = portfolio.id;
        }else{
            return send(401,{error:'not found'});
        }
        console.log('-----'+JSON.stringify(filter));
        if(fromDate !=undefined && toDate !=undefined && fromDate !=null && toDate !=null && !isNaN(fromDate) && !isNaN(toDate)){
            WalletTransaction.orderBy({index: r.desc('createdOn')}).filter(filter).getJoin({portfolio: true}).filter(r.row('createdOn').during(r.time(fYear, fMonth, fDate,"Z"), r.time(tYear, tMonth, ttDate,"Z"), {rightBound: "closed"})).getJoin({slot: true}).skip(offset).limit(limit).run().then(function(walletTransactions) {
               r.table("walletTransaction").filter(filter).filter(r.row('createdOn').during(r.time(fYear, fMonth, fDate,"Z"), r.time(tYear, tMonth, ttDate,"Z"), {rightBound: "closed"})).count().run().then(function(total) {
                res.json({
                    data: walletTransactions,
                    total: (total!=undefined?total:0),
                    pno: pno,
                    psize: limit
                });
            });    
            }).error(handleError(res)); 
            handleError(res);
        }else{
            WalletTransaction.orderBy({index: r.desc('createdOn')}).filter(filter).getJoin({portfolio: true}).getJoin({slot: true}).skip(offset).limit(limit).run().then(function(walletTransactions) {
               r.table("walletTransaction").filter(filter).count().run().then(function(total) {
                res.json({
                    data: walletTransactions,
                    total: (total!=undefined?total:0),
                    pno: pno,
                    psize: limit
                });
            });    
            }).error(handleError(res)); 
            handleError(res);
        }    
    });
};

exports.smsRecharge = function (req, res) {
    var amount = parseInt(req.body.amount);
    if(amount == undefined || amount == null || amount == ''){
    return res.status(500).send({error: 'Invalid amount'});
    }
    var user = JSON.parse(req.decoded);
    Portfolio.get(user.portfolioId).run().then(function(result){
       if(result){
            console.log(result.wallet);
            console.log(amount);
            if(result.wallet < amount){
                return res.status(500).send({error: 'In-sufficient balance'});
            }
            var nwallet = result.wallet - amount;
            var portfolio = result;
            var portfolioEmailId,portfolioSenderName;
            portfolioEmailId= config.smtp.gmail.auth.user;
            portfolioSenderName= config.smtp.senderName;
            var sender = config.smtp.gmail;
            var portfolioSmsId = config.sms.msg91.senderId;
            var authKey = config.sms.msg91.authKey;
            var newWalletTransaction = new WalletTransaction({
                portfolioId:portfolio.id,
                beforeBal: parseInt(portfolio.wallet) || 0,
                afterBal: parseInt(nwallet),
                deposite: 0,
                withdrawl: amount,
                comments: 'Paid for sms recharge '+amount
            });
            Portfolio.get(user.portfolioId).update({wallet:nwallet}).then(function(newWallet) {
                newWalletTransaction.save().then(function(walletTran){
                    if(portfolio.mobile != undefined && portfolio.mobile != null){
                        smsService.sendSms(req, authKey, portfolioSmsId, 9886681566,portfolio.name+' have requested for sms balance of amount Rs.'+ amount+'. Please recharge it soon. Thank you');
                        if(portfolio.mobile != undefined && portfolio.mobile != null){
                            smsService.sendSms(req, authKey, portfolioSmsId, portfolio.mobile,'Thank you for recharging your sms wallet.Your new sms balance will be credited in sometime');
                        }
                    }
                    if(portfolio.email != undefined && portfolio.email != null){
                      emailService.sendEmail(sender, {
                            from: '"'+portfolioSenderName +'" <'+portfolioEmailId+'>', // sender address
                            to: portfolio.email, // list of receivers
                            subject: 'Request for sms credit of '+amount, // Subject line
                            text: 'Dear '+portfolio.name +'! \n\nThank you for recharging your sms wallet.Your new sms balance will be credited  in sometime',
                            html: 'Dear '+portfolio.name +'!<br><br>! Thank you for recharging your sms wallet.Your new sms balance will be credited  in sometime'
                        });
                    }  
                    emailService.sendEmail(sender, {
                        from: '"'+portfolioSenderName +'" <'+portfolioEmailId+'>', // sender address
                        to: portfolioEmailId, // list of receivers
                        subject: 'Request for sms credit of '+amount, // Subject line
                        text: portfolio.name+' have requested for sms balance of amount Rs.'+ amount+'. Please recharge it soon. Thank you',
                        html: portfolio.name+' have requested for sms balance of amount Rs.'+ amount+'. Please recharge it soon. Thank you'
                    });
                    res.json({success: true,'message':'! You have sms recharge has been done successfully'}); 
                });
            });
        }else{
            return res.status(500).send({error: 'Invalid request'});
        }
    });
};


function handleError(res) {
    return function(error) {
        console.log(error.message);
        return res.send(500, {error: error.message});
    }
}
