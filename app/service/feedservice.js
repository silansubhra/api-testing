var thinky = require(__dirname+'/../util/thinky.js'),
    r = thinky.r,
    Feed = require(__dirname+'/../model/feed.js'),
    Org = require(__dirname+'/../model/org.js');

// list feeds
// TODO: all filter, page size and offset, columns, sort
exports.listFeeds = function (req, res) {
    var count;
    var pno=1,offset=0,limit=10;
    if(req.query.psize != undefined && req.query.psize != null && !isNaN(req.query.psize)){
        limit = parseInt(req.query.psize);
    }

    if(req.query.pno != undefined && req.query.pno != null && !isNaN(req.query.pno)){
       pno =  parseInt(req.query.pno);
    }

    offset = (pno -1) * limit;

    var sort =req.query.sort;
    var pluck = req.query.pluck;
    var token = req.body.token || req.query.token || req.headers['x-access-token'];
    var tokenObject = JSON.parse(req.decoded);
    
    Feed.filter({orgId: tokenObject.orgId}).count().execute().then(function(total) {
            count = total;
            console.log(total);
        });    
    Feed.orderBy(r.desc('createdOn')).filter({portfolioId: tokenObject.portfolioId}).getJoin({user: true}).skip(offset).limit(limit).run().then(function(feeds) {
        res.json({
            data: feeds,
            total: count,
            pno: pno,
            psize: limit
        });
    }).error(handleError(res));
    handleError(res);
       
};
// get by id
exports.getFeed = function (req, res) {
    var id = req.params.id;
    Feed.get(id).getJoin({org: true}).run().then(function(feed) {
     res.json({
         feed: feed
     });
    }).error(handleError(res));
};


// delete by id
exports.deleteFeed = function (req, res) {
    var id = req.params.id;
    Feed.get(id).delete().run().then(function(org) {
        res.json({
            status: "success"
        });
    }).error(handleError(res));
};

//bulk delete
exports.bulkDeleteFeed = function (req, res) {
    console.log("bulk delete called")
    var ids = req.query.ids;
    console.log("ids"+ids);
    if(ids && ids != null){
        var idsArray = ids.split(",");
        for(var i=0;i<idsArray.length;i++){
            var deptId =idsArray[i];
            console.log("dept"+deptId);
            var token = req.body.token || req.query.token || req.headers['x-access-token'];
            var tokenObject = JSON.parse(req.decoded);
            Feed.filter({portfolioId: tokenObject.portfolioId,id: deptId,status: 'active'}).run().then(function(deptt){
                if(deptt && deptt>0){
                    Feed.get(deptt.id).update({status:'inactive'}).run().then(function(branch) {
                        
                    }).error(handleError(res));
                }
            });
        }
        res.json({
            status: "success"
        });
    }else{
        res.send(404, {error: 'Select a feed'});
    }  
};

// Add user
exports.addFeed = function (req, res) {
    var newFeed = new Feed(req.body);
    var user = JSON.parse(req.decoded);
    newFeed.createdBy =user.userId;
    newFeed.portfolioId=user.portfolioId;
    newFeed.orgId =user.orgId;
    newFeed.updatedBy =user.userId;
    newFeed.updatedOn =r.now();
    console.log(newFeed);
    newFeed.save().then(function(result) {
        res.json({
            result: result
        });
    }).error(handleError(res));
};

// update user
exports.updateFeed = function (req, res) {
    var pro = new Feed(req.body);
    var user = JSON.parse(req.decoded);
    pro.updatedBy =user.userId;
    pro.updatedOn =r.now();
    Feed.get(pro.id).update(pro).then(function(result) {
        
        res.json({
            result: result
        });
    }).error(handleError(res));
};

function handleError(res) {
    return function(error) {
        console.log(error.message);
        return res.send(500, {error: error.message});
    }
}