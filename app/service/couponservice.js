var thinky = require(__dirname+'/../util/thinky.js'),
    r = thinky.r,
    Coupon = require(__dirname+'/../model/coupon.js');
    UserProfile = require(__dirname+'/../model/userProfile.js');

// list coupons
// TODO: all filter, page size and offset, columns, sort
exports.listCoupons = function (req, res) {
    var count;
    var pno=1,offset=0,limit=10;
    if(req.query.psize != undefined && req.query.psize != null && !isNaN(req.query.psize)){
        limit = parseInt(req.query.psize);
    }

    if(req.query.pno != undefined && req.query.pno != null && !isNaN(req.query.pno)){
       pno =  parseInt(req.query.pno);
    }

    offset = (pno -1) * limit;

    var token = req.body.token || req.query.token || req.headers['x-access-token'];
    var tokenObject = JSON.parse(req.decoded);

        Coupon.filter({portfolioId: tokenObject.portfolioId}).count().execute().then(function(total) {
            count = total;
            console.log(total);
        });


        Coupon.orderBy({index: r.desc('createdOn')}).filter({portfolioId: tokenObject.portfolioId}).skip(offset).limit(limit).run().then(function(coupons) {
            res.json({
                data: coupons,
                total: count,
                pno: pno,
                psize: limit
            });
        }).error(handleError(res));
    
    handleError(res);
};

// get by id
exports.getCoupon = function (req, res) {
    var id = req.params.id;
    Coupon.get(id).getJoin({portfolio: true}).run().then(function(coupon) {
     res.json({
         coupon: coupon
     });
    }).error(handleError(res));
};


// delete by id
exports.deleteCoupon = function (req, res) {
    var id = req.params.id;
    Coupon.get(id).delete().run().then(function(coupon) {
        res.json({
            status: "success"
        });
    }).error(handleError(res));
};

// Add user
exports.addCoupon = function (req, res) {
    var newCoupon = new Coupon(req.body);
    var user = JSON.parse(req.decoded);
    newCoupon.createdBy =user.userId;
    newCoupon.portfolioId=user.portfolioId;
    newCoupon.updatedBy =user.userId;
    newCoupon.updatedOn =r.now();
    newCoupon.validTill = new Date(newCoupon.validTill);
    newCoupon.save().then(function(result) {
        res.json({
            result: result
        });
    }).error(handleError(res));
};

exports.applyCoupon = function (req, res) {
    var key = req.params.key;
    if(key == undefined || key == null){
        return res.send(500, {error: 'API key not set'});
    }
    console.log(JSON.stringify(req.query));
    if(req.query){
        var couponData = req.query;
    }
    if(couponData == undefined || couponData == null){
        return res.send(500, {error: 'Not data passed to API'});
    }else{
        
        if(couponData.coupon == undefined || couponData.coupon == null || couponData.coupon == ""){
            return res.send(500, {error: 'Invalid Coupon'});
        }
        if(couponData.grossTotal == undefined || couponData.grossTotal == null || couponData.grossTotal == ""){
            return res.send(500, {error: 'Invalid grossTotal'});
        }

        var grossTotal =parseInt(couponData.grossTotal);

        var user = JSON.parse(req.decoded);

        console.log(JSON.stringify(user));

        Portfolio.filter({tpKey:key}).run().then(function(result){
            if(result && result.length >0){
                var portfolio = result[0];
                Coupon.filter({couponCode :couponData.coupon,portfolioId: portfolio.id }).run().then(function(cp){
                    console.log(cp.length);
                    if(!cp || cp.length<1){
                        return res.send(404, {error: 'Invalid Coupon'});
                    }else{
                        var cop =cp[0];
                        var tdy = Date.parse(new Date());
                        var validity = Date.parse(cop.validTill);
                        if(tdy > validity){
                            return res.send(404, {error: 'Coupon has Expired'});
                        }else{
                            if(grossTotal <cop.minOrder){
                                return res.send(404, {error: 'Minimum order should be '+cop.minOrder});

                            }else{
                               // console.log("cop.dicountType"+JSON.stringify(cop.dicountType));
                                if(cop.couponType === 'Discount'){
                                    if(cop.discountType === 'flat'){
                                        var dAmount=discountAmount;
                                    }else{
                                        var dAmount = (cop.discountAmount/100)*grossTotal;
                                    }

                                    res.json({
                                        discountAmount: dAmount,
                                        couponCode: couponData.coupon,
                                        discountType:'Discount',
                                        grossTotal: grossTotal-dAmount
                                    });
                                }else{
                                    console.log("cop.dicountType"+JSON.stringify(cop));
                                    if(cop.discountType === 'flat'){
                                        console.log('discountAmount'+cop.discountAmount);
                                        var dAmount=cop.discountAmount;

                                    }else{
                                        var dAmount = (cop.discountAmount/100)*grossTotal;
                                    }
                                    
                                    res.json({
                                        discountAmount: dAmount,
                                        couponCode: couponData.coupon,
                                        discountType:'Cashback'
                                    });
                                
                                }
                            }
                        }
                    }
                }).error(handleError(res));
            }
        }).error(handleError(res));
    }
};

// update user
exports.updateCoupon = function (req, res) {
    console.log("id--------"+req.params.id);
    if(req.params.id && req.params.id !=null){
    var id =req.params.id;
      }else{
        var id =req.body.id;
      }
      var data = req.body;
      var user = JSON.parse(req.decoded);
        data.updatedBy =user.userId;
        data.updatedOn =r.now();
    Coupon.get(id).update(data).then(function(result) {
        res.json({
            result: result
        });
    }).error(handleError(res));
};

function handleError(res) {
    return function(error) {
        console.log(error.message);
        return res.send(500, {error: error.message});
    }
}