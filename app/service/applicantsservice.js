var thinky = require(__dirname+'/../util/thinky.js'),
    r = thinky.r,
    Applicant = require(__dirname+'/../model/applicant.js'),
    Portfolio = require(__dirname+'/../model/portfolio.js'),
    Branch = require(__dirname+'/../model/branch.js'),
    smsService  = require(__dirname+'/smsservice.js'),
    emailService  = require(__dirname+'/emailservice.js'),
    Org = require(__dirname+'/../model/org.js'),
    Customer = require(__dirname+'/../model/customer.js'),
    env         = require(__dirname+'/../../env'),
    MediaService = require(__dirname+'/mediaservice.js'),
    Media = require(__dirname+'/../model/media.js'),
    og = require(__dirname+'/../util/og.js'),
    config      = require(__dirname+'/../../config/' + env.name);


// list contacts
// TODO: all filter, page size and offset, columns, sort
exports.listApplicant = function (req, res) { 
    var count;
    var pno=1,offset=0,limit=10;
    if(req.query.psize != undefined && req.query.psize != null && !isNaN(req.query.psize)){
        limit = parseInt(req.query.psize);
    }

    if(req.query.pno != undefined && req.query.pno != null && !isNaN(req.query.pno)){
       pno =  parseInt(req.query.pno);
    }

    offset = (pno -1) * limit;

    var sort =req.query.sort;
    var pluck = req.query.pluck;
    var token = req.body.token || req.query.token || req.headers['x-access-token'];
    var tokenObject = JSON.parse(req.decoded);
    if(sort == undefined || sort == null ){
        Applicant.filter({portfolioId: tokenObject.portfolioId}).count().execute().then(function(total) {
                count = total;
                console.log(total);
            });   
        Applicant.orderBy(r.desc('createdOn')).filter({portfolioId: tokenObject.portfolioId}).getJoin({jobopenings: true}).skip(offset).limit(limit).run().then(function(contacts) {
            res.json({
                data: contacts,
                total: count,
                pno: pno,
                psize: limit
            });
        }).error(handleError(res));
        handleError(res);
    }else{
        var result =sort.substring(0, 1);
        var sortLength =sort.length;
        if(result ==='-'){
            field=sort.substring(1,sortLength);
            console.log("field--"+field);
            console.log(typeof field);
            console.log("has field--"+Applicant.hasFields(field));
            if(Applicant.hasFields(field)){
                Applicant.filter({portfolioId: tokenObject.portfolioId}).count().execute().then(function(total) {
                    count = total;
                    console.log(total);
                });   
                Applicant.orderBy(r.desc(field)).filter({portfolioId: tokenObject.portfolioId}).skip(offset).limit(limit).run().then(function(contacts) {
                res.json({
                    data: contacts,
                    total: count,
                    pno: pno,
                    psize: limit
                });
                }).error(handleError(res));
                handleError(res);  
            }else{
                res.status(500).send({ message: 'No field exist.'});
            }   
        }else{
            field=sort.substring(0,sortLength);
            console.log("field--"+field);
            console.log("has field--"+Applicant.hasFields(field));
            if(Applicant.hasFields(field)){
                Applicant.filter({portfolioId: tokenObject.portfolioId}).count().execute().then(function(total) {
                    count = total;
                    console.log(total);
                });   
                Applicant.orderBy(r.asc(field)).filter({portfolioId: tokenObject.portfolioId}).skip(offset).limit(limit).run().then(function(contacts) {
                res.json({
                    data: contacts,
                    total: count,
                    pno: pno,
                    psize: limit
                });
                }).error(handleError(res));
                handleError(res); 
            }else{
                res.status(500).send({ message: 'No field exist.'});
            }      
        }
    }   
};

// get by id
exports.getApplicant = function (req, res) {
    var id = req.params.id;
    Applicant.get(id).getJoin({branch: true}).run().then(function(applicant) {
     res.json({
         applicant: applicant
     });
    }).error(handleError(res));
};


// delete by id
exports.deleteApplicant = function (req, res) {
    var id = req.params.id;
    Applicant.get(id).delete().run().then(function(branch) {
        res.json({
            status: "success"
        });
    }).error(handleError(res));
};


// Add user
exports.addExternalApplicant = function (req, res) {
    var key = req.params.key;
    console.log(key)
    if(key == undefined || key == null){
        return res.send(500, {error: 'API key not set'});
    }
    var data, newContact;
    if(req.query.data){
        //do nothing
        data = JSON.parse(req.query.data);
        newContact =  new Applicant(data);
    }else{
       newContact = new Applicant(req.body);
    }

    if(newContact == undefined || key == null){
        return res.send(500, {error: 'Not data passed to API'});
    }else{
        var message = newContact.message;
        if(newContact.mobile == undefined || newContact.mobile == null || newContact.mobile == ""){
            return res.send(500, {error: 'Missing required field mobile'});
        }
        if(newContact.name == undefined || newContact.name == null || newContact.name == ""){
            return res.send(500, {error: 'Missing required field name'});
        }
        if(newContact.email == undefined || newContact.email == null || newContact.email == ""){
            return res.send(500, {error: 'Missing required field email'});
        }
        if(message ==undefined || message == null || message == ""){
            message = '';
        }

        console.log(req.files);
        if(req.files && req.files !=null){
            var file = req.files.fileToUpload;
            if(file && file != null){
                newContact.resume = file.name;
            }else{
                newContact.resume = null;
            }
        }
        Portfolio.filter({tpKey:key}).run().then(function(result){
            if(result && result.length >0){
                var portfolio = result[0];
                newContact.portfolioId = portfolio.id;
                var mob = parseInt(newContact.mobile);
                newContact.save().then(function(applicant) {
                    if (!req.files){
                        res.json({
                        result: {'status':'success','message':'We have received application'}
                        });
                    }else{
                        console.log(req.files.fileToUpload);
                        if(file){
                            var obj = new Object();
                            obj.name = file.name;
                            obj.mime = file.mimetype;
                            obj.forId = applicant.id;
                            obj.portfolioId = portfolio.id;
                            obj.updatedOn = r.now();
                            var newMedia = new Media(obj);
                        }

                        var sendResponse = function(error, data){
                            if(!error){
                                var resume = config.media.upload.s3.accessUrl + '/media/'+ og.getIdPath(data.mediaId) + '/' + file.name;

                                Applicant.get(applicant.id).update({mediaId: data.mediaId, resume: resume}).then(function(r1) {
                                    res.json({
                                        result: r1
                                    });
                                }).error(handleError(res));
                            }else{
                                console.log(JSON.stringify(error));
                                return res.send(500, {error: error.message});
                            }
                        }
                        newMedia.save().then(function(media) { 
                            MediaService.uploadMedia(file, media.id, sendResponse);
                        });
                    }
                }).error(handleError(res)); 
            }
        }).error(handleError(res)); 
    }   
};

/*exports.addExternalContact = function (req, res) {
    var key = req.params.key;
    if(key == undefined || key == null){
        return res.send(500, {error: 'API key not set'});
    }
    var data, newContact;
    if(req.query.data){
        //do nothing
        data = JSON.parse(req.query.data);
        newContact =  new Applicant(data);
    }else{
       newContact = new Applicant(req.body);
    }

    if(newContact == undefined || key == null){
        return res.send(500, {error: 'Not data passed to API'});
    }else{
        if(newContact.mobile == undefined || newContact.mobile == null || newContact.mobile == ""){
            return res.send(500, {error: 'Missing required field mobile'});
        }
        if(newContact.name == undefined || newContact.name == null || newContact.name == ""){
            return res.send(500, {error: 'Missing required field name'});
        }
        //validate(newContact);
        // TODO: check for the following mandatory fields
        // Mobile, Name, Service
        //return res.send(500, {error: 'Not data passed to API'});

        Portfolio.filter({tpKey:key}).run().then(function(result){
            if(result && result.length >0){
                var portfolio = result[0];
                Branch.filter({portfolioId:result[0].id,isDefault:true}).run().then(function(branch){
                    if(branch && branch.length > 0){
                        newContact.branchId = branch[0].id;
                        newContact.portfolioId = result[0].id;
                        newContact.save().then(function(applicant) {

                            if(portfolio.mobile != undefined && portfolio.mobile != null){
                                        smsService.sendSms(portfolio.mobile,'You have applicant request from ' +applicant.name + ' ' + applicant.mobile +'. \n\nBest,\nTeam OnGround');
                                        smsService.sendSms(applicant.mobile,'We have received your applicant request will applicant you soon' +' \n\nBest,\n'+ portfolio.name);
                                    }
                                    if(portfolio.email != undefined && portfolio.email != null){
                                        emailService.sendEmail({
                                            from: '"OnGround Support" <support@hurreh.com>', // sender message
                                            to: portfolio.email, // list of receivers
                                            subject: 'You have received a new applicant request', // Subject line
                                            text: 'Dear '+portfolio.name +'! You have received a new applicant request from '+applicant.name + ' ' + applicant.mobile +'. \n\nBest,\nTeam OnGround', // plain text
                                            html: '<b>Dear '+portfolio.name +'</b>!<br><br>You have received a new applicant request from '+applicant.name + ' ' + applicant.mobile +'.<br><br>Best,<br>Team OnGround' // html body
                                        });
                                    }
                                    if(applicant.email != undefined && applicant.email != null){
                                        emailService.sendEmail({
                                            from: '"'+portfolio.name +'" <'+portfolio.email+'>', // sender message
                                            to: applicant.email, // list of receivers
                                            subject: 'We have received your applicant request', // Subject line
                                            text: 'Dear '+applicant.name +'! We have received your applicant request .We will applicant you soon'+'.\n\nBest,\n'+portfolio.name, // plain text
                                            html: '<b>Dear '+applicant.name +'</b>!<br><br>We have received your applicant request.We will applicant you soon'+'.<br><br>Best,<br>'+portfolio.name
                                        });
                                    }
                               
                                res.json({
                                    result: {'status':'success','message':'We have received your request.'}
                            });
                        }).error(handleError(res));
                    }
                }).error(handleError(res));
            }
        }).error(function(error){
            //
        });
    }
};*/

// update user
exports.updateApplicant = function (req, res) {
    Applicant.get(req.body.id).update(req.body).then(function(result) {
        var user = JSON.parse(req.decoded);
        result.updatedBy =user.userId;
        result.updatedOn =r.now();
        res.json({
            result: result
        });
    }).error(handleError(res));
};

function handleError(res) {
    return function(error) {
        console.log(error.message);
        return res.send(500, {error: error.message});
    }
}
