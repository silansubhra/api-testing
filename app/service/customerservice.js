var thinky = require(__dirname+'/../util/thinky.js'),
    r = thinky.r,
    Customer = require(__dirname+'/../model/customer.js'),
    Org = require(__dirname+'/../model/org.js'),
    Branch = require(__dirname+'/../model/branch.js'),
    User = require(__dirname+'/../model/user.js');

// list customers
// TODO: all filter, page size and offset, columns, sort
exports.listCustomers = function (req, res) {
    var count;
    var pno=1,offset=0,limit=10;
    if(req.query.psize != undefined && req.query.psize != null && !isNaN(req.query.psize)){
        limit = parseInt(req.query.psize);
    }

    if(req.query.pno != undefined && req.query.pno != null && !isNaN(req.query.pno)){
       pno =  parseInt(req.query.pno);
    }

    offset = (pno -1) * limit;

    var sort =req.query.sort;
    var pluck = req.query.pluck;
    var token = req.body.token || req.query.token || req.headers['x-access-token'];
    var tokenObject = JSON.parse(req.decoded);

    var address =req.query.address;
    var name = req.query.name;
    var mobile =parseInt(req.query.mobile);

    var q ={
        portfolioId: tokenObject.portfolioId,
        status: 'active'
    };

    if(address !=null && address !=undefined){
        q.address =address;
    }
    if(name !=null && name !=undefined){
        q.name =name;
    }

    if(mobile !=null && mobile !=undefined && !isNaN(mobile)){
        q.mobile =mobile;
    }


    if(sort == undefined || sort == null ){
        Customer.orderBy(r.desc('createdOn')).filter(q).skip(offset).limit(limit).run().then(function(customers) {
            Customer.filter(q).count().execute().then(function(count) {
                res.json({
                    data: customers,
                    total: (count!=undefined?count:0),
                    pno: pno,
                    psize: limit
                });
        });            
        }).error(handleError(res));
        handleError(res);
    }else{
        var result =sort.substring(0, 1);
        var sortLength =sort.length;
        if(result ==='-'){
            field=sort.substring(1,sortLength);
            console.log("field--"+field);
            console.log(typeof field);
            Customer.filter(filter).match(field).run().then(function(result){
                console.log("length--->"+result.length);
            });    
            if(Customer.hasFields(field)){
                Customer.filter(filter).count().execute().then(function(total) {
                    count = total;
                    console.log(total);
                });    
                Customer.orderBy(r.desc(field)).filter(filter).skip(offset).limit(limit).run().then(function(customers) {
                res.json({
                    data: customers,
                    total: count,
                    pno: pno,
                    psize: limit
                });
                }).error(handleError(res));
                handleError(res);   
            }else{
                res.status(500).send({ message: 'No field exist.'});
            }    
        }else{
            field=sort.substring(0,sortLength);
            console.log("field--"+field);
            console.log("has field--"+Customer.hasFields(field));
            if(Customer.hasFields(field)){
                Customer.filter(filter).count().execute().then(function(total) {
                    count = total;
                    console.log(total);
                });    
                Customer.orderBy(r.asc(field)).filter(filter).skip(offset).limit(limit).run().then(function(customers) {
                res.json({
                    data: customers,
                    total: count,
                    pno: pno,
                    psize: limit
                });
                }).error(handleError(res));
                handleError(res);  
            }else{
                res.status(500).send({ message: 'No field exist.'});
            }       
        }
    }    
};
// get by id
exports.getCustomer = function (req, res) {
    var id = req.params.id;
    Customer.get(id).getJoin({portfolio: true}).run().then(function(customer) {
     res.json(customer);
    }).error(handleError(res));
};



// delete by id
exports.deleteCustomer = function (req, res) {
    var id = req.params.id;
    Customer.get(id).delete().run().then(function(customer) {
        res.json({
            status: "success"
        });
    }).error(handleError(res));
};

//bulk delete
exports.bulkDeleteCustomer = function (req, res) {
    console.log("bulk delete called")
    var ids = req.query.ids;
    console.log("ids"+ids);
    if(ids && ids != null){
        var idsArray = ids.split(",");
        for(var i=0;i<idsArray.length;i++){
            var custId =idsArray[i];
            console.log("cust"+custId);
            var token = req.body.token || req.query.token || req.headers['x-access-token'];
            var tokenObject = JSON.parse(req.decoded);
            Customer.filter({portfolioId: tokenObject.portfolioId,id: custId,status: 'active'}).run().then(function(custm){
                if(custm && custm.length > 0){
                    Customer.get(custm[0].id).update({status:'inactive'}).run().then(function(customer1) {
                        console.log('delete: '+ custm[0].id);
                    }).error(handleError(res));
                }
            });
        }

        console.log('return...');
        res.json({
            status: "success"
        });

    }else{
        res.send(404, {error: 'Select a customer'});
    }  
};

// Add user
// Add user
exports.addCustomer = function (req, res) {
    var newCustomer = new Customer(req.body);
    var user = JSON.parse(req.decoded);
    newCustomer.createdBy =user.userId;
    newCustomer.portfolioId=user.portfolioId;
    newCustomer.updatedBy =user.userId;
    newCustomer.updatedOn =r.now();
    console.log(newCustomer);
    newCustomer.save().then(function(result) {
        res.json({
            result: result
        });
    }).error(handleError(res));
};

// update user
exports.updateCustomer = function (req, res) {
    var cust = new Customer(req.body);
    var user = JSON.parse(req.decoded);
    cust.updatedBy =user.userId;
    cust.updatedOn =r.now();

    Customer.get(cust.id).update(cust).then(function(result) {        
        res.json({
            result: result
        });
    }).error(handleError(res));
};

function handleError(res) {
    return function(error) {
        console.log(error.message);
        return res.send(500, {error: error.message});
    }
}
