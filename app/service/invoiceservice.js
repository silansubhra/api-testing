var thinky = require(__dirname+'/../util/thinky.js'),
    r = thinky.r,
    Invoice = require(__dirname+'/../model/invoice.js'),
    Payment = require(__dirname+'/../model/payment.js'),
    Customer = require(__dirname+'/../model/customer.js'),
    Lead = require(__dirname+'/../model/lead.js'),
    MediaService = require(__dirname+'/mediaservice.js'),
    emailService  = require(__dirname+'/emailservice.js'),
    Preference = require(__dirname+'/../model/preference.js'),
    Media = require(__dirname+'/../model/media.js'),
    og = require(__dirname+'/../util/og.js'),
    env = require(__dirname+'/../../env'),
    config = require(__dirname+'/../../config/'+ env.name);
    var moment = require('moment');
    var fs = require('fs');
    var url = require('url');
    var pdf = require('html-pdf');
    //var phantom = require('phantom'),
    var numeral = require('numeral');
    var Handlebars = require('handlebars');  
    var ejs = require('ejs');  
    var path = require('path'),
    EmailTemplate = require('email-templates').EmailTemplate,
    templatesDir = path.resolve(__dirname, '..', 'templates');
    var options = { format: 'Letter' };

// list Invoices
// TODO: all filter, page size and offset, columns, sort
exports.listInvoices = function (req, res) {
    var count;
    var pno=1,offset=0,limit=10;
    if(req.query.psize != undefined && req.query.psize != null && !isNaN(req.query.psize)){
        limit = parseInt(req.query.psize);
    }

    if(req.query.pno != undefined && req.query.pno != null && !isNaN(req.query.pno)){
       pno =  parseInt(req.query.pno);
    }

    offset = (pno -1) * limit;

    var token = req.body.token || req.query.token || req.headers['x-access-token'];
    var tokenObject = JSON.parse(req.decoded);

    

    console.log(req.decoded);

    Invoice.orderBy({index: r.desc('createdOn')}).filter({portfolioId:tokenObject.portfolioId}).getJoin({portfolio: true,order: true,job: true}).skip(offset).limit(limit).run().then(function(invoices) {
        Invoice.filter({portfolioId:tokenObject.portfolioId}).count().execute().then(function(total) {
            count = total;
            console.log(count);
            res.json({
                data: invoices,
                total: count,
                pno: pno,
                psize: limit
            });
        });
    }).error(handleError(res));
};

exports.getInvoiceNo = function (req, res) {

    var token = req.body.token || req.query.token || req.headers['x-access-token'];
    var tokenObject = JSON.parse(req.decoded);

    Portfolio.get(tokenObject.portfolioId).getJoin({org: true}).run().then(function(portfolio){
        Invoice.orderBy({index: r.desc('rank')}).filter({portfolioId: tokenObject.portfolioId}).run().then(function(ld){
            if(ld && ld.length>0){
                var rank = ld[0].rank || 0;
            }else{
                var rank = 0;
            }
            
            Preference.filter({portfolioId:tokenObject.portfolioId}).run().then(function(eConfig){
                if(eConfig && eConfig.length>0){
                    var mailConf = eConfig[0];
                    if(mailConf && mailConf.invoice && mailConf.invoice.prefix && mailConf.invoice.prefix !=''){
                        if(mailConf.invoice.sequence && rank < parseInt(mailConf.invoice.sequence)){
                            //newLead.leadId= mailConf.invoice.prefix +'-'+mailConf.invoice.sequence;
                            var nrank=parseInt(mailConf.invoice.sequence)+1;
                            var ino = mailConf.invoice.prefix +'-'+nrank;
                        }else{
                            ino= mailConf.invoice.prefix +'-'+(rank+1);
                        }
                    }else{
                        var ino = rank+1;
                    }
                }else{
                    var ino = rank+1;
                }
                res.json({
                    invoiceNo: ino,
                    portfolio: portfolio
                });
            });
        });
    });
};


// get by id
exports.getInvoice = function (req, res) {
    var id = req.params.id;
    Invoice.get(id).getJoin({branch: true,payment: true}).run().then(function(invoice) {
     res.json({
         invoice: invoice
     });
    }).error(handleError(res));
};


// delete by id
exports.deleteInvoice = function (req, res) {
    var id = req.params.id;
    Invoice.get(id).delete().run().then(function(branch) {
        res.json({
            status: "success"
        });
    }).error(handleError(res));
};

exports.bulkDeleteInvoice = function (req, res) {
    var ids = req.query.ids;
    console.log("ids"+ids);
    if(ids && ids != null){
        var idsArray = ids.split(",");
        for(var i=0;i<idsArray.length;i++){
            var invId =idsArray[i];
            console.log("inv"+invId);
            var token = req.body.token || req.query.token || req.headers['x-access-token'];
            var tokenObject = JSON.parse(req.decoded);
            Invoice.filter({portfolioId: tokenObject.portfolioId,id: invId,status: 'active'}).run().then(function(invr){
                if(invr && invr>0){
                    Invoice.get(invr.id).update({status:'inactive'}).run().then(function(branch) {
                        
                    }).error(handleError(res));
                }
            });
        }
        res.json({
            status: "success"
        });
    }else{
        res.send(404, {error: 'Select a invoice'});
    }  
};


// Add user
exports.addInvoice = function (req, res) {
    var newInvoice = new Invoice(req.body);
    var user = JSON.parse(req.decoded);
    newInvoice.createdBy =user.userId;
    newInvoice.portfolioId=user.portfolioId;
    newInvoice.updatedBy =user.userId;
    newInvoice.updatedOn =r.now();
    Invoice.orderBy({index: r.desc('rank')}).filter({portfolioId: user.portfolioId}).run().then(function(ld){
        if (ld && ld.length>0) {
            led=ld[0];
            newInvoice.rank=led.rank+1;
        }else{
            newInvoice.rank=1;
        } 
        Org.get(user.orgId).getJoin({portfolios: true}).run().then(function(org){
            var leads = {
                from:newInvoice.from.split("\n").join("<br>") || '',
                to:newInvoice.to.split("\n").join("<br>") || '',
                invoiceNo:newInvoice.invoiceNo || '',
                pan:newInvoice.panNo || '',
                gst:newInvoice.gstNo || '',
                invoiceDate:moment(newInvoice.date).format('DD-MMM-YYYY') || '',
                stotal:'₹'+numeral(newInvoice.subTotal).format('0,0.00')  || '₹0.00',
                tax:newInvoice.tax || 0,
                discount:'₹'+numeral(newInvoice.discount).format('0,0.00')  || '₹0.00',
                total:'₹'+numeral(newInvoice.total).format('0,0.00')  || '₹0.00',
                note:newInvoice.notes.split("\n").join("<br>") || '',
                terms:newInvoice.terms.split("\n").join("<br>") || '',
                tableData:newInvoice.services || ''
            }
            if(org){
                leads.isrc = org.portfolios[0].letterheadheader || org.coverImage;
            }
            newInvoice.save().then(function(quote){
                //var template1 = new EmailTemplate(path.join(templatesDir, 'invoice'));

                var html = ejs.render(fs.readFileSync(path.join(templatesDir, 'invoice/html.ejs'), 'utf8'),leads);
               // var html = html2(leads);

                pdfile = config.media.upload.local.path + '/'+quote.id+'.pdf';
                
                    console.log(html);
                    pdf.create(html, options).toFile(pdfile, function(err, res1) {
                        console.log(res1);
                      if (err) return console.log(err);
                      var email = false;
                        upload(req,res,res1.filename,quote.id,email);
                    });

            });
        });
    });
};

exports.addEmailInvoice = function (req, res) {
    var newInvoice = new Invoice(req.body);
    var user = JSON.parse(req.decoded);
    newInvoice.createdBy =user.userId;
    newInvoice.portfolioId=user.portfolioId;
    newInvoice.updatedBy =user.userId;
    newInvoice.updatedOn =r.now();
    Org.get(user.orgId).getJoin({portfolios: true}).run().then(function(org){
        var leads = {
            from:newInvoice.from.split("\n").join("<br>") || '',
            to:newInvoice.to.split("\n").join("<br>") || '',
            invoiceNo:newInvoice.invoiceNo || '',
            pan:newInvoice.panNo || '',
            gst:newInvoice.gstNo || '',
            invoiceDate:moment(newInvoice.date).format('DD-MMM-YYYY') || '',
            stotal:newInvoice.subTotal || 0,
            tax:newInvoice.tax || 0,
            discount:newInvoice.discount ||'',
            total:newInvoice.total || '',
            note:newInvoice.notes.split("\n").join("<br>") || '',
            terms:newInvoice.terms.split("\n").join("<br>") || '',
            tableData:newInvoice.services || ''
        }

        if(org){
            leads.isrc = org.portfolios[0].letterheadheader || org.coverImage;
        }
        //console.log(JSON.stringify(leads));
        newInvoice.save().then(function(quote){
            var template1 = new EmailTemplate(path.join(templatesDir, 'invoice'));
            pdfile = config.media.upload.local.path + '/'+quote.id+'.pdf';
             var html = ejs.render(fs.readFileSync(path.join(templatesDir, 'invoice/html.ejs'), 'utf8'),leads);
            pdf.create(html, options).toFile(pdfile, function(err, res1) {
              if (err) return console.log(err);
              console.log('---'+JSON.stringify(res1));
              var email = true;
               // upload(req,res,res1.filename,quote.id,email);
            });
        });
    });
};

var upload = function(req,res,path,id,email){
   var user = JSON.parse(req.decoded);
    //var scriptName = path.basename(path);
    fs.readFile(path,  function (err,pdata){
        var file = 
            { name: id+'.pdf',
              data: pdata,
              encoding: '7bit',
              mimetype: 'application/pdf'
          };
        if (!err) {
            console.log(file);
            console.log('received data: ' + file.name);
            var obj = new Object();
            obj.name = file.name;
            obj.mime = file.mimetype;
            obj.forId = id;
            obj.userId = user.userId;
            obj.updatedBy =  user.userId;
            obj.portfolioId = user.portfolioId;
            obj.updatedOn = r.now();
            var newMedia = new Media(obj);
            var sendResponse = function(error, data){
                console.log('error==='+error);
                console.log(data);
                if(!error){
                    var coverImage = config.media.upload.s3.accessUrl + '/media/'+ file.name;
                    Invoice.get(id).update({mediaId: data.mediaId, pdf: coverImage}).then(function(r1) {
                        if(email == false){
                            res.json({
                                result: r1.pdf
                            });
                        }else{
                            Lead.filter({leadId: r1.leadId,portfolioId: user.portfolioId}).run().then(function(lead){
                                Customer.get(lead[0].customerId).run().then(function(customer){
                                    if(customer.email){
                                        console.log(customer.email);
                                        //var template1 = new EmailTemplate(path.join(templatesDir, 'invoice'));
                                        Preference.filter({portfolioId:user.portfolioId}).run().then(function(eConfig){
                                            var portfolioEmailId,portfolioSenderName;
                                            if(eConfig && eConfig.length>0 && eConfig[0].emailConfig){
                                                var mailConf = eConfig[0];
                                                 portfolioEmailId= mailConf.emailConfig.gmail.auth.user;
                                                 portfolioSenderName= mailConf.emailConfig.gmail.auth.name;
                                                 var sender = mailConf.emailConfig.gmail;
                                            }else{
                                                var sender=config.smtp.gmail;
                                                portfolioEmailId= 'support@zinetgo.com';
                                                portfolioSenderName= 'Zinetgo';
                                            }
                                            emailService.sendEmail(sender, {
                                                from: '"'+user.name +'" <'+portfolioEmailId+'>', // sender address
                                                to: customer.email, // list of receivers
                                                subject: 'Your '+user.name+' invoice for '+r1.leadId, // Subject line
                                                text: 'Invoice',
                                                html:'Dear '+customer.name+',<br><br> Your '+user.name+' invoice is available.<br> <p>Invoice Number: '+r1.invoiceNo+'</p><p>Amount: '+r1.total+'</p>',
                                                attachments: [
                                                    {   // filename and content type is derived from path
                                                        filename: r1.leadId+'.pdf',
                                                        path: r1.pdf, 
                                                        contentType: 'application/pdf'
                                                    }
                                                ]  
                                            }), function(err, success) {
                                                console.log(success);
                                                if (err) {
                                                    console.log(err);
                                                }
                                            }
                                            res.send(200,{message:'email send successfully'});
                                        });
                                    }else{
                                        res.send(500,{error:'please update the customer email address'})
                                    }
                                }).error(handleError(res));
                            }).error(handleError(res));
                        }
                    }).error(handleError(res));
                    
                }else{
                    return res.send(500, {error: error.message});
                }
            }
            newMedia.save().then(function(media) {  
                MediaService.uploadPdf(file, media.id, sendResponse);
            });
        } else {
            console.log(err);
        }
    });
}

// update user
exports.updateInvoice = function (req, res) {
    Invoice.get(req.body.id).update(req.body).then(function(result) {
        var user = JSON.parse(req.decoded);
        result.updatedBy =user.userId;
        result.updatedOn =r.now();
        res.json({
            result: result
        });
    }).error(handleError(res));
};

function handleError(res) {
    return function(error) {
        console.log(error.message);
        return res.send(500, {error: error.message});
    }
}