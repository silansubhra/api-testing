var thinky = require(__dirname+'/../util/thinky.js'),
    r = thinky.r,
    Product = require(__dirname+'/../model/item.js'),
    Org = require(__dirname+'/../model/org.js'),
    Branch = require(__dirname+'/../model/branch.js'),
    ProductInventory = require(__dirname+'/../model/productInventory.js'),
    Category = require(__dirname+'/../model/category.js'),
    MediaService = require(__dirname+'/mediaservice.js'),
    Media = require(__dirname+'/../model/media.js'),
    og = require(__dirname+'/../util/og.js'),
    env = require(__dirname+'/../../env'),
    config = require(__dirname+'/../../config/'+ env.name),
    numeral = require('numeral');

// list products
// TODO: all filter, page size and offset, columns, sort
exports.listProducts = function (req, res) {
    var count;
    var pno=1,offset=0,limit=12;
    if(req.query.psize != undefined && req.query.psize != null && !isNaN(req.query.psize)){
        limit = parseInt(req.query.psize);
    }
    if(req.query.pno != undefined && req.query.pno != null && !isNaN(req.query.pno)){
       pno =  parseInt(req.query.pno);
    }
    offset = (pno -1) * limit;
    var sort =req.query.sort;
    var pluck = req.query.pluck;
    var token = req.body.token || req.query.token || req.headers['x-access-token'];
    var uName = req.params.id;
    var input =req.query.q;
    if(input && input!=null){
    var q1 ="(?i)"+input;
    }else{
        var q1 ='';
    }
    var filter ={   
        status: 'active',
        type: 'product'
    } 
    if(req.query.featured && req.query.featured !=null && !isNaN(req.query.featured)){
        if(req.query.featured ==='1'){
            filter.isFeatured = true;
        }else if(req.query.featured ==='0'){
            filter.isFeatured = false;
        }
    } 
    if(req.query.sale === 'true'){
        filter.isSale = true;
    }else{
      filter =filter;  
    }
    if(req.query.sort==='asc'){
        var sorting = r.asc('price');
    }else if(req.query.sort===  'desc'){
        var sorting = r.desc('price');
    }else{
        var sorting = r.desc('createdOn');
    }

    if(req.decoded !=undefined && req.decoded !=null){
        var tokenObject = JSON.parse(req.decoded);
        filter.portfolioId =tokenObject.portfolioId;
        Product.filter(filter).run().then(function(prd){
            if(prd && prd.length >0){
                Product.filter(filter).max('price').then(function(maxProduct){
                    if(req.query.toPrice && req.query.toPrice !=null && !isNaN(req.query.toPrice) && req.query.fromPrice && req.query.fromPrice !=null && !isNaN(req.query.fromPrice)){
                        var fromPrice = parseInt(req.query.fromPrice);
                        var toPrice = parseInt(req.query.toPrice)+1; 
                    }else{
                        var fromPrice = 0;
                        var toPrice = maxProduct.price+1;
                    }
                    if(uName && uName!=null){
                        Category.filter({portfolioId: tokenObject.portfolioId,uName: uName}).run().then(function(cat){
                            if(cat && cat.length>0){
                                var catId = cat[0].id;
                                filter.categoryId=catId;
                            }else{
                                res.status(404).send({ error: 'Not Found' });
                            }
                            filter.portfolioId =tokenObject.portfolioId;
                            Product.between(fromPrice, toPrice, {index: "price"}).orderBy(sorting).filter(filter).filter(function(doc){
                                return doc('name').match(q1)}).getJoin({slot: true, category: true,productVariation: true,inventorylog:true, productInventory: true,media: true}).skip(offset).limit(limit).run().then(function(products) {
                                Product.between(fromPrice, toPrice, {index: "price"}).filter(filter).filter(function(doc){
                                    return doc('name').match(q1)}).count().execute().then(function(total) {
                                    res.json({
                                        data: products,
                                        total: total,
                                        pno: pno,
                                        psize: limit
                                    });
                                }).error(handleError(res));    
                           }).error(handleError(res));
                        }).error(handleError(res));
                    }else{
                        filter.portfolioId =tokenObject.portfolioId;
                        Product.between(fromPrice, toPrice, {index: "price"}).orderBy(sorting).filter(filter).filter(function(doc){
                            return doc('name').match(q1)}).getJoin({slot: true, category: true,productVariation: true,inventorylog:true, productInventory: true}).skip(offset).limit(limit).run().then(function(products) {
                            Product.between(fromPrice, toPrice, {index: "price"}).filter(filter).filter(function(doc){
                                return doc('name').match(q1)}).count().execute().then(function(total) {
                                res.json({
                                    data: products,
                                    total: total,
                                    pno: pno,
                                    psize: limit
                                });
                            }).error(handleError(res));    
                       }).error(handleError(res));
                        handleError(res);
                    }    
                }).error(handleError(res)); 
            }else{
                res.json({
                    data: [],
                    total: 0,
                    pno: pno,
                    psize: limit
                });
            }
        });
        
    }else{
        var apiKey = req.query.apikey;
        Portfolio.filter({apiKey: apiKey}).run().then(function(portfolio){
            var portId = portfolio[0].id;
            filter.portfolioId = portId;
            Product.filter(filter).run().then(function(prd){
                if(prd && prd.length >0){
                    Product.filter({portfolioId: portId}).max('price').default(0).then(function(maxProduct){
                        if(req.query.toPrice && req.query.toPrice !=null && !isNaN(req.query.toPrice) && req.query.fromPrice && req.query.fromPrice !=null && !isNaN(req.query.fromPrice)){
                            var fromPrice = parseInt(req.query.fromPrice);
                            var toPrice = parseInt(req.query.toPrice); 
                        }else{
                            var fromPrice = 0;
                            var toPrice = maxProduct.price+1;
                        }
                        if(uName && uName!=null){
                            Category.filter({portfolioId: portId,uName: uName}).run().then(function(cat){
                                if(cat && cat.length>0){
                                    var catId = cat[0].id;
                                    filter.categoryId=catId;
                                }else{
                                    res.status(404).send({ error: 'Not Found' });
                                }
                                filter.portfolioId = portId
                                Product.between(fromPrice, toPrice, {index: "price"}).orderBy(sorting).filter(filter).filter(function(doc){
                                    return doc('name').match(q1)}).getJoin({slot: true, category: true,inventorylog:true,productVariation: true, productInventory: true}).skip(offset).limit(limit).run().then(function(products) {
                                    Product.between(fromPrice, toPrice, {index: "price"}).filter(filter).filter(function(doc){
                                        return doc('name').match(q1)}).count().execute().then(function(total) {
                                            
                                        res.json({
                                            data: products,
                                            total: total,
                                            pno: pno,
                                            psize: limit
                                        });
                                    }).error(handleError(res));    
                               }).error(handleError(res));
                            }).error(handleError(res));;
                        }else{
                            filter.portfolioId = portId
                            Product.between(fromPrice, toPrice, {index: "price"}).orderBy(sorting).filter(filter).filter(function(doc){
                                    return doc('name').match(q1)}).getJoin({slot: true, category: true,inventorylog:true,productVariation: true, productInventory: true}).skip(offset).limit(limit).run().then(function(products) {
                                    Product.between(fromPrice, toPrice, {index: "price"}).filter(filter).filter(function(doc){
                                        return doc('name').match(q1)}).count().execute().then(function(total) {
                                            
                                        res.json({
                                            data: products,
                                            total: total,
                                            pno: pno,
                                            psize: limit
                                        });
                                    }).error(handleError(res));    
                               }).error(handleError(res));
                            handleError(res);
                        }
                    }).error(handleError(res));  
                }else{
                    res.json({
                        data: [],
                        total: 0,
                        pno: pno,
                        psize: limit
                    });
                }
            });
              
        }).error(handleError(res));
    } 
};

exports.listNewProducts = function (req, res) {
    var count;
    var pno=1,offset=0,limit=10;
    if(req.query.psize != undefined && req.query.psize != null && !isNaN(req.query.psize)){
        limit = parseInt(req.query.psize);
    }

    if(req.query.pno != undefined && req.query.pno != null && !isNaN(req.query.pno)){
       pno =  parseInt(req.query.pno);
    }

    offset = (pno -1) * limit;

    var sort =req.query.sort;
    var pluck = req.query.pluck;
    var token = req.body.token || req.query.token || req.headers['x-access-token'];
    var uName = req.params.id;
    var today =new Date();
    var priorDate = new Date(new Date().setDate(today.getDate() - 10));
    today.setDate(today.getDate()+1);
    var tdDate=today.getDate();
    var tdMonth=today.getMonth() +1;
    var tdYear =today.getFullYear();

    var pDate = priorDate.getDate();
    var pMonth = priorDate.getMonth() +1;
    var pYear = priorDate.getFullYear();
    var filter ={   
        status: 'active',
        type: 'product'
    }

    if(req.query.sort==='asc'){
        var sorting = r.asc('price');
    }else if(req.query.sort==='desc'){
        var sorting = r.desc('price');
    }else{
        var sorting = r.desc('createdOn');
    }

    if(req.decoded !=undefined && req.decoded !=null){
        var tokenObject = JSON.parse(req.decoded);
        filter.portfolioId =tokenObject.portfolioId;
        Product.filter({portfolioId: tokenObject.portfolioId}).max('price').default(0).then(function(maxProduct){
            if(req.query.toPrice && req.query.toPrice !=null && !isNaN(req.query.toPrice) && req.query.fromPrice && req.query.fromPrice !=null && !isNaN(req.query.fromPrice)){
                var fromPrice = parseInt(req.query.fromPrice);
                var toPrice = parseInt(req.query.toPrice)+1; 
            }else{
                var fromPrice = 0;
                var toPrice = maxProduct.price+1;
            }
            if(uName && uName!=null){
                Category.filter({portfolioId: tokenObject.portfolioId,uName: uName}).run().then(function(cat){
                    if(cat && cat.length>0){
                        var catId = cat[0].id;
                        filter.categoryId=catId;
                    }else{
                        res.status(404).send({ error: 'Not Found' });
                    }
                    Product.between(fromPrice, toPrice, {index: "price"}).orderBy(sorting).filter(filter).getJoin({category: true,productVariation: true, productInventory: true}).filter(r.row('createdOn').during(r.time(pYear, pMonth, pDate,"Z"), r.time(tdYear, tdMonth, tdDate,"Z"), {rightBound: "closed"})).skip(offset).limit(limit).run().then(function(products) {
                        r.table("item").between(fromPrice, toPrice, {index: "price"}).filter(filter).filter(r.row('createdOn').date().during(r.time(pYear, pMonth, pDate,"Z"), r.time(tdYear, tdMonth, tdDate,"Z"), {rightBound: "closed"})).count().run().then(function(total) {
                           
                            res.json({
                                data: products,
                                total: (total!=undefined?total:0),
                                pno: pno,
                                psize: limit
                            });
                        });    
                    }).error(handleError(res)); 
                    handleError(res);
                }).error(handleError(res));
            }else{
                Product.between(fromPrice, toPrice, {index: "price"}).orderBy(sorting).filter(filter).getJoin({category: true,productVariation: true, productInventory: true}).filter(r.row('createdOn').during(r.time(pYear, pMonth, pDate,"Z"), r.time(tdYear, tdMonth, tdDate,"Z"), {rightBound: "closed"})).skip(offset).limit(limit).run().then(function(products) {
                    r.table("item").between(fromPrice, toPrice, {index: "price"}).filter(filter).filter(r.row('createdOn').date().during(r.time(pYear, pMonth, pDate,"Z"), r.time(tdYear, tdMonth, tdDate,"Z"), {rightBound: "closed"})).count().run().then(function(total) {
                       
                        res.json({
                            data: products,
                            total: (total!=undefined?total:0),
                            pno: pno,
                            psize: limit
                        });
                    });    
                }).error(handleError(res)); 
                handleError(res);
            }
        }).error(handleError(res));    
    }else{
        var apiKey = req.query.apikey;
        Portfolio.filter({apiKey: apiKey}).run().then(function(portfolio){
            var portId = portfolio[0].id;
            filter.portfolioId = portId;
            Product.filter({portfolioId: portId}).max('price').default(0).then(function(maxProduct){
                if(req.query.toPrice && req.query.toPrice !=null && !isNaN(req.query.toPrice) && req.query.fromPrice && req.query.fromPrice !=null && !isNaN(req.query.fromPrice)){
                    var fromPrice = parseInt(req.query.fromPrice);
                    var toPrice = parseInt(req.query.toPrice)+1; 
                }else{
                    var fromPrice = 0;
                    var toPrice = maxProduct.price+1;
                }
                if(uName && uName!=null){
                    Category.filter({portfolioId: portId,uName: uName}).run().then(function(cat){
                        if(cat && cat.length>0){
                            var catId = cat[0].id;
                            filter.categoryId=catId;
                            Product.between(fromPrice, toPrice, {index: "price"}).orderBy(sorting).filter(filter).getJoin({category: true,productVariation: true, productInventory: true}).filter(r.row('createdOn').during(r.time(pYear, pMonth, pDate,"Z"), r.time(tdYear, tdMonth, tdDate,"Z"), {rightBound: "closed"})).skip(offset).limit(limit).run().then(function(products) {
                                r.table("item").between(fromPrice, toPrice, {index: "price"}).filter(filter).filter(r.row('createdOn').date().during(r.time(pYear, pMonth, pDate,"Z"), r.time(tdYear, tdMonth, tdDate,"Z"), {rightBound: "closed"})).count().run().then(function(total) {
                                   
                                    res.json({
                                        data: products,
                                        total: (total!=undefined?total:0),
                                        pno: pno,
                                        psize: limit
                                    });
                                });    
                            }).error(handleError(res)); 
                            handleError(res);
                        }else{
                            res.status(404).send({ error: 'Not Found' });
                        }
                    }).error(handleError(res));;
                }else{
                    Product.between(fromPrice, toPrice, {index: "price"}).orderBy(sorting).filter(filter).getJoin({category: true,productVariation: true, productInventory: true}).filter(r.row('createdOn').during(r.time(pYear, pMonth, pDate,"Z"), r.time(tdYear, tdMonth, tdDate,"Z"), {rightBound: "closed"})).skip(offset).limit(limit).run().then(function(products) {
                        r.table("item").between(fromPrice, toPrice, {index: "price"}).filter(filter).filter(r.row('createdOn').date().during(r.time(pYear, pMonth, pDate,"Z"), r.time(tdYear, tdMonth, tdDate,"Z"), {rightBound: "closed"})).count().run().then(function(total) {
                           
                            res.json({
                                data: products,
                                total: (total!=undefined?total:0),
                                pno: pno,
                                psize: limit
                            });
                        });    
                    }).error(handleError(res)); 
                    handleError(res);
                }
            }).error(handleError(res));    
        }).error(handleError(res));
    } 
};
// get by id
exports.getProduct = function (req, res) {
    var uName = req.params.id;
    if(req.decoded !=undefined && req.decoded !=null){
        var tokenObject = JSON.parse(req.decoded);
        Product.filter({portfolioId :tokenObject.portfolioId,uName: uName}).run().then(function(pro){
            if(pro && pro.length>0){
                var porId = pro[0].id;
                Product.get(porId).getJoin({category: true,media: true,productVariation: true,inventorylog:true, productInventory: true}).run().then(function(product) {
                 res.json({
                     product: product
                 });
                }).error(handleError(res));
            }
        });
    }else{
        var apiKey = req.apikey ;
        Portfolio.filter({apiKey: apiKey}).run().then(function(portfolio){
            var portId = portfolio[0].id;
            Product.filter({portfolioId :portId,uName: uName}).run().then(function(pro){
                if(pro && pro.length>0){
                    var porId = pro[0].id;
                    Product.get(porId).getJoin({category: true,productVariation: true,inventorylog:true, productInventory: true}).run().then(function(product) {
                     res.json({
                         product: product
                     });
                    }).error(handleError(res));
                }
            });
        });    
    }          
};


// delete by id
exports.deleteProduct = function (req, res) {
    var id = req.params.id;
    Product.get(id).delete().run().then(function(org) {
        ProductInventory.filter({productId: id}).run().then(function(pi){
            if(pi && pi.length >0){
                ProductInventory.filter({productId: id}).delete().run().then(function(del){
                    res.json({
                        status: "success"
                    });
                }).error(handleError(res));
            }else{
                res.json({
                    status: "success"
                });
            }
        }).error(handleError(res));
        
    }).error(handleError(res));
};

// Add user
exports.addProduct = function (req, res) {
    var newProduct = new Product(req.body);
    var user = JSON.parse(req.decoded);
    var portfolio = JSON.parse(req.decoded);
    newProduct.portfolioId=user.portfolioId;
    newProduct.createdBy =user.userId;
    newProduct.updatedBy =user.userId;
    if (user.centerId !=null || user.centerId !=undefined || !isNaN(user.centerId)) {
        newProduct.centerId = user.centerId;
    };
    newProduct.updatedOn =r.now();
    newProduct.type ='product';
    newProduct.rating = 3;
    newProduct.inHouseStock = parseInt(newProduct.stock) || 0;
    newProduct.employeeStock = 0;
    newProduct.uName = req.body.name.toLowerCase().split(' ').join('-');

    if(newProduct.isFeatured == '1'){
        newProduct.isFeatured = true;
    }else{
        newProduct.isFeatured = false;
    }
    if(req.files && req.files !=null){
        var file = req.files.fileToUpload;
        if(file && file != null){
            newProduct.picture = file.name;
        }else{
            newProduct.picture = null;
        }
    }
    if(newProduct.discountedPrice && newProduct.discountedPrice !=null && !isNaN(newProduct.discountedPrice) && newProduct.discountedPrice !=''){
        if(newProduct.discountedPrice < newProduct.price){
            newProduct.discountPercentage = Math.round(((newProduct.price - newProduct.discountedPrice)/newProduct.price)*100);
            newProduct.isSale =true;
        }
    }else{
        newProduct.discountedPrice = parseInt(newProduct.price);
    }
    newProduct.save().then(function(result) {
        r.table("inventorylog").insert([
        {productId: result.id,quantity: result.stock,comment:'product added to inventory',inhouse:result.stock,emp:0,portfolioId: user.portfolioId,
        createdBy :user.userId,updatedBy :user.userId,createdOn:r.now()}
        ]).run()
        if (!req.files){
            res.json({
                result: result
            });
        }else{
            if(file){
                var obj = new Object();
                obj.name = file.name;
                obj.mime = file.mimetype;
                obj.forId = result.id;
                obj.userId = user.userId;
                obj.updatedBy =  user.userId;
                obj.portfolioId = user.portfolioId;
                obj.updatedOn = r.now();
                var newMedia = new Media(obj);
            }

            var sendResponse = function(error, data){
                if(!error){
                    var coverImage = config.media.upload.s3.accessUrl + '/media/'+ og.getIdPath(data.mediaId) + '/' + file.name;

                    Product.get(result.id).update({mediaId: data.mediaId, coverImage: coverImage}).then(function(r1) {

                        res.json({
                            result: r1
                        });
                    }).error(handleError(res));
                    // res.json({
                    //     result: data
                    // });
                }else{
                    return res.send(500, {error: error.message});
                }
            }

            newMedia.save().then(function(media) {  
                MediaService.uploadMedia(file, media.id, sendResponse);
            });
        }
        
    }).error(handleError(res));
};

// update user
exports.updateProduct = function (req, res) {
    var user = JSON.parse(req.decoded);
    var data =req.body;
    if(data.discountedPrice && data.discountedPrice !=null && !isNaN(data.discountedPrice) && data.discountedPrice !=''){
        if(req.body.discountedPrice < req.body.price){
            data.discountPercentage = Math.round(((data.price - data.discountedPrice)/data.price)*100);
            data.isSale =true;
        }
    }else{
        data.discountedPrice = data.price;
    }

    data.updatedBy =user.userId;
    data.updatedOn =r.now();
    var uName= req.params.id;
    Product.filter({uName:uName,portfolioId:user.portfolioId}).run().then(function(prod){
        var product = prod[0];
        if(product && product.employeeStock && data.stock){
            data.inHouseStock = parseInt(data.stock) - product.employeeStock;
        }
        if(data.updatedStock){
            data.stock = product.stock + data.updatedStock;
            data.inHouseStock = product.inHouseStock + data.updatedStock;
        }

        if(req.files && req.files !=null){
            var file = req.files.fileToUpload;
            if(file && file != null){
                data.picture = file.name;
            }else{
                data.picture = null;
            }
        }
        if (!req.files){
            Product.get(product.id).update(data).then(function(result) {
                if(data.updatedStock){
                    r.table("inventorylog").insert([
                    {productId: product.id,quantity: data.updatedStock,comment:'updated stock',inhouse:result.inHouseStock,emp:result.employeeStock,portfolioId: user.portfolioId,
                    createdBy :user.userId,updatedBy :user.userId,createdOn:r.now()}
                    ]).run()
                }else if(data.stock){
                    r.table("inventorylog").insert([
                    {productId: product.id,quantity: data.stock,comment:'updated stock',inhouse:result.inHouseStock,emp:result.employeeStock,portfolioId: user.portfolioId,
                    createdBy :user.userId,updatedBy :user.userId,createdOn:r.now()}
                    ]).run()
                }  
                res.json({
                    result: result
                });
            }).error(handleError(res));
            
        }else{
            if(file){
                var obj = new Object();
                obj.name = file.name;
                obj.mime = file.mimetype;
                obj.forId = result.id;
                obj.userId = user.userId;
                obj.updatedBy =  user.userId;
                obj.portfolioId = user.portfolioId;
                obj.updatedOn = r.now();
                var newMedia = new Media(obj);
            }

            var sendResponse = function(error, data){
                if(!error){
                    data.coverImage = config.media.upload.s3.accessUrl + '/media/'+ og.getIdPath(data.mediaId) + '/' + file.name;
                    Product.get(product.id).update(data).then(function(result) {
                        if(data.updatedStock){
                            r.table("inventorylog").insert([
                            {productId: product.id,quantity: data.updatedStock,comment:'updated stock',inhouse:result.inHouseStock,emp:result.employeeStock,portfolioId: user.portfolioId,
                            createdBy :user.userId,updatedBy :user.userId,createdOn:r.now()}
                            ]).run()
                        }else if(data.stock){
                            r.table("inventorylog").insert([
                            {productId: product.id,quantity: data.stock,comment:'updated stock',inhouse:result.inHouseStock,emp:result.employeeStock,portfolioId: user.portfolioId,
                            createdBy :user.userId,updatedBy :user.userId,createdOn:r.now()}
                            ]).run()
                        }  
                        res.json({
                            result: result
                        });
                    }).error(handleError(res));
                }else{
                    return res.send(500, {error: error.message});
                }
            }
            newMedia.save().then(function(media) {  
                MediaService.uploadMedia(file, media.id, sendResponse);
            });
        }

    })
   
};


exports.uploadPhotos = function (req, res) {
    MediaService.addMedia(req,res);
};

function handleError(res) {
    return function(error) {
        console.log(error.message);
        return res.status(500).send({error: error.message});
    }
}