


var thinky = require(__dirname+'/../util/thinky.js'),
  r = thinky.r,
  Sms = require(__dirname+'/../model/sms.js'),
  env = require(__dirname+'/../../env'),
  config = require(__dirname+'/../../config/'+ env.name),
  Portfolio = require(__dirname+'/../model/portfolio.js'),
  Preference = require(__dirname+'/../model/preference.js'),
  User = require(__dirname+'/../model/user.js');
  var request=require('request');

// Create


function getBalance(authkey,callback){
    var url='http://sms.zinetgo.com/api/v3/?method=account.credits&api_key='+authkey;
    var encodeurl=encodeURI(url);
    request(encodeurl, function (error, response, body) {
      if (!error && response.statusCode == 200) {
        callback(null,response);
      }else if(error && error !=null){
        callback(error,null)
      }
    });
};

exports.smsBalance= function(req,res) {
    var user = JSON.parse(req.decoded);
    var meCallback = function(error, data){
      if(data && data !=null){
          var sms = JSON.parse(data.body);
          console.log(sms);
          if(sms.status==='OK'){
            res.send(sms.data.credits);
          }else if(sms.status==='A402'){
            res.send('0');
          }
      }else{
          res.send(error);
      }
    }
     Preference.filter({portfolioId:user.portfolioId}).run().then(function(eConfig){
        var portfolioEmailId,portfolioSenderName;
        if(eConfig && eConfig.length>0){
          var mailConf = eConfig[0];
           getBalance(mailConf.smsConfig.smsProvider.authKey,meCallback)
        }else{
          res.send('0');
        }
      });
};


function sendAlert(authKey, portfolio,callback){
  var mCallback = function(error,data){
   callback(null,null);
  }
 var meCallback = function(error,data){
    if(data && data !=null){
      var sms = JSON.parse(data.body);
      if((parseInt(sms.data.credits) <= 300 && parseInt(sms.data.credits) >= 297) || (parseInt(sms.data.credits) <= 250 && parseInt(sms.data.credits) >= 247) || (parseInt(sms.data.credits) <= 200 && parseInt(sms.data.credits) >= 197)|| (parseInt(sms.data.credits) <= 150 && parseInt(sms.data.credits) >= 157)){
        Portfolio.get(portfolio).run().then(function(port){
          if(port){
              var apiKey = config.sms.msg91.authKey;
              var senderId = config.sms.msg91.senderId;
              sendSms(apiKey,port.mobile,senderId,'Your sms balance is below '+sms.data.credits+'. Please recharge it to avoid interruption. Thank you.',mCallback);
          }else{
            callback(null,null);
          }
        });
      }else{
        callback(null,null);
      } 
    }else{
      res.send(error);
    }
 }
  getBalance(authKey,meCallback);
}


function sendSms(authkey,number,senderid,message,callback){
    var url='http://alerts.variforrm.in/api?method=sms.normal&api_key='+authkey+'&to='+number+'&sender='+senderid+'&message='+message+'&flash=0&unicode=0';
    var encodeurl=encodeURI(url);
	console.log(encodeurl);
    request(encodeurl, function (error, response, body) {
	console.log(error);
	//console.log(response);
	console.log(body+'---');
      if (!error && response.statusCode == 200) {
        callback(null,response);
      }else{
        callback(error,null);
      }
    });
}

exports.sendSms = function (req, apiKey, senderId, mobileNo, message) {
  if(req && req !=null){
    var user = req.decoded;
  }else{
    user = null;
  }
  var newSms = new Sms({
    mobile: mobileNo,
    message: message,
    sender: senderId,
    route: config.sms.msg91.defaultRoute,
    createdOn: r.now(),
    status: "NEW"
  }); 
  if(user != undefined && user != null){
    newSms.portfolioId = user.portfolioId;
    newSms.createdBy = user.userId;
  }
  
  if(req && req !=null && req.params.key ){
    Portfolio.filter({tpKey:req.params.key}).run().then(function(result){
      if(result && result.length >0){
        newSms.portfolioId = result[0].id;
        console.log(result[0].id);
        console.log(newSms.portfolioId);
        var mob =result[0].mobile;
        User.filter({mobile:result[0].mobile}).run().then(function(user){
          //newSms.createdBy = user[0].id;
          newSms.save().then(function(result) {
            var mCallback = function(err, response){
              if(err == null){
                console.log("req:"+JSON.stringify(response));
                Sms.get(result.id).update({status:"SENT"}).execute();
                //return res.status(200).send({message: "sms send failed"});
              }else{
                console.log("err:"+err);
              }
            }
            sendSms(apiKey,mobileNo,senderId,message,mCallback);
          });
        }).error(function(error){
          console.log(error);
        });
      }
    });
  }else{
    newSms.save().then(function(result) {
      
        
        var mCallback = function(err, response){
        if(err == null){
          console.log("req:"+JSON.stringify(response));
          Sms.get(result.id).update({status:"SENT"}).execute();
         // return res.status(200).send({message: "sms send successfully"});
        }else{
          console.log("err:"+err);
         // return res.status(500).send({message: "sms send failed"});
        }
      }
      sendSms(apiKey,mobileNo,senderId,message,mCallback);
    });
  }
};


exports.bulksms = function (req, res) {

  var bulkSms = new Sms(req.body);
  var user = JSON.parse(req.decoded);
  console.log("token---"+JSON.stringify(user));

  console.log('bulksms: '+bulkSms.mobile + ' '+bulkSms.message);
  Preference.filter({portfolioId:user.portfolioId}).run().then(function(eConfig){
      var portfolioEmailId,portfolioSenderName;
      if(eConfig && eConfig.length>0){
        var mailConf = eConfig[0];
        if(mailConf.smsConfig.enableSms==true){
          var portfolioSmsId = mailConf.smsConfig.smsProvider.senderId;
          if(bulkSms){
            console.log('comes here');
            if(bulkSms.mobile){
              console.log('comes here also');
              var mn = ''+bulkSms.mobile;
              var mobileNumbers = mn.split(',');
              for(i=0;i<mobileNumbers.length;i++){
                var newSms = new Sms({
                  mobile: mobileNumbers[i],
                    message: bulkSms.message,
                    sender: portfolioSmsId,
                    route: 4,
                    createdOn: r.now(),
                    status: "NEW"
                });

                if(user != undefined && user != null){
                    newSms.portfolioId = user.portfolioId;
                    newSms.createdBy = user.userId;
                  }

                newSms.save().then(function(result) {
                  var mmCallback = function(err,response){
                    if(response && response != null){
                        console.log("req:"+response);
                        Sms.get(result.id).update({status:"SENT"}).execute();
                        return res.status(200).send({message: "bulk sms processing"});
                      }else{
                        return res.status(500).send({message: "something went wrong"});
                      }
                  }
                  //var mmmcalback = function(error,data){
                    sendSms(mailConf.smsConfig.smsProvider.authKey,result.mobile,result.sender,result.message,mmCallback);
                  //}
                //  sendAlert(mailConf.smsConfig.smsProvider.authKey,user.portfolioId,mmmcalback);
                }).error(function(error){
                  console.log(error);
                });
              }
            }
          }
        }
      }else{
          res.send(404, {error: 'Invalid request'});
      } 
  });
};

