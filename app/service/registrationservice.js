var uuid = require('node-uuid'),
    crypto = require('crypto'),
    jwt    = require('jsonwebtoken'),
    thinky = require(__dirname+'/../util/thinky.js'),
    r = thinky.r,
    Org = require(__dirname+'/../model/org.js'),
    Branch = require(__dirname+'/../model/branch.js'),
    Customer = require(__dirname+'/../model/customer.js'),
    SalaryComponent = require(__dirname+'/../model/salaryComponent.js'),
    Portfolio = require(__dirname+'/../model/portfolio.js'),
    UserProfile = require(__dirname+'/../model/userProfile.js'),
    User = require(__dirname+'/../model/user.js'),
    smsService = require(__dirname+'/smsservice.js'),
    emailService = require(__dirname+'/emailservice.js'),
	env = require(__dirname+'/../../env'),
	config = require(__dirname+'/../../config/'+ env.name),
    EmailTemplate = require('email-templates').EmailTemplate,
    path = require('path'),
    templatesDir = path.resolve(__dirname, '..', 'templates'),
    template = new EmailTemplate(path.join(templatesDir, 'register'));



// Add user
exports.register = function (req, res) {
    //validate  
    // name, mobile
    if(req.body.mobile != undefined && req.body.mobile != null && req.body.name != undefined &&
        req.body.name != null &&  req.body.password != undefined && req.body.password != null){

        var passwd = req.body.password;

        if(req.body.email != undefined && req.body.email != null){
             var verificationtoken = jwt.sign({email: req.body.email}, config.secret,{expiresIn:"7d"})
        }else{
            var verificationtoken = null
        }
        if(req.body.address!=undefined && req.body.address !=null){
            address=req.body.address
        }else{
            var address = null
        }
        if(req.body.instantMessager!=undefined && req.body.instantMessager !=null){
        instantMessager=req.body.instantMessager
        }else{
            var instantMessager = null
        }
        if(req.body.picture!=undefined && req.body.picture !=null){
        picture=req.body.picture
        }else{
            var picture = null
        }
        if(req.body.referralCode!=undefined && req.body.referralCode !=null){
        referralCode=req.body.referralCode
        }else{
            var referralCode = null
        }
        if(req.body.socialAccounts!=undefined && req.body.socialAccounts !=null){

        socialAccounts=req.body.socialAccounts
        }else{
            var socialAccounts = null
        }
        if(req.body.website!=undefined && req.body.website !=null){
        website= req.body.website
        }else{
            var website = null
        }
        if(req.body.contacts!=undefined && req.body.contacts !=null){
        contacts= req.body.contacts
        }else{
            var contacts = null
        }
        if(req.body.emails!=undefined && req.body.emails !=null){
        emails= req.body.emails
        }else{
            var emails = null
        }
        if(req.body.email!=undefined && req.body.email !=null){
        email= req.body.email
        }else{
            var email = null
        }
        UserProfile.filter({mobile: parseInt(req.body.mobile),apiKey:'6ec1c63b-08a6-444f-ae6e-72a97d47c079',role:'admin'}).run().then(function(userProf){
            if(userProf && userProf.length > 0){
                return res.status(500).send({error: 'Already registered with zinetgo'});
            }else{
                Portfolio.filter({mobile: parseInt(req.body.mobile)}).run().then(function(portf){
                    if(portf && portf.length >0){
                        UserProfile.filter({mobile: parseInt(req.body.mobile),portfolioId:portf[0].id,role:'vendor'}).run().then(function(up){

                            var newUserProfile = new UserProfile({
                                name: req.body.name,
                                email : email,
                                mobile: req.body.mobile,
                                orgId: portf[0].orgId,
                                userId: up[0].userId,
                                portfolioId: portf[0].id,
                                apiKey: portf[0].apiKey,
                                createdBy: up[0].userId,
                                updatedBy :up[0].userId,
                                roles: ["admin"],
                                role:'admin',
                                permissions:{'/api/items/':["GET"],'/api/item/':["GET","PUT","POST","DELETE"],'/api/employees/':["GET"],
                                     '/api/employee/':["GET","PUT","POST","DELETE"],'/api/designations/':["GET"],'/api/designation/':["GET","PUT","POST","DELETE"],
                                     '/api/departments/':["GET"],'/api/department/':["GET","PUT","POST","DELETE"]
                                    }
                            });
                            newUserProfile.save().then(function(userProfile){
                                var newUserProfile1 = new UserProfile({
                                    name: req.body.name,
                                    password : crypto.createHash('md5').update(passwd).digest("hex"),
                                    email : email,
                                    mobile: req.body.mobile,
                                    orgId: portf[0].orgId,
                                    userId: up[0].userId,
                                    portfolioId: portf[0].id,
                                    apiKey: config.onground.apiKey,
                                    createdBy: up[0].userId,
                                    updatedBy :up[0].userId,
                                    status:'approval pending',
                                    role:'admin',
                                    roles: ["admin"],
                                    permissions:{'/api/items/':["GET"],'/api/item/':["GET","PUT","POST","DELETE"],'/api/employees/':["GET"],
                                     '/api/employee/':["GET","PUT","POST","DELETE"],'/api/designations/':["GET"],'/api/designation/':["GET","PUT","POST","DELETE"],
                                     '/api/departments/':["GET"],'/api/department/':["GET","PUT","POST","DELETE"]
                                    } 
                                });
                                newUserProfile1.save().then(function(userProfile1){
                                    res.status(200).send({error: 'registered successfully'});
                                })
                            }).error(function(error) {
                                console.log(error.message);
                                return res.status(500).send({error: error.message});
                            });
                        }).error(function(error) {
                            console.log(error.message);
                            return res.status(500).send({error: error.message});
                        });
                    }else{
                        var newOrg = new Org({
                             email: req.body.email,
                             mobile: req.body.mobile,
                             name: req.body.name,
                             socialAccounts: req.body.socialAccounts,
                             address: req.body.address,
                             orgType: req.body.orgType,
                             city: req.body.city
                        });
                        newOrg.save().then(function(org) {
                            if(req.body.smsProvider != undefined && req.body.smsProvider !=null){
                                    var smsProvider = req.body.smsProvider
                            }else{
                                var smsProvider = null
                            }
                            if(req.body.emailProvider != undefined && req.body.emailProvider !=null){
                                var emailProvider = req.body.emailProvider
                            }else{
                                var emailProvider = null
                            }
                            var uName = '';
                            var un = org.name.toLowerCase().split(' ');
                            for(var i=0;i<un.length;i++){
                                uName+= un[i].charAt(0);
                            }
                            var vmobile = ''+req.body.mobile;
                            uName += '-'+(vmobile.substring(0, 4));
                            var newPortfolio = new Portfolio({
                                orgId:org.id,
                                name: org.name,
                                email: req.body.email,
                                mobile:req.body.mobile,
                                apiKey : uuid.v4(),
                                tpKey : uuid.v4(),
                                smsProvider: smsProvider,
                                emailProvider: emailProvider,
                                contacts: req.body.contacts,
                                emails :req.body.emails,
                                website: req.body.website,
                                enableEmail: false,
                                enableSms: false,
                                wallet:0,
                                uName: uName,
                                city: org.city
                            });
                            
                            newPortfolio.save().then(function(portfolio) {
                                var newCustomer = new Customer({
                                    portfolioId: portfolio.id,
                                    password : passwd,
                                    name : req.body.name,
                                    email: email
                                });
                                newCustomer.save().then(function(customer) {
                                    var newUser = new User({
                                        createdOn : r.now(),
                                        mobile: req.body.mobile,
                                        name: req.body.name,
                                        password : crypto.createHash('md5').update(passwd).digest("hex"),
                                        verificationToken : verificationtoken,
                                        address : address,
                                        instantMessager: instantMessager,
                                        picture : picture,
                                        referralCode: referralCode,
                                        socialAccounts : socialAccounts,
                                        website : website,
                                        contacts : contacts,
                                        emails : emails,
                                        email : email,
                                        status : 'approval pending'
                                    });
                                    newUser.save().then(function(user) {
                                            var newUserProfile = new UserProfile({
                                            name: req.body.name,
                                            email : email,
                                            mobile: user.mobile,
                                            orgId: org.id,
                                            userId: user.id,
                                            portfolioId: portfolio.id,
                                            apiKey: portfolio.apiKey,
                                            createdBy: user.id,
                                            updatedBy :user.id,
                                            roles: ["admin"],
                                            role:'admin',
                                            permissions:{'/api/items/':["GET"],'/api/item/':["GET","PUT","POST","DELETE"],'/api/employees/':["GET"],
                                                 '/api/employee/':["GET","PUT","POST","DELETE"],'/api/designations/':["GET"],'/api/designation/':["GET","PUT","POST","DELETE"],
                                                 '/api/departments/':["GET"],'/api/department/':["GET","PUT","POST","DELETE"]
                                                }
                                            });

                                         newUserProfile.save().then(function(userProfile){
                                            var newUserProfile1 = new UserProfile({
                                                name: req.body.name,
                                                password : crypto.createHash('md5').update(passwd).digest("hex"),
                                                email : email,
                                                mobile: user.mobile,
                                                orgId: org.id,
                                                userId: user.id,
                                                portfolioId: portfolio.id,
                                                apiKey: config.onground.apiKey,
                                                createdBy: user.id,
                                                updatedBy :user.id,
                                                status:'approval pending',
                                                role:'admin',
                                                roles: ["admin"],
                                                permissions:{'/api/items/':["GET"],'/api/item/':["GET","PUT","POST","DELETE"],'/api/employees/':["GET"],
                                                 '/api/employee/':["GET","PUT","POST","DELETE"],'/api/designations/':["GET"],'/api/designation/':["GET","PUT","POST","DELETE"],
                                                 '/api/departments/':["GET"],'/api/department/':["GET","PUT","POST","DELETE"]
                                                } 
                                            });

                                         newUserProfile1.save().then(function(userProfile1){
                                            //console.log("apiKey"+userProfile1[0].apiKey);
                                               var newBranch = new Branch({
                                                    name: org.name + ' HQ',
                                                    address: org.address,
                                                    geoLocation: org.geoLocation,
                                                    mobile: org.mobile,
                                                    contacts: org.contacts,
                                                    email: org.email,
                                                    emails: org.emails,
                                                    orgId: org.id,
                                                    portfolioId: portfolio.id,
                                                    createdBy :user.id,
                                                    updatedBy :user.id,
                                                    isDefault: true,
                                                    city: org.city
                                                });

                                                newBranch.save().then(function(branch) {
                                                    r.table("salaryComponent").insert([
                                                    {name: "Basic",value: 50,calculationType: "formulae",mapTo: "grossSalary",payType:"Earning",orgId: org.id,portfolioId: portfolio.id,
                                                    createdBy :user.id,updatedBy :user.id,createdOn:r.now(),effectiveDate:r.now(),rank:1},
                                                    {name: "HRA",value: 25, calculationType: "formulae", mapTo: "Basic",payType:"Earning",orgId: org.id,portfolioId: portfolio.id,
                                                    createdBy :user.id,updatedBy :user.id,createdOn:r.now(),effectiveDate:r.now(),rank:2},
                                                    {name: "Conveyance",value: 19200,calculationType: "flat",payType:"Earning",orgId: org.id,portfolioId: portfolio.id,
                                                    createdBy :user.id,updatedBy :user.id,createdOn:r.now(),effectiveDate:r.now(),rank:3},
                                                    {name: "Other Allowances",value: 0,calculationType: "flat",payType:"Earning",orgId: org.id,portfolioId: portfolio.id,
                                                    createdBy :user.id,updatedBy :user.id,createdOn:r.now(),effectiveDate:r.now(),rank:4},
                                                    {name: "LOP",value: 0,calculationType: "flat",payType:"Deduction",orgId: org.id,portfolioId: portfolio.id,
                                                    createdBy :user.id,updatedBy :user.id,createdOn:r.now(),effectiveDate:r.now(),rank:5}
                                                    ]).run()
                                                    var port ={
                                                            name: portfolio.name,
                                                            mobile: portfolio.mobile,
                                                            passwd : passwd,
                                                            vToken :verificationtoken,
                                                            url :config.url
                                                         };
                                                    template.render(port, function (err, tmp) {
                                                        if (err) {
                                                        return console.error(err)
                                                        }
                                                        //smsService.sendSms(newUser.mobile,tmp.text);
                                                        if(branch.email != undefined && branch.email != null){

                                                         emailService.sendEmail(config.smtp.gmail,{
                                                         portfolioId: portfolio.id,
                                                         createdBy: user.id,
                                                         updatedBy: user.id,
                                                         from: '"Zinetgo Support" <support@zinetgo.com>', // sender address
                                                         to: branch.email, // list of receivers
                                                         subject: tmp.subject, // Subject line
                                                          text: tmp.text,
                                                          html: tmp.html
                                                           });
                                                        }
                                                    });

                                                 res.json({
                                                    org: org,
                                                    branch: branch,
                                                    user: user,
                                                    profile:userProfile,
                                                    profile1: userProfile1,
                                                    portfolio: portfolio
                                                  });
                                               }).error(function(error) {
                                               Org.get(org.id).delete().execute();
                                               Branch.get(branch.id).delete().execute();
                                               console.log(error.message);
                                               return res.status(500).send({error: error.message});
                                               });
                                            }).error(function(error) {
                                            //Org.get(org.id).delete().execute();
                                            console.log(error.message);
                                            return res.status(500).send({error: error.message});
                                           });
                                       }).error(function(error) {
                                        console.log(error.message);
                                        return res.status(500).send({error: error.message});
                                       });
                                    }).error(function(error) {
                                        console.log(error.message);
                                        return res.status(500).send({error: error.message});
                                    });
                                }).error(function(error) {
                                    console.log(error.message);
                                    return res.status(500).send({error: error.message});
                                });
                            }).error(function(error) {
                                console.log(error.message);
                                return res.status(500).send({error: error.message});
                            });
                        }).error(function(error) {
                            console.log(error.message);
                            return res.status(500).send({error: error.message});
                        });
                    }
                });
            }
        });
    }else{
        return res.status(500).send({error: 'Mandatory fields mobile or name is missing'});
    }
};
