var thinky = require(__dirname+'/../util/thinky.js'),
    r = thinky.r,
    Item = require(__dirname+'/../model/item.js'),
    Org = require(__dirname+'/../model/org.js'),
    Branch = require(__dirname+'/../model/branch.js'),
    MediaService = require(__dirname+'/mediaservice.js'),
    env = require(__dirname+'/../../env'),
    MediaService = require(__dirname+'/mediaservice.js'),
    Media = require(__dirname+'/../model/media.js'),
    og = require(__dirname+'/../util/og.js'),
    config = require(__dirname+'/../../config/'+ env.name);

// list items
// TODO: all filter, page size and offset, columns, sort
exports.listItems = function (req, res) {
    var count;
    var pno=1,offset=0,limit=10;
    if(req.query.psize != undefined && req.query.psize != null && !isNaN(req.query.psize)){
        limit = parseInt(req.query.psize);
    }

    if(req.query.pno != undefined && req.query.pno != null && !isNaN(req.query.pno)){
       pno =  parseInt(req.query.pno);
    }

    offset = (pno -1) * limit;
    var filter = {
        status: 'active',
        type:'service'
    }
    if(req.query.status ==='inactive'){
        filter.status = req.query.status;
    }
    
    var input =req.query.q;
    if(input && input!=null){
    var q1 ="(?i)"+input;
    }else{
        var q1 ='';
    }
    var token = req.body.token || req.query.token || req.headers['x-access-token'];
   if(req.decoded !=undefined && req.decoded !=null){
        var tokenObject = JSON.parse(req.decoded);
        filter.portfolioId = tokenObject.portfolioId;
        if(tokenObject.apiKey !==config.onground.apiKey){
            filter.mpId = tokenObject.mpId;
        }
        Item.orderBy({index: r.desc('createdOn')}).filter(filter).getJoin({category :true}).filter(function(doc){
                return doc('name').match(q1).default(false).
        or(doc('uName').match(q1).default(false));}).skip(offset).limit(limit).run().then(function(items) {
             Item.filter(filter).filter(function(doc){
                return doc('name').match(q1).default(false).
        or(doc('uName').match(q1).default(false));}).count().execute().then(function(total) {
                count = total;
                console.log(total);
                res.json({
                    data: items,
                    total: count,
                    pno: pno,
                    psize: limit
                });
            });
        }).error(handleError(res));
    
    handleError(res);
    }else {
        var apiKey = req.query.apikey;
        console.log('apiKey---'+apiKey);
        Portfolio.filter({apiKey: apiKey}).run().then(function(portfolio){
            var portId = portfolio[0].id;
            filter.portfolioId = portId;
            Item.orderBy({index: r.desc('createdOn')}).filter(filter).getJoin({category :true}).filter(function(doc){
                return doc('name').match(q1).default(false).
        or(doc('uName').match(q1).default(false));}).skip(offset).limit(limit).run().then(function(items) {
                Item.filter(filter).filter(function(doc){
                return doc('name').match(q1).default(false).
        or(doc('uName').match(q1).default(false));}).count().execute().then(function(total) {
                    count = total;
                    console.log(total);
                    res.json({
                        data: items,
                        total: count,
                        pno: pno,
                        psize: limit
                    });
                });
            });
        }).error(handleError(res));
    }
};

// get by UName
exports.getItemByUname = function (req, res) {
    var uName = req.params.uName;
    var filter = {
        status: 'active',
        type:'service',
        uName: uName
    }
    if(req.decoded !=undefined && req.decoded !=null){
        var tokenObject = JSON.parse(req.decoded);
        filter.portfolioId = tokenObject.portfolioId;
        Item.filter(filter).getJoin({category :true, medias: true}).run().then(function(item) {
             res.json({
                 item: item[0]
             });
        }).error(handleError(res));
    }else{
        var apiKey = req.query.apikey;
        console.log('apiKey---'+apiKey);
        Portfolio.filter({apiKey: apiKey}).run().then(function(portfolio){
            var portId = portfolio[0].id;
            filter.portfolioId = portId;
            Item.filter(filter).getJoin({category :true, medias: true}).run().then(function(item) {
                 res.json({
                     item: item[0]
                 });
            }).error(handleError(res));
        }).error(handleError(res));
    }
};


exports.listItemsByVendor = function (req, res) {
    var count;
    var pno=1,offset=0,limit=10;
    if(req.query.psize != undefined && req.query.psize != null && !isNaN(req.query.psize)){
        limit = parseInt(req.query.psize);
    }

    if(req.query.pno != undefined && req.query.pno != null && !isNaN(req.query.pno)){
       pno =  parseInt(req.query.pno);
    }

    if(req.query.type != undefined && req.query.type != null && !isNaN(req.query.type)){
       var type =  req.query.pno;
    }

    var token = req.body.token || req.query.token || req.headers['x-access-token'];
    var tokenObject = JSON.parse(req.decoded);
    

    var filter = {
        status:'active'
    };
    var filter1 ={
        status:'inactive'
    };
    
    if(tokenObject.roles.indexOf('vendor')>-1){
        filter.mpId = tokenObject.mpId,
        filter1.mpId = tokenObject.mpId,
        filter.portfolioId = tokenObject.portfolioId;
        filter1.portfolioId = tokenObject.portfolioId;
        console.log(JSON.stringify(filter));
        Item.orderBy({index: r.desc('createdOn')}).filter(filter).hasFields('mpServiceId').getJoin({category :true,portfolio:true}).skip(offset).limit(limit).run().then(function(items) {
            Item.filter(filter).hasFields('mpServiceId').count().execute().then(function(total) {
                Item.orderBy({index: r.desc('createdOn')}).filter(filter1).hasFields('mpServiceId').getJoin({category :true,portfolio:true}).skip(offset).limit(limit).run().then(function(itemsin) {
                    Item.filter(filter1).hasFields('mpServiceId').count().execute().then(function(totalin) {
                        count = total;
                        console.log(total);
                        res.json({
                            data: items,
                            total: count,
                            data1: itemsin,
                            total: totalin,
                            pno: pno,
                            psize: limit
                        });
                     });
                }).error(handleError(res));
            });
        }).error(handleError(res));
    }else if(tokenObject.roles.indexOf('aggregator')>-1){
        filter.mpId = tokenObject.portfolioId,        
        filter1.mpId = tokenObject.portfolioId,
        Portfolio.filter({uName: req.query.sId}).run().then(function(port){
            if(port && port.length>0){
                var portfolio = port[0];
                filter.portfolioId = portfolio.id;
                filter1.portfolioId = portfolio.id;
                console.log('aggregator----'+JSON.stringify(filter));
                Item.orderBy({index: r.desc('createdOn')}).filter(filter).hasFields('mpServiceId').getJoin({category :true,portfolio:true}).skip(offset).limit(limit).run().then(function(items) {
                    Item.filter(filter).hasFields('mpServiceId').count().execute().then(function(total) {
                        Item.orderBy({index: r.desc('createdOn')}).filter(filter1).hasFields('mpServiceId').getJoin({category :true,portfolio:true}).skip(offset).limit(limit).run().then(function(itemsin) {
                            Item.filter(filter1).hasFields('mpServiceId').count().execute().then(function(totalin) {
                                count = total;
                                console.log(total);
                                res.json({
                                    data: items,
                                    total: count,
                                    data1: itemsin,
                                    total: totalin,
                                    pno: pno,
                                    psize: limit
                                });
                             });
                        }).error(handleError(res));
                    });
                }).error(handleError(res));
            }
        }).error(handleError(res)); 
    }else if(tokenObject.roles.indexOf('super-admin')>-1){
        Portfolio.filter({uName: req.query.sId}).run().then(function(port){
            if(port && port.length>0){
                var portfolio = port[0];
                filter.portfolioId = portfolio.id;
                filter1.portfolioId = portfolio.id;
                console.log('aggregator----'+JSON.stringify(filter));
                Item.orderBy({index: r.desc('createdOn')}).filter(filter).hasFields('mpServiceId').getJoin({category :true,portfolio:true}).skip(offset).limit(limit).run().then(function(items) {
                    Item.filter(filter).hasFields('mpServiceId').count().execute().then(function(total) {
                        Item.orderBy({index: r.desc('createdOn')}).filter(filter1).hasFields('mpServiceId').getJoin({category :true,portfolio:true}).skip(offset).limit(limit).run().then(function(itemsin) {
                            Item.filter(filter1).hasFields('mpServiceId').count().execute().then(function(totalin) {
                                count = total;
                                console.log(total);
                                res.json({
                                    data: items,
                                    total: count,
                                    data1: itemsin,
                                    total: totalin,
                                    pno: pno,
                                    psize: limit
                                });
                             });
                        }).error(handleError(res));
                    });
                }).error(handleError(res));
            }
        }).error(handleError(res)); 
    }else{
        return res.send(401,{error:'do not have permission'});
    }
};

exports.listItemsByPort = function (req, res) {
    var count;
    var pno=1,offset=0,limit=10;
    if(req.query.psize != undefined && req.query.psize != null && !isNaN(req.query.psize)){
        limit = parseInt(req.query.psize);
    }

    if(req.query.pno != undefined && req.query.pno != null && !isNaN(req.query.pno)){
       pno =  parseInt(req.query.pno);
    }

    offset = (pno -1) * limit;

    var token = req.body.token || req.query.token || req.headers['x-access-token'];
   
    var tokenObject = JSON.parse(req.decoded);
    console.log('portId-----'+ req.params.portId);
    console.log('mp service Id-----'+ req.query.sId);
    Item.orderBy({index: r.desc('createdOn')}).filter({portfolioId: req.params.portId,type:'service',mpId : tokenObject.portfolioId,mpServiceId: req.query.sId,status:'active'}).skip(offset).limit(limit).run().then(function(items) {
        Item.filter({portfolioId: req.params.portId,type:'service',mpId : tokenObject.portfolioId,mpServiceId: req.query.sId,status:'active'}).count().execute().then(function(total) {
            count = total;
            console.log(total);
            res.json({
                data: items,
                total: count,
                pno: pno,
                psize: limit
            });
        }).error(handleError(res));
    }).error(handleError(res));

handleError(res);
};

/*// list items by uname
exports.getItemByUname = function (req, res) {
    var uName = req.params.uName;
    Item.filter({uName: uName}).getJoin({category :true, medias: true}).run().then(function(item) {
     res.json({
         item: item[0]
     });
    }).error(handleError(res));
};*/


// get by id
exports.getItem = function (req, res) {
    var id = req.params.id;
    Item.get(id).getJoin({category :true, medias: true}).run().then(function(item) {
        console.log(JSON.stringify(item));
     res.json({
         item: item
     });
    }).error(handleError(res));
};


// delete by id
exports.deleteItem = function (req, res) {
    var id = req.params.id;
    Item.get(id).delete().run().then(function(service) {
        res.json({
            status: "success"
        });
    }).error(handleError(res));
};

exports.bulkDeleteItem = function (req, res) {
    var ids = req.query.ids;
    console.log("ids"+ids);
    if(ids && ids != null){
        var idsArray = ids.split(",");
        for(var i=0;i<idsArray.length;i++){
            var itmId =idsArray[i];
            console.log("itm"+itmId);
            var token = req.body.token || req.query.token || req.headers['x-access-token'];
            var tokenObject = JSON.parse(req.decoded);
            Item.filter({portfolioId: tokenObject.portfolioId,id: itmId,status: 'active'}).run().then(function(itmr){
                if(itmr && itmr>0){
                    Item.get(itmr.id).update({status:'inactive'}).run().then(function(branch) {
                        
                    }).error(handleError(res));
                }
            });
        }
        res.json({
            status: "success"
        });
    }else{
        res.send(404, {error: 'Select a item'});
    }  
};


// Add user
exports.addItem = function (req, res) {
    var newItem = new Item(req.body);
    var user = JSON.parse(req.decoded);
    newItem.createdBy =user.userId;
    newItem.portfolioId=user.portfolioId;
    newItem.updatedBy =user.userId;
    newItem.updatedOn =r.now();
    var nam = req.body.name;
    var nm =nam.toLowerCase();
    if(newItem.video){
        newItem.video = JSON.parse(newItem.video);
    }
    console.log(newItem);
    var newchar = '-'
    nm = nm.split(' ').join(newchar);
    newItem.uName = nm;
    if(user.mpId){
        newItem.mpId = user.mpId;
    }
    newItem.save().then(function(result) {
        if (!req.files){
            res.json({
                result: result
            });
        }else{
            console.log(req.files.fileToUpload);

            var file = req.files.fileToUpload;
            if(file){
                var obj = new Object();
                obj.name = file.name;
                obj.mime = file.mimetype;
                obj.forId = result.id;
                obj.userId = user.userId;
                obj.updatedBy =  user.userId;
                obj.portfolioId = user.portfolioId;
                obj.updatedOn = r.now();
                var newMedia = new Media(obj);
            }

            var sendResponse = function(error, data){
                if(!error){
                    var coverImage = config.media.upload.s3.accessUrl + '/media/'+ og.getIdPath(data.mediaId) + '/' + file.name;

                    Item.get(result.id).update({mediaId: data.mediaId, coverImage: coverImage}).then(function(r1) {
                       res.json({
                            result: data
                        });
                    }).error(handleError(res));
                }else{
                    return res.send(500, {error: error.message});
                }
            }

            newMedia.save().then(function(result) {  
                MediaService.uploadMedia(file, result.id, sendResponse);
            });
        }
        
    }).error(handleError(res));
};

// Add user
exports.addItemByVendor = function (req, res) {
    var newItem = new Item(req.body);
    var user = JSON.parse(req.decoded);
    newItem.createdBy =user.userId;
    newItem.updatedBy =user.userId;
    newItem.mpId = user.portfolioId;
    newItem.updatedOn =r.now();
    newItem.createdOn = r.now();
    var nam = req.body.name;
    var nm =nam.toLowerCase();
    console.log(newItem);
    var newchar = '-';
    nm = nm.split(' ').join(newchar);
    newItem.uName = nm;
    Portfolio.filter({uName: req.params.uname}).run().then(function(portfolio){
        if(portfolio && portfolio.length>0){
            newItem.portfolioId= portfolio[0].id;
            newItem.save().then(function(result) {
                res.json({
                    result: result
                });
            }).error(handleError(res));
        }else{
            return res(500).send({error:'in-valid vendor'});
        }
    }).error(handleError(res));
    
};
// update user
exports.updateItem = function (req, res) {
    var user = JSON.parse(req.decoded);
    var it = new Item(req.body);
    var user = JSON.parse(req.decoded);
    it.updatedBy =user.userId;
    it.updatedOn =r.now();
    if(req.files && req.files.oimage){
        var file = req.files.oimage;
        var obj = new Object();
            obj.name = file.name;
            obj.mime = file.mimetype;
            obj.forId = req.params.id;
            obj.userId = user.userId;
            obj.updatedBy =  user.userId;
            obj.portfolioId = user.portfolioId;
            obj.updatedOn = r.now();
            var newMedia = new Media(obj);
        var sendResponse = function(error, data){
            if(!error){
                var coverImage = config.media.upload.s3.accessUrl + '/media/'+ og.getIdPath(data.mediaId) + '/' + file.name;
                if(it.seo){
                    it.seo = JSON.parse(it.seo);
                }
                it.seo.ogImage = coverImage;
                it.seo.publish_date = r.now();
                it.seo.modified_date = r.now();

                Item.get(req.params.id).update(it).then(function(result) {
                    res.json({
                        result: result
                    });
                }).error(handleError(res));
            }else{
                return res.send(500, {error: error.message});
            }
        }
        newMedia.save().then(function(media) {  
            MediaService.uploadMedia(file, media.id, sendResponse);
        });
    }else{
        Item.get(req.params.id).update(it).then(function(result) {
            res.json({
                result: result
            });
        }).error(handleError(res));
    }
};

exports.updateProcess = function (req, res) {
    var it = new Item(req.body);
    var user = JSON.parse(req.decoded);
    it.updatedBy =user.userId;
    it.updatedOn =r.now();
    Item.get(req.params.id).update(it).then(function(result) {
        res.json({
            result: result
        });
    }).error(handleError(res));
};

function handleError(res) {
    return function(error) {
        console.log(error.message);
        return res.send(500, {error: error.message});
    }
}