var Insta = require('instamojo-nodejs'),
Portfolio = require(__dirname+'/../model/portfolio.js')
env = require(__dirname+'/../../env'),
Payment = require(__dirname+'/../model/payment.js'),
config = require(__dirname+'/../../config/'+ env.name);
/*data.webhook                 = 'Your endpoint to capture POST data from a payment';
data.redirect_url            = 'Your endpoint where instamojo redirects user to after payment';
 */

exports.payment = function (req, res) {
  var apiKey = req.query.apiKey;
	console.log('api key-----------'+apiKey); 
  Portfolio.filter({apiKey :apiKey}).run().then(function(portfolio){
    if(portfolio && portfolio.length>0){
      var iapikey = portfolio[0].instamojo.apiKey;
      var authtoken = portfolio[0].instamojo.authtoken;
      Insta.setKeys(iapikey, authtoken);
      var data = new Insta.PaymentData();
      var amnt = parseInt(req.body.amount);
      data.purpose = req.body.purpose;            // REQUIRED 
      data.amount = amnt;                  // REQUIRED 
      data.redirect_url=req.body.redirect_url;
      data.currency                = 'INR';
      data.buyer_name              = req.body.name;
      data.email                   = req.body.email;
      data.phone                   = req.body.mobile;
      data.send_sms                = false;
      data.send_email              = false;
      data.allow_repeated_payments = false;
      var newPayment = new Payment({
            orgId: portfolio[0].orgId,
            portfolioId: portfolio[0].id,
            orderId: data.purpose,
            amount: data.amnt,
            paymentDate: new Date(),
            paymentMode: 'Credit/Debit Card'
          });
          newPayment.save().then(function(payment){
            data.webhook = 'https://api.zinetgo.com/api/payment/'+ payment.id + '?apikey='+config.onground.apiKey,
            Insta.createPayment(data, function(error, response) {
              if (error) {
                // some error 
                res.status(500).send({ error: error });
              } else {
                // Payment redirection link at response.payment_request.longurl 
              console.log("payment done");
              
                console.log(response);
                response.paymentId = payment.id;
                res.json({
                   result:JSON.parse(response)
                });
              } 
            });
          });
    }else{
      return res.send(500, {error: 'Invalid request'}); 
    }
  });
};

/*Insta.seeAllLinks(function(error, response) {
  if (error) {
    // Some error 
  } else {
    console.log(response);
  }
});


Insta.getAllPaymentRequests(function(error, response) {
  if (error) {
    // Some error 
  } else {
    console.log(response);
  }
});*/
exports.refund = function (req, res) {
  var refund = new Insta.RefundRequest();
  refund.payment_id = '';     // This is the payment_id, NOT payment_request_id 
  refund.type       = '';     // Available : ['RFD', 'TNR', 'QFL', 'QNR', 'EWN', 'TAN', 'PTH'] 
  refund.body       = '';     // Reason for refund 
  refund.setRefundAmount(8);  // Optional, if you want to refund partial amount 
  Insta.createRefund(refund, function(error, response) {
    console.log(response);
  });
}
