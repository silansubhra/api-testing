var thinky = require(__dirname+'/../util/thinky.js'),
    r = thinky.r,
    Designation = require(__dirname+'/../model/designation.js'),
    Org = require(__dirname+'/../model/org.js');

// list designations
// TODO: all filter, page size and offset, columns, sort
exports.listDesignations = function (req, res) {
    var count;
    var pno=1,offset=0,limit=10;
    if(req.query.psize != undefined && req.query.psize != null && !isNaN(req.query.psize)){
        limit = parseInt(req.query.psize);
    }

    if(req.query.pno != undefined && req.query.pno != null && !isNaN(req.query.pno)){
       pno =  parseInt(req.query.pno);
    }

    offset = (pno -1) * limit;

    var sort =req.query.sort;
    var pluck = req.query.pluck;
    var token = req.body.token || req.query.token || req.headers['x-access-token'];
    var tokenObject = JSON.parse(req.decoded);
    if(sort == undefined || sort == null ){
        Designation.filter({portfolioId: tokenObject.portfolioId}).count().execute().then(function(total) {
                count = total;
                console.log(total);
            });    
        Designation.orderBy(r.desc('createdOn')).filter({portfolioId: tokenObject.portfolioId}).skip(offset).limit(limit).run().then(function(designations) {
            res.json({
                data: designations,
                total: count,
                pno: pno,
                psize: limit
            });
        }).error(handleError(res));
        handleError(res);
    }else{
        var result =sort.substring(0, 1);
        sortLength =sort.length;
        if(result ==='-'){
            field=sort.substring(1,sortLength);
            console.log("field--"+field);
            console.log(typeof field);
            console.log("has field--"+Designation.hasFields(field));
            if(Designation.hasFields(field)){
                Designation.filter({portfolioId: tokenObject.portfolioId}).count().execute().then(function(total) {
                    count = total;
                    console.log(total);
                });    
                Designation.orderBy(r.desc(field)).filter({portfolioId: tokenObject.portfolioId}).skip(offset).limit(limit).run().then(function(designations) {
                res.json({
                    data: designations,
                    total: count,
                    pno: pno,
                    psize: limit
                });
                }).error(handleError(res));
                handleError(res);   
            }else{
                res.status(500).send({ message: 'No field exist.'});
            }    
        }else{
            field=sort.substring(0,sortLength);
            console.log("field--"+field);
            console.log("has field--"+Designation.hasFields(field));
            if(Designation.hasFields(field)){
                Designation.filter({portfolioId: tokenObject.portfolioId}).count().execute().then(function(total) {
                    count = total;
                    console.log(total);
                });    
                Designation.orderBy(r.asc(field)).filter({portfolioId: tokenObject.portfolioId}).skip(offset).limit(limit).run().then(function(designations) {
                res.json({
                    data: designations,
                    total: count,
                    pno: pno,
                    psize: limit
                });
                }).error(handleError(res));
                handleError(res);  
            }else{
                res.status(500).send({ message: 'No field exist.'});
            }       
        }
    }    
};
// get by id
exports.getDesignation = function (req, res) {
    var id = req.params.id;
    Designation.get(id).getJoin({org: true}).run().then(function(designation) {
     res.json({
         designation: designation
     });
    }).error(handleError(res));
};


// delete by id
exports.deleteDesignation = function (req, res) {
    var id = req.params.id;
    Designation.get(id).delete().run().then(function(org) {
        res.json({
            status: "success"
        });
    }).error(handleError(res));
};

//bulk delete
exports.bulkDeleteDesignation = function (req, res) {
    console.log("bulk delete called")
    var ids = req.query.ids;
    console.log("ids"+ids);
    if(ids && ids != null){
        var idsArray = ids.split(",");
        for(var i=0;i<idsArray.length;i++){
            var desgId =idsArray[i];
            console.log("desg"+desgId);
            var token = req.body.token || req.query.token || req.headers['x-access-token'];
            var tokenObject = JSON.parse(req.decoded);
            Designation.filter({portfolioId: tokenObject.portfolioId,id: desgId,status: 'active'}).run().then(function(desgt){
                if(desgt && desgt>0){
                    Designation.get(desgt.id).update({status:'inactive'}).run().then(function(branch) {
                        
                    }).error(handleError(res));
                }
            });
        }
        res.json({
            status: "success"
        });
    }else{
        res.send(404, {error: 'Select a designation'});
    }  
};


// Add user
exports.addDesignation = function (req, res) {
    var newDesignation = new Designation(req.body);
    var user = JSON.parse(req.decoded);
    newDesignation.createdBy =user.userId;
    newDesignation.portfolioId=user.portfolioId;
    newDesignation.orgId = user.orgId;
    newDesignation.updatedBy =user.userId;
    newDesignation.updatedOn =r.now();
    console.log(newDesignation);
    newDesignation.save().then(function(result) {
        res.json({
            result: result
        });
    }).error(handleError(res));
};

// update user
exports.updateDesignation = function (req, res) {
    var deg = new Designation(req.body);
    var user = JSON.parse(req.decoded);
    deg.updatedBy =user.userId;
    deg.updatedOn =r.now();
    Designation.get(deg.id).update(deg).then(function(result) {
        res.json({
            result: result
        });
    }).error(handleError(res));
};

function handleError(res) {
    return function(error) {
        console.log(error.message);
        return res.send(500, {error: error.message});
    }
}