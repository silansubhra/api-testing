// Import
var thinky = require(__dirname+'/../util/thinky.js'),
    r = thinky.r,
    type = thinky.type;

var Org = thinky.createModel('org', {
    id: type.string(),
    name: type.string().required(),
    /*apiKey: type.string(),
    tpKey: type.string(),*/
    startDate: type.date(),
    registration: {
        number: type.string(),
        tin: type.string(),
        tan: type.string(),
        serviceTax: type.string(),
        pan: type.string()
    },
    geoLocation :type.point(),
    mobile: type.number().integer().min(1000000000).max(9999999999).required(),
    contacts: [{
        type: type.string().enum(["Mobile", "Phone","Home", "Work","Home Fax","WhatsApp", "Work Fax","Custom"]),
        mobileCountryCode:type.string(),
        number: type.string(),
        validated: type.boolean()
    }],
    email: type.string().email(),
    emails:[{
        type: type.string().enum(["Home", "Work","Other","Custom"]),
        id: type.string(),
        validated: type.boolean()
    }],
    addresses:[{
        type: type.string().enum(["Home", "Work","Other","Custom"]),
        street: type.string(),
        province: type.string(),
        city: type.string(),
        state: type.string(),
        country: type.string(),
        pin: type.number(),
        validated: type.boolean()
    }],
    websites: [{
        type:type.string().enum(["Homepage","Home", "Work","Blog","Other","Custom"]),
        url: type.string()
    }],
    socialAccounts:[{
        type: type.string().enum(["Twitter", "Facebook","LinkedIn","Flicker","Myspace","Sina Weibo","Custom"]),
        url: type.string()
    }],
    instantMessagers:[{
        type: type.string().enum(["AIM", "Facebook","Google Talk","MSN","Skype","Yahoo","Custom"]),
        url: type.string()
    }],

    referralCode: type.string(),
    logo: type.string(),
    description: type.string(),
    categories: [type.string()],
    enableSms: type.boolean(),
    enableEmail: type.boolean(),
    createdOn: type.date().default(r.now),
    createdBy: type.string(),
    updatedBy: type.string(),
    updatedOn: type.date(),
    status: type.string().enum(["active", "inactive"]).required().default("active"),
    orgType: type.string().enum(["business","welfare-association","school-college","blog"]).required().default("business"),
   
});

module.exports = Org;

var User = require(__dirname+'/user.js');
Org.hasMany(User, "users", "id", "orgId");

var Branch = require(__dirname+'/branch.js');
Org.hasMany(Branch, "branches", "id", "orgId");

var UserProfile = require(__dirname+'/userProfile.js');
Org.hasMany(UserProfile, "userProfiles", "id", "orgId");

var Department = require(__dirname+'/department.js');
Org.hasMany(Department, "departments", "id", "orgId");

var Slot = require(__dirname+'/slot.js');
Org.hasMany(Slot, "slots", "id", "orgId");

var Support = require(__dirname+'/support.js');
Org.hasMany(Support, "support", "id", "orgId");

var Portfolio = require(__dirname+'/portfolio.js');
Org.hasMany(Portfolio, "portfolios", "id", "orgId");

var Product = require(__dirname+'/product.js');
Org.hasMany(Product, "products", "id", "orgId");


Org.ensureIndex("createdOn");
Org.ensureIndex("name");

Org.pre('save', function(next) {
    Org.filter({mobile: parseInt(this.mobile)}).run().then(function(result) {
        // result is an array of instances of `Org`
        if(result.length > 0){
            next(new Error("Mobile no already registered"));
        }else{
            next();
        }
    });
});
