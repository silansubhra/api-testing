// Import
var thinky = require(__dirname+'/../util/thinky.js'),
    r = thinky.r,
    type = thinky.type;


var Sms = thinky.createModel('sms', {
    id: type.string(),
    mobile: type.number().integer().required(), //Keep numbers in international format (with country code), multiple numbers should be separated by comma (,)
    message: type.string().required(), //Message content to send
    sender: type.string().max(6).required(), //Receiver will see this as sender's ID.
    route: type.number().integer().required(), //If your operator supports multiple routes then give one route name. Eg: route=1 for promotional, route=4 for transactional SMS. 

    countryCode: type.number(), //0 for international,1 for USA, 91 for India.
    flash: type.boolean(), //flash=1 (for flash SMS)
    unicode: type.boolean(), //unicode=1 (for unicode SMS)
    ignoreNdnc: type.number(), //ignoreNdnc=1 (if you want system to ignore all NDNC Numbers)
    schtime: type.date(), //When you want to schedule the SMS to be sent. Time format will be Y-m-d h:i:s

    requestId: type.string(), //By default you will get response in string format but you want to receive in other format (json,xml) then set this parameter. for example: &response=json or &response=xml
    campaign: type.string(),//Campaign name you wish to create.
    response: type.string(),
    /*{
        requestId: type.string(), //Request ID (a unique 24 character alphanumeric value for identification of a particular SMS)
        userId:type.string(),
        report:[{
            desc: type.string(),
            status: type.number().integer(), //Status of SMS delivery 1=delivered 2=failed 16=rejected
            date:type.date() //Displays the delivery time and date of SMS YY: year MM: month DD: date hh: hours mm: minutes
        }]
    }*/
    senderId:type.string(),
    status: type.string(),
    createdOn: type.date().default(r.now),
    createdBy: type.string(),
    updatedBy: type.string(),
    updatedOn: type.date(),
    orgId:type.string(),
    portfolioId: type.string()
});

module.exports = Sms;

Sms.ensureIndex("createdOn");
//User.ensureIndex("requestId");

// User.pre('save', function(next) {
//     User.filter({mobile: this.mobile}).run().then(function(result) {
//         if(result.length > 0){
//             next(new Error("Mobile already registered."));
//         }else{
//             next();
//         }
//     });
// });
