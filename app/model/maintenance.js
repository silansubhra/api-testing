// Import
var uuid = require('node-uuid'),
    thinky = require(__dirname+'/../util/thinky.js'),
    r = thinky.r,
    type = thinky.type,
    Portfolio = require(__dirname+'/portfolio.js'),
    Org = require(__dirname+'/org.js'),
    User = require(__dirname+'/user.js'),
    Employee = require(__dirname+'/employee.js');
    Asset = require(__dirname+'/asset.js');
    Item = require(__dirname+'/item.js');

    var Maintenance = thinky.createModel('maintenance', {
    	id: type.string(),
    	userId:type.string(),
    	orgId:type.string(),
    	assetId: type.string(),
    	rank: type.number(),
    	portfolioId:type.string(),
    	employeeId: type.string(),
    	itemId: type.string(),
    	dueDate: type.date(),
    	mstatus: type.string(),
    	description: type.string(),
    	type: type.string().enum(["PREVENTIVE", "BREAKDOWN"]),
        mstatus: type.string().enum(["NEW", "PENDING", "COMPLETED"]),
    	createdOn: type.date().default(r.now),
    	createdBy: type.string(),
    	updatedBy: type.string(),
    	updatedBy: type.string(),
    	status: type.string().enum(["active", "inactive"]).required().default("active")
	});

module.exports = Maintenance;


Maintenance.belongsTo(Org,"org","orgId","id");

Maintenance.belongsTo(Asset,"asset","assetId","id");



Maintenance.belongsTo(Employee,"employee","employeeId","id");


Maintenance.belongsTo(Item,"item","itemId","id");

Maintenance.belongsTo(Portfolio,"portfolio","portfolioId","id");


Maintenance.ensureIndex("createdOn");

Maintenance.ensureIndex("rank");
