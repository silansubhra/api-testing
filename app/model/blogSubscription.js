// Import
var uuid = require('node-uuid'),
    thinky = require(__dirname+'/../util/thinky.js'),
    r = thinky.r,
    type = thinky.type,
    Portfolio = require(__dirname+'/portfolio.js'),
    Org = require(__dirname+'/org.js'),
    User = require(__dirname+'/user.js');


var BlogSubscription = thinky.createModel('blogSubscription', {
    id: type.string(),
    userId:type.string(),
    orgId:type.string(),
    portfolioId:type.string(),
    mobile: type.number(),
    emailId: type.string(),
    enableSms: type.boolean(),
    enableEmail: type.boolean(),
    verificationToken: type.string(),
    subscriptionStatus: type.string().enum(["new","subscribed", "unsubscribed"]).required().default("new"),
    createdOn: type.date().default(r.now),
    createdBy: type.string(),
    updatedBy: type.string(),
    updatedBy: type.string(),
    status: type.string().enum(["active", "inactive"]).required().default("active")
});

module.exports = BlogSubscription;


BlogSubscription.belongsTo(Org,"org","orgId","id");
BlogSubscription.belongsTo(User,"user","userId","id");
BlogSubscription.belongsTo(Portfolio,"portfolio","portfolioId","id");

BlogSubscription.ensureIndex("createdOn");
