// Import
var uuid = require('node-uuid'),
    thinky = require(__dirname+'/../util/thinky.js'),
    r = thinky.r,
    type = thinky.type,
    Portfolio = require(__dirname+'/portfolio.js'),
    Org = require(__dirname+'/org.js'),
    User = require(__dirname+'/user.js');

var Bank = thinky.createModel('bank', {
    id: type.string(),
    userId:type.string(),
    orgId:type.string(),
    portfolioId:type.string(),
    type: type.string().default("bank"),
    bankName: type.string(),
    accountNo: type.string(),
    bankAccount:type.string(),
    accountType: type.string().enum(["current","demand","saving"]).default("saving"),
    branchName: type.string(),
    ifscCode: type.string(),
    micrCode: type.string(),
    contactNo: type.number(),
    description: type.string(),
    createdOn: type.date().default(r.now),
    createdBy: type.string(),
    updatedBy: type.string(),
    updatedBy: type.string(),
    status: type.string().enum(["active", "inactive"]).required().default("active")
});

module.exports = Bank;


Bank.belongsTo(Org,"org","orgId","id");
Bank.belongsTo(User,"user","userId","id");
Bank.belongsTo(Portfolio,"portfolio","portfolioId","id");

Bank.ensureIndex("createdOn");

Bank.pre('save', function(next) {
    Bank.filter({accountNo: this.accountNo,portfolioId: this.portfolioId,type: this.type}).run().then(function(result) {
        if(result.length > 0){
            next(new Error("Bank already exist."));
        }else{
            next();
        }
    });
});