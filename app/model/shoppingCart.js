 // Import
var uuid = require('node-uuid'),
    thinky = require(__dirname+'/../util/thinky.js'),
    r = thinky.r,
    type = thinky.type,
    User = require(__dirname+'/user.js'),
    Portfolio = require(__dirname+'/portfolio.js'),
    Org = require(__dirname+'/org.js');

var ShoppingCart = thinky.createModel('shoppingCart', {
    id: type.string(),
    userId:type.string(),
    portfolioId: type.string(),
    orgId:type.string(),
    bagTotal: type.number().default(0),
    bagDiscount:type.number().default(0),
    vat:type.number().default(0),
    deliveryDate:type.date(),
    subTotal:type.date().default(0),
    createdOn: type.date().default(r.now),
    createdBy: type.string(),
    updatedBy: type.string(),
    updatedOn: type.date(),
    status: type.string().enum(["active", "inactive"]).required().default("active")
});

module.exports = ShoppingCart;

ShoppingCart.ensureIndex("createdOn");

ShoppingCart.belongsTo(Portfolio,"portfolio","portfolioId","id");

ShoppingCart.belongsTo(User,"user","userId","id");

ShoppingCart.belongsTo(Org,"org","orgId","id");

var ShoppingCartItem = require(__dirname+'/item.js');
ShoppingCart.hasMany(ShoppingCartItem, "items", "id", "shoppingCartId");


