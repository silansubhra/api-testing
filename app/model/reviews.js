// Import
var uuid = require('node-uuid'),
    thinky = require(__dirname+'/../util/thinky.js'),
    r = thinky.r,
    type = thinky.type,
    Portfolio = require(__dirname+'/portfolio.js'),
    User = require(__dirname+'/user.js'),
    Org = require(__dirname+'/org.js');

var Review = thinky.createModel('reviews', {
    id: type.string(),
    portfolioId:type.string(),
    mpId: type.string(),
    rating: type.number(),
    comment: type.string(),
    createdOn: type.date().default(r.now),
    createdBy: type.string(),
    updatedOn: type.date().default(r.now),
    updatedBy: type.string(),
    status: type.string().enum(["active", "inactive"]).required().default("active")
});

module.exports = Review;


Review.belongsTo(Org,"org","orgId","id");
Review.belongsTo(Portfolio,"portfolio","portfolioId","id");

var User =require(__dirname+'/user.js');
Review.belongsTo(User,"user","createdBy","id");

Review.ensureIndex("createdOn");
Review.ensureIndex("rating");
