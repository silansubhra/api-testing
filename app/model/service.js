// Import
var uuid = require('node-uuid'),
    thinky = require(__dirname+'/../util/thinky.js'),
    r = thinky.r,
    type = thinky.type,
    Branch = require(__dirname+'/branch.js'),
    Org = require(__dirname+'/org.js'),
    Portfolio = require(__dirname+'/portfolio.js');



var Service = thinky.createModel('service', {
    id: type.string(),
    portfolioId: type.string(),
    branchId: type.string(),
    orgId: type.string(),
    name: type.string().required(),
    parentId: type.string(),
    metaServiceId :type.string(),
    metaAttributeId:type.string(),
    serviceMediaId :type.string(),
    minPrice: type.number(),
    maxPrice: type.number(),
    metaTitle: type.string(),
    metaLocTitle: type.string(),
    metaDescription: type.string(),
    metaLocDescription: type.string(),
    metaKeywords: type.string(),
    popular: type.boolean(),
    frequencyType: type.string().enum(["daily","weekly","monthly","quarterly","half yearly","yearly","two year","three year","four year","five year"]).default("yearly"),
    frequencyValue: type.number(),
    coverMedia :({mediaId :type.string()}),
    icon :({mediaId :type.string()}),
    description: type.string(),
    slots: [{
        slotId: type.string(),
        maxSlots:type.number(),
        slotFor: type.string().enum(["job","inspection"]).default("job")
    }],
    createdOn: type.date().default(r.now),
    createdBy: type.string(),
    updatedBy: type.string(),
    updatedOn: type.date(),
    status: type.string().enum(["active", "inactive"]).required().default("active")
});
  
    
module.exports = Service;


Service.belongsTo(Branch,"branch","branchId","id");
Service.belongsTo(Org,"org","orgId","id");
Service.belongsTo(Portfolio,"portfolio","portfolioId","id");

/*var Lead = require(__dirname+'/lead.js');
Service.hasOne(Lead, "leads", "id", "serviceId");*/

/*var Job = require(__dirname+'/job.js');
Service.hasOne(Job, "jobs", "id", "serviceId");*/

Service.ensureIndex("createdOn");
Service.ensureIndex("name");


Service.pre('save', function(next) {
    Service.filter({name: this.name,portfolioId: this.portfolioId}).run().then(function(result) {
        if(result.length > 0){
            next(new Error("Service already exist."));
        }else{
            next();
        }
    });
});


