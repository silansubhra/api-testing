// Import
var uuid = require('node-uuid'),
    thinky = require(__dirname+'/../util/thinky.js'),
    r = thinky.r,
    type = thinky.type,
    Product = require(__dirname+'/product.js');




var ProductMedia = thinky.createModel('productmedia', {
    id: type.string(),
    productId: type.string(),
    mediaId: type.string(),
    createdOn: type.date().default(r.now),
    createdBy: type.string(),
    updatedBy: type.string(),
    updatedOn: type.date(),
    status: type.string().enum(["active", "inactive"]).required().default("active")
});
  

module.exports = ProductMedia;


ProductMedia.belongsTo(Product,"product","productId","id"),
    Media = require(__dirname+'/media.js');
ProductMedia.belongsTo(Media,"media","mediaId","id");


ProductMedia.ensureIndex("createdOn");


