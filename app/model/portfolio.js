// Import

var uuid = require('node-uuid'),
    thinky = require(__dirname+'/../util/thinky.js'),
    r = thinky.r,
    type = thinky.type,
    Org = require(__dirname+'/org.js'),
    Branch = require(__dirname+'/branch.js');

var Portfolio = thinky.createModel('portfolio', {
    id: type.string(),
    orgId:type.string(),
    apiKey: type.string(),
    tpKey: type.string(),
    name:type.string().required(),
    mobile: type.number().integer().min(1000000000).max(9999999999),
    wallet: type.number().default(0),
    trunetoWallet: type.number().default(0),
    email: type.string(),
    type: type.string().enum(["service","product","both service and product"]).default("both service and product"),
    channel: type.string().enum(["online","offline","both online and offline"]).default("both online and offline"),
    deliveredBy: type.string().enum(["company","third party","store pickup"]).default("company"),
    homeDelivery: type.boolean().default(true),
    booking: type.string().enum(["online","offline","both online and offline"]).default("both online and offline"),
    servedAt: type.string().enum(["company location","client location","other location"]).default("company location"),
    serviceType: type.string().enum(["physical","digital"]).default("physical"),
    paymentMode: type.string().enum(["online","offline","both online and offline"]).default("both online and offline"),
    parishable: type.boolean(),
    tangible: type.boolean(),
    addresses:[{
        type: type.string().enum(["Home", "Work","Other","Custom"]),
        street: type.string(),
        province: type.string(),
        city: type.string(),
        state: type.string(),
        country: type.string(),
        pin: type.number(),
        validated: type.boolean()
    }],
    enableSms: type.boolean().default(false),
    enableEmail: type.boolean().default(false),
    createdOn: type.date(),
    createdBy: type.string(),
    updatedBy: type.string(),
    updatedOn: type.date(),
    isMarketPlace: type.boolean(),
     smsProvider: {
      authKey: type.string(),
      name: type.string(),
      senderId: type.string()
    },
    emailProvider: {
      auth: {
          pass: type.string(),
          user: type.string(),
          name: type.string()
        } ,
      host: type.string(),
      port: type.number(),
      secure: type.boolean()
    },
    websites: [{
        type:type.string().enum(["Homepage","Home", "Work","Blog","Other","Custom"]),
        url: type.string()
    }],
    contacts: [{
        type: type.string().enum(["Mobile", "Phone","Home", "Work","Home Fax","WhatsApp", "Work Fax","Custom"]),
        mobileCountryCode:type.string(),
        number: type.string(),
        validated: type.boolean()
    }],
emails:[{
        type: type.string().enum(["Home", "Work","Other","Custom"]),
        id: type.string(),
        validated: type.boolean()
    }],
    socialAccounts:[{
        type: type.string().enum(["Twitter", "Facebook","LinkedIn","Flicker","Myspace","Sina Weibo","Custom"]),
        url: type.string()
    }],
    serviceArea:[{
        metaCountyId: type.string(),
        metaStateId: type.string(),
        //[{metaCityId: type.string()}]
    }],
    status: type.string().enum(["active", "inactive"]).required().default("active")
});


module.exports = Portfolio;


Portfolio.belongsTo(Org,"org","orgId","id");


Portfolio.hasMany(Branch, "branches", "id", "portfolioId");


var UserProfile = require(__dirname+'/userProfile.js');
Portfolio.hasMany(UserProfile, "userProfiles", "id", "portfolioId");


var Customer = require(__dirname+'/customer.js');
Portfolio.hasMany(Customer, "customers", "id", "portfolioId");


var WalletTransaction = require(__dirname+'/walletTransaction.js');
Portfolio.hasMany(WalletTransaction, "WalletTransactions", "id", "portfolioId");

var Item = require(__dirname+'/item.js');
Portfolio.hasMany(Item, "items", "id", "portfolioId");

var Employee = require(__dirname+'/employee.js');
Portfolio.hasMany(Employee, "employees", "id", "portfolioId");


var Support = require(__dirname+'/support.js');
Portfolio.hasMany(Support, "support", "id", "portfolioId");

var Slot = require(__dirname+'/slot.js');
Portfolio.hasMany(Slot, "slots", "id", "portfolioId");


var Payment = require(__dirname+'/payment.js');
Portfolio.hasMany(Payment, "payments", "id", "portfolioId");

var Transaction = require(__dirname+'/transaction.js');
Portfolio.hasMany(Transaction, "transactions", "id", "portfolioId");

var Invoice = require(__dirname+'/invoice.js');
Portfolio.hasMany(Invoice, "invoices", "id", "portfolioId");


var Product = require(__dirname+'/product.js');
Portfolio.hasMany(Product, "products", "id", "portfolioId");

var Category = require(__dirname+'/category.js');
Portfolio.hasMany(Category, "categories", "id", "portfolioId");

var ShoppingCart = require(__dirname+'/shoppingCart.js');
Portfolio.hasMany(ShoppingCart, "shoppingCarts", "id", "portfolioId");


var Task = require(__dirname+'/task.js');
Portfolio.hasMany(Task, "tasks", "id", "portfolioId");

var MPort = require(__dirname+'/mport.js');
Portfolio.hasMany(MPort, "mPort", "id", "portfolioId");


var Review = require(__dirname+'/reviews.js');
Portfolio.hasMany(Review, "review", "id", "portfolioId");


Portfolio.ensureIndex("createdOn");
Portfolio.ensureIndex("name");

Portfolio.pre('save', function(next) {
    Portfolio.filter({mobile: parseInt(this.mobile)}).run().then(function(result) {
        if(result.length > 0){
            next(new Error("This mobile already register with our system."));
        }else{
            next();
        }
    });
});

//Portfolio.belongsTo(PortfolioType,"userType","idPortfolioType","id");
