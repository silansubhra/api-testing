// Import
var uuid = require('node-uuid'),
    thinky = require(__dirname+'/../util/thinky.js'),
    r = thinky.r,
    type = thinky.type,
    Org = require(__dirname+'/org.js'),
    Branch = require(__dirname+'/branch.js'),
    Portfolio = require(__dirname+'/portfolio.js');


var Product = thinky.createModel('product', {
    id: type.string(),
    portfolioId: type.string(),
    orgId:type.string(),
    branchId: type.string(),
    categoryId: type.string(),
    name: type.string().required(),
    sku: type.string(),
    salesPackage: type.number(),
    description: type.string(),
    keywords: type.string(),
    weight: type.number(),
    price: type.number(),
    visibility: type.boolean(),
    marketplaceApproved: type.boolean(),
    createdOn: type.date().default(r.now),
    createdBy: type.string(),
    updatedBy: type.string(),
    updatedOn: type.date(),
    status: type.string().enum(["active", "inactive"]).required().default("active")

});

module.exports = Product;

Product.belongsTo(Org,"org","orgId","id");
Product.belongsTo(Branch,"branch","branchId","id");
Product.belongsTo(Portfolio,"portfolio","portfolioId","id"),
Category = require(__dirname+'/category.js');
Product.belongsTo(Category,"category","categoryId","id");

Product.ensureIndex("createdOn");
Product.ensureIndex("name");
Product.ensureIndex("discountedPrice");



/*Product.pre('save', function(next) {
    Product.filter({name: this.name,orgId: this.orgId}).run().then(function(result) {
        if(result.length > 0){
            next(new Error("Product name already exist."));
        }else{
            next();
        }
    });
});
*/

