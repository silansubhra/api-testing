// Import
var uuid = require('node-uuid'),
    thinky = require(__dirname+'/../util/thinky.js'),
    r = thinky.r,
    type = thinky.type,
    User = require(__dirname+'/user.js'),
    Portfolio = require(__dirname+'/portfolio.js'),
    Org = require(__dirname+'/org.js'),
    ShoppingCart = require(__dirname+'/shoppingCart.js'),
    Item = require(__dirname+'/item.js');

var ShoppingCartItem = thinky.createModel('shoppingCartItem', {
    id: type.string(),
    shoppingCartId:type.string(),
    portfolioId: type.string(),
    orgId:type.string(),
    itemId: type.string(),
    quantity: type.number().default(0),
    amount:type.number().default(0),
    total: type.number().default(0),
    itemName:type.string(),
    createdOn: type.date().default(r.now),
    createdBy: type.string(),
    updatedBy: type.string(),
    updatedOn: type.date(),
    status: type.string().enum(["active", "inactive"]).required().default("active")
});

module.exports = ShoppingCartItem;

ShoppingCartItem.ensureIndex("createdOn");

ShoppingCartItem.belongsTo(Portfolio,"portfolio","portfolioId","id");

ShoppingCartItem.belongsTo(ShoppingCart,"shoppingCart","shoppingCartId","id");

ShoppingCartItem.belongsTo(Item,"item","itemId","id");

ShoppingCartItem.belongsTo(Org,"org","orgId","id");

