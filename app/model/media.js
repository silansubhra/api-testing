// Import
var thinky = require(__dirname+'/../util/thinky.js'),
    r = thinky.r,
    type = thinky.type;


var Media = thinky.createModel('media', {
    id: type.string(),
    title: type.string(),
    createdOn: type.date().default(r.now),
    createdBy: type.string(),
    updatedBy: type.string(),
    updatedOn: type.date(),
    originalFileName: type.string(),
    originalURL: type.string(),
    imageCredit: type.string(),
    
    name: type.string(),
    extension: type.string(),
    size: type.number(),
    url:  type.string(),
    thumbnailUrl:  type.string(),
    deleteUrl:  type.string(),
    deleteType:  type.string(),

    description: type.string(),
    shortDescription: type.string(),
    private: type.boolean().default(false),
    verified: type.boolean().default(false),
    type: type.string().enum(["document","image","video","zip","pdf","csv"]).default("document"),
    
    mediaFor: type.string(["profilePic","coverPic","event"]),
    forId: type.string(),
    
    tags: type.string(),    
    status: type.string().enum(["active", "inactive"]).required().default("active"),
    userId: type.string(),
    mime: type.string(),
    s3Url: type.string(),
    accessUrl: type.string()
});

module.exports = Media;

Media.ensureIndex("createdOn");
Media.ensureIndex("userId");
Media.ensureIndex("forId");

// var User = require(__dirname+'/user.js');
// Media.belongsTo(User,"user","userId","id");

// var ServiceMedia = require(__dirname+'/serviceMedia.js');
// Media.hasMany(ServiceMedia, "serviceMedias", "id", "mediaId");

 var Item = require(__dirname+'/item.js');
 Media.belongsTo(Item,"item","forId","id");

var Category = require(__dirname+'/category.js');
 Media.belongsTo(Category,"category","forId","id");
