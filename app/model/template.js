// Import
var uuid = require('node-uuid'),
    thinky = require(__dirname+'/../util/thinky.js'),
    r = thinky.r,
    type = thinky.type,
    Portfolio = require(__dirname+'/portfolio.js'),
    Branch = require(__dirname+'/branch.js'),
    Org = require(__dirname+'/org.js'),
    Customer = require(__dirname+'/customer.js');

var Template = thinky.createModel('template', {
    id: type.string(),
    portfolioId:type.string(),
    branchId:type.string(),
    orgId: type.string(),
    lead:{
        new:{
            html:type.string(),
            text: type.string(),
            subject:type.string()

        },
        reschedule:{
            html:type.string(),
            text: type.string(),
            subject:type.string()
        },
        cancel:{
            html:type.string(),
            text: type.string(),
            subject:type.string()
        },
        assign:{
            html: type.string(),
            text: type.string(),
            subject:type.string()
        }
    },
    visitor:{
        new:{
            html:type.string(),
            text: type.string(),
            subject:type.string()

        },
        measure:{
            html:type.string(),
            text: type.string(),
            subject:type.string()
        },
        cancel:{
            html:type.string(),
            text: type.string(),
            subject:type.string()
        },
        quote:{
            html: type.string(),
            text: type.string(),
            subject:type.string()
        }
    },
    order:{
        new:{
            html:type.string(),
            text:type.string(),
            subject:type.string()
        }
    },
    signUp:{
        new:{
            html:type.string(),
            text:type.string(),
            subject:type.string()
        }
    },
    createdOn: type.date().default(r.now),
    createdBy: type.string(),
    updatedBy: type.string(),
    updatedOn: type.date(),
    status: type.string().enum(["active", "inactive"]).required().default("active")
});

module.exports = Template;

Template.ensureIndex("createdOn");
// unique key on portfolio id & mobile number

Template.belongsTo(Portfolio,"portfolio","portfolioId","id");


