// Import
var uuid = require('node-uuid')
    thinky = require(__dirname+'/../util/thinky.js'),
    r = thinky.r,
    type = thinky.type,
    Portfolio = require(__dirname+'/portfolio.js'),
    Org = require(__dirname+'/org.js');


var ProductInventory = thinky.createModel('productInventory', {
    id: type.string(),
    orgId:type.string(),
    portfolioId:type.string(),
    productId: type.string(),
    employeeId: type.string(),
    createdOn: type.date().default(r.now),
    createdBy: type.string(),
    updatedBy: type.string(),
    updatedBy: type.string(),
    status: type.string().enum(["active", "inactive"]).required().default("active")
});

module.exports = ProductInventory;

ProductInventory.belongsTo(Org,"org","orgId","id");

ProductInventory.belongsTo(Portfolio,"portfolio","portfolioId","id");

var Item = require(__dirname+'/item.js');
ProductInventory.belongsTo(Item, "product", "productId", "id");

var Employee = require(__dirname+'/employee.js');
ProductInventory.belongsTo(Employee, "employee", "employeeId", "id");

ProductInventory.ensureIndex("createdOn");
ProductInventory.ensureIndex("price");
