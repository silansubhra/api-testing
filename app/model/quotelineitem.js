// Import
var uuid = require('node-uuid'),
    thinky = require(__dirname+'/../util/thinky.js'),
    r = thinky.r,
    type = thinky.type,
    Quotation = require(__dirname+'/quotation.js'),
    Lead = require(__dirname+'/lead.js'),
    Org = require(__dirname+'/org.js'),
    Portfolio = require(__dirname+'/portfolio.js');


var QuotationLineItem = thinky.createModel('quotationLineItem', {
    id: type.string(),
    portfolioId: type.string(),
    branchId:type.string().required(),
    leadId:type.string().required(),
    orgId:type.string(),
    quoteId: type.string(),
    itemId: type.string(),
    name:type.string(),
    type :type.string().enum(["hour", "items"]),
    price :type.number(),
    quantity :type.number(),
    discount :type.number(),
    total :type.number(),
    createdOn: type.date().default(r.now),
    createdBy: type.string(),
    updatedBy: type.string(),
    updatedOn: type.date(),
    status: type.string().enum(["active", "inactive"]).required().default("active")
});


module.exports = QuotationLineItem;


QuotationLineItem.belongsTo(Org,"org","orgId","id");

QuotationLineItem.belongsTo(Lead,"lead","leadId","id");

QuotationLineItem.belongsTo(Portfolio,"portfolio","portfolioId","id");

Quotation.belongsTo(Quotation,"quote","quoteId","id");

var Item = require(__dirname+'/item.js');
Quotation.belongsTo(Item,"item","itemId","id");


QuotationLineItem.ensureIndex("createdOn");

QuotationLineItem.ensureIndex("name");

