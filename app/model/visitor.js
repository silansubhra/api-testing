// Import
var uuid = require('node-uuid'),
    thinky = require(__dirname+'/../util/thinky.js'),
    r = thinky.r,
    type = thinky.type,
    Portfolio = require(__dirname+'/portfolio.js'),
    User = require(__dirname+'/user.js'),
    Org = require(__dirname+'/org.js'),
    Employee= require(__dirname+'/employee.js');

var Visitor = thinky.createModel('visitor', {
    id: type.string(),
    portfolioId: type.string(),
    mobile: type.string(),
    name: type.string(),
    email: type.string(),
    address: type.string(),
    orgId:type.string(),
    type: type.string().enum(["Website","Store","App"]).default("Store"),
    salesStage: type.string().enum(["visit","measurement","quotation","confirmed","unsuccessful"]),
    description: type.string(),
    employeeId: type.string(),
    vDate: type.date(),
    fDate: type.date(),
    requirement: type.string(),
    enableSms: type.boolean(),
    enableEmail: type.boolean(),
    createdOn: type.date().default(r.now),
    createdBy: type.string(),
    updatedBy: type.string(),
    updatedOn: type.date(),
    status: type.string().enum(["active", "inactive"]).required().default("active")
});

module.exports = Visitor;


Visitor.belongsTo(Org,"org","orgId","id");
Visitor.belongsTo(User,"user","userId","id");

Visitor.belongsTo(Portfolio,"portfolio","portfolioId","id");

Visitor.belongsTo(Employee,"employee","employeeId","id");

Visitor.ensureIndex("createdOn");
Visitor.ensureIndex("name");
Visitor.ensureIndex("vDate");


Visitor.pre('save', function(next) {
    if(this.mobile != undefined && this.mobile !== null && this.mobile !== '' && this.mobile !== NaN){
       Visitor.filter({mobile:this.mobile,portfolioId: this.portfolioId,status: 'active'}).run().then(function(result) {
            if(result.length > 0){
                next(new Error("This visitor already exists."));
            }else{
                next();
            }
        }); 
   }else{
        next(); 
   }
   
});