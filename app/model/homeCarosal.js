// Import
var uuid = require('node-uuid'),
    thinky = require(__dirname+'/../util/thinky.js'),
    r = thinky.r,
    type = thinky.type,
    Portfolio = require(__dirname+'/portfolio.js'),
    Org = require(__dirname+'/org.js');

var HomeCarosal = thinky.createModel('homeCarosal', {
    id: type.string(),
    orgId:type.string(),
    portfolioId:type.string(),
    centerId : type.string(),
    text: type.string(),
    coverImage: type.string(),
    positon: type.number(),
    picture: type.string(),
    createdOn: type.date().default(r.now),
    createdBy: type.string(),
    updatedBy: type.string(),
    updatedOn: type.date(),
    status: type.string().enum(["active", "inactive"]).required().default("active")
});

module.exports = HomeCarosal;


HomeCarosal.belongsTo(Org,"org","orgId","id");
HomeCarosal.belongsTo(Portfolio,"portfolio","portfolioId","id");

HomeCarosal.ensureIndex("createdOn");
HomeCarosal.ensureIndex("positon");

// HomeCarosal.pre('save', function(next) {
//     HomeCarosal.filter({positon:parseInt(this.positon),portfolioId:this.portfolioId}).run().then(function(result) {
//         if(result.length > 0){
//             next(new Error("positon cannot be duplicate"));
//         }else{
//             next();
//         }
//     });
// });