// Import
var uuid = require('node-uuid'),
    thinky = require(__dirname+'/../util/thinky.js'),
    r = thinky.r,
    type = thinky.type,
    Portfolio = require(__dirname+'/portfolio.js'),
    Branch = require(__dirname+'/branch.js'),
    Org = require(__dirname+'/org.js'),
    Customer = require(__dirname+'/customer.js');

var MailConfig = thinky.createModel('mailConfig', {
    id: type.string(),
    portfolioId:type.string(),
    branchId:type.string(),
    orgId: type.string(),
    gmail:{
        host: type.string().default('smtp.gmail.com'),
        port:type.number().default(465),
        secure:type.boolean().default(true),
        auth: {
            user:type.string(),
            pass:type.string()
        }
    },
    createdOn: type.date().default(r.now),
    createdBy: type.string(),
    updatedBy: type.string(),
    updatedOn: type.date(),
    status: type.string().enum(["active", "inactive"]).required().default("active")
});

module.exports = MailConfig;

MailConfig.ensureIndex("createdOn");
// unique key on portfolio id & mobile number

MailConfig.belongsTo(Portfolio,"portfolio","portfolioId","id");
MailConfig.belongsTo(Branch,"branch","branchId","id");
MailConfig.belongsTo(Org,"org","orgId","id");