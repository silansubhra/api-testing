// Import
var uuid = require('node-uuid'),
    thinky = require(__dirname+'/../util/thinky.js'),
    r = thinky.r,
    type = thinky.type,
    Portfolio = require(__dirname+'/portfolio.js'),
    Org = require(__dirname+'/org.js');
    

var Wishlist = thinky.createModel('wishlist', {
    id: type.string(),
    portfolioId:type.string(),
    hostId:type.string(),
    orgId: type.string(),
    eventId: type.string(),
    name: type.string(),
    itemId: type.string(),
    enableSms: type.boolean(),
    enableEmail: type.boolean(),
    createdOn: type.date().default(r.now),
    createdBy: type.string(),
    updatedBy: type.string(),
    updatedOn: type.date(),
    status: type.string().enum(["active", "inactive"]).required().default("active")
});


module.exports = Wishlist;

Wishlist.ensureIndex("createdOn");
// unique key on portfolio id & mobile number

Wishlist.belongsTo(Portfolio,"portfolio","portfolioId","id");
Wishlist.belongsTo(Org,"org","orgId","id");
var Item = require(__dirname+'/item.js');
Wishlist.belongsTo(Item,"item","itemId","id");


Wishlist.pre('save', function(next) {
    Wishlist.filter({itemId: this.itemId,userId: this.userId}).run().then(function(result) {
        if(result.length > 0){
            next(new Error("This item already in  list."));
        }else{
            next();
        }
    });
});
