// Import
var uuid = require('node-uuid'),
    thinky = require(__dirname+'/../util/thinky.js'),
    r = thinky.r,
    type = thinky.type,
    Portfolio = require(__dirname+'/portfolio.js'),
    Branch = require(__dirname+'/branch.js'),
    Org = require(__dirname+'/org.js'),
    Customer = require(__dirname+'/customer.js');

var Preference = thinky.createModel('preference', {
    id: type.string(),
    portfolioId:type.string(),
    branchId:type.string(),
    orgId: type.string(),
    emailConfig:{
        enableEmail:type.boolean(),
        gmail: {
            auth: {
                pass:  type.string() ,
                user: type.string(),
            },
            host:  type.string().default('smtp.gmail.com') ,
            port: type.number().default(465) ,
            secure: type.boolean().default(true)
        }

    },
    smsConfig:{
        enableSms: type.boolean(),
        smsProvider: {
            authKey:  type.string() ,
            name:  type.string().default('VERIFORMM') ,
            senderId: type.string()
            }
    },
    templetes:{
        leads:type.string(),
        signUp: type.string(),
        order:type.string()
    },
    createdOn: type.date().default(r.now),
    createdBy: type.string(),
    updatedBy: type.string(),
    updatedOn: type.date(),
    status: type.string().enum(["active", "inactive"]).required().default("active")
});

module.exports = Preference;

Preference.ensureIndex("createdOn");
// unique key on portfolio id & mobile number

Preference.belongsTo(Portfolio,"portfolio","portfolioId","id");


