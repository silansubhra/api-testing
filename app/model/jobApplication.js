// Import
var thinky = require(__dirname+'/../util/thinky.js'),
    r = thinky.r,
    type = thinky.type;
    Org = require(__dirname+'/org.js');


var JobApplication = thinky.createModel('jobapplication', {
	id: type.string(),
	orgId: type.string(),
    portfolioId: type.string(),
	name : type.string(),
	email : type.string(),
	mobile: type.number(),
	location: type.string(),
	gender : type.string().enum(["Male", "Female"]),
	experience : type.string(),
	createdOn: type.date().default(r.now)
});

module.exports = JobApplication;

var Portfolio = require(__dirname+'/portfolio.js');
JobApplication.belongsTo(Portfolio,"portfolio","portfolioId","id");
