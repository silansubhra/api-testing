// Import
var uuid = require('node-uuid'),
    thinky = require(__dirname+'/../util/thinky.js'),
    r = thinky.r,
    type = thinky.type,
    Org = require(__dirname+'/org.js'),
    Portfolio = require(__dirname+'/portfolio.js');

var Item = thinky.createModel('item', {
    /*id: type.string(),
    portfolioId :type.string(),
    mpId: type.string(),
    mpServiceId: type.string(),
    orgId: type.string(),
    parentId: type.string(),
    mpId: type.string(),
    mediaId: type.string(),
    name: type.string(),
    categoryId: type.string(),
    type :type.string().enum(["service","product"]),
    shoppingCartId:type.string(),
    unit :type.string(),
    price: type.number(),
    image:type.string(),
    coverImageUrl: type.string(),
    description: type.string(),
    checklist:[
        {
            seq:type.number(),
            title:type.string(),
            description:type.string(),
            stepsVideoUrl:type.string()
        }
    ],
    healthWarnings: [
        {
            description:type.string()  ,
            moreInfoUrl: type.string() ,
            seq: type.number() ,
            stepsVideoUrl: type.string() ,
            title: type.string()
        }
    ],
    process: [
        {
            coverImageUrl: type.string() ,
            description: type.string()  ,
            images: [
                {
                    title:type.string()  ,
                    url: type.string()
                }
            ] ,
            seq: type.number() ,
            title: type.string(),
            video: {
                credit:type.string(),
                title:type.string() ,
                youtubeId: type.string() ,
                youtubeThumbnailUrl: type.string() ,
                youtubeUrl:type.string()
            }
        } 
    ],
    shortDescription: type.string(),
    video: {
        credit:type.string(),
        title:type.string() ,
        youtubeId: type.string() ,
        youtubeThumbnailUrl: type.string() ,
        youtubeUrl:type.string()
    },
    whyThisService:[
        {
            seq:type.number(),
            title:type.string(),
            description:type.string(),
            videoUrl:type.string(),
            imageUrl:type.string()
        }
    ],
    stats:{
        bookingsCount:type.number(),
        favoriteCount:type.number(),
        reviews:{
            totalCount:type.number(),
            totalValue:type.number(),
            averageValue:type.number()
        }
    },
    vendors: type.number(),
    seo:{
        ogType:type.string(),
        ogTitle:type.string(),
        ogDescription:type.string(),
        ogUrl:type.string(),
        ogImage:type.string(),
        publish_date:type.date().default(r.now),
        modified_date:type.date()
    },  
    discountedPrice: type.number(),
    discountPercentage: type.number(),
    stock: type.number(),
    inHouseStock: type.number(),
    employeeStock: type.number(),
    isSale: type.boolean(),
    isPurchase: type.boolean(),
    isFeatured: type.boolean(),
    uName: type.string(),
    rank :type.number(),
    createdOn: type.date().default(r.now),
    createdBy: type.string(),
    updatedBy: type.string(),
    updatedOn: type.date().default(r.now),
    status: type.string().enum(["active", "inactive"]).required().default("active")*/
    id: type.string(),
    portfolioId :type.string(),
    mpId: type.string(),
    mpServiceId: type.string(),
    orgId: type.string(),
    parentId: type.string().default(null),
    mpId: type.string(),
    mediaId: type.string(),
    coverImage: type.string(),
    name: type.string(),
    categoryId: type.string(),
    type :type.string().enum(["service","product"]).default("service"),
    shoppingCartId:type.string(),
    unit :type.string(),
    price: type.number(),
    vendors: type.number(),
    image:type.string(),
    shortDescription:type.string(),
    whyThisService:[
        {
            seq:type.number(),
            title:type.string(),
            description:type.string(),
            videoUrl:type.string(),
            imageUrl:type.string()
        }
    ],
     process:[
        {
            seq:type.number(),
            title:type.string(),
            description:type.string(),
            stepsVideoUrl:type.string(),
            stepsImageUrl:type.string()
        }
    ],

    checklist:[
        {
            seq:type.number(),
            title:type.string(),
            description:type.string(),
            stepsVideoUrl:type.string()
        }
    ],

    stats:{
        bookingsCount:type.number(),
        favoriteCount:type.number(),
        reviews:{
            totalCount:type.number(),
            totalValue:type.number(),
            averageValue:type.number()
        }
    },
    healthWarnings:{
            seq:type.number(),
            title:type.string(),
            description:type.string(),
            stepsVideoUrl:type.string(),
            moreInfoUrl:type.string()
    },
    images:[
        {
            seq:type.number(),
            title:type.string(),
            description:type.string(),
            imageUrl:type.string()
        }
    ],
    videoLink:type.string().default(null),
    discountedPrice: type.number(),
    discountPercentage: type.number(),
    stock: type.number(),
    inHouseStock: type.number(),
    employeeStock: type.number(),
    description :type.string(),
    isSale: type.boolean().default(false),
    isPurchase: type.boolean().default(false),
    isFeatured: type.boolean(),
    uName: type.string(),
    rank :type.number(),
    createdOn: type.date().default(r.now),
    createdBy: type.string(),
    updatedBy: type.string(),
    updatedOn: type.date(),
    status: type.string().enum(["active", "inactive"]).required().default("active")
});

module.exports = Item;

Item.belongsTo(Portfolio,"portfolio","portfolioId","id");
Item.belongsTo(Org,"org","orgId","id");

Item.ensureIndex("createdOn");
Item.ensureIndex("price");
Item.ensureIndex("discountedPrice");



var Order = require(__dirname+'/order.js');
Item.hasMany(Order, "orders", "id", "itemId");

var Category = require(__dirname+'/category.js');
Item.belongsTo(Category,"category","categoryId","id");

var Testimony = require(__dirname+'/testimony.js');
Item.hasMany(Testimony,"testimony","id","serviceId");

Lead = require(__dirname+'/lead.js');
Item.hasMany(Lead,"lead","id","itemId");

ProductVariation = require(__dirname+'/productVariation.js');
Item.hasMany(ProductVariation,"productVariation","id","productId");

InventoryLog = require(__dirname+'/inventorylog.js');
Item.hasMany(InventoryLog,"inventorylog","id","productId");

ProductInventory = require(__dirname+'/productInventory.js');
Item.hasMany(ProductInventory,"productInventory","id","productId");

var Media = require(__dirname+'/media.js');
Item.hasMany(Media,"media","id","forId");


Item.pre('save', function(next) {
    Item.filter({name:this.name,portfolioId: this.portfolioId}).run().then(function(result) {
        if(result.length > 0){
            next(new Error("Item already exist"));
        }else{
            next();
        }
    });
});

