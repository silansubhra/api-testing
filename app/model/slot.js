// Import
var uuid = require('node-uuid'),
    thinky = require(__dirname+'/../util/thinky.js'),
    r = thinky.r,
    type = thinky.type,
    Org = require(__dirname+'/org.js'),
    Branch = require(__dirname+'/branch.js'),
    Portfolio = require(__dirname+'/portfolio.js')
    Item = require(__dirname+'/item.js')
;


var Slot = thinky.createModel('slot', {
    id: type.string(),
    portfolioId: type.string(),
    serviceId: type.string(),
    branchId :type.string(),
    orgId: type.string(),
    name: type.string(),
    fromTime :type.string(),
    toTime :type.string(),
    maxSlot: type.number(),
    hours :type.number(),
    description :type.string(),
    createdOn: type.date().default(r.now),
    createdBy: type.string(),
    updatedBy: type.string(),
    updatedOn: type.date(),
    timing: type.string(),
    status: type.string().enum(["active", "inactive"]).required().default("active")
});


module.exports = Slot;

Slot.belongsTo(Branch,"branch","branchId","id");
Slot.belongsTo(Org,"org","orgId","id");
Slot.belongsTo(Portfolio,"portfolio","portfolioId","id");
Slot.belongsTo(Item, "service", "serviceId", "id");


Slot.ensureIndex("createdOn");
Slot.ensureIndex("name");
