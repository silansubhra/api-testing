// Import
var uuid = require('node-uuid'),
    thinky = require(__dirname+'/../util/thinky.js'),
    r = thinky.r,
    type = thinky.type,
    Org = require(__dirname+'/org.js'),
    Branch = require(__dirname+'/branch.js'),
    Portfolio = require(__dirname+'/portfolio.js');


var Cronjob = thinky.createModel('cronjob', {
    id: type.string(),
    mobile: type.number(),
    scheduleTime: type.string(),
    message: type.string(),
    reqobj:type.string(),
    createdOn: type.date().default(r.now),
    createdBy: type.string(),
    updatedBy: type.string(),
    updatedOn: type.date(),
    status: type.string().enum(["active", "inactive"]).required().default("active")

});


module.exports = Cronjob;


