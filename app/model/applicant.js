// Import
var uuid = require('node-uuid'),
    thinky = require(__dirname+'/../util/thinky.js'),
    r = thinky.r,
    type = thinky.type,
    Portfolio = require(__dirname+'/portfolio.js');
    //Branch = require(__dirname+'/branch.js'),
    //Org = require(__dirname+'/org.js'),
    

var Applicant = thinky.createModel('applicant', {
    id: type.string(),
    portfolioId:type.string(),
    jobId:type.string(),
    name: type.string().required(),
    mobile:type.number().required(),
    email:type.string(),
    resumeUpload: type.string(),
    message: type.string(),
    customerId: type.string(),
    enableSms: type.boolean(), 
    enableEmail: type.boolean(),
    createdOn: type.date().default(r.now),
    createdBy: type.string(),
    updatedBy: type.string(),
    updatedOn: type.date(),
    status: type.string().enum(["active", "inactive"]).required().default("active")
});

module.exports = Applicant;

Applicant.ensureIndex("createdOn");
// unique key on portfolio id & mobile number

Applicant.belongsTo(Portfolio,"portfolio","portfolioId","id");
//Applicant.belongsTo(Branch,"branch","branchId","id");
//Applicant.belongsTo(Org,"org","orgId","id");
var JobOpening = require(__dirname+'/jobOpening.js');
Applicant.belongsTo(JobOpening,"jobopenings","jobId","id");