// Import
var uuid = require('node-uuid'),
    thinky = require(__dirname+'/../util/thinky.js'),
    r = thinky.r,
    type = thinky.type,
    Portfolio = require(__dirname+'/portfolio.js'),
    Org = require(__dirname+'/org.js');

var SalaryComponent = thinky.createModel('salaryComponent', {
    id: type.string(),
    portfolioId:type.string(),
    orgId:type.string(),
    name: type.string().required(),
    value:type.number(),
    rank: type.number(),
    calculationType:type.string().enum(["flat","formulae"]),
    payType: type.string().enum(["Deduction","Earning"]),
    effectiveDate: type.date(),
    mapTo: type.string(),
    createdOn: type.date().default(r.now),
    createdBy: type.string(),
    updatedBy: type.string(),
    updatedOn: type.date(),
    status: type.string().enum(["active", "inactive"]).required().default("active")
});

module.exports = SalaryComponent;

SalaryComponent.ensureIndex("createdOn");
SalaryComponent.ensureIndex("rank");

SalaryComponent.belongsTo(Portfolio,"portfolio","portfolioId","id");

SalaryComponent.belongsTo(Org,"org","orgId","id");

SalaryComponent.pre('save', function(next) {
    SalaryComponent.filter({name: this.name,portfolioId: this.portfolioId}).run().then(function(result) {
        if(result.length > 0){
            next(new Error("Salary Component already exist."));
        }else{
            next();
        }
    });
});


