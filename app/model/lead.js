// Import
var thinky = require(__dirname+'/../util/thinky.js'),
    r = thinky.r,
    type = thinky.type,
    Portfolio = require(__dirname+'/portfolio.js'),
    Branch = require(__dirname+'/branch.js'),
    Org = require(__dirname+'/org.js');


var Lead = thinky.createModel('lead', {
    id: type.string(),
    portfolioId: type.string(),
    branchId:type.string(),
    orgId:type.string(),
    customerId:type.string(),
    itemId:type.string(),
    billNo:type.string(),
    employeeId:type.string(),
    leadId: type.string(),
    mpId: type.string(),
    proLeadId: type.string(),
    price: type.number(),
    rank: type.number(),
    slots: [{
        slotId: type.string(),
        maxSlots:type.number(),
        slotFor: type.string().enum(["job","inspection"]).default("job")
    }],
    aggregatorId: type.string(),
    referenceNumber:type.string(),
    referralId:type.number(),
    referralTrackingId:type.string(), 
    couponId:type.string(), 
    employementType: type.string(),
    channel:type.string().enum(["OFFLINE","WEBSITE","MOBILEAPP","SMS","EMAIL","WHATSAPP"]).default("OFFLINE"),
    city:type.string(),
    dueDate:type.date(),
    leadSource:type.string().enum(["YET5","HOMETRIANGLE","BRO4U","URBANCLAP","HOUSEJOY","JUSTDAIL","SULEKHA","TRUNETO"]),
    leadStatus:type.string().enum(["NEW","INSPECTION PENDING","INSPECTION IN PROGRESS","QUOTE PENDING","QUOTE SEND","QUOTE APPROVED","ON HOLD","CANCELLED","CONVERTED","PAID","COMPLETED","TRANSFERRED"]).default("NEW"),
    address: type.string(),/*{
        street: type.string(),
        province: type.string(),
        city: type.string(),
        state: type.string(),
        country: type.string(),
        pin: type.number(),
        validated: type.boolean()
    },*/
    geoLocation :type.point(),
    referralType:type.string().enum(["Direct","Vendor","Customer","Lead Source"]).default("Direct"),
    description:type.string(),
    createdOn: type.date().default(r.now),
    createdBy: type.string(),
    updatedBy: type.string(),
    updatedOn: type.date(),
    status: type.string().enum(["active", "inactive"]).required().default("active")
});

module.exports = Lead;

Lead.belongsTo(Org,"org","orgId","id");

Lead.belongsTo(Branch,"branch","branchId","id");

Lead.belongsTo(Portfolio,"portfolio","portfolioId","id");

var Customer = require(__dirname+'/customer.js');
Lead.belongsTo(Customer,"customer","customerId","id");

var Employee = require(__dirname+'/employee.js');
Lead.belongsTo(Employee,"employee","employeeId","id");

var Coupon = require(__dirname+'/coupon.js');
Lead.belongsTo(Coupon,"coupon","couponId","id");

var Item = require(__dirname+'/item.js');
Lead.belongsTo(Item,"item","itemId","id");

LeadInventory = require(__dirname+'/leadInventory.js');
Lead.hasMany(LeadInventory,"leadInventory","id","leadId");

Lead.hasMany(Lead,"proLead","id","proLeadId");


InventoryLog = require(__dirname+'/inventorylog.js');
Lead.hasMany(InventoryLog,"inventorylog","id","productId");


Lead.ensureIndex("createdOn");
Lead.ensureIndex("dueDate");
Lead.ensureIndex("customerId");
Lead.ensureIndex("orgId");
Lead.ensureIndex("rank");


/*Lead.pre('save', function(next) {
    if(this.proLeadId != undefined && this.proLeadId !== null && this.proLeadId !== '' && this.proLeadId !== NaN){
       Lead.filter({proLeadId:this.mobile,portfolioId: this.portfolioId,status:'active'}).run().then(function(result) {
            if(result.length > 0){
                next(new Error("You have already assigned this lead to this Vendor"));
            }else{
                next();
            }
        }); 
   }else{
        next(); 
   }
});*/