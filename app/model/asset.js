// Import
var uuid = require('node-uuid'),
    thinky = require(__dirname+'/../util/thinky.js'),
    r = thinky.r,
    type = thinky.type,
    Org = require(__dirname+'/org.js'),
    User = require(__dirname+'/user.js'),
    Customer = require(__dirname+'/customer.js');

var Asset = thinky.createModel('asset', {
    id: type.string(),
    userId:type.string(),
    orgId:type.string(),
    assetId: type.string(),
    maintenanceId: type.string(),
    rank: type.number(),
    portfolioId:type.string(),
    tagId:type.string(),
    allocatedTo: type.string(),
    price: type.number(),
    description: type.string(),
    type: type.string(),
    astatus: type.string(),
    createdOn: type.date().default(r.now),
    createdBy: type.string(),
    updatedBy: type.string(),
    updatedBy: type.string(),
    status: type.string().enum(["active", "inactive"]).required().default("active")
});

module.exports = Asset;


Asset.belongsTo(Org,"org","orgId","id");


var Maintenance = require(__dirname+'/asset.js');
Asset.hasMany(Maintenance, "maintenance", "id", "assetId");

Asset.belongsTo(Customer,"customer","allocatedTo","id");


Asset.ensureIndex("createdOn");

Asset.ensureIndex("rank");


