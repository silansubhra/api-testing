// Import
var uuid = require('node-uuid'),
    thinky = require(__dirname+'/../util/thinky.js'),
    r = thinky.r,
    type = thinky.type,
    Org = require(__dirname+'/org.js');

var Branch = thinky.createModel('branch', {
    id: type.string(),
    orgId: type.string().required(),
    portfolioId: type.string(),
    mobile: type.number().integer().min(1000000000).max(9999999999).required(),
    name: type.string().required(),
    parentId: type.string().default(null),
    inaugurationDate: type.date(),
    inauguratedBy :type.string(),
    isDefault :type.boolean().required().default(false),
    contacts: [{
        type: type.string().enum(["Mobile", "Phone","Home", "Work","Home Fax","WhatsApp", "Work Fax","Custom"]).default("Mobile"),
        mobileCountryCode:type.string(),
        number: type.string(),
        validated: type.boolean()
    }],
    emails:[{
        type: type.string().enum(["Home", "Work","Other","Custom"]),
        id: type.string(),
        validated: type.boolean()
    }],
    geoLocation :type.point(),
    addresses:[{
        type: type.string().enum(["Home", "Work","Other","Custom"]),
        street: type.string(),
        province: type.string(),
        city: type.string(),
        state: type.string(),
        country: type.string(),
        pin: type.number(),
        validated: type.boolean()
    }],
    website: type.string(),
    socialAccount:[{
        type: type.string().enum(["Twitter", "Facebook","LinkedIn","Flicker","Myspace","Sina Weibo","Custom"]),
        url: type.string()
    }],
    instantMessager:[{
        type: type.string().enum(["AIM", "Facebook","Google Talk","MSN","Skype","Yahoo","Custom"]),
        url: type.string()
    }],
    description: type.string(),
    enableSms: type.boolean(),
    enableEmail: type.boolean(),
    createdOn: type.date().default(r.now),
    createdBy: type.string(),
    updatedBy: type.string(),
    updatedOn: type.date(),
    status: type.string().enum(["active", "inactive"]).required().default("active")
});

module.exports = Branch;

Branch.belongsTo(Org,"org","orgId","id");

var Portfolio = require(__dirname+'/portfolio.js');
Branch.belongsTo(Portfolio,"portfolio","portfolioId","id");

var Customer = require(__dirname+'/customer.js');
Branch.hasMany(Customer, "customers", "id", "branchId");

var Employee = require(__dirname+'/employee.js');
Branch.hasMany(Employee, "employees", "id", "branchId");



var Slot = require(__dirname+'/slot.js');
Branch.hasMany(Slot, "slots", "id", "branchId");



var Payment = require(__dirname+'/payment.js');
Branch.hasMany(Payment, "payments", "id", "branchId");

var Transaction = require(__dirname+'/transaction.js');
Branch.hasMany(Transaction, "transactions", "id", "branchId");

var Invoice = require(__dirname+'/invoice.js');
Branch.hasMany(Invoice, "invoices", "id", "branchId");


var Product = require(__dirname+'/product.js');
Branch.hasMany(Product, "products", "id", "branchId");

var Category = require(__dirname+'/category.js');
Branch.hasMany(Category, "categories", "id", "branchId");



Branch.ensureIndex("createdOn");
Branch.ensureIndex("name");

Branch.pre('save', function(next) {
    Branch.filter({name: this.name,orgId: this.orgId}).run().then(function(result) {
        if(result.length > 0){
            next(new Error("Branch name already exist."));
        }else{
            next();
        }
    });
});

//Branch.belongsTo(BranchType,"userType","idBranchType","id");

