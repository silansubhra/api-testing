// Import
var uuid = require('node-uuid')
    thinky = require(__dirname+'/../util/thinky.js'),
    r = thinky.r,
    type = thinky.type,
    Portfolio = require(__dirname+'/portfolio.js'),
    Org = require(__dirname+'/org.js'),
    User = require(__dirname+'/user.js');

var Feed = thinky.createModel('feed', {
    id: type.string(),
    orgId:type.string(),
    portfolioId:type.string(),
    userId : type.string(),
    ip: type.string(),
    loc: { },   
    url: type.string(),
    userAgent:type.string(),
    aDate: type.date(),
    createdOn: type.date().default(r.now),
    createdBy: type.string(),
    updatedBy: type.string(),
    updatedBy: type.string(),
    status: type.string().enum(["active", "inactive"]).required().default("active")
});

module.exports = Feed;

Feed.belongsTo(Org,"org","orgId","id");

Feed.belongsTo(Portfolio,"portfolio","portfolioId","id");
Feed.belongsTo(User,"user","userId","id");

Feed.ensureIndex("createdOn");
Feed.ensureIndex("name");
