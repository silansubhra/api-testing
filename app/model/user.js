// Import
var thinky = require(__dirname+'/../util/thinky.js'),
    r = thinky.r,
    type = thinky.type;


var User = thinky.createModel('user', {
    id: type.string(),
    mobile: type.number().integer().min(1000000000).max(9999999999).required(),
    password: type.string(),
    createdOn: type.date().default(r.now),
    createdBy: type.string(),
    updatedBy: type.string(),
    updatedOn: type.date(),
    email: type.string().email(),
    blogUserType: type.string().enum(["Editor","Author"]),
    userType: type.string().enum(["Manager","Lead"]),
    emailValidated: type.boolean(),
    verificationToken: type.string(),
    firstName:type.string(),
    lastName: type.string(),
    name: type.string(),
    dob:type.date(),
    addresses:[{
        type: type.string().enum(["Home", "Work","Other","Custom"]),
        street: type.string(),
        province: type.string(),
        city: type.string(),
        state: type.string(),
        country: type.string(),
        pin: type.number(),
        validated: type.boolean()
    }],
    website: type.string(),
    socialAccounts:[{
        type: type.string().enum(["Twitter", "Facebook","LinkedIn","Flicker","Myspace","Sina Weibo","Custom"]),
        url: type.string()
    }],
    instantMessagers:[{
        type: type.string().enum(["AIM", "Facebook","Google Talk","MSN","Skype","Yahoo","Custom"]),
        url: type.string()
    }],
    referralCode: type.string(),
    picture:type.string(),
    smsProvider: {
      authKey: type.string(),
      name: type.string(),
      senderId: type.string()
    },
    emailProvider: {
      auth: {
          pass: type.string(),
          user: type.string(),
          name: type.string()
        } ,
      host: type.string(),
      port: type.number(),
      resetPasswordToken: type.string(),
      resetPasswordExpires: type.date(),
      secure: type.boolean()
    },
    status: type.string().enum(["active", "inactive","approval pending","suspended"]).required().default("active")
});

module.exports = User;

var Org = require(__dirname+'/org.js');
User.belongsTo(Org,"org","orgId","id");

var Customer = require(__dirname+'/customer.js');
User.hasMany(Customer, "customers", "id", "userId");

var Employee = require(__dirname+'/employee.js');
User.hasMany(Employee, "employees", "id", "userId");

var Media = require(__dirname+'/media.js');
User.hasMany(Media, "media", "id", "userId");

var Feed = require(__dirname+'/feed.js');
User.hasMany(Feed, "feed", "id", "userId");

var ShoppingCart = require(__dirname+'/shoppingCart.js');
User.hasOne(ShoppingCart, "shoppingCart", "id", "userId");

var Support = require(__dirname+'/support.js');
User.hasMany(Support, "support", "id", "userId");

var Review = require(__dirname+'/reviews.js');
User.hasMany(Review, "review", "id", "createdBy");

User.ensureIndex("createdOn");
User.ensureIndex("mobile");
