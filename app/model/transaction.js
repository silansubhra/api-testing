// Import
var uuid = require('node-uuid'),
    thinky = require(__dirname+'/../util/thinky.js'),
    r = thinky.r,
    type = thinky.type,
    Branch = require(__dirname+'/branch.js'),
    Payment =require(__dirname+'/payment.js'),
    Portfolio = require(__dirname+'/portfolio.js');

var Transaction = thinky.createModel('transaction', {
    id: type.string(),
    portfolioId: type.string(),
    branchId:type.string(),
    paymentId: type.string(),
    transactionDate: type.date(),
    referanceNumber: type.string(),
    amount: type.number(),
    mode:type.string().enum(["cash","cheque","online","wallet"]).default("cash"),
    transactionStatus: type.string().enum(["Pending","Initiated","Paid","Completed"]).default("Pending"),
    description: type.string(),
    enableSms: type.boolean(),
    enableEmail: type.boolean(),
    createdOn: type.date().default(r.now),
    createdBy: type.string(),
    updatedBy: type.string(),
    updatedOn: type.date(),
    status: type.string().enum(["active", "inactive"]).required().default("active")
});


module.exports = Transaction;

Transaction.ensureIndex("createdOn");

Transaction.belongsTo(Branch,"branch","branchId","id");
Transaction.belongsTo(Payment,"payments","paymentId","id");
Transaction.belongsTo(Portfolio,"portfolio","portfolioId","id");

Transaction.pre('save', function(next) {
    Transaction.filter({paymentId: this.paymentId}).run().then(function(result) {
        if(result.length > 0){
            next(new Error(" Transaction already exists."));
        }else{
            next();
        }
    });
});
