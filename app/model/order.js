// Import
var uuid = require('node-uuid'),
    thinky = require(__dirname+'/../util/thinky.js'),
    r = thinky.r,
    type = thinky.type,
    Org = require(__dirname+'/org.js'),
    Branch = require(__dirname+'/branch.js'),
    Customer = require(__dirname+'/customer.js'),
    Portfolio = require(__dirname+'/portfolio.js');
    

var Order = thinky.createModel('order', {
    id: type.string(),
    portfolioId: type.string(),
    branchId :type.string(),
    customerId :type.string(),
    orgId: type.string(),
    itemId: type.string(),
    orderId: type.string(),
    rank: type.number(),
    email: type.string(),
    description: type.string(),
    billingAdress: {
        name: type.string(),
        pinCode: type.string(),
        mobile: type.string(),
        email: type.string(),
        address: type.string(),
        country: type.string(),
        landmark: type.string()
    },
    shippingAddress: {
        name: type.string(),
        pinCode: type.string(),
        mobile: type.string(),
        email: type.string(),
        address: type.string(),
        country: type.string(),
        landmark: type.string()
    },
    estimatedDate:type.date(),
    deliveryDate: type.date(),
    orderDate: type.date(),
    amountPaid: type.number(),
    amountDue: type.number(),
    netTotal: type.number(),
    discount: type.number().default(0),
    taxes: type.number().default(0),
    deliveryCharge: type.number(0), 
    grossTotal: type.number(),
    reviewed: type.boolean(),
    paymentCollected: type.boolean(),
    orderStatus: type.string().enum(["open","confirm","packing","shipping","dispatched","delivered","cancelled"]).default("open"),
    createdOn: type.date().default(r.now),
    createdBy: type.string(),
    updatedBy: type.string(),
    updatedOn: type.date(),
    status: type.string().enum(["active", "inactive"]).required().default("active")
});



module.exports = Order;

Order.belongsTo(Branch,"branch","branchId","id");
Order.belongsTo(Portfolio,"portfolio","portfolioId","id");
Order.belongsTo(Org,"org","orgId","id");
Order.belongsTo(Customer,"customer","customerId","id");

var Payment = require(__dirname+'/payment.js');
Order.hasMany(Payment, "payment", "id", "orderId");

var Invoice = require(__dirname+'/invoice.js');
Order.hasMany(Invoice, "invoice", "id", "orderId");

var Item = require(__dirname+'/item.js');
Order.belongsTo(Item, "item", "id", "itemId");

Order.ensureIndex("orderDate");
Order.ensureIndex("amountDue");
Order.ensureIndex("rank");


