// Import
var thinky = require(__dirname+'/../util/thinky.js'),
	r = thinky.r,
	type = thinky.type,
    Portfolio = require(__dirname+'/portfolio.js'),
    User = require(__dirname+'/user.js'),
    Org = require(__dirname+'/org.js');

var Subscription = thinky.createModel('subscription', {
	id: type.string(),
	portfolioId: type.string(),
	customerId: type.string(),
	itemId: type.string(),
	leadId: type.string(),
	subscribeId: type.string(),
	startDate: type.date(),
	nextDueDate: type.date(),
	location: type.string(),
	noOfService: type.string(),
	totalAmount: type.number(),
	amountPerService: type.number(),
	taxApplicable: type.number(),
	frequency: type.string().enum(["DAILY", "WEEKLY", "FORTNIGHTLY", "MONTHLY", "QUATERLY", "HALFYEALY", "YEARLY"]),
	taxClass: type.string().enum(["VAT", "SERVICETAX"]),
	paymentMethod: type.string().enum(["PREPAID", "POSTPAID"]),
	paymentMode: type.string().enum(["CASH", "CHEQUE", "NEFT", "WALLET"]),
	payable: type.string().enum(["ONETIME", "AFTEREACHSERVICE", "MONTHLY", "YEARLY"]),
	createdOn: type.date().default(r.now),
    createdBy: type.string(),
    updatedBy: type.string(),
    updatedOn: type.date(),
    status: type.string().enum(["active", "inactive"]).required().default("active")
});


module.exports = Subscription;


Subscription.belongsTo(Org,"org","orgId","id");

Subscription.belongsTo(Portfolio,"portfolio","portfolioId","id");


var Customer = require(__dirname+'/customer.js');
Subscription.belongsTo(Customer,"customer","customerId","id");

var Item = require(__dirname+'/item.js');
Subscription.belongsTo(Item,"item","itemId","id");


Subscription.ensureIndex("createdOn");
Subscription.ensureIndex("dueDate");
Subscription.ensureIndex("customerId");
Subscription.ensureIndex("orgId");
Subscription.ensureIndex("rank");
