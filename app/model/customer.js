// Import
var uuid = require('node-uuid'),
    thinky = require(__dirname+'/../util/thinky.js'),
    r = thinky.r,
    type = thinky.type,
    Portfolio = require(__dirname+'/portfolio.js'),
    User = require(__dirname+'/user.js'),
    Org = require(__dirname+'/org.js');

var Customer = thinky.createModel('customer', {
    id: type.string(),
    portfolioId: type.string(),
    mobile: type.number(),
    name: type.string(),
    email: type.string(),
    address: type.string(),
    // -- add counts
    stats: {
      leads: type.number(),  
      orders: type.number(),
      gmv: type.number()
    },
    //userId:type.string().required(),
    orgId:type.string(),
    type: [type.string().enum(["B2B-Org","Individual"])],
    description: type.string(),
    pan: type.string(),
    tan: type.string(),
    serviceTax: type.string(),
    enableSms: type.boolean(),
    enableEmail: type.boolean(),
    createdOn: type.date().default(r.now),
    createdBy: type.string(),
    updatedBy: type.string(),
    updatedOn: type.date(),
    status: type.string().enum(["active", "inactive"]).required().default("active")
});

module.exports = Customer;


Customer.belongsTo(Org,"org","orgId","id");
Customer.belongsTo(User,"user","userId","id");

Customer.belongsTo(Portfolio,"portfolio","portfolioId","id");


var Order = require(__dirname+'/order.js');
Customer.hasMany(Order, "orders", "id", "customerId");

var Contact = require(__dirname+'/contact.js');
Customer.hasMany(Contact, "contact", "id", "customerId");


Customer.ensureIndex("createdOn");
Customer.ensureIndex("name");
// Customer.ensureIndex("mobile");

Customer.pre('save', function(next) {
    if(this.mobile != undefined && this.mobile !== null && this.mobile !== '' && this.mobile !== NaN){
       Customer.filter({mobile:parseInt(this.mobile),portfolioId: this.portfolioId,status:'active'}).run().then(function(result) {
            if(result.length > 0){
                next(new Error("This customer already exists."));
            }else{
                next();
            }
        }); 
   }else{
        next(); 
   }
   
});

//Customer.belongsTo(CustomerType,"userType","idCustomerType","id");
