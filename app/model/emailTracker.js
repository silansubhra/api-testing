// Import
var thinky = require(__dirname+'/../util/thinky.js'),
    r = thinky.r,
    type = thinky.type;


var EmailTracker = thinky.createModel('emailTracker', {
    id: type.string(),
    action: type.string(),
    emailId: type.string(),
    createdOn: type.date().default(r.now),
    createdBy: type.string(),
    updatedBy: type.string(),
    updatedOn: type.date(),
    userId: type.string()
});

module.exports = EmailTracker;

EmailTracker.ensureIndex("createdOn");

var Email = require(__dirname+'/email.js');
EmailTracker.belongsTo(Email,"email","emailId","id");