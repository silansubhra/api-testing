// Import
var uuid = require('node-uuid'),
    thinky = require(__dirname+'/../util/thinky.js'),
    r = thinky.r,
    type = thinky.type,
    Portfolio = require(__dirname+'/portfolio.js'),
    Customer = require(__dirname+'/customer.js');

var Testimoni = thinky.createModel('testimoni', {
    id: type.string(),
    portfolioId:type.string(),
    name: type.string(),
    mobile:type.number(),
    message: type.string(),
    serviceId: type.string(),
    ratings: type.string(),
    createdOn: type.date().default(r.now),
    createdBy: type.string(),
    updatedBy: type.string(),
    updatedOn: type.date(),
    status: type.string().enum(["active", "inactive"]).required().default("inactive")
});

module.exports = Testimoni;

Testimoni.ensureIndex("createdOn");
var Item = require(__dirname+'/item.js');
Testimoni.belongsTo(Item, "items", "serviceId", "id");

Testimoni.belongsTo(Portfolio,"portfolio","portfolioId","id");