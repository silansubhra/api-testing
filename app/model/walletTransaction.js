var thinky = require(__dirname+'/../util/thinky.js');
var type = thinky.type;
var Portfolio = require(__dirname+'/portfolio.js');

var WalletTransaction = thinky.createModel("walletTransaction", {
    id: type.string(),
    userId: type.string(),
    portfolioId:type.string(),
    beforeBal: type.number(),
    afterBal: type.number(),
    withdrawl:type.number(),
    deposite: type.number(),
    comments: type.string(),
    mpId: type.string(),
    transactionFor: type.string(),
    transactionType: type.string().enum(["leadModel", "converstion"]),
    createdOn: type.date().default(r.now),
    createdBy: type.string(),
    updatedBy: type.string(),
    updatedOn: type.date(),
    status: type.string().enum(["active", "inactive"]).required().default("active")
});

module.exports = WalletTransaction;

WalletTransaction.ensureIndex("createdOn");


var User = require(__dirname+'/user.js');
WalletTransaction.belongsTo(User, "user", "userId", "id");


var Portfolio = require(__dirname+'/portfolio.js');
WalletTransaction.belongsTo(Portfolio, "portfolio", "portfolioId", "id");

