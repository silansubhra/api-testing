// Import
var uuid = require('node-uuid')
    thinky = require(__dirname+'/../util/thinky.js'),
    r = thinky.r,
    type = thinky.type,
    Portfolio = require(__dirname+'/portfolio.js'),
    Org = require(__dirname+'/org.js');

var InventoryLog = thinky.createModel('inventorylog', {
    id: type.string(),
    orgId:type.string(),
    portfolioId:type.string(),
    productId: type.string(),
    leadId: type.string(),
    employeeId: type.string(),
    comment: type.string(),
    quantity: type.string(),
    before: type.number(),
    after: type.number(),
    createdOn: type.date().default(r.now),
    createdBy: type.string(),
    updatedBy: type.string(),
    updatedBy: type.string(),
    status: type.string().enum(["active", "inactive"]).required().default("active")
});

module.exports = InventoryLog;

InventoryLog.belongsTo(Org,"org","orgId","id");

InventoryLog.belongsTo(Portfolio,"portfolio","portfolioId","id");

var Item = require(__dirname+'/item.js');
InventoryLog.belongsTo(Item, "product", "productId", "id");

var Lead = require(__dirname+'/lead.js');
InventoryLog.belongsTo(Lead, "lead", "leadId", "id");

var Employee = require(__dirname+'/employee.js');
InventoryLog.belongsTo(Employee, "employee", "employeeId", "id");

InventoryLog.ensureIndex("createdOn");
InventoryLog.ensureIndex("price");
