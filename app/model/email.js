// Import
var thinky = require(__dirname+'/../util/thinky.js'),
    r = thinky.r,
    type = thinky.type;


var Email = thinky.createModel('email', {
    id: type.string(),
    to: type.string(),
    toUserId: type.string(),
    from: type.string(),
    fromUserId: type.string(),
    subject: type.string(),
    text: type.string(),
    html: type.string(),
    campaign: type.string(),//Campaign name you wish to create.
    status: type.string(),
    createdOn: type.date().default(r.now),
    createdBy: type.string(),
    updatedBy: type.string(),
    updatedOn: type.date(),
    userId: type.string(),
    orgId:type.string(),
    portfolioId: type.string()
});

module.exports = Email;

Email.ensureIndex("createdOn");

var EmailTracker = require(__dirname+'/emailTracker.js');
Email.hasMany(EmailTracker, "trackers", "id", "emailId");