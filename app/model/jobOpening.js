// Import
var thinky = require(__dirname+'/../util/thinky.js'),
    r = thinky.r,
    type = thinky.type;
    Org = require(__dirname+'/org.js'); 


var JobOpening = thinky.createModel('jobopening', {
	id: type.string(),
	orgId: type.string(),
    portfolioId: type.string(),
	jobTitle: type.string(),
	shortDescription: type.string(),  
	noOfOpenings: type.number(),                
	reqExperience: type.string(),
	postedDate: type.date(),
	lastDate: type.date(),
	jobLocation: type.string(),
	jobDescription: type.string(),
	qualification: type.string(),
	jobType: type.string().enum(["Full-time","Part-time"]),
	shifTtype: type.string().enum(["Day","Night"]),
	salary: type.number(),
	createdOn: type.date().default(r.now),
    createdBy: type.string(),
    updatedBy: type.string(),
    updatedOn: type.date(),
    status: type.string().enum(["active", "inactive"]).required().default("active"),
	skillLevel : type.string()
});
 
module.exports = JobOpening;

var Portfolio = require(__dirname+'/portfolio.js');
JobOpening.belongsTo(Portfolio,"portfolio","portfolioId","id");
var Applicant = require(__dirname+'/applicant.js');
JobOpening.hasMany(Applicant, "applicants", "id", "jobId");