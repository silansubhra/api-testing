// Import
var uuid = require('node-uuid'),
    thinky = require(__dirname+'/../util/thinky.js'),
    r = thinky.r,
    type = thinky.type,
    Portfolio = require(__dirname+'/portfolio.js'),
    User = require(__dirname+'/user.js'),
    Org = require(__dirname+'/org.js');


var Employee = thinky.createModel('employee', {
    id: type.string(),
    portfolioId:type.string(),
    userId:type.string(),
    branchId: type.string(),
    orgId:type.string(),
    departmentId:type.string(),
    managerId:type.string(),
    empId:type.string(),
    rank: type.number(),
    cCode: type.string(),
    dob:type.date(),
    address:type.string(),
    gender: type.string().enum(["male","female"]).default("male"),
    emailId: type.string(),
    emergencyNo: type.number().integer().min(1000000000).max(9999999999),
    panNo: type.string(),
    role:type.string(),
    name:type.string(),
    startDate:type.date(),
    endDate:type.date() ,
    mobile:type.number().integer().min(1000000000).max(9999999999),
    description:type.string(),
    designationId: type.string(),
    bankAccountNo: type.string(),
    picture:type.string(),
    employmentType:type.string().enum(["PERMANENT","CONTRACT","C2H","PARTTIME","INTERN","FREELANCER"]).default("PERMANENT"),
    enableSms: type.boolean(),
    enableEmail: type.boolean(),
    createdOn: type.date().default(r.now),
    createdBy: type.string(),
    updatedBy: type.string(),
    updatedOn: type.date(),
    status: type.string().enum(["active", "inactive"]).required().default("active")
});

module.exports = Employee;

Employee.belongsTo(Portfolio,"portfolio","portfolioId","id");
Employee.belongsTo(Org,"org","orgId","id");
Employee.belongsTo(User,"user","userId","id");

var Department = require(__dirname+'/department.js');
Employee.belongsTo(Department,"department","departmentId","id");

var Designation = require(__dirname+'/designation.js');
Employee.belongsTo(Designation,"designation","designationId","id");


var Task = require(__dirname+'/task.js');
Employee.hasMany(Task, "tasks", "id", "employeeId");

var Support = require(__dirname+'/support.js');
Employee.hasMany(Support, "support", "id", "employeeId");

ProductInventory = require(__dirname+'/productInventory.js');
Employee.hasMany(ProductInventory,"productInventory","id","employeeId");

InventoryLog = require(__dirname+'/inventorylog.js');
Employee.hasMany(InventoryLog,"inventorylog","id","employeeId");



Employee.ensureIndex("createdOn");
Employee.ensureIndex("name");
Employee.ensureIndex("rank");

Employee.pre('save', function(next) {
    Employee.filter({mobile:parseInt(this.mobile),portfolioId: this.portfolioId}).run().then(function(result) {
        if(result.length > 0){
            next(new Error("This Employee already exists"));
        }else{
            next();
        }
    });
});

//Employee.belongsTo(EmployeeType,"userType","idEmployeeType","id");

