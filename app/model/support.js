// Import
var uuid = require('node-uuid'),
    thinky = require(__dirname+'/../util/thinky.js'),
    r = thinky.r,
    type = thinky.type,
    Portfolio = require(__dirname+'/portfolio.js'),
    Org = require(__dirname+'/org.js'),
    User = require(__dirname+'/user.js'),
    Customer = require(__dirname+'/customer.js'),
    Item = require(__dirname+'/item.js');

var Support = thinky.createModel('support', {
    id: type.string(),
    userId:type.string(),
    orgId:type.string(),
    portfolioId:type.string(),
    customerId:type.string(),
    itemId:type.string(),
    name:type.string(),
    mobile:type.number(),
    email:type.string(),
    dueDate:type.string(),
    forPort: type.string(),
    tokenId: type.string(),
    title: type.string(),
    description: type.string(),
    type: type.string().enum(["bug","enhanancement","task","proposal"]).default("bug"),
    priority: type.string().enum(["low","medium","high"]).default("low"),
    iStatus: type.string().enum(["open", "pending", "closed"]).required().default("open"),
    employeeId: type.string(),
    picture: type.string(),
    rank: type.number(),
    createdOn: type.date().default(r.now),
    createdBy: type.string(),
    updatedBy: type.string(),
    updatedBy: type.string(),
    status: type.string().enum(["active", "inactive"]).required().default("active")
});

module.exports = Support;


Support.belongsTo(Org,"org","orgId","id");
Support.belongsTo(User,"user","userId","id");
Support.belongsTo(Portfolio,"portfolio","portfolioId","id");


var Employee = require(__dirname+'/employee.js');
Support.belongsTo(Employee,"employee","employeeId","id");

Support.belongsTo(Customer,"customer","customerId","id");

Support.belongsTo(Item,"item","itemId","id");

Support.ensureIndex("createdOn");

Support.ensureIndex("rank");


