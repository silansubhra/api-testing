// Import
var uuid = require('node-uuid')
    thinky = require(__dirname+'/../util/thinky.js'),
    r = thinky.r,
    type = thinky.type,
    Portfolio = require(__dirname+'/portfolio.js'),
    Org = require(__dirname+'/org.js');

var LeadInventory = thinky.createModel('leadInventory', {
    id: type.string(),
    orgId:type.string(),
    portfolioId:type.string(),
    productId: type.string(),
    leadId: type.string(),
    employeeId: type.string(),
    createdOn: type.date().default(r.now),
    createdBy: type.string(),
    updatedBy: type.string(),
    updatedBy: type.string(),
    status: type.string().enum(["active", "inactive"]).required().default("active")
});

module.exports = LeadInventory;

LeadInventory.belongsTo(Org,"org","orgId","id");

LeadInventory.belongsTo(Portfolio,"portfolio","portfolioId","id");

var Item = require(__dirname+'/item.js');
LeadInventory.belongsTo(Item, "product", "productId", "id");

var Lead = require(__dirname+'/lead.js');
LeadInventory.belongsTo(Lead, "lead", "leadId", "id");

var Employee = require(__dirname+'/employee.js');
LeadInventory.belongsTo(Employee, "employee", "employeeId", "id");

LeadInventory.ensureIndex("createdOn");
LeadInventory.ensureIndex("price");
