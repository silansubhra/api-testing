// Import
var uuid = require('node-uuid'),
    thinky = require(__dirname+'/../util/thinky.js'),
    r = thinky.r,
    type = thinky.type,
    Portfolio = require(__dirname+'/portfolio.js'),
    Order =require(__dirname+'/order.js');
    

var Invoice = thinky.createModel('invoice', {
    id: type.string(),
    portfolioId: type.string(),
    logo: type.string(),
    orderId: type.string(),
    jobId: type.string(),
    cName: type.string(),
    rank: type.number(),
    /*addresses:[{
        type: type.string().enum(["Home", "Work","Other","Custom"]),
        street: type.string(),
        province: type.string(),
        city: type.string(),
        state: type.string(),
        country: type.string(),
        pin: type.number(),
        validated: type.boolean()
    }],
    billTo: type.string(),
    invoiceNo: {
        key: type.string(),
        value: type.string()
    },
    deliveryNote: {
        key: type.string(),
        value: type.string()
    },
    supplierRef: {
        key: type.string(),
        value: type.string()
    },
    iDate: {
        key:type.string(),
        value: type.date()
    },
    paymentMode:{
        key: type.string(),
        value: type.string()
    },
    otherRef:{
        key: type.string(),
        value: type.string()
    },
    orderNo: {
        key: type.string(),
        value: type.string()
    },
    dispatchedNo: {
        key: type.string(),
        value: type.string()
    },
    dispatchedThrough:{
        key: type.string(),
        value: type.string()
    },
    orderDate:{
        key: type.string(),
        oDate: type.date()
    },
    dispatchedDate: {
        key: type.string(),
        dDate: type.date()
    },
    destination:{
        key: type.string(),
        value: type.string()
    },
    itemDescription:{
        key: type.string(),
        value: type.string()
    },
    quantity:{
        key: type.string(),
        value: type.number()
    },
    rate: {
        key: type.string(),
        value: type.number()
    },
    amount: {
        key : type.string(),
        value: type.number()
    },
    subTotal:{
        key: type.string(),
        value: type.number()
    },
    salesTax:{
        key: type.string(),
        value: type.number()
    },
    total: {
        key: type.string(),
        value: type.number()
    },
    inWords: type.string(),
    cDetails: type.string(),
    serviceTaxNo: {
        key: type.string(),
        value: type.string()
    },
    panNo:{
        key: type.string(),
        value: type.string()
    },
    tinNo:{
        key: type.string(),
        value: type.string()
    },
    bankName:{
        key: type.string(),
        value: type.string()
    },
    accountNo:{
        key: type.string(),
        value: type.string()
    },
    branchIfsc:{
        key: type.string(),
        value: type.string()
    },
    description: type.string(),
    signature: {
        key:type.string(),
        value: type.string()
    },
    notes:{
        key: type.string(),
        value: type.string()
    },
    terms: {
        key: type.string(),
        value: type.string()
    },
    address: type.string(),*/
    createdOn: type.date().default(r.now),
    createdBy: type.string(),
    updatedBy: type.string(),
    updatedOn: type.date(),
    status: type.string().enum(["active", "inactive"]).required().default("active")
});


module.exports = Invoice;

Invoice.ensureIndex("createdOn");

Invoice.ensureIndex("rank");

Invoice.belongsTo(Portfolio,"portfolio","portfolioId","id");
Invoice.belongsTo(Order,"order","orderId","id");


/*Invoice.pre('save', function(next) {
    Invoice.filter({paymentId: this.paymentId}).run().then(function(result) {
        if(result.length > 0){
            next(new Error("invoice already exists."));
        }else{
            next();
        }
    });
});*/
