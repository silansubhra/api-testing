// Import
var uuid = require('node-uuid'),
    thinky = require(__dirname+'/../util/thinky.js'),
    r = thinky.r,
    type = thinky.type,
    Branch = require(__dirname+'/branch.js');

var Portfolio = require(__dirname+'/portfolio.js');

var Payment = thinky.createModel('payment', {
    id: type.string(),
    portfolioId: type.string(),
    branchId:type.string(),
    jobId: type.string(),
    orderId: type.string(),
    amount: type.number(),
    remarks: type.string(),
    paymentDate: type.date(),
    completionDueDate: type.date(),
    transactionCreated: type.boolean(),
    paymentStatus: type.string().enum(["Pending","Paid"]).default("Pending"),
    paymentMode: type.string().enum(["COD","Credit/Debit Card","Neft"]),
    description: type.string(),
    enableSms: type.boolean(),
    enableEmail: type.boolean(),
    createdOn: type.date().default(r.now),
    createdBy: type.string(),
    updatedBy: type.string(),
    updatedOn: type.date(),
    paymentStatus: type.string().enum(["active", "inactive"]).required().default("active")
});



module.exports = Payment;

Payment.ensureIndex("createdOn");

Payment.belongsTo(Branch,"branch","branchId","id");
Payment.belongsTo(Portfolio,"portfolio","portfolioId","id");

var Order =require(__dirname+'/order.js');
Payment.belongsTo(Order,"order","orderId","id");

var Invoice = require(__dirname+'/invoice.js');
Payment.hasOne(Invoice, "invoice", "id", "paymentId");

var Transaction = require(__dirname+'/transaction.js');
Payment.hasOne(Transaction, "transactions", "id", "paymentId");


