// Import
var uuid = require('node-uuid'),
    thinky = require(__dirname+'/../util/thinky.js'),
    r = thinky.r,
    type = thinky.type,
    Org = require(__dirname+'/org.js'),
    Portfolio = require(__dirname+'/portfolio.js');

var Category = thinky.createModel('category', {
    id: type.string(),
    portfolioId :type.string(),
    orgId: type.string(),
    name: type.string(),
    serviceID: type.string().default(null),
    uName :type.string(),
    parentId :type.string(),
    rank :type.number(),
    description :type.string(),
    mediaId: type.string(),
    coverImage: type.string(),
    createdOn: type.date().default(r.now),
    createdBy: type.string(),
    updatedBy: type.string(),
    updatedOn: type.date(),
    status: type.string().enum(["active", "inactive"]).required().default("active")
});

module.exports = Category;

Category.belongsTo(Portfolio,"portfolio","portfolioId","id");
Category.belongsTo(Org,"org","orgId","id");

var Item = require(__dirname+'/item.js');
Category.hasMany(Item, "items", "id", "categoryId");

var Media = require(__dirname+'/media.js');
Category.hasMany(Media, "medias", "id", "forId");

Category.ensureIndex("createdOn");
Category.ensureIndex("name");
Category.ensureIndex("rank");


Category.pre('save', function(next) {
    Category.filter({name:this.name,portfolioId: this.portfolioId}).run().then(function(result) {
        if(result.length > 0){
            next(new Error("Category  already exist"));
        }else{
            next();
        }
    });
});
