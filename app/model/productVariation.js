// Import
var uuid = require('node-uuid')
    thinky = require(__dirname+'/../util/thinky.js'),
    r = thinky.r,
    type = thinky.type,
    Portfolio = require(__dirname+'/portfolio.js'),
    Org = require(__dirname+'/org.js');

var ProductVariation = thinky.createModel('productVariation', {
    id: type.string(),
    orgId:type.string(),
    portfolioId:type.string(),
    productId: type.string(),
    unit: type.string(),
    price: type.number(),
    displayName: type.string(),
    quantity: type.number(),
    createdOn: type.date().default(r.now),
    createdBy: type.string(),
    updatedBy: type.string(),
    updatedBy: type.string(),
    status: type.string().enum(["active", "inactive"]).required().default("active")
});

module.exports = ProductVariation;

ProductVariation.belongsTo(Org,"org","orgId","id");

ProductVariation.belongsTo(Portfolio,"portfolio","portfolioId","id");

var Item = require(__dirname+'/item.js');
ProductVariation.belongsTo(Item, "product", "productId", "id");

ProductVariation.ensureIndex("createdOn");
ProductVariation.ensureIndex("price");
