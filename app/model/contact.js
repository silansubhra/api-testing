// Import
var uuid = require('node-uuid'),
    thinky = require(__dirname+'/../util/thinky.js'),
    r = thinky.r,
    type = thinky.type,
    Portfolio = require(__dirname+'/portfolio.js'),
    Branch = require(__dirname+'/branch.js'),
    Org = require(__dirname+'/org.js'),
    Customer = require(__dirname+'/customer.js');

var Contact = thinky.createModel('contact', {
    id: type.string(),
    portfolioId:type.string(),
    branchId:type.string(),
    orgId: type.string(),
    name: type.string().required(),
    mobile:type.number().required(),
    email:type.string(),
    address:type.string(),
    message: type.string(),
    customerId: type.string(),
    enableSms: type.boolean(),
    enableEmail: type.boolean(),
    createdOn: type.date().default(r.now),
    createdBy: type.string(),
    updatedBy: type.string(),
    updatedOn: type.date(),
    status: type.string().enum(["active", "inactive"]).required().default("active")
});

module.exports = Contact;

Contact.ensureIndex("createdOn");
// unique key on portfolio id & mobile number

Contact.belongsTo(Portfolio,"portfolio","portfolioId","id");
Contact.belongsTo(Branch,"branch","branchId","id");
Contact.belongsTo(Org,"org","orgId","id");
Contact.belongsTo(Customer,"customer","customerId","id");
