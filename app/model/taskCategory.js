// Import
var uuid = require('node-uuid'),
    thinky = require(__dirname+'/../util/thinky.js'),
    r = thinky.r,
    type = thinky.type,
    Org = require(__dirname+'/org.js'),
    Portfolio = require(__dirname+'/portfolio.js');

var TaskCategory = thinky.createModel('taskCategory', {
    id: type.string(),
    portfolioId :type.string(),
    orgId: type.string(),
    name: type.string(),
    description :type.string(),
    createdOn: type.date().default(r.now),
    createdBy: type.string(),
    updatedBy: type.string(),
    updatedOn: type.date(),
    status: type.string().enum(["active", "inactive"]).required().default("active")
});


module.exports = TaskCategory;

TaskCategory.belongsTo(Portfolio,"portfolio","portfolioId","id");
TaskCategory.belongsTo(Org,"org","orgId","id");

var Task = require(__dirname+'/task.js');
TaskCategory.hasMany(Task, "tasks", "id", "taskCategoryId");

TaskCategory.ensureIndex("createdOn");
TaskCategory.ensureIndex("name");

