// Import
var uuid = require('node-uuid'),
    thinky = require(__dirname+'/../util/thinky.js'),
    r = thinky.r,
    type = thinky.type,
    crypto = require('crypto'),
    Org = require(__dirname+'/org.js'),
    User = require(__dirname+'/user.js'),
    Portfolio =require(__dirname+'/portfolio.js');

var UserProfile = thinky.createModel('userProfile', {
    id: type.string(),
    orgId: type.string(),
    userId: type.string(),
    portfolioId: type.string(),
    tpKey:type.string(),
    apiKey:type.string(),
    roles:[type.string()],
    role:type.string(),
    mobile: type.number(),
    email: type.string(),
    password: type.string(),
    name: type.string(),
   // type: type.string().enum(["Org", "Employee","B2B-Org","Vendor","Customer", "B2B-Employee","B2B-B2B-Org","B2B-Vendor"]),
    createdOn: type.date().default(r.now),
    createdBy: type.string(),
    updatedBy: type.string(),
    updatedOn: type.date(),
    status: type.string().enum(["active", "inactive","approval pending","suspended"]).required().default("active")
});

module.exports = UserProfile;

UserProfile.belongsTo(Org,"org","orgId","id");
UserProfile.belongsTo(User,"user","userId","id");
UserProfile.belongsTo(Portfolio,"portfolio","portfolioId","id");

UserProfile.ensureIndex("createdOn");

UserProfile.pre('save', function(next) {
    UserProfile.filter({apiKey: this.apiKey,mobile: parseInt(this.mobile),email: this.email,role: this.role}).run().then(function(result) {
        if(result.length > 0){
            next(new Error("User already exist."));
        }else{
            next();
        }
    });
});

//UserProfile.belongsTo(UserProfileType,"userType","idUserProfileType","id");

