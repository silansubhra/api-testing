// Import
var uuid = require('node-uuid'),
    thinky = require(__dirname+'/../util/thinky.js'),
    r = thinky.r,
    type = thinky.type,
    Portfolio = require(__dirname+'/portfolio.js'),
    User = require(__dirname+'/user.js'),
    Org = require(__dirname+'/org.js');

var Vendor = thinky.createModel('vendor', {
    id: type.string(),
    portfolioId: type.string(),
    mobile: type.number(),
    name: type.string(),
    email: type.string(),
    address: type.string(),
    //userId:type.string().required(),
    orgId:type.string(),
   // type: [type.string().enum(["B2B-Org","Individual"])],
    description: type.string(),
    pan: type.string(),
    tan: type.string(),
    serviceTax: type.string(),
    enableSms: type.boolean(),
    enableEmail: type.boolean(),
    createdOn: type.date().default(r.now),
    createdBy: type.string(),
    updatedBy: type.string(),
    updatedOn: type.date(),
    status: type.string().enum(["active", "inactive"]).required().default("active")
});

module.exports = Vendor;


Vendor.belongsTo(Org,"org","orgId","id");
//Vendor.belongsTo(User,"user","userId","id");

Vendor.belongsTo(Portfolio,"portfolio","portfolioId","id");


Vendor.ensureIndex("createdOn");
Vendor.ensureIndex("name");

Vendor.pre('save', function(next) {
    Vendor.filter({mobile:parseInt(this.mobile),portfolioId: this.portfolioId}).run().then(function(result) {
        if(result.length > 0){
            next(new Error("This vendor already in  list."));
        }else{
            next();
        }
    });
});

//Vendor.belongsTo(VendorType,"userType","idVendorType","id");
