// Import
var uuid = require('node-uuid'),
    thinky = require(__dirname+'/../util/thinky.js'),
    r = thinky.r,
    type = thinky.type,
    Portfolio = require(__dirname+'/portfolio.js'),
    Org = require(__dirname+'/org.js'),
    Item = require(__dirname+'/item.js');

var Coupon = thinky.createModel('coupon', {
    id: type.string(),
    portfolioId:type.string(),
    orgId:type.string(),
    couponCode: type.string(),
    discountPercentage:type.number(),
    discountAmount: type.number(),
    maxDiscount:type.number(),
    minOrder: type.number(),
    validFrom:type.date(),
    validTill:type.date(),
    termsAndCondition:type.string(),
    discountType: type.string().enum(['flat','percentage']),
    couponType: type.string().enum(['Cashback','Discount']),
    description: type.string(),
    items: [],
    validPerUser: type.number(), 
    enableSms: type.boolean(),
    enableEmail: type.boolean(),
    createdOn: type.date().default(r.now),
    createdBy: type.string(),
    updatedBy: type.string(),
    updatedOn: type.date(),
    status: type.string().enum(["active", "inactive"]).required().default("active")
});

module.exports = Coupon;

Coupon.ensureIndex("createdOn");
Coupon.ensureIndex("couponCode");

Coupon.belongsTo(Portfolio,"portfolio","portfolioId","id");

Coupon.belongsTo(Org,"org","orgId","id");

Coupon.belongsTo(Item,"item","itemId","id");

/*var Lead = require(__dirname+'/lead.js');
Coupon.hasMany(Lead, "leads", "id", "couponId");*/


Coupon.pre('save', function(next) {
    Coupon.filter({couponCode: this.couponCode,portfolioId: this.portfolioId,status:'active'}).run().then(function(result) {
        if(result.length > 0){
            next(new Error("Coupon code already exist."));
        }else{
            next();
        }
    });
});


