// Import
var uuid = require('node-uuid'),
    thinky = require(__dirname+'/../util/thinky.js'),
    r = thinky.r,
    type = thinky.type,
    Portfolio = require(__dirname+'/portfolio.js');

var Mport = thinky.createModel('mPort', {
    id: type.string(),
    portfolioId:type.string(),
    mpId:type.string(),
    createdOn: type.date().default(r.now),
    createdBy: type.string(),
    updatedBy: type.string(),
    updatedOn: type.date(),
    status: type.string().enum(["active", "inactive"]).required().default("active")
});

module.exports = Mport;

Mport.ensureIndex("createdOn");
// unique key on portfolio id & mobile number

Mport.belongsTo(Portfolio,"portfolio","portfolioId","id");


