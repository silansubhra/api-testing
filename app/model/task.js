// Import
var uuid = require('node-uuid'),
    thinky = require(__dirname+'/../util/thinky.js'),
    r = thinky.r,
    type = thinky.type,
    Org = require(__dirname+'/org.js'),
    Portfolio = require(__dirname+'/portfolio.js');

var Task = thinky.createModel('task', {
    id: type.string(),
    portfolioId :type.string(),
    orgId: type.string(),
    name: type.string(),
    description :type.string(),
    employeeId :type.string(),
    tStatus :type.string().enum(["To-do","In-Progress","Completed","Cancelled"]).default("To-do"),
    startDate: type.date(),
    dueDate: type.date(),
    urgency: type.string().enum(["high","low"]),
    relatedContact: type.string(),
    relatedDetails: type.string(),
    taskCategoryId: type.string(),
    url: type.string(),
    importance: type.string().enum(["high","low"]).default("high"),
    createdOn: type.date().default(r.now),
    createdBy: type.string(),
    updatedBy: type.string(),
    updatedOn: type.date(),
    status: type.string().enum(["active", "inactive"]).required().default("active")
});


module.exports = Task;

Task.belongsTo(Portfolio,"portfolio","portfolioId","id");

Task.belongsTo(Org,"org","orgId","id");

var Employee = require(__dirname+'/employee.js');
Task.belongsTo(Employee,"employee","employeeId","id");

var TaskCategory = require(__dirname+'/taskCategory.js');
Task.belongsTo(TaskCategory,"category","taskCategoryId","id");

Task.ensureIndex("createdOn");
Task.ensureIndex("startDate");
Task.ensureIndex("name");

