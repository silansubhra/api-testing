// Import
var uuid = require('node-uuid'),
    thinky = require(__dirname+'/../util/thinky.js'),
    r = thinky.r,
    type = thinky.type,
    Org = require(__dirname+'/org.js'),
    Portfolio = require(__dirname+'/portfolio.js');

var BlogCategory = thinky.createModel('blogCategory', {
    id: type.string(),
    portfolioId :type.string(),
    orgId: type.string(),
    name: type.string(),
    displayName :type.string(),
    parentId :type.string(),
    description :type.string(),
    createdOn: type.date().default(r.now),
    createdBy: type.string(),
    updatedBy: type.string(),
    updatedOn: type.date(),
    status: type.string().enum(["active", "inactive"]).required().default("active")
});


module.exports = BlogCategory;

BlogCategory.belongsTo(Portfolio,"portfolio","portfolioId","id");
BlogCategory.belongsTo(Org,"org","orgId","id");

var Blog = require(__dirname+'/blog.js');
BlogCategory.hasMany(Blog, "blogs", "id", "categoryId");

BlogCategory.ensureIndex("createdOn");
BlogCategory.ensureIndex("name");

