   // Import
var uuid = require('node-uuid'),
    thinky = require(__dirname+'/../util/thinky.js'),
    r = thinky.r,
    type = thinky.type,
    Portfolio = require(__dirname+'/portfolio.js'),
    User = require(__dirname+'/user.js'),
    Org = require(__dirname+'/org.js');

var Blog = thinky.createModel('blog', {
    id: type.string(),
    orgId:type.string(),
    portfolioId:type.string(),
    mobile: type.number(),
    title: type.string(),
    displayTitle: type.string(),
    content: type.string(),
    approvedBy: type.string(),
    approvedOn: type.date(),
    summary: type.string(),
    categoryId: type.string(),
    pId: type.number(),
    //tags: type.string(),
    isBreakingNews: type.boolean(),
    approvedStatus: type.string().enum(["Pending","Approved","Disapproved"]) .default('Pending'),
    enableSms: type.boolean(),
    enableEmail: type.boolean(),
    createdOn: type.date(),
    createdBy: type.string(),
    updatedBy: type.string(),
    updatedBy: type.string(),
    status: type.string().enum(["active", "inactive"]).required().default("active")
});

module.exports = Blog;


Blog.belongsTo(Org,"org","orgId","id");
Blog.belongsTo(Portfolio,"portfolio","portfolioId","id");
var BlogCategory = require(__dirname+'/blogCategory.js');
Blog.belongsTo(BlogCategory,"category","categoryId","id");

var User =require(__dirname+'/user.js');
Blog.belongsTo(User,"user","createdBy","id");

// var Tag = require(__dirname+'/tag.js');
// Blog.hasAndBelongsToMany(Tag, "tags", "id", "id")

Blog.ensureIndex("createdOn");
Blog.ensureIndex("title");
Blog.ensureIndex("pId");

