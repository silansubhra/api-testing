// Import
var uuid = require('node-uuid'),
    thinky = require(__dirname+'/../util/thinky.js'),
    r = thinky.r,
    type = thinky.type,
    Branch = require(__dirname+'/branch.js'),
    Lead = require(__dirname+'/lead.js'),
    Org = require(__dirname+'/org.js'),
    Portfolio = require(__dirname+'/portfolio.js');

var Quotation = thinky.createModel('quotation', {
    id: type.string(),
    portfolioId: type.string(),
    branchId:type.string(),
    leadId:type.string(),
    quoteId: type.string(),
    date: type.date(),
    validUntil: type.date(),
    orgId:type.string(),
    customerId:type.string(),
    rank:type.number(),
    cName:type.string(),
    /*addresses:[{
        type: type.string().enum(["Home", "Work","Other","Custom"]),
        street: type.string(),
        province: type.string(),
        city: type.string(),
        state: type.string(),
        country: type.string(),
        pin: type.number(),
        validated: type.boolean()
    }],
    lineItem:[{
        quoteLineId: type.string(),
        quantity: type.number(),
        total: type.number()
    }],
    billTo: type.string(),
    invoiceNo: {
        key: type.string(),
        value: type.string()
    },
    deliveryNote: {
        key: type.string(),
        value: type.string()
    },
    supplierRef: {
        key: type.string(),
        value: type.string()
    },
    iDate: {
        key:type.string(),
        value: type.date()
    },
    paymentMode:{
        key: type.string(),
        value: type.string()
    },
    otherRef:{
        key: type.string(),
        value: type.string()
    },
    orderNo: {
        key: type.string(),
        value: type.string()
    },
    dispatchedNo: {
        key: type.string(),
        value: type.string()
    },
    dispatchedThrough:{
        key: type.string(),
        value: type.string()
    },
    orderDate:{
        key: type.string(),
        oDate: type.date()
    },
    dispatchedDate: {
        key: type.string(),
        dDate: type.date()
    },
    destination:{
        key: type.string(),
        value: type.string()
    },
    itemDescription:{
        key: type.string(),
        value: type.string()
    },
    quantity:{
        key: type.string(),
        value: type.number()
    },
    rate: {
        key: type.string(),
        value: type.number()
    },
    amount: {
        key : type.string(),
        value: type.number()
    },
    subTotal:{
        key: type.string(),
        value: type.number()
    },
    salesTax:{
        key: type.string(),
        value: type.number()
    },
    total: {
        key: type.string(),
        value: type.number()
    },
    inWords: type.string(),
    cDetails:type.string(),
    serviceTaxNo: {
        key: type.string(),
        value: type.string()
    },
    panNo:{
        key: type.string(),
        value: type.string()
    },
    tinNo:{
        key: type.string(),
        value: type.string()
    },
    bankName:{
        key: type.string(),
        value: type.string()
    },
    accountNo:{
        key: type.string(),
        value: type.string()
    },
    branchIfsc:{
        key: type.string(),
        value: type.string()
    },
    description: type.string(),
    signature: {
        key:type.string(),
        value: type.string()
    },
    notes:{
        key: type.string(),
        value: type.string()
    },
    terms: {
        key: type.string(),
        value: type.string()
    },*/
    address: type.string(),
    createdOn: type.date().default(r.now),
    createdBy: type.string(),
    updatedBy: type.string(),
    updatedOn: type.date(),
    status: type.string().enum(["active", "inactive"]).required().default("active")
});


module.exports = Quotation;


Quotation.belongsTo(Org,"org","orgId","id");

Quotation.belongsTo(Lead,"lead","leadId","id");

Quotation.belongsTo(Branch,"branch","branchId","id");

Quotation.belongsTo(Portfolio,"portfolio","portfolioId","id");

//var QuotationLineItem = require(__dirname+'/quotelineitem.js');

//Quotation.hasMany(QuotationLineItem, "quotelineitem", "id", "quoteLineId");

Quotation.ensureIndex("createdOn");

Quotation.ensureIndex("name");
Quotation.ensureIndex("rank");

/*Quotation.pre('save', function(next) {
    Quotation.filter({id: this.id,leadId: this.leadId}).run().then(function(result) {
        if(result.length > 0){
            next(new Error("This quotation already in list."));
        }else{
            next();
        }
    });
});
*/
//Quotation.belongsTo(QuotationType,"userType","idQuotationType","id");

var Customer = require(__dirname+'/customer.js');
Quotation.belongsTo(Customer,"customer","customerId","id");
