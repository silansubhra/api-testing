// Import
var uuid = require('node-uuid'),
    thinky = require(__dirname+'/../util/thinky.js'),
    r = thinky.r,
    type = thinky.type,
    Org = require(__dirname+'/org.js'),
    Portfolio = require(__dirname+'/portfolio.js');


var Designation = thinky.createModel('designation', {
    id: type.string(),
    orgId:type.string().required(),
    portfolioId: type.string(),
    name: type.string().required(),
    description: type.string(),
    enableSms: type.boolean(),
    enableEmail: type.boolean(),
    createdOn: type.date().default(r.now),
    createdBy: type.string(),
    updatedBy: type.string(),
    updatedOn: type.date(),
    status: type.string().enum(["active", "inactive"]).required().default("active")
});

module.exports = Designation;

Designation.belongsTo(Org,"org","orgId","id");

Designation.belongsTo(Portfolio,"portfolio","portfolioId","id");

var Employee = require(__dirname+'/employee.js');
Designation.hasMany(Employee, "employees", "id", "designationId");

Designation.ensureIndex("createdOn");
Designation.ensureIndex("name");

Designation.pre('save', function(next) {
    Designation.filter({name: this.name,orgId: this.orgId}).run().then(function(result) {
        if(result.length > 0){
            next(new Error("designation already exist."));
        }else{
            next();
        }
    });
});


