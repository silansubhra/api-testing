var env 		= require(__dirname+'/env'),
    config 		= require(__dirname+'/config/'+ env.name),
    express     = require('express'),
    app         = express(),
    bodyParser  = require('body-parser'),
    morgan      = require('morgan'),
    jwt    		= require('jsonwebtoken'),
    apiRoutes 	= express.Router(),
    fileUpload = require('express-fileupload'),
    redis = require('redis'),
    redis_cli = redis.createClient(config.redis),
    compress = require('compression'),
    log = require('./app/middleware/logger'),
    errorhandler = require('./app/middleware/errorHandler'),
    accesscontrol = require('./app/middleware/accessController');
    //var cron = require('./app/service/cronJob');

var cors = require('cors')


var userService = require(__dirname+'/app/service/userservice'),
    userProfileService = require(__dirname+'/app/service/userProfileservice'),
    orgService = require(__dirname+'/app/service/orgservice'),
    branchService = require(__dirname+'/app/service/branchservice'),
    regService = require(__dirname+'/app/service/registrationservice'),
    smsService = require(__dirname+'/app/service/smsservice'),
    veriformmService = require(__dirname+'/app/service/veriformmservice'),
    emailService = require(__dirname+'/app/service/emailservice'),
    departmentService = require(__dirname+'/app/service/departmentservice'),
    assetService = require(__dirname+'/app/service/assetservice'),
    maintenanceService = require(__dirname+'/app/service/maintenanceservice'),
    customerService = require(__dirname+'/app/service/customerservice'),
    vendorService = require(__dirname+'/app/service/vendorservice'),
    employeeService = require(__dirname+'/app/service/employeeservice'),
    couponService = require(__dirname+'/app/service/couponservice'),
    leadService = require(__dirname+'/app/service/leadservice'),
    quotationService = require(__dirname+'/app/service/quotationservice'),
    serviceService = require(__dirname+'/app/service/serviceservice'),
    productvariationService = require(__dirname+'/app/service/productVariationservice'),
    paymentService = require(__dirname+'/app/service/paymentservice'),
    invoiceService = require(__dirname+'/app/service/invoiceservice'),
    contactService = require(__dirname+'/app/service/contactservice'),
    orderService = require(__dirname+'/app/service/orderservice'),
    dashboardService = require(__dirname+'/app/service/dashboardService'),
    trunetodashboard = require(__dirname+'/app/service/trunetodashboard'),
    portfolioService = require(__dirname+'/app/service/portfolioservice'),
    categoryService = require(__dirname+'/app/service/categoryservice'),
    taskcategoryService = require(__dirname+'/app/service/taskCategoryservice'),
    productService = require(__dirname+'/app/service/productservice'),
    blogCategoryService = require(__dirname+'/app/service/blogCategoryservice'),
    blogService = require(__dirname+'/app/service/blogservice'),
    wishlistService = require(__dirname+'/app/service/wishlistservice'),
    shoppingCartService = require(__dirname+'/app/service/shoppingCartservice'),
    shoppingCartItemService = require(__dirname+'/app/service/shoppingCartItemservice'),
    blogSubscriptionService = require(__dirname+'/app/service/blogSubscriptionservice'),
    modeService = require(__dirname+'/app/service/modeservice'),
    visitorService = require(__dirname+'/app/service/visitorservice'),
    designationService = require(__dirname+'/app/service/designationservice'),
    itemService = require(__dirname+'/app/service/itemservice'),
    taskService = require(__dirname+'/app/service/taskservice'),
    quotelineService = require(__dirname+'/app/service/quotationlineitemservice'),
    supportService = require(__dirname+'/app/service/supportservice'),
    slotService = require(__dirname+'/app/service/slotservice'),
    homecarosalService = require(__dirname+'/app/service/homecarosalservice'),
    //loggerService = require(__dirname+'/app/service/loggerservice'),
    UserDashboardService = require(__dirname+'/app/service/userDashboardservice'),
    feedService = require(__dirname+'/app/service/feedservice'),
    mailConfigService = require(__dirname+'/app/service/mailConfigservice'),
    applicantsservice= require(__dirname+'/app/service/applicantsservice'),
    jobopeningservice = require(__dirname+'/app/service/jobopeningservice'), 
    subscriptionService = require(__dirname+'/app/service/subscriptionService'),
    preferenceService = require(__dirname+'/app/service/preferenceservice'), 
    templeteService = require(__dirname+'/app/service/templateservice'), 
    productInventoryService = require(__dirname+'/app/service/productInventoryservice'),
    leadInventoryService = require(__dirname+'/app/service/leadInventoryservice'),
    instamojoPaymentService = require(__dirname+'/app/service/instamojoPaymentservice');
    testimonyService = require(__dirname+'/app/service/testimonyService'),
    walletTransactionService = require(__dirname+'/app/service/wallet-transactionservice'),
    testemailService = require(__dirname+'/app/service/testemail'),
    jsonToCsvService = require(__dirname+'/app/service/JsonTocsv'),
    cronJobService = require(__dirname+'/app/service/cronJob'),
    mailReader = require(__dirname+'/app/service/mailread');




var port = process.env.PORT || config.port || 8088;
app.set('superSecret', config.secret);

//Enable GZIP       
app.use(compress());

//app.use(log.log);

// use body parser so we can get info from POST and/or URL parameters
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

// use morgan to log requests to the console
app.use(morgan('dev'));


//file upload
app.use(fileUpload());
apiRoutes.use(function(req, res, next) {
	var token = req.body.token || req.query.token || req.headers['x-access-token'];
    var apiKey = req.body.apikey || req.query.apikey || req.headers['x-api-key'];
    console.log("apiKey=="+apiKey);
    console.log("apiKey=="+token);
    if (token) {
        redis_cli.get(token, function(err, reply) {
            if (reply && reply != null) {
                req.decoded = reply;
                next();
            } else {
                console.log('doesn\'t exist');
                return (errorhandler.inValid(res));
            }
        });
    }else if(apiKey){
        //validate the API and get the portfolio object from that key
        req.apikey = apiKey;
        next();

    }else{
        return (errorhandler.unAuthorised(res));
    }

});

app.use(log.log);

app.use(cors());


app.use('/api', apiRoutes);
//app.use(accesscontrol.access);

// routes
// basic route
app.get('/', function(req, res) {
    res.send('API is up and running!! at http://localhost:' + port + '/api');
});
//market place 

app.route('/mp/signUp').post(userService.vSignUp);

app.route('/mp/login').post(userService.vLogin);
app.route('/mp/dd').get(userService.getd);

app.route('/login').post(userService.login);
app.route('/signUp').post(userService.signUp);
app.route('/register').post(regService.register);
app.route('/api/checkuser').get(userService.checkUser);
app.route('/api/forgot-password').post(userService.forgot);
app.route('/api/reset').post(userService.reset);
app.route('/api/change-password').post(userService.changePassword);
//app.route('/sms-delivery-report').get(smsService.smsDeliveryReport);
app.route('/sms-delivery-report').post(smsService.smsDeliveryReport);
app.route('/verify/:token').get(userService.verifyEmail);

// EMAIL API ROUTES
app.route('/trk/em/:id/:action').get(emailService.trackEmail);
app.route('/lead/:key').post(leadService.addExternalLead);
app.route('/lead/:key').get(leadService.addExternalLead);
app.route('/contactUs/:key').post(contactService.addExternalContact);
app.route('/contactUs/:key').get(contactService.addExternalContact);

//SMS service
//app.route('/api/bulksms').post(smsService.bulksms);

app.route('/api/bulksms').post(veriformmService.bulksms);
app.route('/api/sms-balance').get(veriformmService.smsBalance);

    
//DASHBOARD service
app.route('/api/dashboard/orders').get(dashboardService.getDashboardData);
app.route('/api/dashboard/leads').get(dashboardService.getDashboardData);
app.route('/api/dashboard/blogs').get(dashboardService.getDashboardData);
app.route('/api/mp/dashboard').get(trunetodashboard.getDashboardData);


//Create payment
app.route('/api/pay').post(instamojoPaymentService.payment);
app.route('/api/recharge').post(portfolioService.rechargeWallet);
app.route('/api/update-wallet/:id').post(portfolioService.updateWallet);

app.route('/api/wallets').get(walletTransactionService.listWallets);
app.route('/api/wallets/:id').get(walletTransactionService.listWalletsByVendor);
app.route('/api/recharge/:id').put(portfolioService.walletRecharge);
app.route('/api/refund/:id').put(portfolioService.walletRefund);
app.route('/api/deduct/:id').put(portfolioService.walletDeduct);


app.route('/api/recharge-sms').post(walletTransactionService.smsRecharge);

// USER API ROUTES
app.route('/api/logout').get(userService.logout);
app.route('/api/user').post(userService.addUser);
app.route('/api/user/:id').put(userService.updateUser);
app.route('/api/user/:id').get(userService.getUser);
app.route('/api/user/:id').delete(userService.deleteUser);
app.route('/api/users').get(userService.listUsers);

// USER PROFILE API ROUTES
app.route('/api/userProfile').put(userProfileService.updateUserProfile);
app.route('/api/userProfile').get(userProfileService.getUserProfile);
app.route('/api/wallet').get(userProfileService.listWalletTransaction);
app.route('/api/vendors-list').get(userProfileService.listUserProfileActive);
app.route('/api/userProfile/:id').put(userProfileService.updateUserProfile);



// USER API ROUTES
app.route('/api/user/:id/avtar').post(userService.uploadAvtar);

// ORG API ROUTES
app.route('/api/org').post(orgService.addOrg);
app.route('/api/org/:id').put(orgService.updateOrg);
app.route('/api/orgs/:uname').put(orgService.updateOrgs);
app.route('/api/org/:id').get(orgService.getOrg);
app.route('/api/org/:id').delete(orgService.deleteOrg);
app.route('/api/orgs').get(orgService.listOrgs);

// BRANCH API ROUTES
app.route('/api/branch').post(branchService.addBranch);
app.route('/api/branch').put(branchService.updateBranch);
app.route('/api/branch/:id').get(branchService.getBranch);
app.route('/api/branch/:id').delete(branchService.deleteBranch);
app.route('/api/branches').get(branchService.listBranches);

// DEPARTMENT API ROUTES
app.route('/api/department').post(departmentService.addDepartment);
app.route('/api/department').put(departmentService.updateDepartment);
app.route('/api/department/:id').get(departmentService.getDepartment);
app.route('/api/department/:id').delete(departmentService.deleteDepartment);
app.route('/api/departments').get(departmentService.listDepartments);
app.route('/api/department').delete(departmentService.bulkDeleteDepartment);

//COUPON
app.route('/api/coupon').post(couponService.addCoupon);
app.route('/api/coupon/:id').put(couponService.updateCoupon);
app.route('/api/coupon/:id').get(couponService.getCoupon);
app.route('/api/coupon/:id').delete(couponService.deleteCoupon);
app.route('/api/coupons').get(couponService.listCoupons);
app.route('/api/applyCoupon/:key').get(couponService.applyCoupon);

// Applicant API ROUTES 
//app.route('/api/contact').post(applicantsservice.addContact);
app.route('/api/applicant/:key').post(applicantsservice.addExternalApplicant);
app.route('/api/applicant').put(applicantsservice.updateApplicant);
app.route('/api/applicant/:id').get(applicantsservice.getApplicant);
app.route('/api/applicant/:id').delete(applicantsservice.deleteApplicant);
app.route('/api/applicant').get(applicantsservice.listApplicant);

// JobOpening API ROUTES
app.route('/api/jobopening').post(jobopeningservice.addJobOpenings);
app.route('/api/jobopening/:id').put(jobopeningservice.updateJobOpenings);
app.route('/api/jobopening/:id').get(jobopeningservice.getJobOpenings);
app.route('/api/jobopening/:id').delete(jobopeningservice.deleteJobOpenings); 
app.route('/api/jobopenings').get(jobopeningservice.listJobOpenings);


// CUSTOMER API ROUTES
app.route('/api/customer').post(customerService.addCustomer);
app.route('/api/customer').put(customerService.updateCustomer);
app.route('/api/customer/:id').get(customerService.getCustomer);
app.route('/api/customer/:id').delete(customerService.deleteCustomer);
app.route('/api/customers').get(customerService.listCustomers);
app.route('/api/customer').delete(customerService.bulkDeleteCustomer);

// VENDOR API ROUTES
app.route('/api/vendor').post(vendorService.addVendor);
app.route('/api/vendor').put(vendorService.updateVendor);
app.route('/api/vendor/:id').get(vendorService.getVendor);
app.route('/api/vendor/:id').delete(vendorService.deleteVendor);
app.route('/api/vendors').get(vendorService.listVendors);
app.route('/api/vendor').delete(vendorService.bulkDeleteVendor);


// EMPLOYEE API ROUTES
app.route('/api/employee').post(employeeService.addEmployee);
app.route('/api/employee').put(employeeService.updateEmployee);
app.route('/api/employee/:id').get(employeeService.getEmployee);
app.route('/api/employee/:id').delete(employeeService.deleteEmployee);
app.route('/api/employees').get(employeeService.listEmployees);
app.route('/api/employee').delete(employeeService.bulkDeleteEmployee);

// LEAD API ROUTES
app.route('/api/lead').post(leadService.addLead);
app.route('/api/lead/:id').put(leadService.updateLead);
app.route('/api/lead/:id').get(leadService.getLead);
app.route('/api/lead/:id').delete(leadService.deleteLead);
app.route('/api/leads').get(leadService.listLeads);
app.route('/api/vendors/leads').get(leadService.listLeadsByVendor);
app.route('/api/lead').delete(leadService.bulkDeleteLead);
app.route('/api/lead/bulk').post(leadService.bulkUpload);
app.route('/api/lead/assign').post(leadService.assignLead);
app.route('/api/lead/:id/pay').get(leadService.sendPaymentLink);
app.route('/api/lead-converted').get(leadService.convertedLeads);


app.route('/api/send-sms-customer/:id').post(leadService.sendConfirmation);

// QUOTATION API ROUTES
app.route('/api/quotation').post(quotationService.addQuotation);
app.route('/api/quotation').put(quotationService.updateQuotation);
app.route('/api/quotation/:id').get(quotationService.getQuotation);
app.route('/api/quotation/:id').delete(quotationService.deleteQuotation);
app.route('/api/quotations').get(quotationService.listQuotations);

app.route('/api/quotation/sendemail').post(quotationService.addEmailQuotation);


//  SERVICE API ROUTES
app.route('/api/service').post(serviceService.addService);
app.route('/api/service').put(serviceService.updateService);
app.route('/api/service/:id').get(serviceService.getService);
app.route('/api/service/:id').delete(serviceService.deleteService);
app.route('/api/services').get(serviceService.listServices);
app.route('/api/search-services').get(serviceService.searchServices);

//  PRODUCT VARIATION API ROUTES
app.route('/api/product/:id/variation').post(productvariationService.addProductVariation);
app.route('/api/product/:id/variation').put(productvariationService.updateProductVariation);
app.route('/api/product/:id/variation/:id').get(productvariationService.getProductVariation);
app.route('/api/product/variation/:id').delete(productvariationService.deleteProductVariation);
app.route('/api/product/:id/variations').get(productvariationService.listProductVariations);

//  JOB PAYMENT API ROUTES
app.route('/api/payment').post(paymentService.addPayment);
app.route('/api/payment/:id').post(paymentService.updatePayment);
app.route('/api/payment/:id').get(paymentService.getPayment);
app.route('/api/payment/:id').delete(paymentService.deletePayment);
app.route('/api/payments').get(paymentService.listPayments);

//  JOB INVOICE API ROUTES
app.route('/api/invoice').post(invoiceService.addInvoice);

app.route('/api/invoice').put(invoiceService.updateInvoice);
app.route('/api/invoice/:id').get(invoiceService.getInvoice);
app.route('/api/invoice/:id').delete(invoiceService.deleteInvoice);
app.route('/api/invoices').get(invoiceService.listInvoices);

app.route('/api/quotation-get').get(quotationService.getQuotationNo);

app.route('/api/invoice-get').get(invoiceService.getInvoiceNo);


app.route('/api/invoice/sendemail').post(invoiceService.addEmailInvoice);



// CONTACT API ROUTES
//app.route('/api/contact').post(contactService.addContact);
app.route('/api/contact').put(contactService.updateContact);
app.route('/api/contact/:id').get(contactService.getContact);
app.route('/api/contact/:id').delete(contactService.deleteContact);
app.route('/api/contacts').get(contactService.listContacts);


// ORDER API ROUTES
app.route('/api/order').post(orderService.addOrder);
app.route('/api/order/:id').put(orderService.updateOrder);
app.route('/api/order/:id').get(orderService.getOrder);
app.route('/api/order/:id').delete(orderService.deleteOrder);
app.route('/api/orders').get(orderService.listOrders);
app.route('/api/orders/bulk').post(orderService.bulkUpload);
app.route('/api/order').delete(orderService.bulkDeleteOrder);

// PORTFOLIO API ROUTES
app.route('/api/portfolio').post(portfolioService.addPortfolio);
app.route('/api/portfolio/:id').put(portfolioService.updatePortfolio);
app.route('/api/portfolio/:id').get(portfolioService.getPortfolio);
app.route('/api/portfolio/:id').delete(portfolioService.deletePortfolio);
app.route('/api/portfolios').get(portfolioService.listPortfolios);
app.route('/api/mp/portfolios/:id').get(portfolioService.listservicePortfolio);

app.route('/api/upload/adhar/:uname').put(portfolioService.uploadAdhar);
app.route('/api/upload/addressProof/:uname').put(portfolioService.uploadAddress);
app.route('/api/upload/pan/:uname').put(portfolioService.uploadPan);
app.route('/api/upload/serviceTax/:uname').put(portfolioService.uploadServiceTax);

app.route('/api/upload/letter-head/:uname').put(portfolioService.uploadLetterHead);
app.route('/api/upload/letter-footer/:uname').put(portfolioService.uploadLetterHeadFooter);


// CATEGORY API ROUTES
app.route('/api/category').post(categoryService.addCategory);
app.route('/api/category/:id').put(categoryService.updateCategory);
app.route('/api/category/:id').get(categoryService.getCategory);
app.route('/api/category/items/:uName').get(categoryService.getCategoryByUname);
app.route('/api/category/:id').delete(categoryService.deleteCategory);
app.route('/api/categories').get(categoryService.listCategories);


app.route('/api/testimonies').get(testimonyService.listTestimonies);
app.route('/api/testimony/:id').get(testimonyService.getTestimony);
app.route('/api/testimony/:id').put(testimonyService.updateTestimonies);
app.route('/api/testimony').post(testimonyService.addTestimony);
app.route('/api/testimony/:id').delete(testimonyService.deleteTestimony);


//TASK CATEGORY API ROUTES
app.route('/api/task/category').post(taskcategoryService.addTaskCategory);
app.route('/api/task/category').put(taskcategoryService.updateTaskCategory);
app.route('/api/task/category/:id').get(taskcategoryService.getTaskCategory);
app.route('/api/task/category/:id').delete(taskcategoryService.deleteTaskCategory);
app.route('/api/task/categories').get(taskcategoryService.listTaskCategories);


// PRODUCT API ROUTES
app.route('/api/product').post(productService.addProduct);
app.route('/api/product/:id').put(productService.updateProduct);
app.route('/api/product/:id').get(productService.getProduct);
app.route('/api/product/:id').delete(productService.deleteProduct);
app.route('/api/products').get(productService.listProducts);
app.route('/api/products/:id').get(productService.listProducts);
app.route('/api/product/:id/upload-photos').post(productService.uploadPhotos);
app.route('/api/new/products').get(productService.listNewProducts);

//app.route('/api/search-products').get(productService.searchProducts);


// BLOG API ROUTES
app.route('/api/blog').post(blogService.addBlog);
app.route('/api/blog/:id').put(blogService.updateBlog);
app.route('/api/blog/:uName').get(blogService.getBlog);
app.route('/api/blog/:id').delete(blogService.deleteBlog);
app.route('/api/blogs').get(blogService.listBlogs);
app.route('/api/breaking-news').get(blogService.listBreakingBlogs);
app.route('/api/blogs/:id').get(blogService.listBlogs);


// BLOG CATEGORY API ROUTES
app.route('/api/blog-category').post(blogCategoryService.addBlogCategory);
app.route('/api/blog-category').put(blogCategoryService.updateBlogCategory);
app.route('/api/blog-category/:id').get(blogCategoryService.getBlogCategory);
app.route('/api/blog-category/:id').delete(blogCategoryService.deleteBlogCategory);
app.route('/api/blog-categories').get(blogCategoryService.listBlogCategories);


// WISHLIST API ROUTES
app.route('/api/wishlist').post(wishlistService.addWishlist);
app.route('/api/wishlist').put(wishlistService.updateWishlist);
app.route('/api/wishlist/:id').get(wishlistService.getWishlist);
app.route('/api/wishlist/:id').delete(wishlistService.deleteWishlist);
app.route('/api/wishlists').get(wishlistService.listWishlists);


// SHOPPING CART API ROUTES
app.route('/api/shoppingCart').post(shoppingCartService.addShoppingCart);
app.route('/api/shoppingCart').put(shoppingCartService.updateShoppingCart);
app.route('/api/shoppingCart/:id').get(shoppingCartService.getShoppingCart);
app.route('/api/shoppingCart/:id').delete(shoppingCartService.deleteShoppingCart);
app.route('/api/shoppingCarts').get(shoppingCartService.listShoppingCarts);

// SHOPPING CART API ROUTES
app.route('/api/shoppingCartItem').post(shoppingCartItemService.addShoppingCartItem);
app.route('/api/shoppingCartItem').put(shoppingCartItemService.updateShoppingCartItem);
app.route('/api/shoppingCartItem/:id').get(shoppingCartItemService.getShoppingCartItem);
app.route('/api/shoppingCartItem/:id').delete(shoppingCartItemService.deleteShoppingCartItem);
app.route('/api/shoppingCartItems').get(shoppingCartItemService.listShoppingCartItems);

//  SLOT API ROUTES
app.route('/api/:id/slot').post(slotService.addSlot);
app.route('/api/:id/slot').put(slotService.updateSlot);
app.route('/api/:id/slot/:id').get(slotService.getSlot);
app.route('/api/:id/slot/:id').delete(slotService.deleteSlot);
app.route('/api/slots').get(slotService.listSlots);


// SUPPORT TICKET ROUTES
app.route('/api/support/ticket').post(supportService.addSupport);
app.route('/api/support/ticket/:id').put(supportService.updateSupport);
app.route('/api/support/ticket/:id').get(supportService.getSupport);
app.route('/api/support/ticket/:id').delete(supportService.deleteSupport);
app.route('/api/support/tickets').get(supportService.listSupports);

// ASSETS ROUTES
app.route('/api/asset').post(assetService.addAsset);
app.route('/api/asset/:id').put(assetService.updateAsset);
app.route('/api/asset/:id').get(assetService.getAsset);
app.route('/api/asset/:id').delete(assetService.deleteAsset);
app.route('/api/assets').get(assetService.listAssets);

// MAINTENANCE ROUTES
app.route('/api/maintenance').post(maintenanceService.addMaintenance);
app.route('/api/maintenance/:id').put(maintenanceService.updateMaintenance);
app.route('/api/maintenance/:id').get(maintenanceService.getMaintenance);
app.route('/api/maintenance/:id').delete(maintenanceService.deleteMaintenance);
app.route('/api/maintenances').get(maintenanceService.listMaintenances);

// AMC SUBSCRIPTION ROUTES
app.route('/api/subscription').post(subscriptionService.addSubscription);
app.route('/api/subscription/:id').put(subscriptionService.updateSubscription);
app.route('/api/subscription/:id').get(subscriptionService.getSubscription);
app.route('/api/subscription/:id').delete(subscriptionService.deleteSubscription);
app.route('/api/subscriptions').get(subscriptionService.listSubscriptions);
app.route('/api/paidsubscription/:id').put(subscriptionService.updatePaidStatus);



// BLOG SUBSCRIPTION API ROUTES
app.route('/subscribe/:key').get(blogSubscriptionService.addBlogSubscription);
/*app.route('/api/subscribe').put(blogSubscriptionService.updateBlogSubscription);
app.route('/api/subscribe/:id').get(blogSubscriptionService.getBlogSubscription);*/
app.route('/api/subscribe/:id').delete(blogSubscriptionService.deleteBlogSubscription);
app.route('/api/subscribes').get(blogSubscriptionService.listBlogSubscriptions);
app.route('/blog-confirmation/:id').get(blogSubscriptionService.blogConfirmation);


// MODE API ROUTES
app.route('/api/mode').post(modeService.addMode);
app.route('/api/mode').put(modeService.updateMode);
app.route('/api/mode/:id').get(modeService.getMode);
app.route('/api/mode/:id').delete(modeService.deleteMode);
app.route('/api/modes').get(modeService.listModes);


// VISITOR API ROUTES
app.route('/api/visitor').post(visitorService.addVisitor);
app.route('/api/visitors/csv').get(visitorService.listcsvVisitors);

app.route('/api/visitor/:id').put(visitorService.updateVisitor);
app.route('/api/visitor/:id').get(visitorService.getVisitor);
app.route('/api/visitor/:id').delete(visitorService.deleteVisitor);
app.route('/api/visitors').get(visitorService.listVisitors);
app.route('/api/visitor').delete(visitorService.bulkDeleteVisitor);
app.route('/api/visitor/sendsms/:id').get(visitorService.sendSmsVisitor);

// DESIGNATION API ROUTES
app.route('/api/designation').post(designationService.addDesignation);
app.route('/api/designation').put(designationService.updateDesignation);
app.route('/api/designation/:id').get(designationService.getDesignation);
app.route('/api/designation/:id').delete(designationService.deleteDesignation);
app.route('/api/designations').get(designationService.listDesignations);
app.route('/api/designation').delete(designationService.bulkDeleteDesignation);

// TASK API ROUTES
app.route('/api/task').post(taskService.addTask);
app.route('/api/task/:id').put(taskService.updateTask);
app.route('/api/task/:id').get(taskService.getTask);
app.route('/api/task/:id').delete(taskService.deleteTask);
app.route('/api/tasks').get(taskService.listTasks);
app.route('/api/tasks/today').get(taskService.todayTask);


// QUOTE LINE ITEM API ROUTES
app.route('/api/qouteline').post(quotelineService.addQuotationLineItem);
app.route('/api/qouteline').put(quotelineService.updateQuotationLineItem);
app.route('/api/qouteline/:id').get(quotelineService.getQuotationLineItem);
app.route('/api/qouteline/:id').delete(quotelineService.deleteQuotationLineItem);
app.route('/api/qoutelines').get(quotelineService.listQuotationLineItems);

// SUPPORT API ROUTES
app.route('/api/support/issuse').post(supportService.addSupport);
app.route('/api/support/issuse').put(supportService.updateSupport);
app.route('/api/support/issuse/:id').get(supportService.getSupport);
app.route('/api/support/issuse/:id').delete(supportService.deleteSupport);
app.route('/api/support/issuses').get(supportService.listSupports);

// FEED API ROUTES
app.route('/api/feed').post(feedService.addFeed);
app.route('/api/feed').put(feedService.updateFeed);
app.route('/api/feed/:id').get(feedService.getFeed);
app.route('/api/feed/:id').delete(feedService.deleteFeed);
app.route('/api/feeds').get(feedService.listFeeds);

// FEED API ROUTES
app.route('/api/preference').post(preferenceService.addPreference);
app.route('/api/preference').put(preferenceService.updatePreference);
app.route('/api/preference/:id').get(preferenceService.getPreference);
app.route('/api/preference/:id').delete(preferenceService.deletePreference);
app.route('/api/preferences').get(preferenceService.listPreferences);

// FEED API ROUTES
app.route('/api/template').post(templeteService.addTemplate);
app.route('/api/template').put(templeteService.updateTemplate);
app.route('/api/template/:id').get(templeteService.getTemplate);
app.route('/api/template/:id').delete(templeteService.deleteTemplate);
app.route('/api/templates').get(templeteService.listTemplates);


// MAIL CONFIG API ROUTES
app.route('/api/mail').post(mailConfigService.addMailConfig);
app.route('/api/mail').put(mailConfigService.updateMailConfig);
app.route('/api/mail/:id').get(mailConfigService.getMailConfig);
app.route('/api/mail/:id').delete(mailConfigService.deleteMailConfig);
app.route('/api/mails').get(mailConfigService.listMailConfigs);



//  PRODUCT INVENTORY API ROUTES
app.route('/api/product/:id/inventory').post(productInventoryService.addProductInventory);
app.route('/api/product/:id/inventory').put(productInventoryService.updateProductInventory);
app.route('/api/product/:id/inventory/:id').get(productInventoryService.getProductInventory);
app.route('/api/product/:id/inventory/:id').delete(productInventoryService.deleteProductInventory);
app.route('/api/product/:id/inventories').get(productInventoryService.listProductInventories);

//  LEAD INVENTORY API ROUTES
app.route('/api/lead/:id/inventory').post(leadInventoryService.addLeadInventory);
app.route('/api/lead/:id/inventory').put(leadInventoryService.updateLeadInventory);
app.route('/api/lead/:id/inventory/:id').get(leadInventoryService.getLeadInventory);
app.route('/api/lead/:id/inventory/:id').delete(leadInventoryService.deleteLeadInventory);
app.route('/api/lead/:id/inventories').get(leadInventoryService.listLeadInventories);



// HOME CAROSAL API ROUTES
app.route('/api/home/carosal').post(homecarosalService.addHomeCarosal);
app.route('/api/home/carosal/:id').put(homecarosalService.updateHomeCarosal);
app.route('/api/home/carosal/:id').get(homecarosalService.getHomeCarosal);
app.route('/api/home/carosal/:id').delete(homecarosalService.deleteHomeCarosal);
app.route('/api/home/carosals').get(homecarosalService.listHomeCarosals);
app.route('/api/home/carosal/:id').delete(homecarosalService.bulkDeleteHomeCarosal);

// ITEMS
app.route('/api/item').post(itemService.addItem);
app.route('/api/item/:id').put(itemService.updateItem);
app.route('/api/item/:id').get(itemService.getItem);
app.route('/api/item/:id').delete(itemService.deleteItem);
app.route('/api/items').get(itemService.listItems);
app.route('/api/items/:uName').get(itemService.getItemByUname);
app.route('/api/getitems/:portId').get(itemService.listItemsByPort);
app.route('/api/dashboard').get(UserDashboardService.getDashboardData);
app.route('/api/vendors/items').get(itemService.listItemsByVendor);
app.route('/api/item/:uname').post(itemService.addItemByVendor);

app.route('/api/items-truneto').get(userProfileService.trunetoItems);

app.route('/api/jsonToCsv').get(jsonToCsvService.csvData);
app.route('/api/send-attachment').get(testemailService.sendmail);
app.route('/api/leads/csv').get(leadService.listcsvLeads);


app.route('/api/portfolio/roles/:uname').get(userProfileService.getUserProfileByVendor);

app.use(errorhandler.notFound);
app.use(errorhandler.error);
app.use(errorhandler.unAuthorised);

//cronJobService.setCronJob();
//mailReader.readMail();


//cronJobService.sendWeeklyReport();
//cronJobService.sendMonthlyReport();


// start the server
app.listen(port);
console.log('API server is up and running');
